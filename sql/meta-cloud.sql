/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80034
 Source Host           : localhost:3306
 Source Schema         : sop-cloud

 Target Server Type    : MySQL
 Target Server Version : 80034
 File Encoding         : 65001

 Date: 28/04/2024 15:09:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_api_info
-- ----------------------------
DROP TABLE IF EXISTS `auth_api_info`;
CREATE TABLE `auth_api_info`  (
  `id` bigint NOT NULL COMMENT 'id',
  `api_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '接口名称',
  `api_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '接口地址',
  `api_perms` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '接口权限字符',
  `api_mod` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '所属模块（字典:sys_operate_module）',
  `api_status` int NULL DEFAULT 1 COMMENT '接口状态（1.启用 2.停用）',
  `order_num` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '认证API接口基本信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_api_info
-- ----------------------------

-- ----------------------------
-- Table structure for auth_app_api_rela
-- ----------------------------
DROP TABLE IF EXISTS `auth_app_api_rela`;
CREATE TABLE `auth_app_api_rela`  (
  `id` bigint NOT NULL COMMENT 'id',
  `app_id` bigint NULL DEFAULT NULL COMMENT '应用id',
  `api_id` bigint NULL DEFAULT NULL COMMENT 'API接口id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '应用API接口关联关系' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_app_api_rela
-- ----------------------------

-- ----------------------------
-- Table structure for auth_application
-- ----------------------------
DROP TABLE IF EXISTS `auth_application`;
CREATE TABLE `auth_application`  (
  `id` bigint NOT NULL COMMENT 'id',
  `app_id` bigint NULL DEFAULT NULL COMMENT '应用id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '应用名称',
  `app_secret` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '应用密钥',
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '主体（单位）',
  `app_status` int NULL DEFAULT 1 COMMENT '应用状态：1.启用 2.停用',
  `limit_login_num` int NULL DEFAULT -1 COMMENT '限制登录次数',
  `already_login_num` int NULL DEFAULT 0 COMMENT '已登录次数',
  `access_time_limit` datetime NULL DEFAULT NULL COMMENT '访问时间限制（天）',
  `pub_key` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '应用公钥',
  `private_key` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '应用私钥',
  `ip_whites` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'ip白名单',
  `callback_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '回调地址',
  `out_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '登出url',
  `app_type` int NULL DEFAULT NULL COMMENT '应用类型 1API 2统一认证',
  `link_person` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系人',
  `link_phone` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系人电话',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新者',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `app_desc` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '应用描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '认证中心应用表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_application
-- ----------------------------
INSERT INTO `auth_application` VALUES (1701489965808693250, 9932989594718268, 'sec', '$2a$10$HpFz0u8JpFzbvYsD0y5dBeYSd1vbmXPIP6HWKPEjR/8UCTIHSEMBW', '云智信安', 1, -1, -1, '2023-09-22 00:00:00', 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCpnfMljy2bspMoQzCtdXnNf8FA8QvWEfHQo7258QovasKYQOrMnw4/I+LisKJgQx3cgPuKyR/V5m5j3jQzWgDzwDjL6RcY32Hv7dZcynUQiZRPpgrZ8cIVbqfws2LfcXf2GEz6kgtEgubnoqvFd7AvZ9rLzPBnAJpyofaFy5VWHQIDAQAB', 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKmd8yWPLZuykyhDMK11ec1/wUDxC9YR8dCjvbnxCi9qwphA6syfDj8j4uKwomBDHdyA+4rJH9XmbmPeNDNaAPPAOMvpFxjfYe/t1lzKdRCJlE+mCtnxwhVup/CzYt9xd/YYTPqSC0SC5ueiq8V3sC9n2svM8GcAmnKh9oXLlVYdAgMBAAECgYAZ9plKOcctstFHQ7evCZEuNVzP08Xvy/njqbjIPl8EgJdOZnt6fGtaG2Rmtkp4qTVoNB8s8JnsrFPf+hqr9J458bBmh3JXNL+K8EJwOY+39y0Fk6aGFFbqR0hnFkrTI6yjmjuCSwqXuBjCHMhBtsoG8qYz7DMDM1QD8AB+fnuV0QJBANfVWrKhXYmrXhZ6nfQ+4SW0t2ycF933JUIM8Px7mSR1+o6XId74dYYtZkZTanekzq1cKV0upbsRKTyqmTMHBk0CQQDJLsWZ/LrKTzTpnS0v7NE/NzoKy02eWsKXoJ9tJ+PxvqmuxhhUbS8JKmvkUEdf+hUrLsB81kS2ECXQvvhhQRcRAkEAus871Wi9oS7mHOa8WG8kSJKGQEdKkSAc63JwEtiX0GsdFtXQx0Cd2CjrYOtKN4w6JuqiErkRUTylSJEQUI4/8QJAfI74yN4wyBFCMWGfGI0HAJyw0v4MkH6g/D/2dR69gm8Jn6wGZD81X/dj1XfeuQ2dDZ9kjeYbpJ1azGNwZMZoAQJAUEu2Qnkp9UJw0e9Q/Fm6HnTLGDC3v+eDQZJezqXAGUIc0iQWiEynpIL+QYFjUt51H5KgkrusHPU1lMXc0qCDDA==', '', '', '', 1, '', '', 'admin', '2023-09-19 16:32:35', 'admin', '2023-09-19 16:32:36', '1223');
INSERT INTO `auth_application` VALUES (1702513988526354434, 9535484462773979, '运维部', '$2a$10$z27tAQJhRRRklDeAsqoNn.IKyb.b0l/SOGOL8ft5kZNt8zEXxUtWC', '运维部', 1, -1, -1, NULL, 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDZHVRXnB7i4CfN7CykrgFIBl+3NvEW+YpD/N9+Zzu4RsNYQkxiEcCoxP+HbY87qrJmYSNy7xpa4NanbfB4VXagS4KlkwXU/j4SbUK2kfYohJxxDcSUGLCP0K3wdeoDqvzanfbW/rRT9qhL0OWMTwNyEM1gqwLc/fzxF57cSlH+awIDAQAB', 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANkdVFecHuLgJ83sLKSuAUgGX7c28Rb5ikP8335nO7hGw1hCTGIRwKjE/4dtjzuqsmZhI3LvGlrg1qdt8HhVdqBLgqWTBdT+PhJtQraR9iiEnHENxJQYsI/QrfB16gOq/Nqd9tb+tFP2qEvQ5YxPA3IQzWCrAtz9/PEXntxKUf5rAgMBAAECgYABRa4C+ZcUMhDuAaXrrt3CTFu/btPQ+RYrjBacUSsdXpGBs+b1+uBRDFzihYXPG632YqxjfKuyXQUeq7NxGBiuhVc1OXEObRdfCoE1heCrJUzSmBqBME+sq4U8xDlVPH5KJ8MZZpnSAaNQSZpfKakoLTTcVOx9sB8x/HL9jYy+dQJBAOvgULIv+NrJeUOU7mfBgCkgkV1TcMyRXSkIHuYt4V6HQoDXis2vVhF+7pmOlJUhC+5LAXTFvkxuIyHD76aFOocCQQDroz9tBstssEusO/W4sEYJ6YkyR7ExOWa5kT6ITiUFO5QEz+Y0GrJ7Q/p6SAsrF02kFdsT9EpqJL8wGc8zNmH9AkB4BSyqblR+5xNn+ukQLFqNEsBEr4du3+x6S2KFlwTc95A0RPigzj3KKz83LWCXPTh36Px1sYSyGTyRGsTfg10TAkEA4kK5t2XL1UyfDP3yH2pbhk8Rpw8CDMMSaWMXCWkH0TN5GPnMEQIFmakXjW06FCFnMwStfUaYYb1xO628sG8qvQJBAJLZN8WrzujmIPfDhn3U9uBvmzsbaNh4LHAkaeRTDZmkzax3/gjSbqyGlpZXzK7Rg1wl/wcjklwb4sJEFGgh3iI=', '', '', '', 1, '', '', 'admin', '2023-09-19 16:32:38', 'admin', '2023-09-19 16:32:41', '');

-- ----------------------------
-- Table structure for chart_board
-- ----------------------------
DROP TABLE IF EXISTS `chart_board`;
CREATE TABLE `chart_board`  (
  `id` bigint NOT NULL COMMENT 'id，主键',
  `full_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '可视化名称',
  `simple_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '可视化简称',
  `board_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '可视化标识',
  `board_power` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '可视化权限',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '缩略图地址',
  `status` int NULL DEFAULT NULL COMMENT '组件状态，枚举值：1：启用；0：禁用；字典管理：数据看板-看板状态 chart_board_status',
  `bak` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `board_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '看板类型,字典管理：数据看板-看板类型chart_board_type，1、数据统计；2、图表统计',
  `sorted` int NULL DEFAULT NULL COMMENT '顺序',
  `created_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` int NULL DEFAULT 0 COMMENT '删除标志位，1：是；0：否；',
  `assign_flag` int NULL DEFAULT 0 COMMENT '是否已分配的标志位，枚举值：1：是；0：否；默认值为0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '数据看板表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of chart_board
-- ----------------------------

-- ----------------------------
-- Table structure for screen_board
-- ----------------------------
DROP TABLE IF EXISTS `screen_board`;
CREATE TABLE `screen_board`  (
  `id` bigint NOT NULL COMMENT 'id，主键',
  `full_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '可视化名称',
  `simple_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '可视化简称',
  `board_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '可视化标识',
  `board_power` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '可视化权限',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '缩略图地址',
  `status` int NULL DEFAULT NULL COMMENT '组件状态，枚举值：1：启用；0：禁用；字典管理：数据看板-看板状态 chart_board_status',
  `bak` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `board_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '看板类型,字典管理：数据看板-看板类型chart_board_type，1、数据统计；2、图表统计',
  `sorted` int NULL DEFAULT NULL COMMENT '顺序',
  `location` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '位置',
  `created_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` int NULL DEFAULT 0 COMMENT '删除标志位，1：是；0：否；',
  `assign_flag` int NULL DEFAULT 0 COMMENT '是否已分配的标志位，枚举值：1：是；0：否；默认值为0',
  `default_flag` int NULL DEFAULT NULL COMMENT '是否是默认大屏，枚举值：1：是；0：否；',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '大屏看板表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of screen_board
-- ----------------------------
INSERT INTO `screen_board` VALUES (1742790327539806209, '可视化分析大屏', NULL, '/screen/calculate/index', '', 'public-images/2024-01/e2c7b9eb15d54094bf6ed60e6eec31ce1704348519666.jpg', 1, '', NULL, 1, NULL, '-1270012023', '2024-01-25 22:02:02', '-1270012023', NULL, 0, 0, 0);

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area`  (
  `code` bigint NOT NULL COMMENT '区域编码',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父级区域编码',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `short_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区域简称',
  `same_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '行政区域归属（适用县区对应）',
  `level` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '级别：1.省级2.市级3.县区级',
  `order_num` int NULL DEFAULT NULL COMMENT '排序',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述信息',
  `is_self` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自定义：1是，2否',
  `status` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否启用：1.启用，2.停用',
  `user_id` bigint NULL DEFAULT NULL COMMENT '创建人Id',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`code`) USING BTREE,
  INDEX `area_index_all`(`name` ASC) USING BTREE,
  INDEX `level`(`level` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '行政区域管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO `sys_area` VALUES (11, 0, '北京市', '北京市', NULL, '1', 1, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', 'haoxuyang', '2022-09-30 16:22:12');
INSERT INTO `sys_area` VALUES (12, 0, '天津市', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (13, 0, '河北省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (14, 0, '山西省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (15, 0, '内蒙古自治区', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (21, 0, '辽宁省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (22, 0, '吉林省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (23, 0, '黑龙江省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (31, 0, '上海市', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (32, 0, '江苏省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (33, 0, '浙江省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (34, 0, '安徽省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (35, 0, '福建省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (36, 0, '江西省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (37, 0, '山东省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (41, 0, '河南省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (42, 0, '湖北省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (43, 0, '湖南省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (44, 0, '广东省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (45, 0, '广西壮族自治区', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (46, 0, '海南省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (50, 0, '重庆市', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (51, 0, '四川省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (52, 0, '贵州省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (53, 0, '云南省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:29');
INSERT INTO `sys_area` VALUES (54, 0, '西藏自治区', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:30');
INSERT INTO `sys_area` VALUES (61, 0, '陕西省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:30');
INSERT INTO `sys_area` VALUES (62, 0, '甘肃省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:30');
INSERT INTO `sys_area` VALUES (63, 0, '青海省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:30');
INSERT INTO `sys_area` VALUES (64, 0, '宁夏回族自治区', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:30');
INSERT INTO `sys_area` VALUES (65, 0, '新疆维吾尔自治区', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 17:59:50', '', '2021-11-03 18:01:30');
INSERT INTO `sys_area` VALUES (71, 0, '台湾省', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:00:32', '', '2021-11-03 18:01:30');
INSERT INTO `sys_area` VALUES (81, 0, '香港特别行政区', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:00:37', '', '2021-11-03 18:01:30');
INSERT INTO `sys_area` VALUES (82, 0, '澳门特别行政区', NULL, NULL, '1', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:00:41', '', '2021-11-03 18:01:30');
INSERT INTO `sys_area` VALUES (1101, 11, '市辖区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (1201, 12, '市辖区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1301, 13, '石家庄市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1302, 13, '唐山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1303, 13, '秦皇岛市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1304, 13, '邯郸市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1305, 13, '邢台市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1306, 13, '保定市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1307, 13, '张家口市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1308, 13, '承德市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1309, 13, '沧州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1310, 13, '廊坊市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1311, 13, '衡水市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1401, 14, '太原市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1402, 14, '大同市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:04');
INSERT INTO `sys_area` VALUES (1403, 14, '阳泉市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1404, 14, '长治市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1405, 14, '晋城市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1406, 14, '朔州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1407, 14, '晋中市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1408, 14, '运城市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1409, 14, '忻州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1410, 14, '临汾市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1411, 14, '吕梁市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1501, 15, '呼和浩特市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1502, 15, '包头市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1503, 15, '乌海市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1504, 15, '赤峰市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1505, 15, '通辽市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:05');
INSERT INTO `sys_area` VALUES (1506, 15, '鄂尔多斯市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (1507, 15, '呼伦贝尔市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (1508, 15, '巴彦淖尔市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (1509, 15, '乌兰察布市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (1522, 15, '兴安盟', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (1525, 15, '锡林郭勒盟', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (1529, 15, '阿拉善盟', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2101, 21, '沈阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2102, 21, '大连市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2103, 21, '鞍山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2104, 21, '抚顺市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2105, 21, '本溪市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2106, 21, '丹东市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2107, 21, '锦州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2108, 21, '营口市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2109, 21, '阜新市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:06');
INSERT INTO `sys_area` VALUES (2110, 21, '辽阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2111, 21, '盘锦市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2112, 21, '铁岭市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2113, 21, '朝阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2114, 21, '葫芦岛市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2201, 22, '长春市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2202, 22, '吉林市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2203, 22, '四平市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2204, 22, '辽源市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2205, 22, '通化市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2206, 22, '白山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2207, 22, '松原市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2208, 22, '白城市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2224, 22, '延边朝鲜族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2301, 23, '哈尔滨市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2302, 23, '齐齐哈尔市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2303, 23, '鸡西市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2304, 23, '鹤岗市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:07');
INSERT INTO `sys_area` VALUES (2305, 23, '双鸭山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (2306, 23, '大庆市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (2307, 23, '伊春市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (2308, 23, '佳木斯市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (2309, 23, '七台河市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (2310, 23, '牡丹江市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (2311, 23, '黑河市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (2312, 23, '绥化市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (2327, 23, '大兴安岭地区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (3101, 31, '市辖区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (3201, 32, '南京市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (3202, 32, '无锡市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (3203, 32, '徐州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (3204, 32, '常州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (3205, 32, '苏州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (3206, 32, '南通市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (3207, 32, '连云港市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:08');
INSERT INTO `sys_area` VALUES (3208, 32, '淮安市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3209, 32, '盐城市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3210, 32, '扬州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3211, 32, '镇江市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3212, 32, '泰州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3213, 32, '宿迁市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3301, 33, '杭州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3302, 33, '宁波市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3303, 33, '温州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3304, 33, '嘉兴市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3305, 33, '湖州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3306, 33, '绍兴市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3307, 33, '金华市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3308, 33, '衢州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3309, 33, '舟山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3310, 33, '台州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3311, 33, '丽水市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3401, 34, '合肥市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:09');
INSERT INTO `sys_area` VALUES (3402, 34, '芜湖市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3403, 34, '蚌埠市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3404, 34, '淮南市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3405, 34, '马鞍山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3406, 34, '淮北市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3407, 34, '铜陵市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3408, 34, '安庆市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3410, 34, '黄山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3411, 34, '滁州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3412, 34, '阜阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3413, 34, '宿州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3415, 34, '六安市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3416, 34, '亳州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3417, 34, '池州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3418, 34, '宣城市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3501, 35, '福州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3502, 35, '厦门市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3503, 35, '莆田市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:10');
INSERT INTO `sys_area` VALUES (3504, 35, '三明市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3505, 35, '泉州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3506, 35, '漳州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3507, 35, '南平市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3508, 35, '龙岩市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3509, 35, '宁德市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3601, 36, '南昌市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3602, 36, '景德镇市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3603, 36, '萍乡市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3604, 36, '九江市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3605, 36, '新余市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3606, 36, '鹰潭市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3607, 36, '赣州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3608, 36, '吉安市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3609, 36, '宜春市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3610, 36, '抚州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3611, 36, '上饶市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:11');
INSERT INTO `sys_area` VALUES (3701, 37, '济南市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3702, 37, '青岛市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3703, 37, '淄博市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3704, 37, '枣庄市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3705, 37, '东营市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3706, 37, '烟台市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3707, 37, '潍坊市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3708, 37, '济宁市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3709, 37, '泰安市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3710, 37, '威海市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3711, 37, '日照市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3713, 37, '临沂市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3714, 37, '德州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3715, 37, '聊城市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3716, 37, '滨州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (3717, 37, '菏泽市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (4101, 41, '郑州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:12');
INSERT INTO `sys_area` VALUES (4102, 41, '开封市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4103, 41, '洛阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4104, 41, '平顶山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4105, 41, '安阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4106, 41, '鹤壁市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4107, 41, '新乡市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4108, 41, '焦作市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4109, 41, '濮阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4110, 41, '许昌市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4111, 41, '漯河市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4112, 41, '三门峡市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4113, 41, '南阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4114, 41, '商丘市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4115, 41, '信阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4116, 41, '周口市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4117, 41, '驻马店市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4190, 41, '省直辖县级行政区划', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4201, 42, '武汉市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4202, 42, '黄石市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4203, 42, '十堰市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4205, 42, '宜昌市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:13');
INSERT INTO `sys_area` VALUES (4206, 42, '襄阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4207, 42, '鄂州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4208, 42, '荆门市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4209, 42, '孝感市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4210, 42, '荆州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4211, 42, '黄冈市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4212, 42, '咸宁市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4213, 42, '随州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4228, 42, '恩施土家族苗族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4290, 42, '省直辖县级行政区划', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4301, 43, '长沙市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4302, 43, '株洲市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4303, 43, '湘潭市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4304, 43, '衡阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4305, 43, '邵阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4306, 43, '岳阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4307, 43, '常德市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:14');
INSERT INTO `sys_area` VALUES (4308, 43, '张家界市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4309, 43, '益阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4310, 43, '郴州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4311, 43, '永州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4312, 43, '怀化市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4313, 43, '娄底市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4331, 43, '湘西土家族苗族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4401, 44, '广州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4402, 44, '韶关市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4403, 44, '深圳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4404, 44, '珠海市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4405, 44, '汕头市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4406, 44, '佛山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4407, 44, '江门市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4408, 44, '湛江市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4409, 44, '茂名市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4412, 44, '肇庆市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4413, 44, '惠州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4414, 44, '梅州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4415, 44, '汕尾市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:15');
INSERT INTO `sys_area` VALUES (4416, 44, '河源市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4417, 44, '阳江市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4418, 44, '清远市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4419, 44, '东莞市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4420, 44, '中山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4451, 44, '潮州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4452, 44, '揭阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4453, 44, '云浮市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4501, 45, '南宁市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4502, 45, '柳州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4503, 45, '桂林市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4504, 45, '梧州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4505, 45, '北海市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4506, 45, '防城港市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4507, 45, '钦州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4508, 45, '贵港市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4509, 45, '玉林市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4510, 45, '百色市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:16');
INSERT INTO `sys_area` VALUES (4511, 45, '贺州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (4512, 45, '河池市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (4513, 45, '来宾市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (4514, 45, '崇左市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (4601, 46, '海口市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (4602, 46, '三亚市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (4603, 46, '三沙市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (4604, 46, '儋州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (4690, 46, '省直辖县级行政区划', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (5001, 50, '市辖区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (5002, 50, '县', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (5101, 51, '成都市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (5103, 51, '自贡市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (5104, 51, '攀枝花市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (5105, 51, '泸州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (5106, 51, '德阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (5107, 51, '绵阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:17');
INSERT INTO `sys_area` VALUES (5108, 51, '广元市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5109, 51, '遂宁市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5110, 51, '内江市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5111, 51, '乐山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5113, 51, '南充市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5114, 51, '眉山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5115, 51, '宜宾市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5116, 51, '广安市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5117, 51, '达州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5118, 51, '雅安市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5119, 51, '巴中市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5120, 51, '资阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5132, 51, '阿坝藏族羌族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5133, 51, '甘孜藏族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5134, 51, '凉山彝族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5201, 52, '贵阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5202, 52, '六盘水市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5203, 52, '遵义市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5204, 52, '安顺市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:18');
INSERT INTO `sys_area` VALUES (5205, 52, '毕节市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5206, 52, '铜仁市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5223, 52, '黔西南布依族苗族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5226, 52, '黔东南苗族侗族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5227, 52, '黔南布依族苗族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5301, 53, '昆明市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5303, 53, '曲靖市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5304, 53, '玉溪市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5305, 53, '保山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5306, 53, '昭通市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5307, 53, '丽江市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5308, 53, '普洱市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5309, 53, '临沧市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5323, 53, '楚雄彝族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5325, 53, '红河哈尼族彝族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5326, 53, '文山壮族苗族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5328, 53, '西双版纳傣族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5329, 53, '大理白族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5331, 53, '德宏傣族景颇族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5333, 53, '怒江傈僳族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5334, 53, '迪庆藏族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5401, 54, '拉萨市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5402, 54, '日喀则市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5403, 54, '昌都市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:19');
INSERT INTO `sys_area` VALUES (5404, 54, '林芝市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (5405, 54, '山南市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (5406, 54, '那曲市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (5425, 54, '阿里地区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6101, 61, '西安市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6102, 61, '铜川市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6103, 61, '宝鸡市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6104, 61, '咸阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6105, 61, '渭南市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6106, 61, '延安市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6107, 61, '汉中市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6108, 61, '榆林市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6109, 61, '安康市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6110, 61, '商洛市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6201, 62, '兰州市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6202, 62, '嘉峪关市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6203, 62, '金昌市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6204, 62, '白银市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6205, 62, '天水市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6206, 62, '武威市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6207, 62, '张掖市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6208, 62, '平凉市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6209, 62, '酒泉市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6210, 62, '庆阳市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6211, 62, '定西市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:20');
INSERT INTO `sys_area` VALUES (6212, 62, '陇南市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6229, 62, '临夏回族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6230, 62, '甘南藏族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6301, 63, '西宁市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6302, 63, '海东市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6322, 63, '海北藏族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6323, 63, '黄南藏族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6325, 63, '海南藏族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6326, 63, '果洛藏族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6327, 63, '玉树藏族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6328, 63, '海西蒙古族藏族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6401, 64, '银川市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6402, 64, '石嘴山市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6403, 64, '吴忠市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6404, 64, '固原市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6405, 64, '中卫市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6501, 65, '乌鲁木齐市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6502, 65, '克拉玛依市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6504, 65, '吐鲁番市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6505, 65, '哈密市', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6523, 65, '昌吉回族自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6527, 65, '博尔塔拉蒙古自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6528, 65, '巴音郭楞蒙古自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6529, 65, '阿克苏地区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6530, 65, '克孜勒苏柯尔克孜自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6531, 65, '喀什地区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:21');
INSERT INTO `sys_area` VALUES (6532, 65, '和田地区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:22');
INSERT INTO `sys_area` VALUES (6540, 65, '伊犁哈萨克自治州', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:22');
INSERT INTO `sys_area` VALUES (6542, 65, '塔城地区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:22');
INSERT INTO `sys_area` VALUES (6543, 65, '阿勒泰地区', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:22');
INSERT INTO `sys_area` VALUES (6590, 65, '自治区直辖县级行政区划', NULL, NULL, '2', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:02:09', '', '2021-11-03 18:03:22');
INSERT INTO `sys_area` VALUES (110101, 1101, '东城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110102, 1101, '西城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110105, 1101, '朝阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110106, 1101, '丰台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110107, 1101, '石景山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110108, 1101, '海淀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110109, 1101, '门头沟区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110111, 1101, '房山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110112, 1101, '通州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110113, 1101, '顺义区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110114, 1101, '昌平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110115, 1101, '大兴区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110116, 1101, '怀柔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110117, 1101, '平谷区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (110118, 1101, '密云区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:11');
INSERT INTO `sys_area` VALUES (110119, 1101, '延庆区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2022-06-30 14:11:33');
INSERT INTO `sys_area` VALUES (120101, 1201, '和平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120102, 1201, '河东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120103, 1201, '河西区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120104, 1201, '南开区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120105, 1201, '河北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120106, 1201, '红桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120110, 1201, '东丽区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120111, 1201, '西青区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120112, 1201, '津南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120113, 1201, '北辰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120114, 1201, '武清区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120115, 1201, '宝坻区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120116, 1201, '滨海新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120117, 1201, '宁河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120118, 1201, '静海区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:13');
INSERT INTO `sys_area` VALUES (120119, 1201, '蓟州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130102, 1301, '长安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130104, 1301, '桥西区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130105, 1301, '新华区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130107, 1301, '井陉矿区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130108, 1301, '裕华区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130109, 1301, '藁城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130110, 1301, '鹿泉区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130111, 1301, '栾城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130121, 1301, '井陉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130123, 1301, '正定县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130125, 1301, '行唐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130126, 1301, '灵寿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130127, 1301, '高邑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130128, 1301, '深泽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130129, 1301, '赞皇县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130130, 1301, '无极县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130131, 1301, '平山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130132, 1301, '元氏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130133, 1301, '赵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130171, 1301, '石家庄高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130172, 1301, '石家庄循环化工园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:14');
INSERT INTO `sys_area` VALUES (130181, 1301, '辛集市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130183, 1301, '晋州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130184, 1301, '新乐市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130202, 1302, '路南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130203, 1302, '路北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130204, 1302, '古冶区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130205, 1302, '开平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130207, 1302, '丰南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130208, 1302, '丰润区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130209, 1302, '曹妃甸区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130224, 1302, '滦南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130225, 1302, '乐亭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130227, 1302, '迁西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130229, 1302, '玉田县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130271, 1302, '河北唐山芦台经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130272, 1302, '唐山市汉沽管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:15');
INSERT INTO `sys_area` VALUES (130273, 1302, '唐山高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130274, 1302, '河北唐山海港经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130281, 1302, '遵化市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130283, 1302, '迁安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130284, 1302, '滦州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130302, 1303, '海港区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130303, 1303, '山海关区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130304, 1303, '北戴河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130306, 1303, '抚宁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130321, 1303, '青龙满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130322, 1303, '昌黎县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130324, 1303, '卢龙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130371, 1303, '秦皇岛市经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130372, 1303, '北戴河新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130402, 1304, '邯山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130403, 1304, '丛台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130404, 1304, '复兴区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130406, 1304, '峰峰矿区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130407, 1304, '肥乡区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130408, 1304, '永年区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:16');
INSERT INTO `sys_area` VALUES (130423, 1304, '临漳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130424, 1304, '成安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130425, 1304, '大名县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130426, 1304, '涉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130427, 1304, '磁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130430, 1304, '邱县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130431, 1304, '鸡泽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130432, 1304, '广平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130433, 1304, '馆陶县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130434, 1304, '魏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130435, 1304, '曲周县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130471, 1304, '邯郸经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130473, 1304, '邯郸冀南新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130481, 1304, '武安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130502, 1305, '襄都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130503, 1305, '信都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130505, 1305, '任泽区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130506, 1305, '南和区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130522, 1305, '临城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130523, 1305, '内丘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130524, 1305, '柏乡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:17');
INSERT INTO `sys_area` VALUES (130525, 1305, '隆尧县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130528, 1305, '宁晋县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130529, 1305, '巨鹿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130530, 1305, '新河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130531, 1305, '广宗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130532, 1305, '平乡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130533, 1305, '威县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130534, 1305, '清河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130535, 1305, '临西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130571, 1305, '河北邢台经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130581, 1305, '南宫市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130582, 1305, '沙河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130602, 1306, '竞秀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130606, 1306, '莲池区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130607, 1306, '满城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130608, 1306, '清苑区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130609, 1306, '徐水区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130623, 1306, '涞水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130624, 1306, '阜平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:18');
INSERT INTO `sys_area` VALUES (130626, 1306, '定兴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130627, 1306, '唐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130628, 1306, '高阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130629, 1306, '容城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130630, 1306, '涞源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130631, 1306, '望都县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130632, 1306, '安新县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130633, 1306, '易县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130634, 1306, '曲阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130635, 1306, '蠡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130636, 1306, '顺平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130637, 1306, '博野县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130638, 1306, '雄县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130671, 1306, '保定高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130672, 1306, '保定白沟新城', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130681, 1306, '涿州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:19');
INSERT INTO `sys_area` VALUES (130682, 1306, '定州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130683, 1306, '安国市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130684, 1306, '高碑店市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130702, 1307, '桥东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130703, 1307, '桥西区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130705, 1307, '宣化区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130706, 1307, '下花园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130708, 1307, '万全区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130709, 1307, '崇礼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130722, 1307, '张北县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130723, 1307, '康保县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130724, 1307, '沽源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130725, 1307, '尚义县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130726, 1307, '蔚县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130727, 1307, '阳原县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130728, 1307, '怀安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130730, 1307, '怀来县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:20');
INSERT INTO `sys_area` VALUES (130731, 1307, '涿鹿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130732, 1307, '赤城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130771, 1307, '张家口经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130772, 1307, '张家口市察北管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130773, 1307, '张家口市塞北管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130802, 1308, '双桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130803, 1308, '双滦区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130804, 1308, '鹰手营子矿区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130821, 1308, '承德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130822, 1308, '兴隆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130824, 1308, '滦平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130825, 1308, '隆化县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130826, 1308, '丰宁满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130827, 1308, '宽城满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130828, 1308, '围场满族蒙古族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130871, 1308, '承德高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130881, 1308, '平泉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130902, 1309, '新华区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130903, 1309, '运河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130921, 1309, '沧县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130922, 1309, '青县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:21');
INSERT INTO `sys_area` VALUES (130923, 1309, '东光县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130924, 1309, '海兴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130925, 1309, '盐山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130926, 1309, '肃宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130927, 1309, '南皮县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130928, 1309, '吴桥县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130929, 1309, '献县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130930, 1309, '孟村回族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130971, 1309, '河北沧州经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130972, 1309, '沧州高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130973, 1309, '沧州渤海新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130981, 1309, '泊头市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130982, 1309, '任丘市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130983, 1309, '黄骅市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (130984, 1309, '河间市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (131002, 1310, '安次区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (131003, 1310, '广阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:22');
INSERT INTO `sys_area` VALUES (131022, 1310, '固安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131023, 1310, '永清县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131024, 1310, '香河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131025, 1310, '大城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131026, 1310, '文安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131028, 1310, '大厂回族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131071, 1310, '廊坊经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131081, 1310, '霸州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131082, 1310, '三河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131102, 1311, '桃城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131103, 1311, '冀州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131121, 1311, '枣强县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131122, 1311, '武邑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131123, 1311, '武强县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131124, 1311, '饶阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131125, 1311, '安平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131126, 1311, '故城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131127, 1311, '景县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131128, 1311, '阜城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131171, 1311, '河北衡水高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131172, 1311, '衡水滨湖新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (131182, 1311, '深州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:23');
INSERT INTO `sys_area` VALUES (140105, 1401, '小店区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140106, 1401, '迎泽区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140107, 1401, '杏花岭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140108, 1401, '尖草坪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140109, 1401, '万柏林区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140110, 1401, '晋源区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140121, 1401, '清徐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140122, 1401, '阳曲县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140123, 1401, '娄烦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140171, 1401, '山西转型综合改革示范区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140181, 1401, '古交市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140212, 1402, '新荣区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140213, 1402, '平城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140214, 1402, '云冈区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140215, 1402, '云州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140221, 1402, '阳高县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140222, 1402, '天镇县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:24');
INSERT INTO `sys_area` VALUES (140223, 1402, '广灵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140224, 1402, '灵丘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140225, 1402, '浑源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140226, 1402, '左云县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140271, 1402, '山西大同经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140302, 1403, '城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140303, 1403, '矿区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140311, 1403, '郊区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140321, 1403, '平定县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140322, 1403, '盂县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140403, 1404, '潞州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140404, 1404, '上党区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140405, 1404, '屯留区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140406, 1404, '潞城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140423, 1404, '襄垣县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140425, 1404, '平顺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:25');
INSERT INTO `sys_area` VALUES (140426, 1404, '黎城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140427, 1404, '壶关县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140428, 1404, '长子县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140429, 1404, '武乡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140430, 1404, '沁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140431, 1404, '沁源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140471, 1404, '山西长治高新技术产业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140502, 1405, '城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140521, 1405, '沁水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140522, 1405, '阳城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140524, 1405, '陵川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140525, 1405, '泽州县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140581, 1405, '高平市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140602, 1406, '朔城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140603, 1406, '平鲁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140621, 1406, '山阴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140622, 1406, '应县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140623, 1406, '右玉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140671, 1406, '山西朔州经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:26');
INSERT INTO `sys_area` VALUES (140681, 1406, '怀仁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140702, 1407, '榆次区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140703, 1407, '太谷区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140721, 1407, '榆社县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140722, 1407, '左权县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140723, 1407, '和顺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140724, 1407, '昔阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140725, 1407, '寿阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140727, 1407, '祁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140728, 1407, '平遥县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140729, 1407, '灵石县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140781, 1407, '介休市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140802, 1408, '盐湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140821, 1408, '临猗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140822, 1408, '万荣县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140823, 1408, '闻喜县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:27');
INSERT INTO `sys_area` VALUES (140824, 1408, '稷山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140825, 1408, '新绛县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140826, 1408, '绛县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140827, 1408, '垣曲县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140828, 1408, '夏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140829, 1408, '平陆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140830, 1408, '芮城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140881, 1408, '永济市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140882, 1408, '河津市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140902, 1409, '忻府区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140921, 1409, '定襄县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140922, 1409, '五台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140923, 1409, '代县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140924, 1409, '繁峙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140925, 1409, '宁武县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140926, 1409, '静乐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140927, 1409, '神池县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140928, 1409, '五寨县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:28');
INSERT INTO `sys_area` VALUES (140929, 1409, '岢岚县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (140930, 1409, '河曲县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (140931, 1409, '保德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (140932, 1409, '偏关县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (140971, 1409, '五台山风景名胜区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (140981, 1409, '原平市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141002, 1410, '尧都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141021, 1410, '曲沃县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141022, 1410, '翼城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141023, 1410, '襄汾县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141024, 1410, '洪洞县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141025, 1410, '古县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141026, 1410, '安泽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141027, 1410, '浮山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141028, 1410, '吉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141029, 1410, '乡宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141030, 1410, '大宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141031, 1410, '隰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141032, 1410, '永和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:29');
INSERT INTO `sys_area` VALUES (141033, 1410, '蒲县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141034, 1410, '汾西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141081, 1410, '侯马市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141082, 1410, '霍州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141102, 1411, '离石区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141121, 1411, '文水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141122, 1411, '交城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141123, 1411, '兴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141124, 1411, '临县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141125, 1411, '柳林县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141126, 1411, '石楼县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141127, 1411, '岚县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141128, 1411, '方山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141129, 1411, '中阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141130, 1411, '交口县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141181, 1411, '孝义市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (141182, 1411, '汾阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (150102, 1501, '新城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (150103, 1501, '回民区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:30');
INSERT INTO `sys_area` VALUES (150104, 1501, '玉泉区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150105, 1501, '赛罕区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150121, 1501, '土默特左旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150122, 1501, '托克托县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150123, 1501, '和林格尔县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150124, 1501, '清水河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150125, 1501, '武川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150172, 1501, '呼和浩特经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150202, 1502, '东河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150203, 1502, '昆都仑区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150204, 1502, '青山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150205, 1502, '石拐区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150206, 1502, '白云鄂博矿区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150207, 1502, '九原区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150221, 1502, '土默特右旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150222, 1502, '固阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150223, 1502, '达尔罕茂明安联合旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150271, 1502, '包头稀土高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:31');
INSERT INTO `sys_area` VALUES (150302, 1503, '海勃湾区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150303, 1503, '海南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150304, 1503, '乌达区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150402, 1504, '红山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150403, 1504, '元宝山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150404, 1504, '松山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150421, 1504, '阿鲁科尔沁旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150422, 1504, '巴林左旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150423, 1504, '巴林右旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150424, 1504, '林西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150425, 1504, '克什克腾旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150426, 1504, '翁牛特旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150428, 1504, '喀喇沁旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150429, 1504, '宁城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150430, 1504, '敖汉旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150502, 1505, '科尔沁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150521, 1505, '科尔沁左翼中旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150522, 1505, '科尔沁左翼后旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150523, 1505, '开鲁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150524, 1505, '库伦旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:32');
INSERT INTO `sys_area` VALUES (150525, 1505, '奈曼旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150526, 1505, '扎鲁特旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150571, 1505, '通辽经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150581, 1505, '霍林郭勒市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150602, 1506, '东胜区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150603, 1506, '康巴什区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150621, 1506, '达拉特旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150622, 1506, '准格尔旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150623, 1506, '鄂托克前旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150624, 1506, '鄂托克旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150625, 1506, '杭锦旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150626, 1506, '乌审旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150627, 1506, '伊金霍洛旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150702, 1507, '海拉尔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150703, 1507, '扎赉诺尔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150721, 1507, '阿荣旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150722, 1507, '莫力达瓦达斡尔族自治旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150723, 1507, '鄂伦春自治旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150724, 1507, '鄂温克族自治旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150725, 1507, '陈巴尔虎旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150726, 1507, '新巴尔虎左旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150727, 1507, '新巴尔虎右旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:33');
INSERT INTO `sys_area` VALUES (150781, 1507, '满洲里市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150782, 1507, '牙克石市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150783, 1507, '扎兰屯市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150784, 1507, '额尔古纳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150785, 1507, '根河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150802, 1508, '临河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150821, 1508, '五原县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150822, 1508, '磴口县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150823, 1508, '乌拉特前旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150824, 1508, '乌拉特中旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150825, 1508, '乌拉特后旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150826, 1508, '杭锦后旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150902, 1509, '集宁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150921, 1509, '卓资县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150922, 1509, '化德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150923, 1509, '商都县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150924, 1509, '兴和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150925, 1509, '凉城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150926, 1509, '察哈尔右翼前旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150927, 1509, '察哈尔右翼中旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:34');
INSERT INTO `sys_area` VALUES (150928, 1509, '察哈尔右翼后旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (150929, 1509, '四子王旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (150981, 1509, '丰镇市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152201, 1522, '乌兰浩特市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152202, 1522, '阿尔山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152221, 1522, '科尔沁右翼前旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152222, 1522, '科尔沁右翼中旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152223, 1522, '扎赉特旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152224, 1522, '突泉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152501, 1525, '二连浩特市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152502, 1525, '锡林浩特市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152522, 1525, '阿巴嘎旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152523, 1525, '苏尼特左旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152524, 1525, '苏尼特右旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152525, 1525, '东乌珠穆沁旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152526, 1525, '西乌珠穆沁旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152527, 1525, '太仆寺旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:35');
INSERT INTO `sys_area` VALUES (152528, 1525, '镶黄旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (152529, 1525, '正镶白旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (152530, 1525, '正蓝旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (152531, 1525, '多伦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (152571, 1525, '乌拉盖管委会', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (152921, 1529, '阿拉善左旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (152922, 1529, '阿拉善右旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (152923, 1529, '额济纳旗', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (152971, 1529, '内蒙古阿拉善经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (210102, 2101, '和平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (210103, 2101, '沈河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (210104, 2101, '大东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (210105, 2101, '皇姑区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (210106, 2101, '铁西区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (210111, 2101, '苏家屯区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (210112, 2101, '浑南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (210113, 2101, '沈北新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:36');
INSERT INTO `sys_area` VALUES (210114, 2101, '于洪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210115, 2101, '辽中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210123, 2101, '康平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210124, 2101, '法库县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210181, 2101, '新民市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210202, 2102, '中山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210203, 2102, '西岗区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210204, 2102, '沙河口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210211, 2102, '甘井子区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210212, 2102, '旅顺口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210213, 2102, '金州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210214, 2102, '普兰店区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210224, 2102, '长海县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210281, 2102, '瓦房店市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210283, 2102, '庄河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210302, 2103, '铁东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210303, 2103, '铁西区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210304, 2103, '立山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210311, 2103, '千山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:37');
INSERT INTO `sys_area` VALUES (210321, 2103, '台安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210323, 2103, '岫岩满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210381, 2103, '海城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210402, 2104, '新抚区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210403, 2104, '东洲区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210404, 2104, '望花区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210411, 2104, '顺城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210421, 2104, '抚顺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210422, 2104, '新宾满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210423, 2104, '清原满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210502, 2105, '平山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210503, 2105, '溪湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210504, 2105, '明山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210505, 2105, '南芬区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210521, 2105, '本溪满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210522, 2105, '桓仁满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210602, 2106, '元宝区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:38');
INSERT INTO `sys_area` VALUES (210603, 2106, '振兴区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210604, 2106, '振安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210624, 2106, '宽甸满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210681, 2106, '东港市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210682, 2106, '凤城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210702, 2107, '古塔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210703, 2107, '凌河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210711, 2107, '太和区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210726, 2107, '黑山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210727, 2107, '义县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210781, 2107, '凌海市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210782, 2107, '北镇市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210802, 2108, '站前区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210803, 2108, '西市区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210804, 2108, '鲅鱼圈区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210811, 2108, '老边区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210881, 2108, '盖州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210882, 2108, '大石桥市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:39');
INSERT INTO `sys_area` VALUES (210902, 2109, '海州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (210903, 2109, '新邱区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (210904, 2109, '太平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (210905, 2109, '清河门区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (210911, 2109, '细河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (210921, 2109, '阜新蒙古族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (210922, 2109, '彰武县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211002, 2110, '白塔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211003, 2110, '文圣区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211004, 2110, '宏伟区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211005, 2110, '弓长岭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211011, 2110, '太子河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211021, 2110, '辽阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211081, 2110, '灯塔市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211102, 2111, '双台子区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211103, 2111, '兴隆台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211104, 2111, '大洼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:40');
INSERT INTO `sys_area` VALUES (211122, 2111, '盘山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211202, 2112, '银州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211204, 2112, '清河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211221, 2112, '铁岭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211223, 2112, '西丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211224, 2112, '昌图县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211281, 2112, '调兵山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211282, 2112, '开原市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211302, 2113, '双塔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211303, 2113, '龙城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211321, 2113, '朝阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211322, 2113, '建平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211324, 2113, '喀喇沁左翼蒙古族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211381, 2113, '北票市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211382, 2113, '凌源市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211402, 2114, '连山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211403, 2114, '龙港区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211404, 2114, '南票区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:41');
INSERT INTO `sys_area` VALUES (211421, 2114, '绥中县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (211422, 2114, '建昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (211481, 2114, '兴城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220102, 2201, '南关区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220103, 2201, '宽城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220104, 2201, '朝阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220105, 2201, '二道区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220106, 2201, '绿园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220112, 2201, '双阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220113, 2201, '九台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220122, 2201, '农安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220171, 2201, '长春经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220172, 2201, '长春净月高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220173, 2201, '长春高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220174, 2201, '长春汽车经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220182, 2201, '榆树市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220183, 2201, '德惠市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:42');
INSERT INTO `sys_area` VALUES (220184, 2201, '公主岭市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220202, 2202, '昌邑区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220203, 2202, '龙潭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220204, 2202, '船营区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220211, 2202, '丰满区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220221, 2202, '永吉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220271, 2202, '吉林经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220272, 2202, '吉林高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220273, 2202, '吉林中国新加坡食品区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220281, 2202, '蛟河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220282, 2202, '桦甸市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220283, 2202, '舒兰市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220284, 2202, '磐石市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220302, 2203, '铁西区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:43');
INSERT INTO `sys_area` VALUES (220303, 2203, '铁东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220322, 2203, '梨树县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220323, 2203, '伊通满族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220382, 2203, '双辽市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220402, 2204, '龙山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220403, 2204, '西安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220421, 2204, '东丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220422, 2204, '东辽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220502, 2205, '东昌区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220503, 2205, '二道江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220521, 2205, '通化县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220523, 2205, '辉南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220524, 2205, '柳河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220581, 2205, '梅河口市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:44');
INSERT INTO `sys_area` VALUES (220582, 2205, '集安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220602, 2206, '浑江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220605, 2206, '江源区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220621, 2206, '抚松县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220622, 2206, '靖宇县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220623, 2206, '长白朝鲜族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220681, 2206, '临江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220702, 2207, '宁江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220721, 2207, '前郭尔罗斯蒙古族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220722, 2207, '长岭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220723, 2207, '乾安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220771, 2207, '吉林松原经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220781, 2207, '扶余市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220802, 2208, '洮北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220821, 2208, '镇赉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220822, 2208, '通榆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220871, 2208, '吉林白城经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220881, 2208, '洮南市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:45');
INSERT INTO `sys_area` VALUES (220882, 2208, '大安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (222401, 2224, '延吉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (222402, 2224, '图们市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (222403, 2224, '敦化市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (222404, 2224, '珲春市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (222405, 2224, '龙井市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (222406, 2224, '和龙市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (222424, 2224, '汪清县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (222426, 2224, '安图县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (230102, 2301, '道里区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (230103, 2301, '南岗区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (230104, 2301, '道外区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (230108, 2301, '平房区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (230109, 2301, '松北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (230110, 2301, '香坊区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (230111, 2301, '呼兰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (230112, 2301, '阿城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:46');
INSERT INTO `sys_area` VALUES (230113, 2301, '双城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230123, 2301, '依兰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230124, 2301, '方正县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230125, 2301, '宾县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230126, 2301, '巴彦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230127, 2301, '木兰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230128, 2301, '通河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230129, 2301, '延寿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230183, 2301, '尚志市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230184, 2301, '五常市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230202, 2302, '龙沙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230203, 2302, '建华区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230204, 2302, '铁锋区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230205, 2302, '昂昂溪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:47');
INSERT INTO `sys_area` VALUES (230206, 2302, '富拉尔基区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230207, 2302, '碾子山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230208, 2302, '梅里斯达斡尔族区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230221, 2302, '龙江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230223, 2302, '依安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230224, 2302, '泰来县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230225, 2302, '甘南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230227, 2302, '富裕县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230229, 2302, '克山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230230, 2302, '克东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230231, 2302, '拜泉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230281, 2302, '讷河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230302, 2303, '鸡冠区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230303, 2303, '恒山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230304, 2303, '滴道区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230305, 2303, '梨树区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:48');
INSERT INTO `sys_area` VALUES (230306, 2303, '城子河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230307, 2303, '麻山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230321, 2303, '鸡东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230381, 2303, '虎林市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230382, 2303, '密山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230402, 2304, '向阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230403, 2304, '工农区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230404, 2304, '南山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230405, 2304, '兴安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230406, 2304, '东山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230407, 2304, '兴山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230421, 2304, '萝北县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230422, 2304, '绥滨县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:49');
INSERT INTO `sys_area` VALUES (230502, 2305, '尖山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230503, 2305, '岭东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230505, 2305, '四方台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230506, 2305, '宝山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230521, 2305, '集贤县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230522, 2305, '友谊县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230523, 2305, '宝清县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230524, 2305, '饶河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230602, 2306, '萨尔图区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230603, 2306, '龙凤区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230604, 2306, '让胡路区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230605, 2306, '红岗区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230606, 2306, '大同区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230621, 2306, '肇州县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230622, 2306, '肇源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230623, 2306, '林甸县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:50');
INSERT INTO `sys_area` VALUES (230624, 2306, '杜尔伯特蒙古族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230671, 2306, '大庆高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230717, 2307, '伊美区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230718, 2307, '乌翠区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230719, 2307, '友好区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230722, 2307, '嘉荫县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230723, 2307, '汤旺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230724, 2307, '丰林县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230725, 2307, '大箐山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230726, 2307, '南岔县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230751, 2307, '金林区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230781, 2307, '铁力市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:51', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230803, 2308, '向阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230804, 2308, '前进区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230805, 2308, '东风区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230811, 2308, '郊区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:51');
INSERT INTO `sys_area` VALUES (230822, 2308, '桦南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (230826, 2308, '桦川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (230828, 2308, '汤原县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (230881, 2308, '同江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (230882, 2308, '富锦市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (230883, 2308, '抚远市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (230902, 2309, '新兴区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (230903, 2309, '桃山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (230904, 2309, '茄子河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (230921, 2309, '勃利县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (231002, 2310, '东安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (231003, 2310, '阳明区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (231004, 2310, '爱民区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (231005, 2310, '西安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (231025, 2310, '林口县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (231071, 2310, '牡丹江经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (231081, 2310, '绥芬河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:52');
INSERT INTO `sys_area` VALUES (231083, 2310, '海林市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231084, 2310, '宁安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231085, 2310, '穆棱市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231086, 2310, '东宁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231102, 2311, '爱辉区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231123, 2311, '逊克县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231124, 2311, '孙吴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231181, 2311, '北安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231182, 2311, '五大连池市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231183, 2311, '嫩江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231202, 2312, '北林区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231221, 2312, '望奎县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231222, 2312, '兰西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231223, 2312, '青冈县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:53');
INSERT INTO `sys_area` VALUES (231224, 2312, '庆安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (231225, 2312, '明水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (231226, 2312, '绥棱县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (231281, 2312, '安达市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (231282, 2312, '肇东市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (231283, 2312, '海伦市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (232701, 2327, '漠河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (232721, 2327, '呼玛县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (232722, 2327, '塔河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (232761, 2327, '加格达奇区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (232762, 2327, '松岭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (232763, 2327, '新林区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (232764, 2327, '呼中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (310101, 3101, '黄浦区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:54');
INSERT INTO `sys_area` VALUES (310104, 3101, '徐汇区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310105, 3101, '长宁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310106, 3101, '静安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310107, 3101, '普陀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310109, 3101, '虹口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310110, 3101, '杨浦区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310112, 3101, '闵行区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310113, 3101, '宝山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310114, 3101, '嘉定区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310115, 3101, '浦东新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310116, 3101, '金山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310117, 3101, '松江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310118, 3101, '青浦区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310120, 3101, '奉贤区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (310151, 3101, '崇明区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (320102, 3201, '玄武区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (320104, 3201, '秦淮区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (320105, 3201, '建邺区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (320106, 3201, '鼓楼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (320111, 3201, '浦口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:55');
INSERT INTO `sys_area` VALUES (320113, 3201, '栖霞区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320114, 3201, '雨花台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320115, 3201, '江宁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320116, 3201, '六合区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320117, 3201, '溧水区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320118, 3201, '高淳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320205, 3202, '锡山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320206, 3202, '惠山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320211, 3202, '滨湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320213, 3202, '梁溪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320214, 3202, '新吴区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320281, 3202, '江阴市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320282, 3202, '宜兴市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320302, 3203, '鼓楼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320303, 3203, '云龙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320305, 3203, '贾汪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320311, 3203, '泉山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:56');
INSERT INTO `sys_area` VALUES (320312, 3203, '铜山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320321, 3203, '丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320322, 3203, '沛县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320324, 3203, '睢宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320371, 3203, '徐州经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320381, 3203, '新沂市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320382, 3203, '邳州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320402, 3204, '天宁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320404, 3204, '钟楼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320411, 3204, '新北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320412, 3204, '武进区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320413, 3204, '金坛区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320481, 3204, '溧阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320505, 3205, '虎丘区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320506, 3205, '吴中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320507, 3205, '相城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320508, 3205, '姑苏区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320509, 3205, '吴江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320571, 3205, '苏州工业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:57');
INSERT INTO `sys_area` VALUES (320581, 3205, '常熟市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320582, 3205, '张家港市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320583, 3205, '昆山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320585, 3205, '太仓市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320602, 3206, '崇川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320611, 3206, '港闸区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320612, 3206, '通州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320623, 3206, '如东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320671, 3206, '南通经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320681, 3206, '启东市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320682, 3206, '如皋市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320684, 3206, '海门市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320685, 3206, '海安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320703, 3207, '连云区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320706, 3207, '海州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320707, 3207, '赣榆区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320722, 3207, '东海县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320723, 3207, '灌云县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320724, 3207, '灌南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320771, 3207, '连云港经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:58');
INSERT INTO `sys_area` VALUES (320772, 3207, '连云港高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320803, 3208, '淮安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320804, 3208, '淮阴区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320812, 3208, '清江浦区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320813, 3208, '洪泽区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320826, 3208, '涟水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320830, 3208, '盱眙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320831, 3208, '金湖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320871, 3208, '淮安经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320902, 3209, '亭湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320903, 3209, '盐都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320904, 3209, '大丰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320921, 3209, '响水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320922, 3209, '滨海县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320923, 3209, '阜宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320924, 3209, '射阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320925, 3209, '建湖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320971, 3209, '盐城经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (320981, 3209, '东台市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (321002, 3210, '广陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (321003, 3210, '邗江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (321012, 3210, '江都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (321023, 3210, '宝应县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:04:59');
INSERT INTO `sys_area` VALUES (321071, 3210, '扬州经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321081, 3210, '仪征市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321084, 3210, '高邮市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321102, 3211, '京口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321111, 3211, '润州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321112, 3211, '丹徒区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321171, 3211, '镇江新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321181, 3211, '丹阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321182, 3211, '扬中市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321183, 3211, '句容市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321202, 3212, '海陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321203, 3212, '高港区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321204, 3212, '姜堰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321271, 3212, '泰州医药高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321281, 3212, '兴化市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321282, 3212, '靖江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321283, 3212, '泰兴市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321302, 3213, '宿城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321311, 3213, '宿豫区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321322, 3213, '沭阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321323, 3213, '泗阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321324, 3213, '泗洪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:00');
INSERT INTO `sys_area` VALUES (321371, 3213, '宿迁经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330102, 3301, '上城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330103, 3301, '下城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330104, 3301, '江干区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330105, 3301, '拱墅区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330106, 3301, '西湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330108, 3301, '滨江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330109, 3301, '萧山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330110, 3301, '余杭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330111, 3301, '富阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330112, 3301, '临安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330122, 3301, '桐庐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330127, 3301, '淳安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330182, 3301, '建德市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330203, 3302, '海曙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330205, 3302, '江北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330206, 3302, '北仑区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330211, 3302, '镇海区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330212, 3302, '鄞州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330213, 3302, '奉化区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330225, 3302, '象山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330226, 3302, '宁海县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330281, 3302, '余姚市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330282, 3302, '慈溪市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330302, 3303, '鹿城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:01');
INSERT INTO `sys_area` VALUES (330303, 3303, '龙湾区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330304, 3303, '瓯海区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330305, 3303, '洞头区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330324, 3303, '永嘉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330326, 3303, '平阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330327, 3303, '苍南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330328, 3303, '文成县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330329, 3303, '泰顺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330371, 3303, '温州经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330381, 3303, '瑞安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330382, 3303, '乐清市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330383, 3303, '龙港市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330402, 3304, '南湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330411, 3304, '秀洲区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330421, 3304, '嘉善县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330424, 3304, '海盐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330481, 3304, '海宁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330482, 3304, '平湖市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330483, 3304, '桐乡市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330502, 3305, '吴兴区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330503, 3305, '南浔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330521, 3305, '德清县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330522, 3305, '长兴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330523, 3305, '安吉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:02');
INSERT INTO `sys_area` VALUES (330602, 3306, '越城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330603, 3306, '柯桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330604, 3306, '上虞区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330624, 3306, '新昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330681, 3306, '诸暨市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330683, 3306, '嵊州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330702, 3307, '婺城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330703, 3307, '金东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330723, 3307, '武义县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330726, 3307, '浦江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330727, 3307, '磐安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330781, 3307, '兰溪市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330782, 3307, '义乌市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330783, 3307, '东阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330784, 3307, '永康市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330802, 3308, '柯城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330803, 3308, '衢江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330822, 3308, '常山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330824, 3308, '开化县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330825, 3308, '龙游县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330881, 3308, '江山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330902, 3309, '定海区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330903, 3309, '普陀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330921, 3309, '岱山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:03');
INSERT INTO `sys_area` VALUES (330922, 3309, '嵊泗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331002, 3310, '椒江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331003, 3310, '黄岩区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331004, 3310, '路桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331022, 3310, '三门县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331023, 3310, '天台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331024, 3310, '仙居县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331081, 3310, '温岭市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331082, 3310, '临海市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331083, 3310, '玉环市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331102, 3311, '莲都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331121, 3311, '青田县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331122, 3311, '缙云县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331123, 3311, '遂昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331124, 3311, '松阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331125, 3311, '云和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331126, 3311, '庆元县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331127, 3311, '景宁畲族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (331181, 3311, '龙泉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (340102, 3401, '瑶海区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (340103, 3401, '庐阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (340104, 3401, '蜀山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (340111, 3401, '包河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (340121, 3401, '长丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (340122, 3401, '肥东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (340123, 3401, '肥西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:04');
INSERT INTO `sys_area` VALUES (340124, 3401, '庐江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340171, 3401, '合肥高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340172, 3401, '合肥经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340173, 3401, '合肥新站高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340181, 3401, '巢湖市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340202, 3402, '镜湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340203, 3402, '弋江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340207, 3402, '鸠江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340208, 3402, '三山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340221, 3402, '芜湖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340222, 3402, '繁昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340223, 3402, '南陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340271, 3402, '芜湖经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340272, 3402, '安徽芜湖长江大桥经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340281, 3402, '无为市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340302, 3403, '龙子湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340303, 3403, '蚌山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340304, 3403, '禹会区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340311, 3403, '淮上区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340321, 3403, '怀远县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340322, 3403, '五河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340323, 3403, '固镇县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340371, 3403, '蚌埠市高新技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340372, 3403, '蚌埠市经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:05');
INSERT INTO `sys_area` VALUES (340402, 3404, '大通区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:06');
INSERT INTO `sys_area` VALUES (340403, 3404, '田家庵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:06');
INSERT INTO `sys_area` VALUES (340404, 3404, '谢家集区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:06');
INSERT INTO `sys_area` VALUES (340405, 3404, '八公山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:06');
INSERT INTO `sys_area` VALUES (340406, 3404, '潘集区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:06');
INSERT INTO `sys_area` VALUES (340421, 3404, '凤台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:06');
INSERT INTO `sys_area` VALUES (340422, 3404, '寿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:05:06');
INSERT INTO `sys_area` VALUES (340503, 3405, '花山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340504, 3405, '雨山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340506, 3405, '博望区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340521, 3405, '当涂县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340522, 3405, '含山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340523, 3405, '和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340602, 3406, '杜集区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340603, 3406, '相山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340604, 3406, '烈山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340621, 3406, '濉溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340705, 3407, '铜官区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340706, 3407, '义安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:21');
INSERT INTO `sys_area` VALUES (340711, 3407, '郊区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340722, 3407, '枞阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340802, 3408, '迎江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340803, 3408, '大观区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340811, 3408, '宜秀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340822, 3408, '怀宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340825, 3408, '太湖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340826, 3408, '宿松县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340827, 3408, '望江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340828, 3408, '岳西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340871, 3408, '安徽安庆经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340881, 3408, '桐城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (340882, 3408, '潜山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341002, 3410, '屯溪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341003, 3410, '黄山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341004, 3410, '徽州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341021, 3410, '歙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341022, 3410, '休宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341023, 3410, '黟县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341024, 3410, '祁门县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341102, 3411, '琅琊区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341103, 3411, '南谯区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341122, 3411, '来安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341124, 3411, '全椒县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341125, 3411, '定远县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341126, 3411, '凤阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341171, 3411, '苏滁现代产业园', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:22');
INSERT INTO `sys_area` VALUES (341172, 3411, '滁州经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341181, 3411, '天长市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341182, 3411, '明光市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341202, 3412, '颍州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341203, 3412, '颍东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341204, 3412, '颍泉区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341221, 3412, '临泉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341222, 3412, '太和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341225, 3412, '阜南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341226, 3412, '颍上县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341271, 3412, '阜阳合肥现代产业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341272, 3412, '阜阳经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341282, 3412, '界首市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341302, 3413, '埇桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:23');
INSERT INTO `sys_area` VALUES (341321, 3413, '砀山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341322, 3413, '萧县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341323, 3413, '灵璧县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341324, 3413, '泗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341371, 3413, '宿州马鞍山现代产业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341372, 3413, '宿州经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341502, 3415, '金安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341503, 3415, '裕安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341504, 3415, '叶集区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341522, 3415, '霍邱县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:24');
INSERT INTO `sys_area` VALUES (341523, 3415, '舒城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341524, 3415, '金寨县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341525, 3415, '霍山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341602, 3416, '谯城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341621, 3416, '涡阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341622, 3416, '蒙城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341623, 3416, '利辛县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341702, 3417, '贵池区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341721, 3417, '东至县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341722, 3417, '石台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:25');
INSERT INTO `sys_area` VALUES (341723, 3417, '青阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (341802, 3418, '宣州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (341821, 3418, '郎溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (341823, 3418, '泾县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (341824, 3418, '绩溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (341825, 3418, '旌德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (341871, 3418, '宣城市经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (341881, 3418, '宁国市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (341882, 3418, '广德市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (350102, 3501, '鼓楼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (350103, 3501, '台江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:26');
INSERT INTO `sys_area` VALUES (350104, 3501, '仓山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350105, 3501, '马尾区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350111, 3501, '晋安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350112, 3501, '长乐区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350121, 3501, '闽侯县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350122, 3501, '连江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350123, 3501, '罗源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350124, 3501, '闽清县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350125, 3501, '永泰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350128, 3501, '平潭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350181, 3501, '福清市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350203, 3502, '思明区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350205, 3502, '海沧区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350206, 3502, '湖里区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350211, 3502, '集美区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350212, 3502, '同安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350213, 3502, '翔安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350302, 3503, '城厢区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350303, 3503, '涵江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:27');
INSERT INTO `sys_area` VALUES (350304, 3503, '荔城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350305, 3503, '秀屿区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350322, 3503, '仙游县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350402, 3504, '梅列区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350403, 3504, '三元区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350421, 3504, '明溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350423, 3504, '清流县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350424, 3504, '宁化县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350425, 3504, '大田县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350426, 3504, '尤溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350427, 3504, '沙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350428, 3504, '将乐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350429, 3504, '泰宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350430, 3504, '建宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350481, 3504, '永安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:28');
INSERT INTO `sys_area` VALUES (350502, 3505, '鲤城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350503, 3505, '丰泽区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350504, 3505, '洛江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350505, 3505, '泉港区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350521, 3505, '惠安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350524, 3505, '安溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350525, 3505, '永春县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350526, 3505, '德化县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350527, 3505, '金门县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350581, 3505, '石狮市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350582, 3505, '晋江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350583, 3505, '南安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350602, 3506, '芗城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350603, 3506, '龙文区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350622, 3506, '云霄县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:29');
INSERT INTO `sys_area` VALUES (350623, 3506, '漳浦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350624, 3506, '诏安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350625, 3506, '长泰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350626, 3506, '东山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350627, 3506, '南靖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350628, 3506, '平和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350629, 3506, '华安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350681, 3506, '龙海市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350702, 3507, '延平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350703, 3507, '建阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350721, 3507, '顺昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350722, 3507, '浦城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350723, 3507, '光泽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350724, 3507, '松溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350725, 3507, '政和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350781, 3507, '邵武市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350782, 3507, '武夷山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350783, 3507, '建瓯市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350802, 3508, '新罗区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:30');
INSERT INTO `sys_area` VALUES (350803, 3508, '永定区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350821, 3508, '长汀县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350823, 3508, '上杭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350824, 3508, '武平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350825, 3508, '连城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350881, 3508, '漳平市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350902, 3509, '蕉城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350921, 3509, '霞浦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350922, 3509, '古田县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350923, 3509, '屏南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350924, 3509, '寿宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350925, 3509, '周宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350926, 3509, '柘荣县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350981, 3509, '福安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (350982, 3509, '福鼎市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360102, 3601, '东湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360103, 3601, '西湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360104, 3601, '青云谱区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360111, 3601, '青山湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360112, 3601, '新建区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360113, 3601, '红谷滩区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360121, 3601, '南昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360123, 3601, '安义县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360124, 3601, '进贤县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360202, 3602, '昌江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360203, 3602, '珠山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:31');
INSERT INTO `sys_area` VALUES (360222, 3602, '浮梁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360281, 3602, '乐平市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360302, 3603, '安源区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360313, 3603, '湘东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360321, 3603, '莲花县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360322, 3603, '上栗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360323, 3603, '芦溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360402, 3604, '濂溪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360403, 3604, '浔阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360404, 3604, '柴桑区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360423, 3604, '武宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360424, 3604, '修水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360425, 3604, '永修县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360426, 3604, '德安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360428, 3604, '都昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360429, 3604, '湖口县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360430, 3604, '彭泽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360481, 3604, '瑞昌市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360482, 3604, '共青城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360483, 3604, '庐山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360502, 3605, '渝水区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360521, 3605, '分宜县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360602, 3606, '月湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360603, 3606, '余江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360681, 3606, '贵溪市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360702, 3607, '章贡区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360703, 3607, '南康区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:32');
INSERT INTO `sys_area` VALUES (360704, 3607, '赣县区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360722, 3607, '信丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360723, 3607, '大余县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360724, 3607, '上犹县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360725, 3607, '崇义县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360726, 3607, '安远县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360728, 3607, '定南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360729, 3607, '全南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360730, 3607, '宁都县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360731, 3607, '于都县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360732, 3607, '兴国县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360733, 3607, '会昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360734, 3607, '寻乌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360735, 3607, '石城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360781, 3607, '瑞金市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360783, 3607, '龙南市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360802, 3608, '吉州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360803, 3608, '青原区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360821, 3608, '吉安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360822, 3608, '吉水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360823, 3608, '峡江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360824, 3608, '新干县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360825, 3608, '永丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360826, 3608, '泰和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360827, 3608, '遂川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360828, 3608, '万安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:33');
INSERT INTO `sys_area` VALUES (360829, 3608, '安福县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360830, 3608, '永新县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360881, 3608, '井冈山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360902, 3609, '袁州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360921, 3609, '奉新县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360922, 3609, '万载县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360923, 3609, '上高县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360924, 3609, '宜丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360925, 3609, '靖安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360926, 3609, '铜鼓县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360981, 3609, '丰城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360982, 3609, '樟树市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (360983, 3609, '高安市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361002, 3610, '临川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361003, 3610, '东乡区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361021, 3610, '南城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361022, 3610, '黎川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361023, 3610, '南丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361024, 3610, '崇仁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361025, 3610, '乐安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361026, 3610, '宜黄县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361027, 3610, '金溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361028, 3610, '资溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361030, 3610, '广昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361102, 3611, '信州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:34');
INSERT INTO `sys_area` VALUES (361103, 3611, '广丰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361104, 3611, '广信区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361123, 3611, '玉山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361124, 3611, '铅山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361125, 3611, '横峰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361126, 3611, '弋阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361127, 3611, '余干县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361128, 3611, '鄱阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361129, 3611, '万年县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361130, 3611, '婺源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (361181, 3611, '德兴市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370102, 3701, '历下区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370103, 3701, '市中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370104, 3701, '槐荫区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370105, 3701, '天桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370112, 3701, '历城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370113, 3701, '长清区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370114, 3701, '章丘区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370115, 3701, '济阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370116, 3701, '莱芜区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370117, 3701, '钢城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370124, 3701, '平阴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370126, 3701, '商河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370171, 3701, '济南高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370202, 3702, '市南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370203, 3702, '市北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370211, 3702, '黄岛区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:35');
INSERT INTO `sys_area` VALUES (370212, 3702, '崂山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370213, 3702, '李沧区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370214, 3702, '城阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370215, 3702, '即墨区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370271, 3702, '青岛高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370281, 3702, '胶州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370283, 3702, '平度市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370285, 3702, '莱西市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370302, 3703, '淄川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370303, 3703, '张店区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370304, 3703, '博山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370305, 3703, '临淄区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370306, 3703, '周村区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370321, 3703, '桓台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370322, 3703, '高青县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370323, 3703, '沂源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370402, 3704, '市中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370403, 3704, '薛城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370404, 3704, '峄城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370405, 3704, '台儿庄区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370406, 3704, '山亭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370481, 3704, '滕州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370502, 3705, '东营区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370503, 3705, '河口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370505, 3705, '垦利区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370522, 3705, '利津县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:36');
INSERT INTO `sys_area` VALUES (370523, 3705, '广饶县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370571, 3705, '东营经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370572, 3705, '东营港经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370602, 3706, '芝罘区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370611, 3706, '福山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370612, 3706, '牟平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370613, 3706, '莱山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370614, 3706, '蓬莱区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370671, 3706, '烟台高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370672, 3706, '烟台经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370681, 3706, '龙口市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370682, 3706, '莱阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370683, 3706, '莱州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370685, 3706, '招远市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370686, 3706, '栖霞市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370687, 3706, '海阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370702, 3707, '潍城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370703, 3707, '寒亭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370704, 3707, '坊子区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370705, 3707, '奎文区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370724, 3707, '临朐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370725, 3707, '昌乐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370772, 3707, '潍坊滨海经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370781, 3707, '青州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370782, 3707, '诸城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370783, 3707, '寿光市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:37');
INSERT INTO `sys_area` VALUES (370784, 3707, '安丘市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370785, 3707, '高密市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370786, 3707, '昌邑市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370811, 3708, '任城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370812, 3708, '兖州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370826, 3708, '微山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370827, 3708, '鱼台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370828, 3708, '金乡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370829, 3708, '嘉祥县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370830, 3708, '汶上县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370831, 3708, '泗水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370832, 3708, '梁山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370871, 3708, '济宁高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370881, 3708, '曲阜市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370883, 3708, '邹城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370902, 3709, '泰山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370911, 3709, '岱岳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370921, 3709, '宁阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370923, 3709, '东平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370982, 3709, '新泰市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (370983, 3709, '肥城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (371002, 3710, '环翠区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (371003, 3710, '文登区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (371071, 3710, '威海火炬高技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (371072, 3710, '威海经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (371073, 3710, '威海临港经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:38');
INSERT INTO `sys_area` VALUES (371082, 3710, '荣成市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371083, 3710, '乳山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371102, 3711, '东港区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371103, 3711, '岚山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371121, 3711, '五莲县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371122, 3711, '莒县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371171, 3711, '日照经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371302, 3713, '兰山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371311, 3713, '罗庄区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371312, 3713, '河东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371321, 3713, '沂南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371322, 3713, '郯城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371323, 3713, '沂水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371324, 3713, '兰陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371325, 3713, '费县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371326, 3713, '平邑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371327, 3713, '莒南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371328, 3713, '蒙阴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371329, 3713, '临沭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371371, 3713, '临沂高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371402, 3714, '德城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371403, 3714, '陵城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371422, 3714, '宁津县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371423, 3714, '庆云县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371424, 3714, '临邑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371425, 3714, '齐河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371426, 3714, '平原县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:39');
INSERT INTO `sys_area` VALUES (371427, 3714, '夏津县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371428, 3714, '武城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371471, 3714, '德州经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371472, 3714, '德州运河经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371481, 3714, '乐陵市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371482, 3714, '禹城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371502, 3715, '东昌府区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371503, 3715, '茌平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371521, 3715, '阳谷县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371522, 3715, '莘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371524, 3715, '东阿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371525, 3715, '冠县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371526, 3715, '高唐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371581, 3715, '临清市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371602, 3716, '滨城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371603, 3716, '沾化区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371621, 3716, '惠民县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371622, 3716, '阳信县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371623, 3716, '无棣县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371625, 3716, '博兴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371681, 3716, '邹平市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371702, 3717, '牡丹区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371703, 3717, '定陶区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371721, 3717, '曹县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371722, 3717, '单县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371723, 3717, '成武县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:40');
INSERT INTO `sys_area` VALUES (371724, 3717, '巨野县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (371725, 3717, '郓城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (371726, 3717, '鄄城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (371728, 3717, '东明县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (371771, 3717, '菏泽经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (371772, 3717, '菏泽高新技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410102, 4101, '中原区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410103, 4101, '二七区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410104, 4101, '管城回族区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410105, 4101, '金水区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410106, 4101, '上街区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410108, 4101, '惠济区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410122, 4101, '中牟县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410171, 4101, '郑州经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410172, 4101, '郑州高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410173, 4101, '郑州航空港经济综合实验区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410181, 4101, '巩义市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410182, 4101, '荥阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410183, 4101, '新密市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410184, 4101, '新郑市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410185, 4101, '登封市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410202, 4102, '龙亭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410203, 4102, '顺河回族区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410204, 4102, '鼓楼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410205, 4102, '禹王台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:41');
INSERT INTO `sys_area` VALUES (410212, 4102, '祥符区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410221, 4102, '杞县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410222, 4102, '通许县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410223, 4102, '尉氏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410225, 4102, '兰考县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410302, 4103, '老城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410303, 4103, '西工区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410304, 4103, '瀍河回族区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410305, 4103, '涧西区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410306, 4103, '吉利区', NULL, NULL, '3', NULL, NULL, '2', '2', NULL, '', '2021-11-03 18:03:52', '', '2022-05-30 11:24:05');
INSERT INTO `sys_area` VALUES (410311, 4103, '洛龙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410322, 4103, '孟津区', '孟津区', NULL, '3', 1, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', 'weibaoting', '2022-05-30 11:25:19');
INSERT INTO `sys_area` VALUES (410323, 4103, '新安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410324, 4103, '栾川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410325, 4103, '嵩县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410326, 4103, '汝阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410327, 4103, '宜阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410328, 4103, '洛宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410329, 4103, '伊川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410371, 4103, '洛阳高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410381, 4103, '偃师区', '偃师区', NULL, '3', 1, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', 'weibaoting', '2022-05-30 11:26:15');
INSERT INTO `sys_area` VALUES (410402, 4104, '新华区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410403, 4104, '卫东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410404, 4104, '石龙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410411, 4104, '湛河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410421, 4104, '宝丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:42');
INSERT INTO `sys_area` VALUES (410422, 4104, '叶县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410423, 4104, '鲁山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410425, 4104, '郏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410471, 4104, '平顶山高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410472, 4104, '平顶山市城乡一体化示范区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410481, 4104, '舞钢市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410482, 4104, '汝州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410502, 4105, '文峰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410503, 4105, '北关区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410505, 4105, '殷都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410506, 4105, '龙安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410522, 4105, '安阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410523, 4105, '汤阴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410526, 4105, '滑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410527, 4105, '内黄县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410571, 4105, '安阳高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410581, 4105, '林州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410602, 4106, '鹤山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410603, 4106, '山城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410611, 4106, '淇滨区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410621, 4106, '浚县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410622, 4106, '淇县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410671, 4106, '鹤壁经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:43');
INSERT INTO `sys_area` VALUES (410702, 4107, '红旗区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410703, 4107, '卫滨区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410704, 4107, '凤泉区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410711, 4107, '牧野区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410721, 4107, '新乡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410724, 4107, '获嘉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410725, 4107, '原阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410726, 4107, '延津县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410727, 4107, '封丘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410771, 4107, '新乡高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410772, 4107, '新乡经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410773, 4107, '新乡市平原城乡一体化示范区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410781, 4107, '卫辉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410782, 4107, '辉县市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410783, 4107, '长垣市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410802, 4108, '解放区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410803, 4108, '中站区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410804, 4108, '马村区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410811, 4108, '山阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410821, 4108, '修武县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410822, 4108, '博爱县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410823, 4108, '武陟县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410825, 4108, '温县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410871, 4108, '焦作城乡一体化示范区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410882, 4108, '沁阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:44');
INSERT INTO `sys_area` VALUES (410883, 4108, '孟州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (410902, 4109, '华龙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (410922, 4109, '清丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (410923, 4109, '南乐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (410926, 4109, '范县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (410927, 4109, '台前县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (410928, 4109, '濮阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (410971, 4109, '河南濮阳工业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (410972, 4109, '濮阳经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411002, 4110, '魏都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411003, 4110, '建安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411024, 4110, '鄢陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411025, 4110, '襄城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411071, 4110, '许昌经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411081, 4110, '禹州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411082, 4110, '长葛市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411102, 4111, '源汇区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411103, 4111, '郾城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411104, 4111, '召陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411121, 4111, '舞阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411122, 4111, '临颍县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411171, 4111, '漯河经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411202, 4112, '湖滨区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411203, 4112, '陕州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411221, 4112, '渑池县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411224, 4112, '卢氏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:45');
INSERT INTO `sys_area` VALUES (411271, 4112, '河南三门峡经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411281, 4112, '义马市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411282, 4112, '灵宝市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411302, 4113, '宛城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411303, 4113, '卧龙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411321, 4113, '南召县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411322, 4113, '方城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411323, 4113, '西峡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411324, 4113, '镇平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411325, 4113, '内乡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411326, 4113, '淅川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411327, 4113, '社旗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411328, 4113, '唐河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411329, 4113, '新野县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411330, 4113, '桐柏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411371, 4113, '南阳高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411372, 4113, '南阳市城乡一体化示范区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411381, 4113, '邓州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411402, 4114, '梁园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411403, 4114, '睢阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411421, 4114, '民权县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:46');
INSERT INTO `sys_area` VALUES (411422, 4114, '睢县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411423, 4114, '宁陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411424, 4114, '柘城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411425, 4114, '虞城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411426, 4114, '夏邑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411471, 4114, '豫东综合物流产业聚集区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411472, 4114, '河南商丘经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411481, 4114, '永城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411502, 4115, '浉河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411503, 4115, '平桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411521, 4115, '罗山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411522, 4115, '光山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411523, 4115, '新县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411524, 4115, '商城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411525, 4115, '固始县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411526, 4115, '潢川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411527, 4115, '淮滨县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411528, 4115, '息县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411571, 4115, '信阳高新技术产业开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411602, 4116, '川汇区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411603, 4116, '淮阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411621, 4116, '扶沟县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:47');
INSERT INTO `sys_area` VALUES (411622, 4116, '西华县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411623, 4116, '商水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411624, 4116, '沈丘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411625, 4116, '郸城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411627, 4116, '太康县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411628, 4116, '鹿邑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411671, 4116, '河南周口经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411681, 4116, '项城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411702, 4117, '驿城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411721, 4117, '西平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411722, 4117, '上蔡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411723, 4117, '平舆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411724, 4117, '正阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411725, 4117, '确山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411726, 4117, '泌阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411727, 4117, '汝南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411728, 4117, '遂平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411729, 4117, '新蔡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (411771, 4117, '河南驻马店经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (419001, 4190, '济源市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (420102, 4201, '江岸区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (420103, 4201, '江汉区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (420104, 4201, '硚口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (420105, 4201, '汉阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:48');
INSERT INTO `sys_area` VALUES (420106, 4201, '武昌区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420107, 4201, '青山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420111, 4201, '洪山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420112, 4201, '东西湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420113, 4201, '汉南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420114, 4201, '蔡甸区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420115, 4201, '江夏区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420116, 4201, '黄陂区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420117, 4201, '新洲区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420202, 4202, '黄石港区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420203, 4202, '西塞山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420204, 4202, '下陆区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420205, 4202, '铁山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420222, 4202, '阳新县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420281, 4202, '大冶市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420302, 4203, '茅箭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420303, 4203, '张湾区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420304, 4203, '郧阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420322, 4203, '郧西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420323, 4203, '竹山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420324, 4203, '竹溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420325, 4203, '房县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420381, 4203, '丹江口市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:49');
INSERT INTO `sys_area` VALUES (420502, 4205, '西陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420503, 4205, '伍家岗区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420504, 4205, '点军区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420505, 4205, '猇亭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420506, 4205, '夷陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420525, 4205, '远安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420526, 4205, '兴山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420527, 4205, '秭归县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420528, 4205, '长阳土家族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420529, 4205, '五峰土家族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420581, 4205, '宜都市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420582, 4205, '当阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420583, 4205, '枝江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420602, 4206, '襄城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420606, 4206, '樊城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420607, 4206, '襄州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420624, 4206, '南漳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420625, 4206, '谷城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420626, 4206, '保康县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420682, 4206, '老河口市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420683, 4206, '枣阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420684, 4206, '宜城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:50');
INSERT INTO `sys_area` VALUES (420702, 4207, '梁子湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420703, 4207, '华容区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420704, 4207, '鄂城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420802, 4208, '东宝区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420804, 4208, '掇刀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420822, 4208, '沙洋县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420881, 4208, '钟祥市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420882, 4208, '京山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420902, 4209, '孝南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420921, 4209, '孝昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420922, 4209, '大悟县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420923, 4209, '云梦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420981, 4209, '应城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420982, 4209, '安陆市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (420984, 4209, '汉川市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (421002, 4210, '沙市区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (421003, 4210, '荆州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (421022, 4210, '公安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (421023, 4210, '监利县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (421024, 4210, '江陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (421071, 4210, '荆州经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (421081, 4210, '石首市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:51');
INSERT INTO `sys_area` VALUES (421083, 4210, '洪湖市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421087, 4210, '松滋市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421102, 4211, '黄州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421121, 4211, '团风县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421122, 4211, '红安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421123, 4211, '罗田县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421124, 4211, '英山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421125, 4211, '浠水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421126, 4211, '蕲春县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421127, 4211, '黄梅县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421171, 4211, '龙感湖管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421181, 4211, '麻城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421182, 4211, '武穴市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421202, 4212, '咸安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421221, 4212, '嘉鱼县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421222, 4212, '通城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421223, 4212, '崇阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421224, 4212, '通山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421281, 4212, '赤壁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421303, 4213, '曾都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421321, 4213, '随县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (421381, 4213, '广水市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (422801, 4228, '恩施市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (422802, 4228, '利川市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (422822, 4228, '建始县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (422823, 4228, '巴东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:52');
INSERT INTO `sys_area` VALUES (422825, 4228, '宣恩县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (422826, 4228, '咸丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (422827, 4228, '来凤县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (422828, 4228, '鹤峰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (429004, 4290, '仙桃市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (429005, 4290, '潜江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (429006, 4290, '天门市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (429021, 4290, '神农架林区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430102, 4301, '芙蓉区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430103, 4301, '天心区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430104, 4301, '岳麓区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430105, 4301, '开福区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430111, 4301, '雨花区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430112, 4301, '望城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430121, 4301, '长沙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430181, 4301, '浏阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430182, 4301, '宁乡市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430202, 4302, '荷塘区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430203, 4302, '芦淞区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430204, 4302, '石峰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430211, 4302, '天元区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430212, 4302, '渌口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:53');
INSERT INTO `sys_area` VALUES (430223, 4302, '攸县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430224, 4302, '茶陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430225, 4302, '炎陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430271, 4302, '云龙示范区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430281, 4302, '醴陵市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430302, 4303, '雨湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430304, 4303, '岳塘区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430321, 4303, '湘潭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430371, 4303, '湖南湘潭高新技术产业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430372, 4303, '湘潭昭山示范区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430373, 4303, '湘潭九华示范区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430381, 4303, '湘乡市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430382, 4303, '韶山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430405, 4304, '珠晖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430406, 4304, '雁峰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430407, 4304, '石鼓区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430408, 4304, '蒸湘区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:54');
INSERT INTO `sys_area` VALUES (430412, 4304, '南岳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430421, 4304, '衡阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430422, 4304, '衡南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430423, 4304, '衡山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430424, 4304, '衡东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430426, 4304, '祁东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430471, 4304, '衡阳综合保税区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430472, 4304, '湖南衡阳高新技术产业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430473, 4304, '湖南衡阳松木经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430481, 4304, '耒阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430482, 4304, '常宁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430502, 4305, '双清区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430503, 4305, '大祥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430511, 4305, '北塔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430522, 4305, '新邵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430523, 4305, '邵阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430524, 4305, '隆回县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430525, 4305, '洞口县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430527, 4305, '绥宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430528, 4305, '新宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430529, 4305, '城步苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430581, 4305, '武冈市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430582, 4305, '邵东市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430602, 4306, '岳阳楼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430603, 4306, '云溪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:55');
INSERT INTO `sys_area` VALUES (430611, 4306, '君山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430621, 4306, '岳阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430623, 4306, '华容县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430624, 4306, '湘阴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430626, 4306, '平江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430671, 4306, '岳阳市屈原管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430681, 4306, '汨罗市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430682, 4306, '临湘市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430702, 4307, '武陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430703, 4307, '鼎城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430721, 4307, '安乡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430722, 4307, '汉寿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430723, 4307, '澧县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430724, 4307, '临澧县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430725, 4307, '桃源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430726, 4307, '石门县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430771, 4307, '常德市西洞庭管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430781, 4307, '津市市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430802, 4308, '永定区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430811, 4308, '武陵源区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430821, 4308, '慈利县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430822, 4308, '桑植县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430902, 4309, '资阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430903, 4309, '赫山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430921, 4309, '南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:56');
INSERT INTO `sys_area` VALUES (430922, 4309, '桃江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (430923, 4309, '安化县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (430971, 4309, '益阳市大通湖管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (430972, 4309, '湖南益阳高新技术产业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (430981, 4309, '沅江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431002, 4310, '北湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431003, 4310, '苏仙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431021, 4310, '桂阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431022, 4310, '宜章县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431023, 4310, '永兴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431024, 4310, '嘉禾县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431025, 4310, '临武县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431026, 4310, '汝城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431027, 4310, '桂东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431028, 4310, '安仁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431081, 4310, '资兴市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431102, 4311, '零陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431103, 4311, '冷水滩区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431121, 4311, '祁阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431122, 4311, '东安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431123, 4311, '双牌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431124, 4311, '道县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431125, 4311, '江永县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431126, 4311, '宁远县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431127, 4311, '蓝山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:57');
INSERT INTO `sys_area` VALUES (431128, 4311, '新田县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431129, 4311, '江华瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431171, 4311, '永州经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431172, 4311, '永州市金洞管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431173, 4311, '永州市回龙圩管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431202, 4312, '鹤城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431221, 4312, '中方县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431222, 4312, '沅陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431223, 4312, '辰溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431224, 4312, '溆浦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431225, 4312, '会同县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431226, 4312, '麻阳苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431227, 4312, '新晃侗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431228, 4312, '芷江侗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431229, 4312, '靖州苗族侗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431230, 4312, '通道侗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431271, 4312, '怀化市洪江管理区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431281, 4312, '洪江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431302, 4313, '娄星区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431321, 4313, '双峰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431322, 4313, '新化县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431381, 4313, '冷水江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (431382, 4313, '涟源市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (433101, 4331, '吉首市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (433122, 4331, '泸溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (433123, 4331, '凤凰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:58');
INSERT INTO `sys_area` VALUES (433124, 4331, '花垣县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (433125, 4331, '保靖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (433126, 4331, '古丈县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (433127, 4331, '永顺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (433130, 4331, '龙山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440103, 4401, '荔湾区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440104, 4401, '越秀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440105, 4401, '海珠区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440106, 4401, '天河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440111, 4401, '白云区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440112, 4401, '黄埔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440113, 4401, '番禺区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440114, 4401, '花都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440115, 4401, '南沙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440117, 4401, '从化区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440118, 4401, '增城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440203, 4402, '武江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440204, 4402, '浈江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440205, 4402, '曲江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440222, 4402, '始兴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440224, 4402, '仁化县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:13:59');
INSERT INTO `sys_area` VALUES (440229, 4402, '翁源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440232, 4402, '乳源瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440233, 4402, '新丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440281, 4402, '乐昌市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440282, 4402, '南雄市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440303, 4403, '罗湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440304, 4403, '福田区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440305, 4403, '南山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440306, 4403, '宝安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440307, 4403, '龙岗区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440308, 4403, '盐田区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440309, 4403, '龙华区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440310, 4403, '坪山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440311, 4403, '光明区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440402, 4404, '香洲区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440403, 4404, '斗门区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440404, 4404, '金湾区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440507, 4405, '龙湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440511, 4405, '金平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440512, 4405, '濠江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440513, 4405, '潮阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440514, 4405, '潮南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440515, 4405, '澄海区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440523, 4405, '南澳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440604, 4406, '禅城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:00');
INSERT INTO `sys_area` VALUES (440605, 4406, '南海区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440606, 4406, '顺德区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440607, 4406, '三水区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440608, 4406, '高明区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440703, 4407, '蓬江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440704, 4407, '江海区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440705, 4407, '新会区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440781, 4407, '台山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440783, 4407, '开平市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440784, 4407, '鹤山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440785, 4407, '恩平市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440802, 4408, '赤坎区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440803, 4408, '霞山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440804, 4408, '坡头区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440811, 4408, '麻章区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440823, 4408, '遂溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440825, 4408, '徐闻县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440881, 4408, '廉江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440882, 4408, '雷州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440883, 4408, '吴川市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440902, 4409, '茂南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440904, 4409, '电白区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440981, 4409, '高州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440982, 4409, '化州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:01');
INSERT INTO `sys_area` VALUES (440983, 4409, '信宜市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441202, 4412, '端州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441203, 4412, '鼎湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441204, 4412, '高要区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441223, 4412, '广宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441224, 4412, '怀集县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441225, 4412, '封开县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441226, 4412, '德庆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441284, 4412, '四会市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441302, 4413, '惠城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441303, 4413, '惠阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441322, 4413, '博罗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441323, 4413, '惠东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441324, 4413, '龙门县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441402, 4414, '梅江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441403, 4414, '梅县区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441422, 4414, '大埔县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441423, 4414, '丰顺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441424, 4414, '五华县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441426, 4414, '平远县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441427, 4414, '蕉岭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441481, 4414, '兴宁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:02');
INSERT INTO `sys_area` VALUES (441502, 4415, '城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441521, 4415, '海丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441523, 4415, '陆河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441581, 4415, '陆丰市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441602, 4416, '源城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441621, 4416, '紫金县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441622, 4416, '龙川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441623, 4416, '连平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441624, 4416, '和平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441625, 4416, '东源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441702, 4417, '江城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441704, 4417, '阳东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441721, 4417, '阳西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441781, 4417, '阳春市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441802, 4418, '清城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441803, 4418, '清新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441821, 4418, '佛冈县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441823, 4418, '阳山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441825, 4418, '连山壮族瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441826, 4418, '连南瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441881, 4418, '英德市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441882, 4418, '连州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (441900, 4419, '东莞市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (442000, 4420, '中山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (445102, 4451, '湘桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:03');
INSERT INTO `sys_area` VALUES (445103, 4451, '潮安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445122, 4451, '饶平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445202, 4452, '榕城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445203, 4452, '揭东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445222, 4452, '揭西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445224, 4452, '惠来县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445281, 4452, '普宁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445302, 4453, '云城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445303, 4453, '云安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445321, 4453, '新兴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445322, 4453, '郁南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (445381, 4453, '罗定市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450102, 4501, '兴宁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450103, 4501, '青秀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450105, 4501, '江南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450107, 4501, '西乡塘区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450108, 4501, '良庆区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450109, 4501, '邕宁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450110, 4501, '武鸣区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450123, 4501, '隆安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450124, 4501, '马山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450125, 4501, '上林县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450126, 4501, '宾阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450127, 4501, '横县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:04');
INSERT INTO `sys_area` VALUES (450202, 4502, '城中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450203, 4502, '鱼峰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450204, 4502, '柳南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450205, 4502, '柳北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450206, 4502, '柳江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450222, 4502, '柳城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450223, 4502, '鹿寨县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450224, 4502, '融安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450225, 4502, '融水苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450226, 4502, '三江侗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450302, 4503, '秀峰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450303, 4503, '叠彩区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450304, 4503, '象山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450305, 4503, '七星区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450311, 4503, '雁山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450312, 4503, '临桂区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450321, 4503, '阳朔县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450323, 4503, '灵川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450324, 4503, '全州县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450325, 4503, '兴安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:05');
INSERT INTO `sys_area` VALUES (450326, 4503, '永福县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:26');
INSERT INTO `sys_area` VALUES (450327, 4503, '灌阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:26');
INSERT INTO `sys_area` VALUES (450328, 4503, '龙胜各族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:26');
INSERT INTO `sys_area` VALUES (450329, 4503, '资源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:26');
INSERT INTO `sys_area` VALUES (450330, 4503, '平乐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:26');
INSERT INTO `sys_area` VALUES (450332, 4503, '恭城瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:26');
INSERT INTO `sys_area` VALUES (450381, 4503, '荔浦市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:26');
INSERT INTO `sys_area` VALUES (450403, 4504, '万秀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:26');
INSERT INTO `sys_area` VALUES (450405, 4504, '长洲区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:26');
INSERT INTO `sys_area` VALUES (450406, 4504, '龙圩区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450421, 4504, '苍梧县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450422, 4504, '藤县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450423, 4504, '蒙山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450481, 4504, '岑溪市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450502, 4505, '海城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450503, 4505, '银海区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450512, 4505, '铁山港区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450521, 4505, '合浦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450602, 4506, '港口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450603, 4506, '防城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450621, 4506, '上思县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450681, 4506, '东兴市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450702, 4507, '钦南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450703, 4507, '钦北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450721, 4507, '灵山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450722, 4507, '浦北县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450802, 4508, '港北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450803, 4508, '港南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450804, 4508, '覃塘区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450821, 4508, '平南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450881, 4508, '桂平市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450902, 4509, '玉州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450903, 4509, '福绵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:27');
INSERT INTO `sys_area` VALUES (450921, 4509, '容县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (450922, 4509, '陆川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (450923, 4509, '博白县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (450924, 4509, '兴业县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (450981, 4509, '北流市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451002, 4510, '右江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451003, 4510, '田阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451022, 4510, '田东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451024, 4510, '德保县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451026, 4510, '那坡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451027, 4510, '凌云县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451028, 4510, '乐业县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451029, 4510, '田林县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451030, 4510, '西林县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451031, 4510, '隆林各族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451081, 4510, '靖西市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451082, 4510, '平果市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451102, 4511, '八步区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:28');
INSERT INTO `sys_area` VALUES (451103, 4511, '平桂区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451121, 4511, '昭平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451122, 4511, '钟山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451123, 4511, '富川瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451202, 4512, '金城江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451203, 4512, '宜州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451221, 4512, '南丹县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451222, 4512, '天峨县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451223, 4512, '凤山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451224, 4512, '东兰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451225, 4512, '罗城仫佬族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451226, 4512, '环江毛南族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451227, 4512, '巴马瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451228, 4512, '都安瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451229, 4512, '大化瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451302, 4513, '兴宾区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451321, 4513, '忻城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451322, 4513, '象州县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451323, 4513, '武宣县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451324, 4513, '金秀瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451381, 4513, '合山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451402, 4514, '江州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451421, 4514, '扶绥县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:29');
INSERT INTO `sys_area` VALUES (451422, 4514, '宁明县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (451423, 4514, '龙州县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (451424, 4514, '大新县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (451425, 4514, '天等县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (451481, 4514, '凭祥市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460105, 4601, '秀英区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460106, 4601, '龙华区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460107, 4601, '琼山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460108, 4601, '美兰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460202, 4602, '海棠区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460203, 4602, '吉阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460204, 4602, '天涯区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460205, 4602, '崖州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460321, 4603, '西沙群岛', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460322, 4603, '南沙群岛', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460323, 4603, '中沙群岛的岛礁及其海域', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (460400, 4604, '儋州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (469001, 4690, '五指山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (469002, 4690, '琼海市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (469005, 4690, '文昌市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (469006, 4690, '万宁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (469007, 4690, '东方市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (469021, 4690, '定安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (469022, 4690, '屯昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (469023, 4690, '澄迈县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:30');
INSERT INTO `sys_area` VALUES (469024, 4690, '临高县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (469025, 4690, '白沙黎族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (469026, 4690, '昌江黎族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (469027, 4690, '乐东黎族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (469028, 4690, '陵水黎族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (469029, 4690, '保亭黎族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (469030, 4690, '琼中黎族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500101, 5001, '万州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500102, 5001, '涪陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500103, 5001, '渝中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500104, 5001, '大渡口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500105, 5001, '江北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500106, 5001, '沙坪坝区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500107, 5001, '九龙坡区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500108, 5001, '南岸区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500109, 5001, '北碚区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500110, 5001, '綦江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500111, 5001, '大足区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500112, 5001, '渝北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500113, 5001, '巴南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500114, 5001, '黔江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500115, 5001, '长寿区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500116, 5001, '江津区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500117, 5001, '合川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500118, 5001, '永川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500119, 5001, '南川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:31');
INSERT INTO `sys_area` VALUES (500120, 5001, '璧山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500151, 5001, '铜梁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500152, 5001, '潼南区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500153, 5001, '荣昌区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500154, 5001, '开州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500155, 5001, '梁平区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500156, 5001, '武隆区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500229, 5002, '城口县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500230, 5002, '丰都县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500231, 5002, '垫江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500233, 5002, '忠县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500235, 5002, '云阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500236, 5002, '奉节县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500237, 5002, '巫山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500238, 5002, '巫溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500240, 5002, '石柱土家族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500241, 5002, '秀山土家族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500242, 5002, '酉阳土家族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (500243, 5002, '彭水苗族土家族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (510104, 5101, '锦江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (510105, 5101, '青羊区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (510106, 5101, '金牛区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (510107, 5101, '武侯区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (510108, 5101, '成华区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (510112, 5101, '龙泉驿区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (510113, 5101, '青白江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (510114, 5101, '新都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:32');
INSERT INTO `sys_area` VALUES (510115, 5101, '温江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510116, 5101, '双流区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510117, 5101, '郫都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510118, 5101, '新津区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510121, 5101, '金堂县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510129, 5101, '大邑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510131, 5101, '蒲江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510181, 5101, '都江堰市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510182, 5101, '彭州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510183, 5101, '邛崃市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510184, 5101, '崇州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510185, 5101, '简阳市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510302, 5103, '自流井区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510303, 5103, '贡井区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510304, 5103, '大安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510311, 5103, '沿滩区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510321, 5103, '荣县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510322, 5103, '富顺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510402, 5104, '东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510403, 5104, '西区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510411, 5104, '仁和区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510421, 5104, '米易县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510422, 5104, '盐边县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510502, 5105, '江阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510503, 5105, '纳溪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510504, 5105, '龙马潭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510521, 5105, '泸县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:33');
INSERT INTO `sys_area` VALUES (510522, 5105, '合江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510524, 5105, '叙永县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510525, 5105, '古蔺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510603, 5106, '旌阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510604, 5106, '罗江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510623, 5106, '中江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510681, 5106, '广汉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510682, 5106, '什邡市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510683, 5106, '绵竹市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510703, 5107, '涪城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510704, 5107, '游仙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510705, 5107, '安州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510722, 5107, '三台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510723, 5107, '盐亭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510725, 5107, '梓潼县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510726, 5107, '北川羌族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510727, 5107, '平武县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510781, 5107, '江油市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510802, 5108, '利州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510811, 5108, '昭化区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510812, 5108, '朝天区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510821, 5108, '旺苍县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510822, 5108, '青川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510823, 5108, '剑阁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510824, 5108, '苍溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510903, 5109, '船山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510904, 5109, '安居区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:34');
INSERT INTO `sys_area` VALUES (510921, 5109, '蓬溪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (510923, 5109, '大英县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (510981, 5109, '射洪市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511002, 5110, '市中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511011, 5110, '东兴区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511024, 5110, '威远县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511025, 5110, '资中县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511071, 5110, '内江经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511083, 5110, '隆昌市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511102, 5111, '市中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511111, 5111, '沙湾区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511112, 5111, '五通桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511113, 5111, '金口河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511123, 5111, '犍为县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511124, 5111, '井研县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511126, 5111, '夹江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511129, 5111, '沐川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511132, 5111, '峨边彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511133, 5111, '马边彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511181, 5111, '峨眉山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511302, 5113, '顺庆区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511303, 5113, '高坪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511304, 5113, '嘉陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511321, 5113, '南部县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511322, 5113, '营山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511323, 5113, '蓬安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:35');
INSERT INTO `sys_area` VALUES (511324, 5113, '仪陇县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511325, 5113, '西充县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511381, 5113, '阆中市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511402, 5114, '东坡区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511403, 5114, '彭山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511421, 5114, '仁寿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511423, 5114, '洪雅县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511424, 5114, '丹棱县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511425, 5114, '青神县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511502, 5115, '翠屏区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511503, 5115, '南溪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511504, 5115, '叙州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511523, 5115, '江安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511524, 5115, '长宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511525, 5115, '高县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511526, 5115, '珙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511527, 5115, '筠连县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511528, 5115, '兴文县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511529, 5115, '屏山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511602, 5116, '广安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511603, 5116, '前锋区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511621, 5116, '岳池县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511622, 5116, '武胜县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511623, 5116, '邻水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511681, 5116, '华蓥市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511702, 5117, '通川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:36');
INSERT INTO `sys_area` VALUES (511703, 5117, '达川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511722, 5117, '宣汉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511723, 5117, '开江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511724, 5117, '大竹县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511725, 5117, '渠县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511771, 5117, '达州经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511781, 5117, '万源市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511802, 5118, '雨城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511803, 5118, '名山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511822, 5118, '荥经县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511823, 5118, '汉源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511824, 5118, '石棉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511825, 5118, '天全县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511826, 5118, '芦山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511827, 5118, '宝兴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511902, 5119, '巴州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511903, 5119, '恩阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511921, 5119, '通江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511922, 5119, '南江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511923, 5119, '平昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (511971, 5119, '巴中经济开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (512002, 5120, '雁江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (512021, 5120, '安岳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (512022, 5120, '乐至县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (513201, 5132, '马尔康市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (513221, 5132, '汶川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:37');
INSERT INTO `sys_area` VALUES (513222, 5132, '理县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513223, 5132, '茂县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513224, 5132, '松潘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513225, 5132, '九寨沟县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513226, 5132, '金川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513227, 5132, '小金县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513228, 5132, '黑水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513230, 5132, '壤塘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513231, 5132, '阿坝县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513232, 5132, '若尔盖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513233, 5132, '红原县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513301, 5133, '康定市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513322, 5133, '泸定县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513323, 5133, '丹巴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513324, 5133, '九龙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513325, 5133, '雅江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513326, 5133, '道孚县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513327, 5133, '炉霍县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513328, 5133, '甘孜县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513329, 5133, '新龙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513330, 5133, '德格县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513331, 5133, '白玉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513332, 5133, '石渠县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513333, 5133, '色达县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:38');
INSERT INTO `sys_area` VALUES (513334, 5133, '理塘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513335, 5133, '巴塘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513336, 5133, '乡城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513337, 5133, '稻城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513338, 5133, '得荣县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513401, 5134, '西昌市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513422, 5134, '木里藏族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513423, 5134, '盐源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513424, 5134, '德昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513425, 5134, '会理县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513426, 5134, '会东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513427, 5134, '宁南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513428, 5134, '普格县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513429, 5134, '布拖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513430, 5134, '金阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513431, 5134, '昭觉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513432, 5134, '喜德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513433, 5134, '冕宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513434, 5134, '越西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513435, 5134, '甘洛县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513436, 5134, '美姑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (513437, 5134, '雷波县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (520102, 5201, '南明区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (520103, 5201, '云岩区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (520111, 5201, '花溪区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (520112, 5201, '乌当区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (520113, 5201, '白云区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:39');
INSERT INTO `sys_area` VALUES (520115, 5201, '观山湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520121, 5201, '开阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520122, 5201, '息烽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520123, 5201, '修文县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520181, 5201, '清镇市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520201, 5202, '钟山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520203, 5202, '六枝特区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520221, 5202, '水城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520281, 5202, '盘州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520302, 5203, '红花岗区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520303, 5203, '汇川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520304, 5203, '播州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520322, 5203, '桐梓县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520323, 5203, '绥阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520324, 5203, '正安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520325, 5203, '道真仡佬族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520326, 5203, '务川仡佬族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520327, 5203, '凤冈县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520328, 5203, '湄潭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520329, 5203, '余庆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520330, 5203, '习水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520381, 5203, '赤水市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520382, 5203, '仁怀市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520402, 5204, '西秀区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520403, 5204, '平坝区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520422, 5204, '普定县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:40');
INSERT INTO `sys_area` VALUES (520423, 5204, '镇宁布依族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520424, 5204, '关岭布依族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520425, 5204, '紫云苗族布依族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520502, 5205, '七星关区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520521, 5205, '大方县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520522, 5205, '黔西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520523, 5205, '金沙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520524, 5205, '织金县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520525, 5205, '纳雍县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520526, 5205, '威宁彝族回族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520527, 5205, '赫章县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520602, 5206, '碧江区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520603, 5206, '万山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520621, 5206, '江口县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520622, 5206, '玉屏侗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520623, 5206, '石阡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520624, 5206, '思南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520625, 5206, '印江土家族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520626, 5206, '德江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520627, 5206, '沿河土家族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (520628, 5206, '松桃苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (522301, 5223, '兴义市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (522302, 5223, '兴仁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (522323, 5223, '普安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (522324, 5223, '晴隆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (522325, 5223, '贞丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:41');
INSERT INTO `sys_area` VALUES (522326, 5223, '望谟县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522327, 5223, '册亨县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522328, 5223, '安龙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522601, 5226, '凯里市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522622, 5226, '黄平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522623, 5226, '施秉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522624, 5226, '三穗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522625, 5226, '镇远县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522626, 5226, '岑巩县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522627, 5226, '天柱县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522628, 5226, '锦屏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522629, 5226, '剑河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522630, 5226, '台江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522631, 5226, '黎平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522632, 5226, '榕江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522633, 5226, '从江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522634, 5226, '雷山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522635, 5226, '麻江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522636, 5226, '丹寨县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522701, 5227, '都匀市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522702, 5227, '福泉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522722, 5227, '荔波县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522723, 5227, '贵定县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:42');
INSERT INTO `sys_area` VALUES (522725, 5227, '瓮安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (522726, 5227, '独山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (522727, 5227, '平塘县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (522728, 5227, '罗甸县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (522729, 5227, '长顺县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (522730, 5227, '龙里县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (522731, 5227, '惠水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (522732, 5227, '三都水族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530102, 5301, '五华区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530103, 5301, '盘龙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530111, 5301, '官渡区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530112, 5301, '西山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530113, 5301, '东川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530114, 5301, '呈贡区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530115, 5301, '晋宁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530124, 5301, '富民县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530125, 5301, '宜良县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530126, 5301, '石林彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530127, 5301, '嵩明县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530128, 5301, '禄劝彝族苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530129, 5301, '寻甸回族彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530181, 5301, '安宁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530302, 5303, '麒麟区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530303, 5303, '沾益区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530304, 5303, '马龙区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530322, 5303, '陆良县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:43');
INSERT INTO `sys_area` VALUES (530323, 5303, '师宗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530324, 5303, '罗平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530325, 5303, '富源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530326, 5303, '会泽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530381, 5303, '宣威市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530402, 5304, '红塔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530403, 5304, '江川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530423, 5304, '通海县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530424, 5304, '华宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530425, 5304, '易门县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530426, 5304, '峨山彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530427, 5304, '新平彝族傣族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530428, 5304, '元江哈尼族彝族傣族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530481, 5304, '澄江市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530502, 5305, '隆阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530521, 5305, '施甸县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530523, 5305, '龙陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530524, 5305, '昌宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530581, 5305, '腾冲市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530602, 5306, '昭阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530621, 5306, '鲁甸县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530622, 5306, '巧家县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530623, 5306, '盐津县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530624, 5306, '大关县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530625, 5306, '永善县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530626, 5306, '绥江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:44');
INSERT INTO `sys_area` VALUES (530627, 5306, '镇雄县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530628, 5306, '彝良县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530629, 5306, '威信县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530681, 5306, '水富市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530702, 5307, '古城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530721, 5307, '玉龙纳西族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530722, 5307, '永胜县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530723, 5307, '华坪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530724, 5307, '宁蒗彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530802, 5308, '思茅区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530821, 5308, '宁洱哈尼族彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530822, 5308, '墨江哈尼族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530823, 5308, '景东彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530824, 5308, '景谷傣族彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530825, 5308, '镇沅彝族哈尼族拉祜族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530826, 5308, '江城哈尼族彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530827, 5308, '孟连傣族拉祜族佤族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530828, 5308, '澜沧拉祜族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530829, 5308, '西盟佤族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530902, 5309, '临翔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530921, 5309, '凤庆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530922, 5309, '云县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530923, 5309, '永德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530924, 5309, '镇康县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530925, 5309, '双江拉祜族佤族布朗族傣族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530926, 5309, '耿马傣族佤族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:45');
INSERT INTO `sys_area` VALUES (530927, 5309, '沧源佤族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532301, 5323, '楚雄市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532322, 5323, '双柏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532323, 5323, '牟定县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532324, 5323, '南华县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532325, 5323, '姚安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532326, 5323, '大姚县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532327, 5323, '永仁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532328, 5323, '元谋县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532329, 5323, '武定县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532331, 5323, '禄丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532501, 5325, '个旧市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532502, 5325, '开远市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532503, 5325, '蒙自市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532504, 5325, '弥勒市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532523, 5325, '屏边苗族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532524, 5325, '建水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532525, 5325, '石屏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532527, 5325, '泸西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532528, 5325, '元阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532529, 5325, '红河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532530, 5325, '金平苗族瑶族傣族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:46');
INSERT INTO `sys_area` VALUES (532531, 5325, '绿春县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532532, 5325, '河口瑶族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532601, 5326, '文山市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532622, 5326, '砚山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532623, 5326, '西畴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532624, 5326, '麻栗坡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532625, 5326, '马关县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532626, 5326, '丘北县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532627, 5326, '广南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532628, 5326, '富宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532801, 5328, '景洪市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532822, 5328, '勐海县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532823, 5328, '勐腊县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532901, 5329, '大理市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532922, 5329, '漾濞彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532923, 5329, '祥云县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532924, 5329, '宾川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532925, 5329, '弥渡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532926, 5329, '南涧彝族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532927, 5329, '巍山彝族回族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532928, 5329, '永平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532929, 5329, '云龙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:47');
INSERT INTO `sys_area` VALUES (532930, 5329, '洱源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (532931, 5329, '剑川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (532932, 5329, '鹤庆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533102, 5331, '瑞丽市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533103, 5331, '芒市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533122, 5331, '梁河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533123, 5331, '盈江县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533124, 5331, '陇川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533301, 5333, '泸水市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533323, 5333, '福贡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533324, 5333, '贡山独龙族怒族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533325, 5333, '兰坪白族普米族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533401, 5334, '香格里拉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533422, 5334, '德钦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (533423, 5334, '维西傈僳族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540102, 5401, '城关区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540103, 5401, '堆龙德庆区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540104, 5401, '达孜区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540121, 5401, '林周县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540122, 5401, '当雄县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540123, 5401, '尼木县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540124, 5401, '曲水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540127, 5401, '墨竹工卡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540171, 5401, '格尔木藏青工业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540172, 5401, '拉萨经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:48');
INSERT INTO `sys_area` VALUES (540173, 5401, '西藏文化旅游创意园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540174, 5401, '达孜工业园区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540202, 5402, '桑珠孜区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540221, 5402, '南木林县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540222, 5402, '江孜县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540223, 5402, '定日县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540224, 5402, '萨迦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540225, 5402, '拉孜县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540226, 5402, '昂仁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540227, 5402, '谢通门县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540228, 5402, '白朗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540229, 5402, '仁布县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540230, 5402, '康马县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540231, 5402, '定结县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540232, 5402, '仲巴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540233, 5402, '亚东县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540234, 5402, '吉隆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540235, 5402, '聂拉木县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540236, 5402, '萨嘎县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540237, 5402, '岗巴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540302, 5403, '卡若区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540321, 5403, '江达县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540322, 5403, '贡觉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:49');
INSERT INTO `sys_area` VALUES (540323, 5403, '类乌齐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540324, 5403, '丁青县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540325, 5403, '察雅县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540326, 5403, '八宿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540327, 5403, '左贡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540328, 5403, '芒康县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540329, 5403, '洛隆县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540330, 5403, '边坝县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540402, 5404, '巴宜区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540421, 5404, '工布江达县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540422, 5404, '米林县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540423, 5404, '墨脱县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540424, 5404, '波密县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540425, 5404, '察隅县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540426, 5404, '朗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540502, 5405, '乃东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540521, 5405, '扎囊县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540522, 5405, '贡嘎县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540523, 5405, '桑日县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540524, 5405, '琼结县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540525, 5405, '曲松县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540526, 5405, '措美县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540527, 5405, '洛扎县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540528, 5405, '加查县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540529, 5405, '隆子县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540530, 5405, '错那县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:50');
INSERT INTO `sys_area` VALUES (540531, 5405, '浪卡子县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540602, 5406, '色尼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540621, 5406, '嘉黎县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540622, 5406, '比如县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540623, 5406, '聂荣县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540624, 5406, '安多县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540625, 5406, '申扎县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540626, 5406, '索县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540627, 5406, '班戈县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540628, 5406, '巴青县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540629, 5406, '尼玛县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (540630, 5406, '双湖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (542521, 5425, '普兰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (542522, 5425, '札达县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (542523, 5425, '噶尔县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (542524, 5425, '日土县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (542525, 5425, '革吉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (542526, 5425, '改则县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (542527, 5425, '措勤县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (610102, 6101, '新城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (610103, 6101, '碑林区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (610104, 6101, '莲湖区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (610111, 6101, '灞桥区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (610112, 6101, '未央区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (610113, 6101, '雁塔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:51');
INSERT INTO `sys_area` VALUES (610114, 6101, '阎良区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610115, 6101, '临潼区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610116, 6101, '长安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610117, 6101, '高陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610118, 6101, '鄠邑区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610122, 6101, '蓝田县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610124, 6101, '周至县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610202, 6102, '王益区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610203, 6102, '印台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610204, 6102, '耀州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610222, 6102, '宜君县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610302, 6103, '渭滨区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610303, 6103, '金台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610304, 6103, '陈仓区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610322, 6103, '凤翔县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610323, 6103, '岐山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610324, 6103, '扶风县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610326, 6103, '眉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610327, 6103, '陇县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610328, 6103, '千阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610329, 6103, '麟游县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610330, 6103, '凤县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610331, 6103, '太白县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:52');
INSERT INTO `sys_area` VALUES (610402, 6104, '秦都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610403, 6104, '杨陵区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610404, 6104, '渭城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610422, 6104, '三原县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610423, 6104, '泾阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610424, 6104, '乾县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610425, 6104, '礼泉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610426, 6104, '永寿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610428, 6104, '长武县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610429, 6104, '旬邑县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610430, 6104, '淳化县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610431, 6104, '武功县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610481, 6104, '兴平市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610482, 6104, '彬州市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610502, 6105, '临渭区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610503, 6105, '华州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610522, 6105, '潼关县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610523, 6105, '大荔县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610524, 6105, '合阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610525, 6105, '澄城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610526, 6105, '蒲城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610527, 6105, '白水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610528, 6105, '富平县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610581, 6105, '韩城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610582, 6105, '华阴市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610602, 6106, '宝塔区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610603, 6106, '安塞区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610621, 6106, '延长县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:53');
INSERT INTO `sys_area` VALUES (610622, 6106, '延川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610625, 6106, '志丹县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610626, 6106, '吴起县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610627, 6106, '甘泉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610628, 6106, '富县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610629, 6106, '洛川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610630, 6106, '宜川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610631, 6106, '黄龙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610632, 6106, '黄陵县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610681, 6106, '子长市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610702, 6107, '汉台区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610703, 6107, '南郑区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610722, 6107, '城固县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610723, 6107, '洋县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610724, 6107, '西乡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610725, 6107, '勉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610726, 6107, '宁强县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610727, 6107, '略阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610728, 6107, '镇巴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:54');
INSERT INTO `sys_area` VALUES (610729, 6107, '留坝县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610730, 6107, '佛坪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610802, 6108, '榆阳区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610803, 6108, '横山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610822, 6108, '府谷县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610824, 6108, '靖边县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610825, 6108, '定边县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610826, 6108, '绥德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610827, 6108, '米脂县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610828, 6108, '佳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610829, 6108, '吴堡县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610830, 6108, '清涧县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610831, 6108, '子洲县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610881, 6108, '神木市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610902, 6109, '汉滨区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610921, 6109, '汉阴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610922, 6109, '石泉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610923, 6109, '宁陕县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610924, 6109, '紫阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:55');
INSERT INTO `sys_area` VALUES (610925, 6109, '岚皋县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (610926, 6109, '平利县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (610927, 6109, '镇坪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (610928, 6109, '旬阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (610929, 6109, '白河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (611002, 6110, '商州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (611021, 6110, '洛南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (611022, 6110, '丹凤县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (611023, 6110, '商南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (611024, 6110, '山阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (611025, 6110, '镇安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (611026, 6110, '柞水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620102, 6201, '城关区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620103, 6201, '七里河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620104, 6201, '西固区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620105, 6201, '安宁区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620111, 6201, '红古区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620121, 6201, '永登县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620122, 6201, '皋兰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620123, 6201, '榆中县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620171, 6201, '兰州新区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:56');
INSERT INTO `sys_area` VALUES (620201, 6202, '嘉峪关市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620302, 6203, '金川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620321, 6203, '永昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620402, 6204, '白银区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620403, 6204, '平川区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620421, 6204, '靖远县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620422, 6204, '会宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620423, 6204, '景泰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620502, 6205, '秦州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620503, 6205, '麦积区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620521, 6205, '清水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620522, 6205, '秦安县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620523, 6205, '甘谷县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620524, 6205, '武山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620525, 6205, '张家川回族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620602, 6206, '凉州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620621, 6206, '民勤县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620622, 6206, '古浪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620623, 6206, '天祝藏族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620702, 6207, '甘州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620721, 6207, '肃南裕固族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620722, 6207, '民乐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620723, 6207, '临泽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620724, 6207, '高台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:57');
INSERT INTO `sys_area` VALUES (620725, 6207, '山丹县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620802, 6208, '崆峒区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620821, 6208, '泾川县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620822, 6208, '灵台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620823, 6208, '崇信县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620825, 6208, '庄浪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620826, 6208, '静宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620881, 6208, '华亭市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620902, 6209, '肃州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620921, 6209, '金塔县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620922, 6209, '瓜州县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620923, 6209, '肃北蒙古族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620924, 6209, '阿克塞哈萨克族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620981, 6209, '玉门市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (620982, 6209, '敦煌市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621002, 6210, '西峰区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621021, 6210, '庆城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621022, 6210, '环县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621023, 6210, '华池县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621024, 6210, '合水县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621025, 6210, '正宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621026, 6210, '宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621027, 6210, '镇原县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621102, 6211, '安定区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621121, 6211, '通渭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621122, 6211, '陇西县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621123, 6211, '渭源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:58');
INSERT INTO `sys_area` VALUES (621124, 6211, '临洮县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621125, 6211, '漳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621126, 6211, '岷县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621202, 6212, '武都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621221, 6212, '成县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621222, 6212, '文县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621223, 6212, '宕昌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621224, 6212, '康县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621225, 6212, '西和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621226, 6212, '礼县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621227, 6212, '徽县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (621228, 6212, '两当县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (622901, 6229, '临夏市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (622921, 6229, '临夏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (622922, 6229, '康乐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (622923, 6229, '永靖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (622924, 6229, '广河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (622925, 6229, '和政县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (622926, 6229, '东乡族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (622927, 6229, '积石山保安族东乡族撒拉族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (623001, 6230, '合作市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (623021, 6230, '临潭县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (623022, 6230, '卓尼县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (623023, 6230, '舟曲县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (623024, 6230, '迭部县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:14:59');
INSERT INTO `sys_area` VALUES (623025, 6230, '玛曲县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (623026, 6230, '碌曲县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (623027, 6230, '夏河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630102, 6301, '城东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630103, 6301, '城中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630104, 6301, '城西区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630105, 6301, '城北区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630106, 6301, '湟中区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630121, 6301, '大通回族土族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630123, 6301, '湟源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630202, 6302, '乐都区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630203, 6302, '平安区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630222, 6302, '民和回族土族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630223, 6302, '互助土族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630224, 6302, '化隆回族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (630225, 6302, '循化撒拉族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632221, 6322, '门源回族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632222, 6322, '祁连县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632223, 6322, '海晏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632224, 6322, '刚察县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632321, 6323, '同仁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632322, 6323, '尖扎县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632323, 6323, '泽库县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632324, 6323, '河南蒙古族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632521, 6325, '共和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632522, 6325, '同德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:00');
INSERT INTO `sys_area` VALUES (632523, 6325, '贵德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632524, 6325, '兴海县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632525, 6325, '贵南县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632621, 6326, '玛沁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632622, 6326, '班玛县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632623, 6326, '甘德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632624, 6326, '达日县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632625, 6326, '久治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632626, 6326, '玛多县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632701, 6327, '玉树市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632722, 6327, '杂多县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632723, 6327, '称多县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632724, 6327, '治多县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632725, 6327, '囊谦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632726, 6327, '曲麻莱县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632801, 6328, '格尔木市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632802, 6328, '德令哈市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632803, 6328, '茫崖市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632821, 6328, '乌兰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632822, 6328, '都兰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632823, 6328, '天峻县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (632857, 6328, '大柴旦行政委员会', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (640104, 6401, '兴庆区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (640105, 6401, '西夏区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (640106, 6401, '金凤区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (640121, 6401, '永宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (640122, 6401, '贺兰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:01');
INSERT INTO `sys_area` VALUES (640181, 6401, '灵武市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640202, 6402, '大武口区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640205, 6402, '惠农区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640221, 6402, '平罗县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640302, 6403, '利通区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640303, 6403, '红寺堡区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640323, 6403, '盐池县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640324, 6403, '同心县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640381, 6403, '青铜峡市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640402, 6404, '原州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640422, 6404, '西吉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640423, 6404, '隆德县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640424, 6404, '泾源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640425, 6404, '彭阳县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640502, 6405, '沙坡头区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640521, 6405, '中宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (640522, 6405, '海原县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (650102, 6501, '天山区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (650103, 6501, '沙依巴克区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (650104, 6501, '新市区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (650105, 6501, '水磨沟区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (650106, 6501, '头屯河区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (650107, 6501, '达坂城区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:02');
INSERT INTO `sys_area` VALUES (650109, 6501, '米东区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650121, 6501, '乌鲁木齐县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650202, 6502, '独山子区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650203, 6502, '克拉玛依区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650204, 6502, '白碱滩区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650205, 6502, '乌尔禾区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650402, 6504, '高昌区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:52', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650421, 6504, '鄯善县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650422, 6504, '托克逊县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650502, 6505, '伊州区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650521, 6505, '巴里坤哈萨克自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (650522, 6505, '伊吾县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652301, 6523, '昌吉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652302, 6523, '阜康市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652323, 6523, '呼图壁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652324, 6523, '玛纳斯县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652325, 6523, '奇台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652327, 6523, '吉木萨尔县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652328, 6523, '木垒哈萨克自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652701, 6527, '博乐市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652702, 6527, '阿拉山口市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652722, 6527, '精河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652723, 6527, '温泉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652801, 6528, '库尔勒市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652822, 6528, '轮台县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652823, 6528, '尉犁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652824, 6528, '若羌县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:03');
INSERT INTO `sys_area` VALUES (652825, 6528, '且末县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652826, 6528, '焉耆回族自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652827, 6528, '和静县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652828, 6528, '和硕县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652829, 6528, '博湖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652871, 6528, '库尔勒经济技术开发区', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652901, 6529, '阿克苏市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652902, 6529, '库车市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652922, 6529, '温宿县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652924, 6529, '沙雅县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652925, 6529, '新和县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652926, 6529, '拜城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652927, 6529, '乌什县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652928, 6529, '阿瓦提县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (652929, 6529, '柯坪县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653001, 6530, '阿图什市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653022, 6530, '阿克陶县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653023, 6530, '阿合奇县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653024, 6530, '乌恰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653101, 6531, '喀什市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653121, 6531, '疏附县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653122, 6531, '疏勒县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653123, 6531, '英吉沙县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653124, 6531, '泽普县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653125, 6531, '莎车县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:04');
INSERT INTO `sys_area` VALUES (653126, 6531, '叶城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653127, 6531, '麦盖提县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653128, 6531, '岳普湖县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653129, 6531, '伽师县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653130, 6531, '巴楚县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653131, 6531, '塔什库尔干塔吉克自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653201, 6532, '和田市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653221, 6532, '和田县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653222, 6532, '墨玉县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653223, 6532, '皮山县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653224, 6532, '洛浦县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653225, 6532, '策勒县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653226, 6532, '于田县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (653227, 6532, '民丰县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654002, 6540, '伊宁市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654003, 6540, '奎屯市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654004, 6540, '霍尔果斯市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654021, 6540, '伊宁县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654022, 6540, '察布查尔锡伯自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654023, 6540, '霍城县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654024, 6540, '巩留县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654025, 6540, '新源县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654026, 6540, '昭苏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654027, 6540, '特克斯县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654028, 6540, '尼勒克县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654201, 6542, '塔城市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:05');
INSERT INTO `sys_area` VALUES (654202, 6542, '乌苏市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654221, 6542, '额敏县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654223, 6542, '沙湾县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654224, 6542, '托里县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654225, 6542, '裕民县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654226, 6542, '和布克赛尔蒙古自治县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654301, 6543, '阿勒泰市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654321, 6543, '布尔津县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654322, 6543, '富蕴县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654323, 6543, '福海县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654324, 6543, '哈巴河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654325, 6543, '青河县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (654326, 6543, '吉木乃县', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659001, 6590, '石河子市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659002, 6590, '阿拉尔市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659003, 6590, '图木舒克市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659004, 6590, '五家渠市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659005, 6590, '北屯市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659006, 6590, '铁门关市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659007, 6590, '双河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659008, 6590, '可克达拉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659009, 6590, '昆玉市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (659010, 6590, '胡杨河市', NULL, NULL, '3', NULL, NULL, '2', '1', NULL, '', '2021-11-03 18:03:53', '', '2021-11-03 18:15:06');
INSERT INTO `sys_area` VALUES (1575751638788870146, 4101, '郑东新区', '郑东新区', NULL, '3', 1, '', NULL, '1', 1508685901234966529, 'lidanyang', '2022-09-30 15:37:21', '', '2022-09-30 15:37:21');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` bigint NOT NULL COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL COMMENT '部门id',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '祖级列表',
  `org_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '社会统一信用代码',
  `dept_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `short_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '部门简称',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邮箱',
  `nature` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '机构性质',
  `area_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '区',
  `city_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '市',
  `province_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '省',
  `standard_industry` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标准行业',
  `longitude` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '经度',
  `latitude` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '纬度',
  `dept_type` int NULL DEFAULT NULL COMMENT '单位类型',
  `region_level` int NULL DEFAULT NULL COMMENT '行政级别',
  `detail_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '详细地址',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `dept_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '单位类型(字典：dept_flag)',
  `safety_index` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '安全指数',
  `disposing_capacity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '单位处置能力',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 1716333355760300034, '0,1716333355760300034', NULL, '千寻未来', '千寻未来', 1, '千寻未来', '15888888888', 'qxwl@qq.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', 'admin', '2023-08-21 09:29:08', 'admin', '2023-10-23 14:37:48', '1', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1748254237160058913 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2023-08-21 09:29:08', 'admin', NULL, '性别男11');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-08-21 09:29:08', 'admin', '2024-01-19 15:58:09', '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2023-08-21 09:29:08', 'admin', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (29, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (1433238981721726977, 1, '业务生产类系统', '2', 'sys_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', 'admin', '2021-09-02 09:21:31', NULL);
INSERT INTO `sys_dict_data` VALUES (1433238986129940482, 9, '异常站点', '9', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:52', '', '2021-09-02 09:21:32', '');
INSERT INTO `sys_dict_data` VALUES (1433238992140378113, 5, '基础网络设施入口', '4', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', 'admin', '2021-09-02 09:21:34', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239005763477505, 7, '业务系统', '7', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', 'admin', '2021-09-02 09:21:36', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239013971730434, 50, '其他类系统', '50', 'sys_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:55:00', '', '2021-09-02 09:21:38', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239016815468545, 1, '门户展示类系统', '1', 'sys_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', 'admin', '2021-09-02 09:21:38', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239023723487234, 10, '论坛博客', '10', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:52', '', '2021-09-02 09:21:40', '');
INSERT INTO `sys_dict_data` VALUES (1433239026961489921, 1, '基础环境类系统', '4', 'sys_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', 'admin', '2021-09-02 09:21:41', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239031352926209, 6, '门户网站', '6', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', 'admin', '2021-09-02 09:21:41', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239031784939521, 1, '邮件系统', '1', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', 'admin', '2021-09-02 09:21:42', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239032074346497, 1, '移动微信', '2', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', '', '2021-09-02 09:21:42', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239033366192130, 1, '办公管理类系统', '3', 'sys_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', 'admin', '2021-09-02 09:21:42', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239038575517698, 1, '接口服务', '3', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', '', '2021-09-02 09:21:43', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239040920133634, 8, '办公系统', '8', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:55:00', '', '2021-09-02 09:21:43', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239049359073282, 50, '其他', '50', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:55:00', 'admin', '2021-09-02 09:21:45', NULL);
INSERT INTO `sys_dict_data` VALUES (1433239049975635969, 5, '网络安全设施入口', '5', 'website_type', NULL, NULL, 'N', '0', 'admin', '2021-08-27 12:54:57', 'admin', '2021-09-02 09:21:45', NULL);
INSERT INTO `sys_dict_data` VALUES (1698941553670004737, 1, '网站', 'web', 'assets_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1698941588407230465, 2, 'IP设备', 'ip', 'assets_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1698941657105735681, 3, '服务组件', 'port', 'assets_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699025291183648770, 1, '审批中', '1', 'sys_status', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699251886808887298, 1, '建设单位', '1', 'dept_flag', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699252005952286722, 2, '运维单位', '2', 'dept_flag', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699252096708636673, 3, '使用单位', '3', 'dept_flag', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699260063830958082, 1, '200', '200', 'web_status_code', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699260089525264386, 2, '404', '404', 'web_status_code', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699260113348911105, 3, '500', '500', 'web_status_code', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699331762829602818, 1, '未分级', '0', 'filing_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699331950424043522, 2, '自主保护(一级)', '1', 'filing_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699332032099725313, 3, '指导保护(二级)', '2', 'filing_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699332115599929346, 4, '监督保护(三级)', '3', 'filing_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699332182050287618, 5, '强制保护(四级)', '4', 'filing_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699332274857652226, 6, '专控保护(五级)', '5', 'filing_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699588532848242689, 1, '信息系统', 'infoSystem', 'assets_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699671982607380481, 1, '高', '1', 'info_system_level', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1699672013435514882, 2, '中', '2', 'info_system_level', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1700043183645208577, 1, '手动', '1', 'data_source', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1700043239752413186, 2, '扫描器', '2', 'data_source', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1700043285243834370, 3, '流量池', '3', 'data_source', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1700043409000968194, 4, '源码分析', '4', 'data_source', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701071139612078082, 1, 'web漏洞', 'safe_web_loophole', 'safe_value_param', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, 'web漏洞');
INSERT INTO `sys_dict_data` VALUES (1701071304934764545, 2, '主机漏洞', 'safe_host_loophole', 'safe_value_param', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701071473193463810, 3, '攻击事件', 'safe_attack_event', 'safe_value_param', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701072303904731138, 1, '高', '1', 'safe_web_loophole', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701072328353329153, 2, '中', '2', 'safe_web_loophole', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701072356958482433, 3, '低', '3', 'safe_web_loophole', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701072442610364417, 1, '关键词匹配', 'key_word', 'safe_attack_event', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701072504795115521, 2, '标题篡改', 'title', 'safe_attack_event', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701072555713966081, 3, '图片检测', 'image', 'safe_attack_event', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701072595484356610, 4, '页面篡改', 'page_tampering', 'safe_attack_event', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701179822006284289, 2, 'GLUE_GROOVY (Java)', 'GLUE_GROOVY', 'glue_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701179920505319425, 3, 'GLUE_SHELL (Shell)', 'GLUE_SHELL', 'glue_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701179977812094977, 4, 'GLUE_PYTHON (Python)', 'GLUE_PYTHON', 'glue_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180076684423170, 5, 'GLUE_PHP (PHP)', 'GLUE_PHP', 'glue_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180135928967170, 6, 'GLUE_NODEJS (Nodejs)', 'GLUE_NODEJS', 'glue_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180212135276546, 7, 'GLUE_POWERSHELL (PowerShell)', 'GLUE_POWERSHELL', 'glue_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180295954247681, 1, '单机串行', 'SERIAL_EXECUTION', 'executor_block_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180371548188674, 2, '丢弃后续调度', 'DISCARD_LATER', 'executor_block_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180441265909762, 3, '覆盖之前调度', 'COVER_EARLY', 'executor_block_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180547847368706, 1, '第一个', 'FIRST', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180601530265602, 2, '最后一个', 'LAST', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180651656392706, 3, '轮询', 'ROUND', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180718492626945, 4, '随机', 'RANDOM', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180785727320065, 5, '一致性HASH', 'CONSISTENTHASH', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180841486397442, 6, '最不经常使用', 'LFU', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180910084239362, 7, '最近最久未使用', 'LRU', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701180968515088385, 8, '故障转移', 'FAILOVER', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701181021589811202, 9, '忙碌转移', 'BUSYOVER', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701181090854547458, 10, '分片广播', 'SHARD', 'executor_route_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701181173532667905, 1, '忽略', 'DO_NOTHING', 'misfire_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701181252050038785, 2, '立即执行一次', 'FIRE_ONCE_NOW', 'misfire_strategy', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701181366982356993, 1, '无', 'NONE', 'schedule_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701181429024501761, 2, 'CRON', 'CRON', 'schedule_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701181600080801794, 3, '固定速度', 'FIX_RATE', 'schedule_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701399691108757505, 1, 'BEAN', 'BEAN', 'glue_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701852247454855169, 5, '日志审计(云智)', '5', 'data_source', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1701890173182095361, 4, '主机', 'host', 'ip_asset_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', '2023-11-13 11:35:15', '');
INSERT INTO `sys_dict_data` VALUES (1702606998417125377, 1, 'API-资产模块', 'assets', 'sys_operate_module', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703700981453107202, 1, '数据库', 'database', 'application_classification', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703701052504616962, 1, '防火墙', 'firewall', 'application_classification', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703701292662075394, 1, '态势感知', 'NSSA', 'application_classification', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703701612926545921, 1, 'JAVA', 'java', 'develop_language', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703701740152369154, 1, 'PYTHON', 'Python', 'develop_language', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703701774658908161, 1, 'GO', 'GO', 'develop_language', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703701903558258690, 1, 'C', 'C', 'develop_language', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703702086283112449, 1, 'JavaScript', 'JavaScript', 'develop_language', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703702137101299713, 1, 'Ruby', 'Ruby', 'develop_language', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703702178260004865, 1, 'PHP', 'PHP', 'develop_language', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703702232727236609, 1, 'C++', 'C++', 'develop_language', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703702408019783682, 1, 'Kotlin', 'Kotlin', 'develop_language', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703970114396696578, 1, '通知', 'action_notice', 'action_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703970176967323649, 1, '查询', 'action_search', 'action_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1703970309821902850, 1, '更新', 'action_update', 'action_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1704024222277251074, 3, '低', '3', 'info_system_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1704025655412207617, 2, '筹建中', '2', 'sys_status', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1704054823172648962, 1, '风险仓库类', 'playbook_risk_house', 'playbook_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1704054997794107393, 1, '定时任务类', 'playbook_job_task', 'playbook_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1704055348857352194, 1, '日常维护', 'playbook_maintenance', 'playbook_scene', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1704055563911901186, 1, '重保值守', 'playbook_onduty', 'playbook_scene', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1706842721458532353, 2, '每日零报告', 'dailyReport', 'model_category', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', '2024-01-19 15:54:44', '');
INSERT INTO `sys_dict_data` VALUES (1706842764466925570, 3, '事件上报', 'eventEscalation', 'model_category', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', '2024-01-19 15:55:56', '');
INSERT INTO `sys_dict_data` VALUES (1706919889819611137, 1, '两值比较', '1', 'compare_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1706919994257780738, 1, '集合', '2', 'compare_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1706920079494426626, 1, '自身判断', '3', 'compare_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1710955841483386882, 3, '运行中', '3', 'sys_status', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1710955875906039809, 4, '已退网', '4', 'sys_status', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1710959811849302018, 1, 'SQL注入', 'vul1', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1710960548599771137, 1, '低', '0', 'web_vuln_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1710960588210778113, 1, '中', '1', 'web_vuln_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1710960625091293185, 1, '高', '2', 'web_vuln_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1710960760521175042, 1, 'vul1', 'vul2', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1710960878699884545, 1, 'CRLF注入', 'vul3', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199124482633730, 1, 'XSS', 'vul4', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199212638515202, 1, '管理后台', 'vul5', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '管理后台');
INSERT INTO `sys_dict_data` VALUES (1711199267751669761, 1, '路径遍历', 'vul6', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199325410766850, 1, '信息泄漏', 'vul7', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199426753540098, 1, '页面跳转', 'vul8', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199482848161794, 1, '挂马', 'vul9', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199536291983361, 1, '暗链', 'vul10', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199606081007617, 1, '文件包含', 'vul11', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199678596329473, 1, '应用程序漏洞', 'vul12', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199740571365377, 1, '文件上传', 'vul13', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199816412770305, 1, '关键字检测', 'vul14', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711199892921069569, 1, 'Strus2', 'vul15', 'web_vuln_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1711922540349173761, 1, '公共配置', 'common', 'model_category', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1712672985426374657, 1, '柢', '0', 'host_vuln_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1712673059988516865, 1, '中', '1', 'host_vuln_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1712673091210915842, 1, '高', '2', 'host_vuln_level', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1714202997371645954, 1, 'IPv4', '2', 'ip_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1714203085552693250, 1, 'IPv6', '10', 'ip_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1714488584787931138, 5, '其他', '5', 'sys_status', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1714898560169750529, 1, '日志告警', 'playbook_log_warn', 'playbook_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719591212823818242, 2, '组织机构', 'dept', 'model_infoSystem', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719591703624495106, 1, '信息系统', 'infoSystem', 'model_infoSystem', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719593512640393217, 1, '信息系统', 'model_infoSystem', 'model_asset_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719593540956139522, 2, '网站', 'model_web', 'model_asset_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719593659231318018, 3, 'ip设备', 'model_ip', 'model_asset_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719593778622181378, 4, '服务组件', 'model_port', 'model_asset_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719594014610501633, 1, '网站', 'web', 'model_web', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719594467347869697, 2, 'ip设备', 'ip', 'model_web', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719598906012020738, 3, '服务组件', 'prot', 'model_web', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719599111033794561, 4, '组织机构', 'dept', 'model_web', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719601289983111170, 1, '存活', '存活', 'ip_status', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719601349781303298, 1, '曾存活', '曾存活', 'ip_status', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719601387282575361, 1, '失活', '失活', 'ip_status', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', '2023-11-13 10:20:24', '');
INSERT INTO `sys_dict_data` VALUES (1719602858938019842, 5, '组织机构', 'model_dept', 'model_asset_type', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719603152350556162, 1, '组织机构', 'dept', 'model_dept', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719603854942613506, 2, 'ip设备', 'ip', 'model_port', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719603903256801281, 1, '服务组件', 'port', 'model_port', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719606983427829762, 1, 'ip设备', 'ip', 'model_ip', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719914628235866113, 2, '组织机构', 'dept', 'model_ip', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1719914677158227970, 3, '组织机构', 'dept', 'model_port', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1721796853965467650, 1, '1', '1', 'a', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1721796872038723586, 1, '2', '2', 'a', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1721797126469398529, 1, '1', '1', 'sec-log-level', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1721797325078081538, 1, '低', '1', 'sec_log_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '1');
INSERT INTO `sys_dict_data` VALUES (1721797352760487937, 2, '中低', '2', 'sec_log_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1721797858987814913, 5, '高', '5', 'sec_log_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1721797921742991361, 4, '中高', '4', 'sec_log_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1721797943012306945, 3, '中', '3', 'sec_log_level', NULL, NULL, 'N', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1722428075527843842, 1, '数据统计', '1', 'chart_board_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1722428365593325569, 1, '图表统计', '2', 'chart_board_type', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1722434669657403393, 1, '启用', '1', 'chart_board_status', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1722434705405456385, 1, '禁用', '2', 'chart_board_status', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1723905860565938177, 1, '数据资产', 'data', 'ip_asset_type', NULL, NULL, 'N', '0', 'admin', '2023-11-13 11:29:20', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1723906936207781890, 2, '终端资产', 'zd', 'ip_asset_type', NULL, NULL, 'N', '0', 'admin', '2023-11-13 11:33:37', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1723907291897344002, 3, '应用资产', 'web', 'ip_asset_type', NULL, NULL, 'N', '0', 'admin', '2023-11-13 11:35:02', 'admin', '2023-11-13 15:36:11', '');
INSERT INTO `sys_dict_data` VALUES (1723907835340730370, 4, '网络资产', 'other', 'ip_asset_type', NULL, NULL, 'N', '0', 'admin', '2023-11-13 11:37:11', 'admin', '2023-11-13 15:36:52', '');
INSERT INTO `sys_dict_data` VALUES (1723968241530777601, 6, '未知资产', '未知资产', 'ip_asset_type', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:37:13', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1723970145748987905, 1, '信阳', '411500', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:44:47', 'admin', '2023-11-21 14:26:42', '10240');
INSERT INTO `sys_dict_data` VALUES (1723970824987160578, 2, '开封市', '410200', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:47:29', 'admin', '2023-11-21 14:26:45', '12340');
INSERT INTO `sys_dict_data` VALUES (1723970973549408258, 3, '洛阳市', '410300', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:48:05', 'admin', '2023-11-21 14:26:49', '12500');
INSERT INTO `sys_dict_data` VALUES (1723971027450408961, 4, '平顶山市', '410400', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:48:17', 'admin', '2023-11-21 14:26:52', '14240');
INSERT INTO `sys_dict_data` VALUES (1723971152876875777, 5, '安阳市', '410500', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:48:47', 'admin', '2023-11-21 14:26:56', '4530');
INSERT INTO `sys_dict_data` VALUES (1723971191665799169, 6, '鹤壁市', '410600', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:48:57', 'admin', '2023-11-21 14:26:59', '2340');
INSERT INTO `sys_dict_data` VALUES (1723971241095671810, 7, '新乡市', '410700', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:49:08', 'admin', '2023-11-21 14:27:05', '4530');
INSERT INTO `sys_dict_data` VALUES (1723971285387522049, 8, '焦作市', '410800', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:49:19', 'admin', '2023-11-21 14:27:09', '3460');
INSERT INTO `sys_dict_data` VALUES (1723971341775745026, 9, '濮阳市', '410900', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:49:32', 'admin', '2023-11-21 14:27:12', '5640');
INSERT INTO `sys_dict_data` VALUES (1723971413229907970, 10, '许昌市', '411000', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:49:49', 'admin', '2023-11-21 14:27:16', '24210');
INSERT INTO `sys_dict_data` VALUES (1723971593312350209, 11, '漯河市', '411100', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:50:32', 'admin', '2023-11-21 14:27:09', '13231');
INSERT INTO `sys_dict_data` VALUES (1723971651449597953, 12, '三门峡市', '411200', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:50:46', 'admin', '2023-11-21 14:27:06', '4321');
INSERT INTO `sys_dict_data` VALUES (1723971725864939521, 13, '南阳市', '411300', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:51:04', 'admin', '2023-11-21 14:27:03', '3411');
INSERT INTO `sys_dict_data` VALUES (1723971775999455234, 14, '商丘市', '411400', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:51:16', 'admin', '2023-11-21 14:27:00', '23211');
INSERT INTO `sys_dict_data` VALUES (1723971833276870658, 15, '周口市', '411600', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:51:30', 'admin', '2023-11-21 14:27:50', '52211');
INSERT INTO `sys_dict_data` VALUES (1723971914004639746, 16, '驻马店市', '411700', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:51:49', 'admin', '2023-11-21 14:27:44', '43212');
INSERT INTO `sys_dict_data` VALUES (1723971953934413825, 17, '济源市', '419001', 'asset_screen_map', NULL, NULL, 'N', '0', 'admin', '2023-11-13 15:51:58', 'admin', '2023-11-21 14:27:38', '32111');
INSERT INTO `sys_dict_data` VALUES (1724595427921047553, 1, '高', '1', 'safe_host_loophole', NULL, NULL, 'N', '0', 'admin', '2023-11-15 09:09:26', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1724595460003278850, 2, '中', '2', 'safe_host_loophole', NULL, NULL, 'N', '0', 'admin', '2023-11-15 09:09:34', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1724595512717291522, 3, '低', '3', 'safe_host_loophole', NULL, NULL, 'N', '0', 'admin', '2023-11-15 09:09:46', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1748253109223632898, 4, '事件通报', 'eventNotice', 'model_category', NULL, NULL, 'N', '0', 'admin', '2024-01-19 15:56:37', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1748253444675678209, 5, '漏洞通报', 'leakNotice', 'model_category', NULL, NULL, 'N', '0', 'admin', '2024-01-19 15:57:57', 'admin', '2024-01-19 15:59:06', '');
INSERT INTO `sys_dict_data` VALUES (1748253665853911041, 6, '风险预警', 'riskWarn', 'model_category', NULL, NULL, 'N', '0', 'admin', '2024-01-19 15:58:50', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1748253945924366337, 7, '今日公告', 'dailyNews', 'model_category', NULL, NULL, 'N', '0', 'admin', '2024-01-19 15:59:56', 'admin', '2024-01-19 16:00:09', '');
INSERT INTO `sys_dict_data` VALUES (1748254093060550657, 9, '全体公告', 'allNews', 'model_category', NULL, NULL, 'N', '0', 'admin', '2024-01-19 16:00:31', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1748254237160058881, 1, '今日快报', 'dailyShotNews', 'model_category', NULL, NULL, 'N', '0', 'admin', '2024-01-19 16:01:06', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1748254237160058882, 0, '命中', 'hit', 'log_event_action', NULL, NULL, 'N', '0', 'admin', '2024-02-02 09:48:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058883, 0, '捕获', 'capture', 'log_event_action', NULL, NULL, 'N', '0', 'admin', '2024-02-02 09:48:02', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058884, 0, '无动作', 'na', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058885, 0, '查杀', 'avira', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058886, 0, '阻断并生成日志', '阻断并生成日志', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058887, 0, '修复', 'fixes', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058888, 0, '清除', 'clear', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058889, 0, '放行', '放行', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058890, 0, '生成日志', 'logging', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058891, 0, '检测', 'detection', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058892, 0, '监控', 'monitor', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058893, 0, '重定向', 'redirect', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058894, 0, '发现', 'found', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058895, 0, '丢弃', 'drop', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058896, 0, '隔离', 'isolation', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058897, 0, '下线', 'offline', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058898, 0, '允许', 'permit', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058899, 0, '断开', 'disconnect', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058900, 0, '告警', 'alert', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058901, 0, '告警并生成日志', '告警并生成日志', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058902, 0, '上线', 'online', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058903, 0, '记录', 'log', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058904, 0, '拒绝', 'deny', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058905, 0, '删除', 'delete', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058906, 0, '限制', 'limit', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058907, 0, '关闭', 'close', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058908, 0, '忽略', 'ignore', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058909, 0, '阻断', 'block', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058910, 0, '重置', 'reset', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058911, 0, '未定义', '-999', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1748254237160058912, 0, '重启', 'reboot', 'log_event_action', NULL, NULL, 'N', '0', 'admin', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1760565042584956929, 1, '一级', '1', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:19:51', 'admin', '2024-02-22 15:20:01', '');
INSERT INTO `sys_dict_data` VALUES (1760565116836720642, 2, '二级', '2', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:20:08', 'admin', '2024-03-06 17:58:27', '');
INSERT INTO `sys_dict_data` VALUES (1760566191702618114, 3, '三级', '3', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:24:24', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1760566246807384065, 4, '四级', '4', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:24:38', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1760566271079821314, 5, '五级', '5', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:24:43', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1760566298594455554, 6, '六级', '6', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:24:50', 'admin', '2024-02-22 15:25:04', '');
INSERT INTO `sys_dict_data` VALUES (1760566342185857025, 7, '七级', '7', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:25:00', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1760566382065299458, 8, '八级', '8', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:25:10', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1760566411022774274, 9, '九级', '9', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:25:17', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1760566490714550274, 10, '十级', '10', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-02-22 15:25:36', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1763737665804382209, 50, 'null', 'null', 'web_status_code', NULL, NULL, 'N', '0', 'admin', '2024-03-02 09:26:43', 'admin', '2024-03-02 09:26:57', '');
INSERT INTO `sys_dict_data` VALUES (1768169158068875266, 1, '微应用', '1', 'micro_data_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 14:55:53', '', NULL, '1、微应用；');
INSERT INTO `sys_dict_data` VALUES (1768169230500311042, 1, '微系统', '2', 'micro_data_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 14:56:10', '', NULL, '2、微系统；');
INSERT INTO `sys_dict_data` VALUES (1768169670382137346, 1, '网络安全', '1', 'micro_safe_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 14:57:55', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768169725415600129, 1, '数据安全', '2', 'micro_safe_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 14:58:08', '', NULL, '安全类别(1、网络安全；2、数据安全；3、内容安全)');
INSERT INTO `sys_dict_data` VALUES (1768169768419799042, 1, '内容安全', '3', 'micro_safe_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 14:58:18', 'admin', '2024-03-14 14:58:24', '');
INSERT INTO `sys_dict_data` VALUES (1768170057046634498, 1, '识别监测类', '1', 'micro_app_service_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 14:59:27', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768170096515035138, 1, '防御阻断类', '2', 'micro_app_service_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 14:59:37', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768170141125652481, 1, '管理合规类', '3', 'micro_app_service_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 14:59:47', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768170418851491841, 1, '基础', '1', 'micro_app_service_grade', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:00:54', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768170466700111873, 1, '进阶', '2', 'micro_app_service_grade', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:01:05', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768170518982111234, 1, '全能', '3', 'micro_app_service_grade', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:01:17', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768170795508379650, 1, '显示', '1', 'micro_app_hot', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:02:23', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768170838281891841, 1, '隐藏', '2', 'micro_app_hot', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:02:34', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768171182252568578, 1, '次', '1', 'micro_app_price_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:03:56', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768171243342606338, 1, '周', '2', 'micro_app_price_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:04:10', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768171272170057729, 1, '月', '3', 'micro_app_price_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:04:17', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768171305917427714, 1, '季度', '4', 'micro_app_price_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:04:25', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768171385609203714, 1, '年', '5', 'micro_app_price_type', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:04:44', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1768171667294466050, 1, '已上架', '1', 'micro_app_up_flag', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:05:51', 'admin', '2024-03-14 15:06:15', '1、上架；0、下架');
INSERT INTO `sys_dict_data` VALUES (1768171728971706369, 1, '已下架', '0', 'micro_app_up_flag', NULL, NULL, 'N', '0', 'admin', '2024-03-14 15:06:06', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1777226633342562305, 1, '403', '403', 'web_status_code', NULL, NULL, 'N', '0', 'admin', '2024-04-08 14:47:03', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780438020718014466, 1, '特征检测引擎', 'sde', 'engine_type', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:27:58', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780438105698807810, 1, '人工智能引擎', 'ai', 'engine_type', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:28:18', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780438143774699521, 1, '行为检测引擎', 'bde', 'engine_type', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:28:27', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780438180403556354, 1, '威胁情报引擎', 'intel', 'engine_type', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:28:36', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780438214050263042, 1, '关联分析引擎', 'correlation', 'engine_type', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:28:44', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780438268760764417, 1, 'ATP智能沙箱', 'ais', 'engine_type', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:28:57', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780438416106663938, 1, '低危', '1', 'severity', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:29:32', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780438444598571010, 1, '中危', '3', 'severity', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:29:39', 'admin', '2024-04-17 11:30:56', '');
INSERT INTO `sys_dict_data` VALUES (1780438612106489857, 1, '中低危', '2', 'severity', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:30:19', 'admin', '2024-04-17 11:30:49', '');
INSERT INTO `sys_dict_data` VALUES (1780438661012074498, 1, '高危', '4', 'severity', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:30:30', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780438687792705537, 1, '危急', '5', 'severity', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:30:37', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439110695989250, 1, '低可信', '1', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:32:18', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439230174932994, 1, '低可信2', '2', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:32:46', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439279428644866, 1, '低可信3', '3', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:32:58', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439322143436801, 1, '中可信4', '4', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:33:08', 'admin', '2024-04-17 11:33:47', '');
INSERT INTO `sys_dict_data` VALUES (1780439363528634369, 1, '中可信5', '5', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:33:18', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439403995279362, 1, '中可信6', '6', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:33:28', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439456243724290, 1, '中可信7', '7', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:33:40', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439529555963906, 1, '高可信8', '8', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:33:57', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439558807040001, 1, '高可信9', '9', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:34:04', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439609843331074, 1, '高可信', '10', 'reliability', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:34:17', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439709319639041, 1, '外连', '1', 'visit_direction', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:34:40', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439731213905922, 1, '横向', '2', 'visit_direction', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:34:46', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439754341298178, 1, '外对内', '3', 'visit_direction', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:34:51', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439778097836034, 1, '其他', '4', 'visit_direction', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:34:57', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780439984449204225, 1, '成功', 'success', 'attack_status', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:35:46', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440018213351426, 1, '可疑', 'possible success', 'attack_status', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:35:54', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440047804166145, 1, '尝试', 'attempt', 'attack_status', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:36:01', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440204159430658, 1, '安全缺陷', 'security-defect', 'kill_chain', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:36:38', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440242256293889, 1, '扫描探测', 'scan-detect', 'kill_chain', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:36:47', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440282815213569, 1, '尝试攻击', 'attempt-attack', 'kill_chain', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:36:57', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440332983283714, 1, '初步感染', 'infected', 'kill_chain', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:37:09', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440364755136513, 1, '木马下载', 'malware-download', 'kill_chain', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:37:17', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440402168328193, 1, '远程控制', 'remote-control', 'kill_chain', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:37:26', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440433013239809, 1, '横向渗透', 'internal-penertration', 'kill_chain', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:37:33', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780440464231444482, 1, '行动收割', 'actions-objectives', 'kill_chain', NULL, NULL, 'N', '0', 'admin', '2024-04-17 11:37:40', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866405432766466, 1, 'HTTP', 'http', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:50:13', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866440987881474, 1, 'DNS', 'dns', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:50:21', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866487058116609, 1, 'SSL', 'ssl', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:50:32', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866532583092225, 1, 'HTTPS', 'https', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:50:43', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866597573832706, 1, 'SMTP', 'smtp', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:50:58', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866659158798338, 1, 'IMAP', 'imap', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:51:13', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866704557944833, 1, 'POP3', 'pop3', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:51:24', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866754025566209, 1, 'FTP', 'ftp', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:51:36', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866812506746882, 1, 'SMB', 'smb', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:51:50', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866849445982209, 1, 'SSH', 'ssh', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:51:58', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780866893670723585, 1, 'OTHER', 'other', 'app_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:52:09', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780867242682953729, 1, 'TCP', 'tcp', 'transfer_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:53:32', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780867291697590274, 1, 'UDP', 'udp', 'transfer_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:53:44', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780867339030310914, 1, 'ICMP', 'icmp', 'transfer_protocol', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:53:55', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780867695328047106, 1, 'GET', 'get', 'http_method', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:55:20', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780867737615020033, 1, 'POST', 'post', 'http_method', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:55:30', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780867774206128129, 1, 'PUT', 'put', 'http_method', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:55:39', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780867815767486465, 1, 'DELETE', 'delete', 'http_method', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:55:49', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1780867853029683201, 1, '其他', 'other', 'http_method', NULL, NULL, 'N', '0', 'admin', '2024-04-18 15:55:58', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1783304850897842177, 20, '访问不通', '200', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 09:19:43', 'admin', '2024-04-25 09:28:25', '');
INSERT INTO `sys_dict_data` VALUES (1783305688001228802, 43, '没有权限访问', '403', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 09:23:03', 'admin', '2024-04-25 09:28:19', '');
INSERT INTO `sys_dict_data` VALUES (1783305798873460738, 31, '永久重定向', '301', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 09:23:29', 'admin', '2024-04-25 09:28:14', '');
INSERT INTO `sys_dict_data` VALUES (1783305943954436097, 32, '临时性重定向', '302', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 09:24:04', 'admin', '2024-04-25 09:28:11', '');
INSERT INTO `sys_dict_data` VALUES (1783306303850885122, 1, '不可用', '不可用', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 09:25:30', 'admin', '2024-04-25 09:26:41', '');
INSERT INTO `sys_dict_data` VALUES (1783306542255124481, 44, '服务器找不到请求', '404', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 09:26:27', 'admin', '2024-04-25 09:28:07', '');
INSERT INTO `sys_dict_data` VALUES (1783306716864000002, 45, '方法禁用', '405', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 09:27:08', 'admin', '2024-04-25 09:28:03', '');
INSERT INTO `sys_dict_data` VALUES (1783306848061829122, 41, '未授权', '401', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 09:27:39', 'admin', '2024-04-25 09:27:58', '');
INSERT INTO `sys_dict_data` VALUES (1783307181626437634, 47, '需要代理授权', '407', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 09:28:59', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1783317712580063234, 1, '未分级', '0', 'system_importance_level', NULL, NULL, 'N', '0', 'admin', '2024-04-25 10:10:50', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1783391026447142914, 50, '内部服务器错误', '503', 'status_code_description', NULL, NULL, 'N', '0', 'admin', '2024-04-25 15:02:09', 'admin', '2024-04-25 15:02:42', '');
INSERT INTO `sys_dict_data` VALUES (1783799421083791361, 1, '信息报告', '1', 'report_type', NULL, NULL, 'N', '0', 'admin', '2024-04-26 18:04:58', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1783799497369792514, 1, '分析报告', '2', 'report_type', NULL, NULL, 'N', '0', 'admin', '2024-04-26 18:05:16', '', NULL, '');
INSERT INTO `sys_dict_data` VALUES (1783799650617077761, 1, '建议报告', '3', 'report_type', NULL, NULL, 'N', '0', 'admin', '2024-04-26 18:05:53', '', NULL, '');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2023-08-21 09:29:08', 'admin', '2024-01-19 15:58:23', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2023-08-21 09:29:08', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (1698941408605806594, '资产类型', 'assets_type', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1698987974024671233, 'ip资产类型', 'ip_asset_type', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1698995694144499714, '备案等级', 'filing_level', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1698996894218760193, '系统类型', 'sys_type', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1698998627166760961, '信息系统-系统状态', 'sys_status', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1699251791728209922, '单位类型', 'dept_flag', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1699260024295448577, '网站状态码', 'web_status_code', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1699315740326961153, '网站类型', 'website_type', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1699331506826063874, '信息系统等级', 'info_system_level', '0', 'admin', NULL, 'admin', NULL, '信息系统等级');
INSERT INTO `sys_dict_type` VALUES (1700043072571650050, '数据来源', 'data_source', '0', 'admin', NULL, '', NULL, '数据来源');
INSERT INTO `sys_dict_type` VALUES (1701070872502022145, '安全值参数类型', 'safe_value_param', '0', 'admin', NULL, '', NULL, '安全值参数类型');
INSERT INTO `sys_dict_type` VALUES (1701071700168224770, '安全值-web漏洞', 'safe_web_loophole', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1701072004502728705, '安全值-主机漏洞', 'safe_host_loophole', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1701072157750013954, '安全值-攻击事件', 'safe_attack_event', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1701178751418576898, 'xxl-job调度类型', 'schedule_type', '0', 'admin', NULL, '', NULL, 'xxl-job调度类型');
INSERT INTO `sys_dict_type` VALUES (1701179006994296833, 'xxl-job执行器路由策略', 'executor_route_strategy', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1701179118160130049, 'xxl-job调度过期策略', 'misfire_strategy', '0', 'admin', NULL, 'admin', NULL, 'xxl-job调度过期策略');
INSERT INTO `sys_dict_type` VALUES (1701179451686989826, 'xxl-job阻塞处理策略', 'executor_block_strategy', '0', 'admin', NULL, 'admin', NULL, 'xxl-job阻塞处理策略');
INSERT INTO `sys_dict_type` VALUES (1701179655475638273, 'xxl-job运行模式', 'glue_type', '0', 'admin', NULL, '', NULL, 'xxl-job运行模式');
INSERT INTO `sys_dict_type` VALUES (1702606832507236353, '接口API所属模块', 'sys_operate_module', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1703700700673814529, '应用所属分类', 'application_classification', '0', 'admin', NULL, '', NULL, '应用所属分类');
INSERT INTO `sys_dict_type` VALUES (1703701520056266753, '开发语言', 'develop_language', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1703970013045534721, '动作分类', 'action_type', '0', 'admin', NULL, '', NULL, '动作的分类');
INSERT INTO `sys_dict_type` VALUES (1704054473686462465, '剧本类型', 'playbook_type', '0', 'admin', NULL, '', NULL, '剧本类型');
INSERT INTO `sys_dict_type` VALUES (1704055181416542210, '剧本场景', 'playbook_scene', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1706841963862372354, '模型流程类别', 'model_category', '0', 'admin', NULL, '', NULL, '模型流程类别');
INSERT INTO `sys_dict_type` VALUES (1706919768188989441, '比较符类型', 'compare_type', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1710959379596914689, 'web漏洞-类型', 'web_vuln_type', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1710960064178630657, 'web漏洞-等级', 'web_vuln_level', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1712672822230200321, '主机漏洞等级', 'host_vuln_level', '0', 'admin', NULL, '', NULL, '高中低');
INSERT INTO `sys_dict_type` VALUES (1714202829603680257, 'IP类型', 'ip_type', '0', 'admin', NULL, 'admin', NULL, 'IP类型');
INSERT INTO `sys_dict_type` VALUES (1719590933214736385, '信息系统关联查询对象', 'model_infoSystem', '0', 'admin', NULL, 'admin', NULL, '资产评估模型信息系统可查询的资产类型');
INSERT INTO `sys_dict_type` VALUES (1719591953823117313, '网站关联查询对象', 'model_web', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1719593393815760898, '资产评估模型选取对象', 'model_asset_type', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1719601034407391233, 'ip存活状态', 'ip_status', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1719603088437751810, '组织机构关联查询对象', 'model_dept', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1719603568698142722, '服务组件关联对象', 'model_port', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1719606527569899522, 'ip关联查询对象', 'model_ip', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1721796640517337089, '日志审计告警信息-告警等级', 'sec_log_level', '0', 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1722427814533083137, '数据看板-看板类型', 'chart_board_type', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1722434626951000066, '数据看板-看板状态', 'chart_board_status', '0', 'admin', NULL, '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1723969939582169090, '资产大屏地图区域数量', 'asset_screen_map', '0', 'admin', '2023-11-13 15:43:58', 'admin', '2024-01-19 15:58:20', '资产大屏地图区域数量');
INSERT INTO `sys_dict_type` VALUES (1753231851343785986, '日志事件动作名称', 'log_event_action', '0', 'admin', '2024-02-02 09:40:21', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1760564958963118082, '信息系统重要等级', 'system_importance_level', '0', 'admin', '2024-02-22 15:19:31', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1768169024039890945, '微系统和微应用-数据类别', 'micro_data_type', '0', 'admin', '2024-03-14 14:55:21', '', NULL, '数据类别，1、微应用；2、微系统；');
INSERT INTO `sys_dict_type` VALUES (1768169480854122497, '微应用-安全类别', 'micro_app_safe_type', '0', 'admin', '2024-03-14 14:57:10', 'admin', '2024-03-14 14:57:33', '安全类别(1、网络安全；2、数据安全；3、内容安全)');
INSERT INTO `sys_dict_type` VALUES (1768169991141535745, '微应用-服务类别', 'micro_app_service_type', '0', 'admin', '2024-03-14 14:59:12', '', NULL, '服务类别(1、识别监测类；2、防御阻断类；3、管理合规类)');
INSERT INTO `sys_dict_type` VALUES (1768170338534764546, '微应用-服务等级', 'micro_app_service_grade', '0', 'admin', '2024-03-14 15:00:34', '', NULL, '服务等级(1、基础；2、进阶；3、全能)');
INSERT INTO `sys_dict_type` VALUES (1768170743993937921, '微应用-热度标识', 'micro_app_hot', '0', 'admin', '2024-03-14 15:02:11', '', NULL, '热度标识(1、显示；2、隐藏)');
INSERT INTO `sys_dict_type` VALUES (1768171133837717506, '微应用-计价类型', 'micro_app_price_type', '0', 'admin', '2024-03-14 15:03:44', '', NULL, '计价类型：1、次；2、周；3、月；4、季度；5、年；');
INSERT INTO `sys_dict_type` VALUES (1768171576034799617, '微应用-上架标识', 'micro_app_up_flag', '0', 'admin', '2024-03-14 15:05:29', 'admin', '2024-04-02 12:00:27', '1、已上架；0、已下架');
INSERT INTO `sys_dict_type` VALUES (1780437921447227394, '检测引擎', 'engine_type', '0', 'admin', '2024-04-17 11:27:34', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1780438371655430145, '威胁等级', 'severity', '0', 'admin', '2024-04-17 11:29:21', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1780438872551796738, '确信度', 'reliability', '0', 'admin', '2024-04-17 11:31:21', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1780439678906740738, '攻击方向', 'visit_direction', '0', 'admin', '2024-04-17 11:34:33', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1780439944733339650, '攻击结果', 'attack_status', '0', 'admin', '2024-04-17 11:35:36', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1780440160639332354, '攻击阶段', 'kill_chain', '0', 'admin', '2024-04-17 11:36:28', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1780866319080435714, '应用层协议', 'app_protocol', '0', 'admin', '2024-04-18 15:49:52', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1780867192959479810, '传输层协议', 'transfer_protocol', '0', 'admin', '2024-04-18 15:53:20', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1780867652248350722, 'HTTP请求方法', 'http_method', '0', 'admin', '2024-04-18 15:55:10', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1783304242350469121, '网站状态码说明', 'status_code_description', '0', 'admin', '2024-04-25 09:17:18', '', NULL, '');
INSERT INTO `sys_dict_type` VALUES (1783705411459690498, '报告类型', 'report_type', '0', 'admin', '2024-04-26 11:51:24', '', NULL, '');

-- ----------------------------
-- Table structure for sys_file_record
-- ----------------------------
DROP TABLE IF EXISTS `sys_file_record`;
CREATE TABLE `sys_file_record`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '文件id',
  `bucket_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '桶名称',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件名称',
  `file_dir` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件目录',
  `file_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件路径',
  `file_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件类型',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统文件记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_file_record
-- ----------------------------

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log`  (
  `info_id` bigint NOT NULL COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE,
  INDEX `idx_sys_logininfor_s`(`status` ASC) USING BTREE,
  INDEX `idx_sys_logininfor_lt`(`login_time` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '组件路径',
  `query_param` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '路由参数',
  `is_list` int NULL DEFAULT NULL COMMENT '是否为列表页面',
  `is_frame` int NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '菜单类型（M模块 D目录 N菜单 B按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '显示状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '#' COMMENT '菜单图标',
  `is_recommend` int NULL DEFAULT NULL COMMENT '是否推荐',
  `menu_platform` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '菜单所属平台',
  `modular_id` bigint NULL DEFAULT NULL COMMENT '模块ID',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统配置管理', 0, 5, '/system', NULL, '', NULL, 1, 0, 'M', '0', '0', '', NULL, NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '/system/user/index', '', NULL, 1, 1, 'N', '0', '0', 'system:user:list,web:v1:sysUser:list,web:v1:sysUser:changeStatus', 'ele-User', NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', 'admin', '2023-12-04 15:36:34', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '/system/role/roleManage', '', NULL, 1, 1, 'N', '0', '0', 'system:role:list,web:v1:role:list,web:v1:role:edit', 'ele-ColdDrink', NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', 'haoxuyang', '2023-11-18 09:23:54', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '/system/menu/index', '', NULL, 1, 1, 'N', '0', '0', 'system:menu:list,web:v1:sysMenu:list,web:v1:sysMenu:menuModuleList', 'ele-Menu', NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', 'admin', '2023-11-21 13:42:28', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '单位管理', 1, 4, '/system/dept', '/system/dept/index', '', NULL, 1, 1, 'N', '0', '0', 'system:dept:list,web:v1:dept:list,web:v1:dept:status', 'ele-OfficeBuilding', NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', 'haoxuyang', '2023-11-18 09:32:17', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '/system/dict/index', '', NULL, 1, 1, 'N', '0', '0', 'system:dict:list,web:v1:sysDictType:list,web:v1:sysDictData:list,web:v1:sysDictData:byParentType', 'ele-SetUp', NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', 'haoxuyang', '2023-11-18 09:39:02', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/params', '/system/params/index', '', NULL, 1, 1, 'N', '0', '0', 'system:config:list,web:v1:sysConfig:list', 'ele-Help', NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', 'haoxuyang', '2023-11-18 09:42:28', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 15, '/system/log', '', '', NULL, 1, 0, 'D', '0', '0', '', 'ele-Cellphone', NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', 'admin', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/system/log/operLogManage', '/system/log/operLogManage', '', NULL, 1, 0, 'N', '0', '0', 'system:operlog:list,web:v1:operLog:list', NULL, NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', 'admin', '2023-11-18 10:00:20', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/system/log/loginLogManage', '/system/log/loginLogManage', '', NULL, 1, 0, 'N', '0', '0', 'system:logininfor:list,web:v1:loginLog:list', NULL, NULL, 'PC', NULL, 'admin', '2023-08-21 09:29:08', 'admin', '2023-11-18 10:00:32', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1697868051827617794, '角色新增', 101, 1, '/system/role/roleAdd', '/system/role/roleAdd', NULL, NULL, 1, 1, 'B', '1', '0', 'system:role:add,web:v1:sysUser:deptTree,web:v1:role:add,web:v1:role:query', '', NULL, 'PC', NULL, 'admin', NULL, 'admin', '2023-11-18 14:13:24', '');
INSERT INTO `sys_menu` VALUES (1697868468909207553, '角色编辑', 101, 2, '/system/role/roleUpdate', '/system/role/roleUpdate', NULL, NULL, 1, 1, 'B', '1', '0', 'system:role:update,web:v1:role:getInfo,web:v1:sysUser:deptTree,web:v1:role:edit', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:26:59', '');
INSERT INTO `sys_menu` VALUES (1697868837911490561, '角色复制', 101, 3, '/system/role/roleCopy', '/system/role/roleCopy', NULL, NULL, 1, 1, 'B', '1', '0', 'system:role:copy,web:v1:role:getInfo,web:v1:sysUser:deptTree,web:v1:role:edit,web:v1:role:add', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:28:00', '');
INSERT INTO `sys_menu` VALUES (1697869409616097281, '角色删除', 101, 4, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:role:delete,web:v1:role:remove', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:28:13', '');
INSERT INTO `sys_menu` VALUES (1697876386106798081, '新增用户', 100, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:user:add,web:v1:sysUser:add,web:v1:role:optionselect', '', NULL, 'PC', NULL, 'admin', NULL, 'admin', '2023-11-18 09:15:54', '');
INSERT INTO `sys_menu` VALUES (1697876481057452033, '编辑用户', 100, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:user:edit,web:v1:dept:list,web:v1:role:optionselect,web:v1:sysUser:edit,web:v1:sysUser:details,web:v1:sysUser:authRole', '', NULL, 'PC', NULL, 'admin', NULL, 'admin', '2023-11-18 09:19:23', '');
INSERT INTO `sys_menu` VALUES (1697876613962362882, '删除用户', 100, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:user:delete,web:v1:sysUser:remove', '', NULL, 'PC', NULL, 'admin', NULL, 'admin', '2023-11-18 09:17:46', '');
INSERT INTO `sys_menu` VALUES (1697876693872242690, '重置密码', 100, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:user:reset,web:v1:sysUser:resetPwd', '', NULL, 'PC', NULL, 'admin', NULL, 'admin', '2023-11-18 09:18:39', '');
INSERT INTO `sys_menu` VALUES (1697877174988271618, '新增单位', 103, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dept:add,web:v1:dept:add', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:33:36', '');
INSERT INTO `sys_menu` VALUES (1697877260371718146, '编辑单位', 103, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dept:update,web:v1:dept:getDeptById,web:v1:dept:edit', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:33:59', '');
INSERT INTO `sys_menu` VALUES (1697877534725337089, '删除单位', 103, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dept:delete,web:v1:dept:remove', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:34:08', '');
INSERT INTO `sys_menu` VALUES (1697914422915985409, '新增参数', 106, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:params:add,web:v1:sysConfig:add', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:42:48', '');
INSERT INTO `sys_menu` VALUES (1697914490855321601, '修改参数', 106, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:params:update,web:v1:sysConfig:query,web:v1:sysConfig:edit', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:43:04', '');
INSERT INTO `sys_menu` VALUES (1697914549881761794, '删除参数', 106, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:params:delete,web:v1:sysConfig:remove', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:43:14', '');
INSERT INTO `sys_menu` VALUES (1697915612223467521, '新增字典类型', 105, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dictType:add,web:v1:sysDictType:add', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:37:16', '');
INSERT INTO `sys_menu` VALUES (1697915697208455170, '编辑字典类型', 105, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dictType:update,web:v1:sysDictType:edit,web:v1:sysDictType:details', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:37:39', '');
INSERT INTO `sys_menu` VALUES (1697915775990067201, '删除字典类型', 105, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dictType:delete,web:v1:sysDictType:remove', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:37:50', '');
INSERT INTO `sys_menu` VALUES (1697915901286510593, '清空字典类型缓存', 105, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dictType:clear,web:v1:sysDictType:clearCache', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:38:16', '');
INSERT INTO `sys_menu` VALUES (1697916679480897537, '新增字典值', 105, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dictValue:add,web:v1:sysDictData:add', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:38:54', '');
INSERT INTO `sys_menu` VALUES (1697916777208180737, '编辑字典值', 105, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dictValue:update,web:v1:sysDictData:edit,web:v1:sysDictData:details', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:40:18', '');
INSERT INTO `sys_menu` VALUES (1697916839904636930, '删除字典值', 105, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:dictValue:delete,web:v1:sysDictData:remove', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:42:06', '');
INSERT INTO `sys_menu` VALUES (1697917329090506753, '新增菜单', 102, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:menu:add,web:v1:sysMenu:menuCascade,web:v1:sysMenu:add', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:30:33', '');
INSERT INTO `sys_menu` VALUES (1697917455955619841, '编辑菜单', 102, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:menu:update,web:v1:sysMenu:edit,web:v1:sysMenu:details', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:31:30', '');
INSERT INTO `sys_menu` VALUES (1697917527455920129, '删除菜单', 102, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:menu:delete,web:v1:sysMenu:delete', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:31:45', '');
INSERT INTO `sys_menu` VALUES (1702495698148077569, '认证管理', 1, 10, '/system/auth', '', NULL, NULL, 1, 1, 'D', '0', '0', '', 'ele-Aim', NULL, 'PC', 1, 'admin', NULL, 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1702495850158043137, '应用管理', 1702495698148077569, 1, '/system/auth/app', '/system/auth/app/index', NULL, NULL, 1, 1, 'N', '0', '0', 'web:v1:authApplication:list', '', NULL, 'PC', 1, 'admin', NULL, 'haoxuyang', '2023-11-18 09:47:58', '');
INSERT INTO `sys_menu` VALUES (1702495971818024961, '接口管理', 1702495698148077569, 2, '/system/auth/api', '/system/auth/api/index', NULL, NULL, 1, 1, 'N', '0', '0', 'web:v1:authApplication:applicationNameList,web:v1:authApiInfo:list', '', NULL, 'PC', 1, 'admin', NULL, 'haoxuyang', '2023-11-18 09:56:42', '');
INSERT INTO `sys_menu` VALUES (1702528706578165762, '任务删除', 1701187837774213121, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:timeTask:delete,web:v1:job:delJob', '', NULL, 'PC', NULL, 'admin', NULL, 'haoxuyang', '2023-11-18 09:45:58', '');
INSERT INTO `sys_menu` VALUES (1702529551105470466, '新增', 1702495850158043137, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:auth:app:add,web:v1:authApplication:add', '', NULL, 'PC', 1, 'admin', NULL, 'haoxuyang', '2023-11-18 09:49:57', '');
INSERT INTO `sys_menu` VALUES (1702529739119341570, '编辑', 1702495850158043137, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:app:update,web:v1:authApplication:edit,web:v1:authApplication:details', '', NULL, 'PC', 1, 'admin', NULL, 'haoxuyang', '2023-11-18 09:50:22', '');
INSERT INTO `sys_menu` VALUES (1702529841145786369, '权限设置', 1702495850158043137, 1, '/system/auth/app/config', '/system/auth/app/config', NULL, NULL, 1, 1, 'B', '1', '0', 'system:auth:app:config,web:v1:authApiInfo:list', '', NULL, 'PC', 1, 'admin', NULL, 'haoxuyang', '2023-11-18 09:54:02', '');
INSERT INTO `sys_menu` VALUES (1702532291621761026, '重置密钥', 1702495850158043137, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:auth:app:reset,web:v1:authApplication:resetSecret', '', NULL, 'PC', 1, 'admin', NULL, 'haoxuyang', '2023-11-18 09:55:38', '');
INSERT INTO `sys_menu` VALUES (1702532496903581697, '强制下线', 1702495850158043137, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:auth:app:outline,web:v1:authApplication:forceOffline', '', NULL, 'PC', 1, 'admin', NULL, 'haoxuyang', '2023-11-18 09:55:50', '');
INSERT INTO `sys_menu` VALUES (1702533010949091329, '新增', 1702495971818024961, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:auth:api:add,web:v1:authApiInfo:add', '', NULL, 'PC', 1, 'admin', NULL, 'haoxuyang', '2023-11-18 09:56:53', '');
INSERT INTO `sys_menu` VALUES (1702556177209962498, '编辑', 1702495971818024961, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:auth:api:update,web:v1:authApiInfo:details,web:v1:authApiInfo:edit,web:v1:authApiInfo:changeStatus', '', NULL, 'PC', 1, 'admin', NULL, 'haoxuyang', '2023-11-18 09:57:22', '');
INSERT INTO `sys_menu` VALUES (1702557128989814786, '删除日志', 500, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:log:clear,web:v1:operLog:remove', '', NULL, 'PC', NULL, 'admin', NULL, 'admin', '2023-11-18 10:00:13', '');
INSERT INTO `sys_menu` VALUES (1702557716980903938, '删除日志', 501, 1, '', '', NULL, NULL, 1, 1, 'B', '1', '0', 'system:login:log:clear,web:v1:loginLog:remove', '', NULL, 'PC', NULL, 'admin', NULL, 'admin', '2023-11-18 10:00:46', '');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint NOT NULL COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE,
  INDEX `idx_sys_oper_log_bt`(`business_type` ASC) USING BTREE,
  INDEX `idx_sys_oper_log_s`(`status` ASC) USING BTREE,
  INDEX `idx_sys_oper_log_ot`(`oper_time` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (-1270012023, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2023-08-21 09:29:08', 'admin', '2023-12-04 15:41:46', '超级管理员');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `dept_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '部门名称',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户昵称',
  `user_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'sys_user' COMMENT '用户类型（sys_user系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (-1270012023, NULL, 100, 'admin', 'admin', 'sys_user', 'admin1@qq.com', '18888888888', '0', 'public-images/2023-11/274a0281b6ca4300811137a5c2a378201699840137405.jpg', '$2a$10$vLeGkYLYpSejbhggdM/uIOnzpUv3H3A3hmgcsTc7XZBAZbasNsz62', '0', '0', '', NULL, 'admin', NULL, '', NULL, '');
INSERT INTO `sys_user` VALUES (1699659326722879482, NULL, 100, 'peak', '李四', 'sys_user', 'crazyLionLi@qq.com', '15666666666', '1', '', '$2a$10$8hHAhBewqf56eU0AZjRXFuUgBUQkPwbD1dPa3SKViccLOgw0KLsle', '1', '0', '127.0.0.1', '2023-08-21 09:29:08', 'admin', '2023-08-21 09:29:08', '', NULL, '测试员');
INSERT INTO `sys_user` VALUES (1702501969861488641, NULL, 100, 'zhang', 'zhang', 'sys_user', '2322@vv.com', '13333333333', '0', '', '$2a$10$oGFYyddgZtnnqbMxgb6WxerYl.0pFX4DBF/yHy1dF3IJB4W9A6V8K', '0', '0', '', NULL, 'admin', NULL, '', NULL, '2112');
INSERT INTO `sys_user` VALUES (1711216737640198145, NULL, 100, 'peilinghai', '裴灵海', 'sys_user', 'peilinghai@yunzhisec.com', '18666666666', '0', '', '$2a$10$BCyTar76VF2OFvDteTmEoO7Nt6nsd//4wEKZaJ6V8rAaWzXo/Bsay', '0', '0', '', NULL, 'admin', NULL, '', NULL, '');
INSERT INTO `sys_user` VALUES (1712345661568921602, NULL, 100, 'haoxuyang', '郝旭阳', 'sys_user', '1607842677@qq.com', '15226121952', '0', '', '$2a$10$qJLA9YVBf8Pe.UC/n/fjbuTXomJ2IyzLb/dApISD.ZRVeqe4h86HW', '0', '0', '', NULL, 'admin', NULL, '', NULL, '');
INSERT INTO `sys_user` VALUES (1712748213787635714, NULL, 100, 'weibaoting', '魏保停', 'sys_user', '11@qq.com', '15936727505', '0', '', '$2a$10$qJLA9YVBf8Pe.UC/n/fjbuTXomJ2IyzLb/dApISD.ZRVeqe4h86HW', '0', '0', '', NULL, 'admin', NULL, '', NULL, '');
INSERT INTO `sys_user` VALUES (1715571628529627138, NULL, 101, 'yueshaoyue', '岳绍月', 'sys_user', 'yueshaoyue@yunzhisec.com', '16662284807', '1', '', '$2a$10$g0JUQ6q6EPqgier/wiQJz.pUuCgSG53ag.hfI8D/wbguqZZ.tAey6', '0', '0', '', NULL, 'admin', NULL, '', NULL, '测试的测试数据');
INSERT INTO `sys_user` VALUES (1725770174155403265, NULL, 1716333355760300034, 'gaoxing', '高兴', 'sys_user', 'gaoxing@yunzhisec.com', '15503771511', '0', '', '$2a$10$TOrnnfXhXRX0pPWpBwGE7OJep9SxGB0VoWY6NyasggmTKli44p/rS', '0', '0', '', NULL, 'admin', NULL, '', NULL, '');
INSERT INTO `sys_user` VALUES (1749959513386987522, NULL, 103, 'yyAdmin', '濮阳第三方登录', 'sys_user', 'yyAdmin@qq.com', '15900000000', '0', '', '$2a$10$ZtKaWjqdq6FbzSL.wYMY1OpH0AEGOJ4A7WsH0ezLHhJP5R4xpWXAW', '0', '0', '', NULL, 'admin', NULL, '', NULL, '濮阳第三方登录');
INSERT INTO `sys_user` VALUES (1780052019218690049, NULL, 1714169948743348226, 'zhangsan', '张三', 'sys_user', '16078427@qq.com', '18246549488', '0', '', '$2a$10$JQGXCUMXXkpo0SNto0A3k.TqOcU/WxJe5YeiWEqxYSQy2NEsX9DWS', '0', '0', '', NULL, 'zhangsan', NULL, '', NULL, '');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (-1270012023, 1715574003046756359);

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log`  (
  `branch_id` bigint NOT NULL COMMENT 'branch transaction id',
  `xid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'global transaction id',
  `context` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'undo_log context,such as serialization',
  `rollback_info` longblob NOT NULL COMMENT 'rollback info',
  `log_status` int NOT NULL COMMENT '0:normal status,1:defense status',
  `log_created` datetime(6) NOT NULL COMMENT 'create datetime',
  `log_modified` datetime(6) NOT NULL COMMENT 'modify datetime',
  UNIQUE INDEX `ux_undo_log`(`xid` ASC, `branch_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'AT transaction mode undo table' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of undo_log
-- ----------------------------

-- ----------------------------
-- Table structure for xxl_job_group
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_group`;
CREATE TABLE `xxl_job_group`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '执行器AppName',
  `title` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '执行器名称',
  `address_type` tinyint NOT NULL DEFAULT 0 COMMENT '执行器地址类型：0=自动注册、1=手动录入',
  `address_list` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '执行器地址列表，多地址逗号分隔',
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xxl_job_group
-- ----------------------------
INSERT INTO `xxl_job_group` VALUES (1, 'qianxun-job-executor', '示例执行器', 0, NULL, '2024-04-28 14:35:04');

-- ----------------------------
-- Table structure for xxl_job_info
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_info`;
CREATE TABLE `xxl_job_info`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_group` int NOT NULL COMMENT '执行器主键ID',
  `job_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `add_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `author` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '作者',
  `alarm_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '报警邮件',
  `schedule_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'NONE' COMMENT '调度类型',
  `schedule_conf` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '调度配置，值含义取决于调度类型',
  `misfire_strategy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DO_NOTHING' COMMENT '调度过期策略',
  `executor_route_strategy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器路由策略',
  `executor_handler` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务参数',
  `executor_block_strategy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '阻塞处理策略',
  `executor_timeout` int NOT NULL DEFAULT 0 COMMENT '任务执行超时时间，单位秒',
  `executor_fail_retry_count` int NOT NULL DEFAULT 0 COMMENT '失败重试次数',
  `glue_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'GLUE备注',
  `glue_updatetime` datetime NULL DEFAULT NULL COMMENT 'GLUE更新时间',
  `child_jobid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子任务ID，多个逗号分隔',
  `trigger_status` tinyint NOT NULL DEFAULT 0 COMMENT '调度状态：0-停止，1-运行',
  `trigger_last_time` bigint NOT NULL DEFAULT 0 COMMENT '上次调度时间',
  `trigger_next_time` bigint NOT NULL DEFAULT 0 COMMENT '下次调度时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xxl_job_info
-- ----------------------------

-- ----------------------------
-- Table structure for xxl_job_lock
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_lock`;
CREATE TABLE `xxl_job_lock`  (
  `lock_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '锁名称',
  PRIMARY KEY (`lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xxl_job_lock
-- ----------------------------
INSERT INTO `xxl_job_lock` VALUES ('schedule_lock');

-- ----------------------------
-- Table structure for xxl_job_log
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_log`;
CREATE TABLE `xxl_job_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `job_group` int NOT NULL COMMENT '执行器主键ID',
  `job_id` int NOT NULL COMMENT '任务，主键ID',
  `executor_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器地址，本次执行的地址',
  `executor_handler` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务参数',
  `executor_sharding_param` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务分片参数，格式如 1/2',
  `executor_fail_retry_count` int NOT NULL DEFAULT 0 COMMENT '失败重试次数',
  `trigger_time` datetime NULL DEFAULT NULL COMMENT '调度-时间',
  `trigger_code` int NOT NULL COMMENT '调度-结果',
  `trigger_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '调度-日志',
  `handle_time` datetime NULL DEFAULT NULL COMMENT '执行-时间',
  `handle_code` int NOT NULL COMMENT '执行-状态',
  `handle_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '执行-日志',
  `alarm_status` tinyint NOT NULL DEFAULT 0 COMMENT '告警状态：0-默认、1-无需告警、2-告警成功、3-告警失败',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `I_trigger_time`(`trigger_time` ASC) USING BTREE,
  INDEX `I_handle_code`(`handle_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1784406997316087810 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xxl_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for xxl_job_log_report
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_log_report`;
CREATE TABLE `xxl_job_log_report`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `trigger_day` datetime NULL DEFAULT NULL COMMENT '调度-时间',
  `running_count` int NOT NULL DEFAULT 0 COMMENT '运行中-日志数量',
  `suc_count` int NOT NULL DEFAULT 0 COMMENT '执行成功-日志数量',
  `fail_count` int NOT NULL DEFAULT 0 COMMENT '执行失败-日志数量',
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `i_trigger_day`(`trigger_day` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 249 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xxl_job_log_report
-- ----------------------------

-- ----------------------------
-- Table structure for xxl_job_logglue
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_logglue`;
CREATE TABLE `xxl_job_logglue`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_id` int NOT NULL COMMENT '任务，主键ID',
  `glue_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'GLUE备注',
  `add_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xxl_job_logglue
-- ----------------------------

-- ----------------------------
-- Table structure for xxl_job_registry
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_registry`;
CREATE TABLE `xxl_job_registry`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `registry_group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `registry_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `registry_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `i_g_k_v`(`registry_group` ASC, `registry_key` ASC, `registry_value` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1923 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xxl_job_registry
-- ----------------------------

-- ----------------------------
-- Table structure for xxl_job_user
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_user`;
CREATE TABLE `xxl_job_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `role` tinyint NOT NULL COMMENT '角色：0-普通用户、1-管理员',
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限：执行器ID列表，多个逗号分割',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `i_username`(`username` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xxl_job_user
-- ----------------------------
INSERT INTO `xxl_job_user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
