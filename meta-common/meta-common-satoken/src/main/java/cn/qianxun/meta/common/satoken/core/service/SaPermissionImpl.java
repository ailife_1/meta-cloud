package cn.qianxun.meta.common.satoken.core.service;

import cn.dev33.satoken.stp.StpInterface;
import cn.qianxun.meta.common.core.enums.UserType;
import cn.qianxun.meta.common.satoken.enums.LoginType;
import cn.qianxun.meta.common.satoken.utils.LoginHelper;
import cn.qianxun.meta.common.core.domain.LoginUser;
import cn.qianxun.meta.common.core.utils.string.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * sa-token 权限管理实现类
 *
 * @author fuzhilin
 */
public class SaPermissionImpl implements StpInterface {

    /**
     * 获取菜单权限列表
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        LoginUser loginUser = null;
        //登录环境隔离
        if (LoginType.LOGIN.getCode().equals(loginType)) {
            loginUser = LoginHelper.getLoginUser();
        } else if (LoginType.APP_ID.getCode().equals(loginType)) {
            loginUser = LoginHelper.getAppIdLoginUser();
        }
        if (StringUtils.isNull(loginUser)) {
            return new ArrayList<>();
        }
        UserType userType = UserType.getUserType(loginUser.getUserType());
        if (userType == UserType.SYS_USER) {
            return new ArrayList<>(loginUser.getMenuPermission());
        } else if (userType == UserType.APP_USER) {
            // 其他端 自行根据业务编写
        } else if (userType == UserType.APP_ID) {
            return new ArrayList<>(loginUser.getAppIdPermission());
        }
        return new ArrayList<>();
    }

    /**
     * 获取角色权限列表
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        LoginUser loginUser = null;
        //登录环境隔离
        if (LoginType.LOGIN.getCode().equals(loginType)) {
            loginUser = LoginHelper.getLoginUser();
        } else if (LoginType.APP_ID.getCode().equals(loginType)) {
            loginUser = LoginHelper.getAppIdLoginUser();
        }
        if (StringUtils.isNull(loginUser)) {
            return new ArrayList<>();
        }
        UserType userType = UserType.getUserType(loginUser.getUserType());
        if (userType == UserType.SYS_USER) {
            return new ArrayList<>(loginUser.getRolePermission());
        } else if (userType == UserType.APP_USER) {
            // 其他端 自行根据业务编写
        }
        return new ArrayList<>();
    }
}
