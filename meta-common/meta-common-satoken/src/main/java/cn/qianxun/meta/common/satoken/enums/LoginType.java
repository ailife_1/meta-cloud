package cn.qianxun.meta.common.satoken.enums;

import lombok.Getter;

/**
 * @Author fuzhilin
 * @Date 2023/9/13 14:18
 * @Description 系统登录类型
 */
@Getter
public enum LoginType {
    LOGIN("login", "默认"),
    APP_ID("appId", "APP_ID");

    private final String code;
    private final String info;

    LoginType(String code, String info) {
        this.code = code;
        this.info = info;
    }

}
