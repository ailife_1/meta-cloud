package cn.qianxun.meta.common.satoken.config;

import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.dao.SaTokenDao;
import cn.dev33.satoken.jwt.StpLogicJwtForSimple;
import cn.dev33.satoken.stp.StpInterface;
import cn.dev33.satoken.stp.StpLogic;
import cn.dev33.satoken.stp.StpUtil;
import cn.qianxun.meta.common.satoken.utils.StpAppIdUtil;
import cn.qianxun.meta.common.satoken.core.dao.PlusSaTokenDao;
import cn.qianxun.meta.common.satoken.core.service.SaPermissionImpl;
import cn.qianxun.meta.common.satoken.enums.LoginType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * Sa-Token 配置
 *
 * @author Lion Li
 */
@AutoConfiguration
public class SaTokenConfiguration {

    @Bean
    public StpLogic getStpLogicJwt() {
        return new StpLogicJwtForSimple();
    }

    /**
     * 权限接口实现(使用bean注入方便用户替换)
     */
    @Bean
    public StpInterface stpInterface() {
        return new SaPermissionImpl();
    }

    /**
     * 自定义dao层存储
     */
    @Bean
    public SaTokenDao saTokenDao() {
        return new PlusSaTokenDao();
    }

    /**
     * 在多账户模式中重写 saTokenConfig
     *
     * @param saTokenConfig saTokenConfig
     */
    @Autowired
    public void setSaTokenConfig(SaTokenConfig saTokenConfig) {
        StpUtil.stpLogic.setConfig(saTokenConfig);
        StpAppIdUtil.stpLogic.setConfig(saTokenConfig);
    }

    /**
     * 在多账户模式中集成 jwt
     */
    @Bean
    public void setStpAppIdLogicJwt() {
        StpAppIdUtil.setStpLogic(new StpLogicJwtForSimple(LoginType.APP_ID.getCode()));
    }

}
