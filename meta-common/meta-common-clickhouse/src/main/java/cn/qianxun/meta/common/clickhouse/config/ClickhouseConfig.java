package cn.qianxun.meta.common.clickhouse.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Classname ClickhouseConfig
 * @Description TODO
 * @Date 2021/9/8 16:15
 * @Created by fuzhilin
 */
@Data
@Component
@ConfigurationProperties(prefix = "spring.datasource.click")
public class ClickhouseConfig {
    private int socketTimeout;
    private int connectionTimeout;
    private String url;
    private String username;
    private String password;
}
