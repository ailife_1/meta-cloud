package cn.qianxun.meta.common.minio.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Classname MinIoProperties
 * @Description minio 参数类
 * @Date 2021/8/23 15:17
 * @Created by fuzhilin
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "spring.minio")
public class MinIoProperties {

    /**
     * minio地址+端口号
     */
    private String url;

    /**
     * minio地址+端口号
     */
    private int port;


    private Boolean secure;
    /**
     * minio用户名
     */
    private String accessKey;

    /**
     * minio密码
     */
    private String secretKey;

    /**
     * 文件桶的名称
     */
    private String bucketName;

    /**
     * 完整的minio路径
     */
    private String intactUrl;

    /**
     * 文件白名单
     */
    private String whiteTypeList;
}

