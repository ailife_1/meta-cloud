package cn.qianxun.meta.common.web.loadbalancer;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClients;

/**
 * @Author fuzhilin
 * @Date 2023/10/8 10:06
 * @Description
 */
@LoadBalancerClients(defaultConfiguration = MyLoadBalancerConfig.class)
public class MyLoadBalancerClients {
}
