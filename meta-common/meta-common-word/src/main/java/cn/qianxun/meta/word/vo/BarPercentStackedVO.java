package cn.qianxun.meta.word.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2024/4/14 21:33
 */
@Data
public class BarPercentStackedVO {
    /**
     * 统计图标题
     */
    private String title;

    /**
     * 类别
     */
    private List<String> series;

    /**
     * 统计图类型(参考：ChartType)
     */
    private Integer chartType;

    /**
     * 统计图字段
     * new String[]{"AW Category 1", "AW Category 2"};
     * 每个柱形图下面的名字
     */
    private String[] categories;

    /**
     * 柱形图数量
     * new double[]{1, 2}
     */
    private double[][] values;

   /* *//**
     * 数据
     *//*
    private List<BarPercentStackedData> dataList;

    @Data
    public static class BarPercentStackedData {

        *//**
         * 统计图字段
         *//*
        private String[] categories;

        *//**
         * 柱形图数量
         *//*
        private double[] values;

    }*/
}
