package cn.qianxun.meta.word.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author fuzhilin
 * @Date 2024/4/18 0:16 zhi'd
 */
@Data
@Accessors(chain = true)
public class MergeTableParamVO {

    /**
     * 斜线类型 参考：BorderType 不设置值则不添加斜线
     */
    private int borderType;

    /**
     * 开始行
     */
    private int startLine;

    /**
     * 开始列
     */
    private int startColumn;

    /**
     * 结束行
     */
    private int endLine;

    /**
     * 结束列
     */
    private int endColumn;
}
