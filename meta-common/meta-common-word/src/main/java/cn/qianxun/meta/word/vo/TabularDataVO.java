package cn.qianxun.meta.word.vo;


import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2024/4/14 0:08
 */
@Data
public class TabularDataVO {
    /**
     * 表格第一列的名称
     */
    private String firstName;

    /**
     * 数据
     */
    private List<TableBodyData> tableBodyDataList;
}
