package cn.qianxun.meta.word.replace;

import com.aspose.words.*;
import lombok.Data;
import lombok.experimental.Accessors;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static cn.qianxun.meta.word.util.AsposeUtil.judgeLicense;


/**
 * @Author fuzhilin
 * @Date 2024/4/17 22:54
 */
public class Test1 {
    @Data
    @Accessors(chain = true)
    public static class tt {

        private int borderType;

        //开始行
        private int startLine;

        //开始列
        private int startColumn;

        //结束行
        private int endLine;

        //结束列
        private int endColumn;
    }

    public static void hh(Document doc) throws Exception {

        List<tt> tts = new ArrayList<>();
        tts.add(new tt().setStartLine(0).setStartColumn(0).setEndLine(1).setEndColumn(0));
        tts.add(new tt().setStartLine(0).setStartColumn(1).setEndLine(0).setEndColumn(2));

// Retrieve the first table in the body of the first section.
        Table table = doc.getFirstSection().getBody().getTables().get(0);

        for (tt tt : tts) {
            Cell cellStartRange = table.getRows().get(tt.getStartLine()).getCells().get(tt.getStartColumn());
            Cell cellEndRange = table.getRows().get(tt.getEndLine()).getCells().get(tt.getEndColumn());
            MergeCells(cellStartRange, cellEndRange);
            Border byBorderType1 = cellStartRange.getCellFormat().getBorders().getByBorderType(BorderType.DIAGONAL_UP);
            byBorderType1.setColor(Color.black);
            byBorderType1.setLineWidth(1);
        }

// We want to merge the range of cells found inbetween these two cells.
// Merge all the cells between the two specified cells into one.

// Save the document.
        doc.save("D://saveFile//合并//hebing.docx");
    }

    public static void MergeCells(Cell startCell, Cell endCell) {
        Table parentTable = startCell.getParentRow().getParentTable();

        // Find the row and cell indices for the start and end cell.
        Point startCellPos = new Point(startCell.getParentRow().indexOf(startCell), parentTable.indexOf(startCell.getParentRow()));
        Point endCellPos = new Point(endCell.getParentRow().indexOf(endCell), parentTable.indexOf(endCell.getParentRow()));
        // Create the range of cells to be merged based off these indices. Inverse each index if the end cell if before the start cell.
        Rectangle mergeRange = new Rectangle(Math.min(startCellPos.x, endCellPos.x), Math.min(startCellPos.y, endCellPos.y), Math.abs(endCellPos.x - startCellPos.x) + 1, Math.abs(endCellPos.y - startCellPos.y) + 1);
        for (Row row : parentTable.getRows()) {
            for (Cell cell : row.getCells()) {
                Point currentPos = new Point(row.indexOf(cell), parentTable.indexOf(row));

                // Check if the current cell is inside our merge range then merge it.
                if (mergeRange.contains(currentPos)) {
                    if (currentPos.x == mergeRange.x) {
                        cell.getCellFormat().setHorizontalMerge(CellMerge.FIRST);
                    } else {
                        cell.getCellFormat().setHorizontalMerge(CellMerge.PREVIOUS);
                    }
                    if (currentPos.y == mergeRange.y) {
                        cell.getCellFormat().setVerticalMerge(CellMerge.FIRST);
                    } else {
                        cell.getCellFormat().setVerticalMerge(CellMerge.PREVIOUS);
                    }
                }
            }
        }

    }

    public static void main(String[] args) throws Exception {
        if (!judgeLicense()) {
            System.out.println("license错误");
        }

        try {
            // 没有基于模板创建word
            Document doc = new Document();
            // 建立DocumentBuilder物件
            DocumentBuilder builder = new DocumentBuilder(doc);
            // 设置纸张布局
            builder.getPageSetup().setPaperSize(PaperSize.LETTER);
            builder.getRowFormat().getBorders().setLineStyle(LineStyle.THICK);
            builder.getRowFormat().setHeightRule(HeightRule.AUTO);
            // 一行间距1.0等于12点。所以1.5行=18点
            builder.getParagraphFormat().setLineSpacing(18);    // 设置行间距

            setZzt(builder);


            doc.save("D://saveFile//test//WorkingWithCharts.DefaultOptionsForDataLabels.docx");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 柱状图
     *
     * @param builder
     */
    private static void setZzt(DocumentBuilder builder) {
        // 类别
        String[] categories = new String[]
                {
                        "类别1", "类别2", "类别3"
                };
        // 值
        double[] values = new double[]
                {
                        100, 50, 300
                };
        String title = "三大类类占比";
        addPieChart(builder, ChartType.COLUMN, title, categories, values);
//        addPieChart(builder, ChartType.AREA, title, categories, values);
        }

        /**
         * 加载统计图
         *
         * @param builder
         * @param chatType   统计图类型
         * @param title      统计图标题
         * @param categories 统计图字段
         * @param values     统计图字段数据
         */
        private static void addPieChart (DocumentBuilder builder,int chatType, String title, String[]categories,
        double[] values){
            builder.getParagraphFormat().setFirstLineIndent(0);
            builder.writeln("");
            try {
                var shape = builder.insertChart(chatType, 432, 252);
                shape.setVerticalAlignment(VerticalAlignment.CENTER);
                shape.setHorizontalAlignment(HorizontalAlignment.CENTER);
                shape.setAllowOverlap(false);   // 不覆盖
                Chart chart = shape.getChart();
                ChartSeriesCollection seriesCollection = chart.getSeries();
                seriesCollection.clear();   // 清除默认
                chart.getLegend().setPosition(LegendPosition.BOTTOM);   // 显示在下面
                // 添加数据
                var ct = chart.getTitle();
                ct.setOverlay(false);   // 不覆盖
                var cl = chart.getLegend();
                cl.setOverlay(false);   // 不覆盖
                var chatSeries = seriesCollection.add(title, categories, values);
                // 设置数据显示
                ChartDataLabelCollection dataLabelCollection = chatSeries.getDataLabels();
                for (int i = 0; i < dataLabelCollection.getCount(); i++) {
                    ChartDataLabel chartDataLabel = dataLabelCollection.get(i);
                    chartDataLabel.setShowLegendKey(true);
                    chartDataLabel.setShowLeaderLines(false);
                    chartDataLabel.setShowCategoryName(true);
                    chartDataLabel.setShowPercentage(true);
                    chartDataLabel.setShowSeriesName(false);
                    chartDataLabel.setShowValue(true);
                    chartDataLabel.setSeparator("\r\n");
                    chartDataLabel.getNumberFormat().setFormatCode("0.00%");    // 设置数字格式
                }
                chatSeries.setSmooth(true);
                builder.writeln("");
                builder.getParagraphFormat().setFirstLineIndent(2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
