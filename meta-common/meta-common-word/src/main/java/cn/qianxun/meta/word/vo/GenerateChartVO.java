package cn.qianxun.meta.word.vo;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2021/11/29 0029 17:16
 * @Description Aspose生成图表VO
 */
@Data
public class GenerateChartVO {

    /**
     * 统计图名称
     */
    private String name;

    /**
     * 统计图宽
     */
    private double width;

    /**
     * 统计图高
     */
    private double height;

    /**
     * 数据是否要加百分号
     */
    private Boolean dataPercent=false;

    /**
     * xy轴是否要加百分号
     */
    private Boolean coordinatePercent=false;

    /**
     * 统计图标题
     */
    private String[] title;

    /**
     * 统计图类型(参考：ChartType)
     */
    private Integer chartType;

    /**
     * 统计图字段
     * new String[]{"AW Category 1", "AW Category 2"};
     * 每个柱形图下面的名字
     */
    private String[] categories;

    /**
     * 柱形图数量
     * new double[]{1, 2}
     */
    private double[][] values;
}
