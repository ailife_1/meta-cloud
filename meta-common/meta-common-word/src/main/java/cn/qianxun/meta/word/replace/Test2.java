package cn.qianxun.meta.word.replace;

import cn.qianxun.meta.word.util.AsposeUtil;
import com.aspose.words.Shape;
import com.aspose.words.*;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2024/4/17 22:54
 */
public class Test2 {
    public static void main(String[] args) throws Exception {
        AsposeUtil.registerWord();
        //通过Document空的构造函数，创建一个新的空的文档,如果有参数则是根据模板加载文档。
        Document doc = new Document("D://12312.docx");
        //动态增加内容 获取光标，这里面的`builder`相当于一个画笔，提前给他规定样式，然后他就能根据你的要求画出你想画的Word。这里的画笔使用的是就近原则，当上面没有定义了builder的时候，会使用默认的格式，当上面定义了某个格式的时候，使用最近的一个（即最后一个改变的样式）
        DocumentBuilder builder = new DocumentBuilder(doc);
        //将光标移动到文档末端
        builder.moveToDocumentEnd();
        //分页
        builder.insertBreak(BreakType.PAGE_BREAK);
        //换行
        builder.insertParagraph();

        String[] label = new String[]{"男生人数", "女生人数", "未知人数"};
        double[] value = new double[]{75.0, 21.5, 3.6};
        //使用率的圆环图
        addDrawDoughnutChart(builder, ChartType.DOUGHNUT, "男女比例", true, label, value, true, new Color[]{new Color(147, 205, 221), new Color(250, 192, 144), new Color(230, 224, 236)});
        //使用数值的圆环图
        builder.insertParagraph();
        addDrawDoughnutChart(builder, ChartType.DOUGHNUT, "男女比例", false, label, value, true, new Color[]{new Color(147, 205, 221), new Color(250, 192, 144), new Color(230, 224, 236)});
        //使用默认颜色的圆环图
        builder.insertParagraph();
        addDrawDoughnutChart(builder, ChartType.DOUGHNUT, "男女比例", false, label, value, false, new Color[]{new Color(147, 205, 221), new Color(250, 192, 144), new Color(230, 224, 236)});

        //文件命名
        String fileName = "D://AsposeWord1" + new SimpleDateFormat("MMddHHmmss").format(new Date()) + ".docx";
        //文件保存
        doc.save(fileName);
    }


    /**
     * 插入 圆环图或者饼图 ChartType.DOUGHNUT ChartType.PIE
     * 其他图形在该方法中不适用
     */
    public static void addDrawDoughnutChart(DocumentBuilder builder, Integer ChartType, String title, Boolean showPercent, String[] label, double[] value, Boolean colors, Color[] customColors) throws Exception {
        // 创建图形
        Shape shape = builder.insertChart(ChartType, 432, 252);
        // 设置图表形状的其他属性，如WrapSide、WrapType、VerticalAlignment等
        shape.setWrapSide(WrapSide.BOTH);
        shape.setWrapType(WrapType.TOP_BOTTOM);
        shape.setVerticalAlignment(VerticalAlignment.CENTER);
        shape.setHorizontalAlignment(HorizontalAlignment.CENTER);
        shape.setAllowOverlap(false);
        shape.getStroke().setColor(new Color(221, 221, 221)); // 设置线条颜色
        shape.getFill().setColor(new Color(222, 235, 247)); // 设置填充颜色
        Chart chart = shape.getChart();
        ChartSeriesCollection seriesColl = chart.getSeries();
        seriesColl.clear();
        //向形状中添加数据
        ChartSeries chartSeries = seriesColl.add(title, label, value);
        if (colors) {
            for (int i = 0; i < label.length; i++) {
                // 设置系列的填充颜色为自定义颜色数组中的对应颜色
                chartSeries.getDataPoints().get(i).getFormat().getFill().setForeColor(customColors[i]);
            }
        }
        // 用于启用或禁用图表系列的数据标签
        seriesColl.get(0).hasDataLabels(true);
        // 根据参数 showPercent 决定是否显示百分比，如果为 true，则显示百分比；否则，显示Y轴值。
        ChartDataLabelCollection dataLabelCollection = seriesColl.get(0).getDataLabels();

        if (showPercent) {
            dataLabelCollection.setShowPercentage(true);// 则显示百分比
        } else {
            dataLabelCollection.setShowValue(true);// 显示Y轴值。
        }
        // 设置数据标签的分隔符为换行符，即每个标签显示在一行。
        dataLabelCollection.setSeparator("\r\n");
        dataLabelCollection.setShowLegendKey(true);   // 是否显示图例
        dataLabelCollection.setShowCategoryName(true);//是否显示系列的名称在系列旁边
        dataLabelCollection.setShowLeaderLines(false);
        dataLabelCollection.setShowSeriesName(false);//是否将图形标题显示在系列名称中
        dataLabelCollection.setShowBubbleSize(false);
        // 设置段落格式的首行缩进为5个磅
        builder.getParagraphFormat().setFirstLineIndent(5);
    }
}
