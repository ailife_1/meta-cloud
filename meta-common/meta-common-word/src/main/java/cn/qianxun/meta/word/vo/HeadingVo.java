package cn.qianxun.meta.word.vo;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2024/3/12 15:02
 **/
@Data
public class HeadingVo {

    private int num;

    private String name;

    private String id;

    private String pid;

}
