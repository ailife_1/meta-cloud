package cn.qianxun.meta.word.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2024/4/14 0:09
 */
@Data
public class TableBodyData {

    /**
     * 第二列名称
     */
    private String name;

    /**
     * 数据
     */
    private List<String> data;

}
