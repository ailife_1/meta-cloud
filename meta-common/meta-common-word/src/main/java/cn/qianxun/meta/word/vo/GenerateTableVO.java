package cn.qianxun.meta.word.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2021/12/2 0002 10:05
 * @Description Aspose生成表格VO
 */
@Data
public class GenerateTableVO {
    /**
     * 统计图宽
     */
    private double width;

    /**
     * 统计图高
     */
    private double height;

    /**
     * 表格表头
     */
    private List<String> title;

    /**
     * 表格数据
     */
    private List<List<String>> values;

    /**
     * 合并单元格数据
     */
    private List<MergeTableParamVO> mergeTableParamList;
}
