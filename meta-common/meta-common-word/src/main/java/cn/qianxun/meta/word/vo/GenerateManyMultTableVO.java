package cn.qianxun.meta.word.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2021/12/2 0002 10:05
 * @Description 创建多维度的表格
 */
@Data
public class GenerateManyMultTableVO {
    /**
     * 表格表头
     */
    private List<String> title;

    /**
     * 表格数据
     */
    private List<TabularDataVO> values;
}
