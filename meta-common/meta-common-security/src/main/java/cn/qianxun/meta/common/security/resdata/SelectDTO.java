package cn.qianxun.meta.common.security.resdata;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;


@Data
public class SelectDTO {
    @Schema(description = "id")
    private String value;

    @Schema(description = "name")
    private String label;

}
