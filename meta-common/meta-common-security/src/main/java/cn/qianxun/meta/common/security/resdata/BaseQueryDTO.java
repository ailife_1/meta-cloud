package cn.qianxun.meta.common.security.resdata;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;


/**
 * @author fuzhilin
 */
@Data
public class BaseQueryDTO {
    /**
     * 页码
     */
    @Schema(description = "页码", requiredMode = Schema.RequiredMode.REQUIRED, example = "1")
    private Integer pageNum = 1;

    /**
     * 页面显示长度
     */
    @Schema(description = "页面显示长度", requiredMode = Schema.RequiredMode.REQUIRED, example = "10")
    private Integer pageSize = 10;


}
