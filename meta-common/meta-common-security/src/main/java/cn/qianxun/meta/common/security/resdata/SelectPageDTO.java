package cn.qianxun.meta.common.security.resdata;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;


@Data
public class SelectPageDTO {

    @Schema(description = "搜索字段")
    private String field;

    @Schema(description = "页码")
    private Integer pageNum;

    @Schema(description = "页面显示长度")
    private Integer pageSize;


}
