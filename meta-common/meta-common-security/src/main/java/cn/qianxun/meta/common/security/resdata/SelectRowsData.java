package cn.qianxun.meta.common.security.resdata;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
public class SelectRowsData<T> {
    List<T> list = new ArrayList<>();

    Integer totalRow;

    /*Integer pageNumber;

    Integer totalPage;

    Integer pageSize;*/

    public SelectRowsData() {
    }
}
