package cn.qianxun.meta.common.log.changeField.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author fuzhilin
 * @Date 2023/9/5 10:06
 * @Description
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CompareField {
    /**
     * 字段描述
     */
    String value();

    /**
     * 字典类型
     */
    String dictType() default "";

    /**
     * 自定义类型
     * 例如：字符串 1:正确#2:错误#3:异常
     */
    String customType() default "";


}
