package cn.qianxun.meta.common.log.changeField.util;

import cn.qianxun.meta.common.log.changeField.annotation.ChangeLog;
import org.aspectj.lang.JoinPoint;

/**
 * 解析接口
 *
 * @author zhang
 */
public interface ContentParser {

    /**
     * 获取信息返回查询出的对象
     *
     * @param joinPoint    查询条件的参数
     * @param changeLog 注解
     * @return 获得的结果
     */
    Object getResult(JoinPoint joinPoint, ChangeLog changeLog);


}

