package cn.qianxun.meta.common.log.changeField.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 8:56
 * @Description 安全值响应参数VO
 */
@Data

public class SafeValueLogVO {

    /**
     * id
     */
    private Long id;


    /**
     * 安全值分数
     */
    private String score;

    /**
     * 资产名称
     */
    private String assetName;

    /**
     * 业务类型 0总安全值 1网站 2IP 3端口 4信息系统
     */
    private String businessType;

    /**
     * 业务类id
     */
    private String businessId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 老安全值
     */
    private String oldScore;
}
