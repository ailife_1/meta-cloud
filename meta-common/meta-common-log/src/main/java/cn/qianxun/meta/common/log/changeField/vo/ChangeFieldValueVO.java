package cn.qianxun.meta.common.log.changeField.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2023/9/5 10:11
 * @Description 字段值改变值响应参数VO
 */
@Data
public class ChangeFieldValueVO {

    /**
     * 对象id
     */
    private String objId;

    /**
     * 所属模块
     */
    private String module;

    /**
     * 字段名称
     */
    private String filedName;

    /**
     * 旧值
     */
    private Object oldValue;

    /**
     * 新值
     */
    private Object newValue;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 操作时间
     */
    private Date operationTime;
}
