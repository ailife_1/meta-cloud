package cn.qianxun.meta.common.log.changeField.annotation;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.common.log.changeField.util.DefaultContentParse;

import java.lang.annotation.*;

/**
 * @Author fuzhilin
 * @Date 2023/9/5 14:06
 * @Description
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ChangeLog {

    /**
     * 模块名称
     *
     * @return String
     */
    String moduleName() default "";

    /**
     * 获取编辑信息的解析类，目前为使用id获取，复杂的解析需要自己实现，默认不填写则使用默认解析类
     *
     * @return Class
     */
    Class parseclass() default DefaultContentParse.class;


    /**
     * 主键id字段名称
     *
     * @return String
     */
    String idName() default "id";

    /**
     * 查询数据库所调用的class文件
     *
     * @return Class
     */
    Class serviceclass() default IService.class;

    /**
     * id的类型
     */
    Class idType() default Integer.class;


}
