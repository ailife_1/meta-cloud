package cn.qianxun.meta.common.log.event;

import cn.qianxun.meta.common.log.changeField.vo.ChangeFieldValueVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/6 9:12
 * @Description
 */

@Data
@Accessors(chain = true)
public class ChangeFieldValueLogEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字段变化记录
     */
    private List<ChangeFieldValueVO> fieldValueList;
}
