package cn.qianxun.meta.common.log.event;

import cn.qianxun.meta.common.log.changeField.vo.SafeValueLogVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 8:56
 * @Description
 */
@Accessors(chain = true)
@Data

public class SafeValueLogEvent implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 安全值变化记录
     */
    private List<SafeValueLogVO> safeValueLogList;
}
