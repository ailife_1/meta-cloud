package cn.qianxun.meta.common.mybatis.service;

import cn.qianxun.meta.datascope.api.feign.ISysUserDataScopeProvider;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 数据权限 实现
 * <p>
 * 注意: 此Service内不允许调用标注`数据权限`注解的方法
 * 例如: deptMapper.selectList 此 selectList 方法标注了`数据权限`注解 会出现循环解析的问题
 *
 * @author Lion Li
 */
@Service("sdss")
public class SysDataScopeService {

    @Resource
    private ISysUserDataScopeProvider sysUserDataScopeProvider;

    public String getRoleCustom(Long roleId) {
        return sysUserDataScopeProvider.getRoleCustom(roleId);
    }

    public String getDeptAndChild(Long deptId) {
        return sysUserDataScopeProvider.getDeptAndChild(deptId);
    }
}
