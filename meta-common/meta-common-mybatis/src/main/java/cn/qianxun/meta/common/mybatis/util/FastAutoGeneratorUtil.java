package cn.qianxun.meta.common.mybatis.util;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.fill.Column;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/21 13:45
 **/
public class FastAutoGeneratorUtil {

    static String url = "jdbc:mysql://127.0.0.1:3306/meta-assets?characterEncoding=UTF-8&useUnicode=true&useSSL=false";
    static String username = "root";
    static String password = "meta@yzxa2023";
    static String author = "fuzhilin";


    public static void main(String[] args) {
        String packageName = "cn.qianxun.meta.assethouse.datasource";
        String outputDir = "D://project";
        generateCode(packageName, outputDir, "house_data_house_priority");
    }


    /**
     * 根据表名生成对应的实体类
     * tableNames:表的名称
     */
    public static void generateCode(String parentPackageName, String outputDir, String... tableNames) {
        System.out.println("--------------------------------生成开始--------------------------------");
        FastAutoGenerator.create(url, username, password)
                // 全局配置
                .globalConfig(builder -> {
                    builder.author(author) // 设置作者
                            .commentDate("yyyy-MM-dd hh:mm:ss")   //注释日期
                            .dateType(DateType.ONLY_DATE)   //定义生成的实体类中日期的类型 TIME_PACK=LocalDateTime;ON
                            .outputDir(outputDir) // 指定输出目录
                            //.enableSpringdoc()
                            //.disableOpenDir() //禁止打开输出目录，默认打开
                    ;
                })
                // 包配置
                .packageConfig(builder -> {
                    builder.parent(parentPackageName)
                            //.moduleName("mp")   //设置模块包名
                            .entity("entity")   //pojo 实体类包名
                            .service("service") //Service 包名
                            .controller("controller");//Controller 包名
                    //  .pathInfo(Collections.singletonMap(OutputFile.mapperXml, System.getProperty("user.dir") + "/src/main/resources/mapper"));    //配置 mapper.xml 路径信息：项目的 resources 目录下
                    //.pathInfo(mapping); // 设置mapperXml生成路径
                })
                // 策略配置
                .strategyConfig(builder -> {
                    builder.addInclude(tableNames) // 设置需要生成的表名
                            .addTablePrefix("") // 设置过滤表前缀 sys_
                            // Entity 策略配置
                            .entityBuilder()
                            .enableLombok() //开启 Lombok
                            .enableFileOverride() // 覆盖已生成文件
                            .naming(NamingStrategy.underline_to_camel)  //数据库表映射到实体的命名策略：下划线转驼峰命
                            .columnNaming(NamingStrategy.underline_to_camel)    //数据库表字段映射到实体的命名策略：下划线转驼峰命
                            // Mapper 策略配置
                            .mapperBuilder().superClass(BaseMapperPlus.class)      //开启 @Mapper 注解

                            .entityBuilder().enableLombok().naming(NamingStrategy.underline_to_camel)  //数据库表映射到实体的命名策略：下划线转驼峰命
                            .columnNaming(NamingStrategy.underline_to_camel)    //数据库表字段映射到实体的命名策略：下划线转驼峰命
                            .addTableFills(
                                    new Column("create_time", FieldFill.INSERT),
                                    new Column("update_time", FieldFill.INSERT_UPDATE)
                            )   //添加表字段填充，"create_time"字段自动填充为插入时间，"modify_time"字段自动填充为插入修改时间
                            //.enableTableFieldAnnotation()       // 开启生成实体时生成字段注解
                            .enableFileOverride() // 覆盖已生成文件
                            .idType(IdType.ASSIGN_ID)
                            .controllerBuilder()
                            .enableRestStyle() //开启生成 @RestController 控制器

                            // Service 策略配置
                            .serviceBuilder()
                            .enableFileOverride() // 覆盖已生成文件
                            .formatServiceFileName("I%sService") //格式化 service 接口文件名称，%s进行匹配表名，如 UserService
                            .formatServiceImplFileName("%sServiceImpl") //格式化 service 实现类文件名称，%s进行匹配表名，如 UserServiceImpl
                            // Controller 策略配置
                            .controllerBuilder()
                            .enableFileOverride() // 覆盖已生成文件
                    ;
                })
                .execute();
        System.out.println("--------------------------------生成结束--------------------------------");
    }
}
