package cn.qianxun.meta.common.mybatis.annotation;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

import java.lang.annotation.*;

/**
 * @Classname EnableCustomConfig
 * @Description 自定义加载类
 * @Created by fuzhilin
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
// 表示通过aop框架暴露该代理对象,AopContext能够访问
@EnableAspectJAutoProxy(exposeProxy = true)
// 指定要扫描的Mapper类的包的路径
@MapperScan("cn.qianxun.**.mapper")
// 开启线程异步执行
@EnableAsync
// 自动加载类
public @interface EnableCustomConfig {

}
