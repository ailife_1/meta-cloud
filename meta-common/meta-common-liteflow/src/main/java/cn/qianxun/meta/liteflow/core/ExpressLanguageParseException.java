package cn.qianxun.meta.liteflow.core;


/**
 * @author : zhangrongyan
 * @date : 2023/1/16 10:49
 */
public class ExpressLanguageParseException extends Exception{
    public ExpressLanguageParseException(String message) {
        super(message);
    }
}