package cn.qianxun.meta.common.core.constant;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/**
 * Feign常量类
 *
 * @author fuzhilin
 */
@Slf4j
@UtilityClass
public class FeignConstant {

    /**
     * 网关
     */
    public final String META_CLOUD_GATEWAY = "meta-gateway";

    /**
     * 系统服务
     */
    public final String META_CLOUD_SYSTEM_USER = "meta-system-user";
    public final String META_CLOUD_SYSTEM = "meta-system";
    public final String META_CLOUD_SYSTEM_LOG = "meta-system-log";


    /**
     * 认证服务
     */
    public final String META_CLOUD_AUTH = "meta-auth";


    /**
     * 资产服务
     */
    public final String META_CLOUD_ASSETS = "meta-assets";

    /**
     * 规则服务
     */
    public final String META_CLOUD_RULE_ENGINE = "meta-rule-engine";
    /**
     * 日志告警服务
     */
    public final String META_CLOUD_SECLOG = "meta-seclog";

    /**
     * 风险监测
     */
    public final String META_CLOUD_RISK_MONITOR = "meta-risk-monitor";

    /**
     * 风险监测
     */
    public final String META_CLOUD_SCAN = "meta-scan-util";

    /**
     * 微服务之间传递的唯一标识
     */
    public static final String META_TRACE_ID = "meta-trace-id";

    /**
     * 日志链路追踪id日志标志
     */
    public static final String LOG_TRACE_ID = "traceId";

    /**
     * feign调用失败日志提醒
     * serviceName 调用服务名称
     * methodName 调用方法名称
     */
   /* public static String FeignExceptionMsg(String serviceName, String methodName) {
        return "远程调用失败：服务名称为:" + serviceName + "--调用方法：" + methodName + "--类名：" + Thread.currentThread().getStackTrace()[1].getClassName() + "--java文件名：" + Thread.currentThread().getStackTrace()[1].getFileName() + "--当前方法名：" + Thread.currentThread().getStackTrace()[1].getMethodName() + "--当前代码是第几行：" + Thread.currentThread().getStackTrace()[1].getLineNumber();
    }*/
    public static String FeignException(String serviceName, String methodName) {
        return "远程调用失败：服务名称为:" + serviceName + "--调用方法：" + methodName + "--请稍后重试...";
    }

    public static void FeignExceptionLog(String serviceName, String methodName) {
        //log.error(FeignExceptionMsg(serviceName,methodName));
        throw new RuntimeException(FeignException(serviceName, methodName));
    }


}
