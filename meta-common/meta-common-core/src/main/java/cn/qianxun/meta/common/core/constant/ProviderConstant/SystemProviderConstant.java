package cn.qianxun.meta.common.core.constant.ProviderConstant;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/21 18:01
 **/
public interface SystemProviderConstant {

    /**
     * 远程调用公共前缀
     */
    String PROVIDER = "/provider";

    String AUTH = "/auth";
    String SYSTEM = "/system";

    /**
     * 版本
     */
    String V1 = "/v1";

    /**
     * 远程调用用戶信息
     */
    String SYS_USER = "/sysuser";
    String SYS_LOG = "/syslog";
    String SYS_XXL_JOB = "/sysXxlJob";
    String SYS_DEPT = "/sysDept";

    String SYS_DICT = "/sysDict";

    String SYS_ROLE = "/sysRole";

    String PROVIDER_AUTH_V1_SYS_USER_getUserInfo = PROVIDER + AUTH + V1 + SYS_USER + "/getUserInfo";

    String PROVIDER_AUTH_V1_SYS_USER_getUserInfoByUserId = PROVIDER + AUTH + V1 + SYS_USER + "/getUserInfoByUserId";

    String PROVIDER_AUTH_V1_SYS_USER_getUserInfoByIds = PROVIDER + AUTH + V1 + SYS_USER + "/getUserInfoByIds";

    String PROVIDER_AUTH_V1_SYS_USER_GET_APP_INFO_BY_APPID = PROVIDER + AUTH + V1 + SYS_USER + "/getAppInfoByAppId";
    String PROVIDER_AUTH_V1_SYS_USER_UPDATE_APPLICATION_LOGIN_NUM = PROVIDER + AUTH + V1 + SYS_USER + "/updateApplicationLoginNum";

    String PROVIDER_AUTH_V1_SYS_LOG_saveLog = PROVIDER + AUTH + V1 + SYS_LOG + "/saveLog";
    String PROVIDER_AUTH_V1_SYS_LOG_saveLogininfor = PROVIDER + AUTH + V1 + SYS_LOG + "/saveLogininfor";
    String PROVIDER_AUTH_V1_SYS_CONFIG_configKey = PROVIDER + AUTH + V1 + SYS_LOG + "/configKey";
    String PROVIDER_AUTH_V1_SYS_USER_getRoleCustom = PROVIDER + AUTH + V1 + SYS_USER + "/getRoleCustom";
    String PROVIDER_AUTH_V1_SYS_USER_getDeptAndChild = PROVIDER + AUTH + V1 + SYS_USER + "/getDeptAndChild";

    String PROVIDER_AUTH_V1_SYS_LOG_SAVE_CHANGE_FIELD_LOG = PROVIDER + AUTH + V1 + SYS_LOG + "/saveChangeFieldLog";
    String PROVIDER_AUTH_V1_SYS_LOG_SELECT_CHANGE_FIELD_LOG_BY_ASSET_ID = PROVIDER + AUTH + V1 + SYS_LOG + "/selectChangeFieldLogByAssetId";
    String PROVIDER_AUTH_V1_SYS_LOG_SAVE_SAFE_VALUE_LOG = PROVIDER + AUTH + V1 + SYS_LOG + "/saveSafeValueLog";
    ;
    String PROVIDER_SYSTEM_V1_SYS_XXL_JOB_ADD_XXL_JOB = PROVIDER + SYSTEM + V1 + SYS_XXL_JOB + "addXxlJob";
    String PROVIDER_SYSTEM_V1_SYS_XXL_JOB_EDIT_XXL_JOB = PROVIDER + SYSTEM + V1 + SYS_XXL_JOB + "editXxlJob";
    String PROVIDER_SYSTEM_V1_SYS_XXL_JOB_TRIGGER_JOB = PROVIDER + SYSTEM + V1 + SYS_XXL_JOB + "triggerJob";
    String PROVIDER_SYSTEM_V1_SYS_XXL_JOB_STOP = PROVIDER + SYSTEM + V1 + SYS_XXL_JOB + "stop";
    String PROVIDER_SYSTEM_V1_SYS_XXL_JOB_START = PROVIDER + SYSTEM + V1 + SYS_XXL_JOB + "start";
    String PROVIDER_SYSTEM_V1_SYS_XXL_JOB_DEL_JOB = PROVIDER + SYSTEM + V1 + SYS_XXL_JOB + "delJob";
    String PROVIDER_SYSTEM_V1_SYS_XXL_JOB_getJobTriggerTime = PROVIDER + SYSTEM + V1 + SYS_XXL_JOB + "getJobTriggerTime";


    /**
     * 查询单位列表(API)
     */
    String PROVIDER_AUTH_V1_SYS_DEPT_selectDeptList = PROVIDER + AUTH + V1 + SYS_DEPT + "/selectDeptList";

    String PROVIDER_AUTH_V1_SYS_DEPT_selectDeptCount = PROVIDER + AUTH + V1 + SYS_DEPT + "/selectDeptCount";
    /**
     * 根据单位名称查询单位信息
     */
    String PROVIDER_AUTH_V1_SYS_DEPT_selectDeptInfoByName = PROVIDER + AUTH + V1 + SYS_DEPT + "/selectDeptInfoByName";
    /**
     * 添加单位(API)
     */
    String PROVIDER_AUTH_V1_SYS_DEPT_insertDept = PROVIDER + AUTH + V1 + SYS_DEPT + "/insertDept";
    /**
     * 删除单位(API)
     */
    String PROVIDER_AUTH_V1_SYS_DEPT_deleteDept = PROVIDER + AUTH + V1 + SYS_DEPT + "/deleteDept";
    /**
     * 单位详情(API)
     */
    String PROVIDER_AUTH_V1_SYS_DEPT_deptDetails = PROVIDER + AUTH + V1 + SYS_DEPT + "/deptDetails";
    /**
     * 修改单位(API)
     */
    String PROVIDER_AUTH_V1_SYS_DEPT_updateDept = PROVIDER + AUTH + V1 + SYS_DEPT + "/updateDept";

    /**
     * 根据参数查询单位信息
     */
    String PROVIDER_AUTH_V1_SYS_DEPT_selectDeptInfoByParam = PROVIDER + AUTH + V1 + SYS_DEPT + "/selectDeptInfoByParam";


    /**
     * 根据字典类型获取字典数据
     */
    String PROVIDER_SYSTEM_V1_SYS_DICT_GET_DICT_DATA_BY_DICT_TYPE = PROVIDER + SYSTEM + V1 + SYS_DICT + "/getDictDataByDictType";

    /**
     * 根据字典类型和字典值查询字典数据信息
     */
    String PROVIDER_SYSTEM_V1_SYS_DICT_GET_DICT_TYPE_AND_VALUE = PROVIDER + SYSTEM + V1 + SYS_DICT + "/getDictTypeAndValue";


    /**
     * 角色详情(API)
     */
    String PROVIDER_SYSTEM_V1_SYS_ROLE_roleDetails = PROVIDER + SYSTEM + V1 + SYS_ROLE + "/roleDetails";

}
