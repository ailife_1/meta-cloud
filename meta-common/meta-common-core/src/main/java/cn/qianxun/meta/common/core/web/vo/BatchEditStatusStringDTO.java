package cn.qianxun.meta.common.core.web.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/16/35
 * @Description
 */
@Schema(description = "批量删除Long值VO",name = "删除Long值VO")
@Data
public class BatchEditStatusStringDTO {
    @Schema(description = "需要修改状态的业务ID集合", requiredMode = Schema.RequiredMode.REQUIRED)
    private List<String> businessIdList;

    @Schema(description = "状态根据具体业务变更" , requiredMode = Schema.RequiredMode.REQUIRED)
    private String status;
}
