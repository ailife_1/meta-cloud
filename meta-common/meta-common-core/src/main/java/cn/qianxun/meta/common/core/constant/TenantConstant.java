package cn.qianxun.meta.common.core.constant;

/**
 * 多租户常量
 *
 * @author fuzhilin
 */
public interface TenantConstant {

    /**
     * header 中租户ID
     */
    String meta_TENANT_ID = "meta-tenant";

}
