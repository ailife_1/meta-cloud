package cn.qianxun.meta.common.core.config.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;


public class WrappingScheduledExecutor extends ScheduledThreadPoolExecutor {
    private static final Logger logger = LoggerFactory.getLogger(WrappingScheduledExecutor.class);

    public WrappingScheduledExecutor(int corePoolSize, ThreadFactory build) {
        super(corePoolSize,build);
    }

    @Override
    public ScheduledFuture scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
        return super.scheduleAtFixedRate(wrapRunnable(command), initialDelay, period, unit);
    }

    @Override
    public ScheduledFuture scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return super.scheduleWithFixedDelay(wrapRunnable(command), initialDelay, delay, unit);
    }

    private Runnable wrapRunnable(Runnable command) {
        return new LogOnExceptionRunnable(command);
    }

    private class LogOnExceptionRunnable implements Runnable {
        private Runnable theRunnable;

        public LogOnExceptionRunnable(Runnable theRunnable) {
            super();
            this.theRunnable = theRunnable;
        }

        @Override
        public void run() {
            try {
                theRunnable.run();
            } catch (Throwable t) {
                logger.error(t.getMessage(), t);
                /**
                 *重要，如果你选择继续向上抛出异常，则在外部必须能够catch住这个异常，否则还是会造成后续任务不会执行
                 * IMPORTANT: if you thrown exception. then in the out, you have to try/catch the exception,
                 * otherwise, the executor will stop
                 */
                // throw new RuntimeException(e);
            }
        }
    }
}
