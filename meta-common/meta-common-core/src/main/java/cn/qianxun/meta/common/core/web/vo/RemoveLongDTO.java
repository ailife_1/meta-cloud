package cn.qianxun.meta.common.core.web.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/16/35
 * @Description
 */
@Schema(description = "批量删除Long值VO",name = "删除Long值VO")
@Data
public class RemoveLongDTO {
    @Schema(description = "需要删除的ID集合", requiredMode = Schema.RequiredMode.REQUIRED)
    private List<Long> removeIdList;
}
