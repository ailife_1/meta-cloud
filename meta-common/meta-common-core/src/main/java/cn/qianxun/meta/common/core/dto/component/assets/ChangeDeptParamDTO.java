package cn.qianxun.meta.common.core.dto.component.assets;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 单位变更规则参数
 * @author fuzhilin
 */
@Data
@Accessors(chain = true)
public class ChangeDeptParamDTO {

    /**
     * 资产ID
     */
    private List<Long> assetIds;

    /**
     * 单位类型(1.建设单位 2.运维单位 3.使用单位)
     */
    private String unitType;

    /**
     * 单位ID
     */
    private Long deptId;

    /**
     * 单位名称
     */
    private String deptName;

    /**
     * 单位联系人
     */
    private String deptLeader;

    /**
     * 单位联系人电话
     */
    private String deptPhone;

    /**
     * 资产类型 1信息系统 2web 3服务组件 4IP设备
     */
    private String assetType;

    /**
     * 覆盖原有值 0否 1是
     */
    private Integer overOriginalValue;

    /**
     * className
     */
    private String className;


}