package cn.qianxun.meta.common.core.dto;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 0023 10:06
 * @Description 状态修改DTO
 */
@Data
public class ChangeStatusDTO {

    /**
     * 业务id
     */
    private Long businessId;

    /**
     * 状态根据具体业务变更
     */
    private String status;
}
