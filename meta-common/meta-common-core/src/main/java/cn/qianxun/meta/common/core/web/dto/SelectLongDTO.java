package cn.qianxun.meta.common.core.web.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/24 9:59
 **/
@Data
public class SelectLongDTO {

    @Schema(description = "ID", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long idLong;
}
