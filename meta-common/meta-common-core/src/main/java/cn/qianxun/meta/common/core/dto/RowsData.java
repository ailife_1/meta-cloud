package cn.qianxun.meta.common.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
public class RowsData<T> {
    List<T> rows = new ArrayList<>();

    Long total;

    Integer page;

    public RowsData() {
    }

    public RowsData(Long total, Integer page) {
        this.total = total;
        this.page = page;
    }
}
