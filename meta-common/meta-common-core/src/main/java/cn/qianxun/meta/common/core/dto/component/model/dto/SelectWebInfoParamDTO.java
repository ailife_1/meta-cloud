package cn.qianxun.meta.common.core.dto.component.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/11/1 11:39
 * @Description 根据参数查询web列表DTO
 */
@Data
public class SelectWebInfoParamDTO {

    /**
     * 资产集合
     */
    private List<String> assets;

    /**
     * 标签ids
     */
    private List<String> webLabs;

    /**
     * 状态码(字典 web_status_code)
     */
    private List<String> statusCode;

    /**
     * 网站类型(字典 website_type)
     */
    private String websiteType;

    /**
     * 查询字段
     */
    private String resultField;

    /**
     * 上次查询结果字段
     */
    private String lastResultField;

    /**
     * 节点
     */
    private String node;

    /**
     * 端口集合
     */
    private List<String> postList;

    /**
     * ip集合
     */
    private List<String> ipList;

    /**
     * className
     */
    private String className;
}