package cn.qianxun.meta.common.core.dto;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/20 0023 16:49
 * @Description LabelAndValue Long值响应参数BO
 */
@Data
public class LabelAndValueLongBO {
    /**
     * label
     */
    private Long label;

    /**
     * value
     */
    private Long value;
}
