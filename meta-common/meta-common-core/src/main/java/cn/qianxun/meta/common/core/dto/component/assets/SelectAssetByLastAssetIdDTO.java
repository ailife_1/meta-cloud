package cn.qianxun.meta.common.core.dto.component.assets;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/15 11:05
 * @Description 根据上一步资产ID查询当前资产DTO
 */
@Data
public class SelectAssetByLastAssetIdDTO {
    /**
     * 资产ID
     */
    private List<Long> assetIds;

    /**
     * 模块 1建设单位 2运维单位 3使用单位 4基础标签 5归属信息系统 6资产删除
     */
    private Integer module;

    /**
     * 当前资产对象 1信息系统 2web 3服务组件 4IP设备
     */
    private Integer curAssetObj;

    /**
     * 下一资产对象 1信息系统 2web 3服务组件 4IP设备
     */
    private Integer lastAssetObj;

    /**
     * 覆盖原有值 0否 1是
     */
    private Integer overOriginalValue;

    /**
     * className
     */
    private String className;

    /**
     * 标签列表
     */
    private List<AssetLabelDTO> labelList;


}
