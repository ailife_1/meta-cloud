package cn.qianxun.meta.common.core.bean;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description 定义加解密Jasypt 目前与默认相同 (可以去掉)
 * @Date 2023/8/22 17:49
 **/

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CodeEncryptorCfg {

    @Bean(name = "codeEncryptorBean")
    public StringEncryptor codeStringEncryptor() {
        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setPassword("metajsayptCodeEncryp666888999");
        config.setAlgorithm("PBEWITHHMACSHA512ANDAES_256");
        config.setKeyObtentionIterations("1000");
        config.setPoolSize("1");
        config.setProviderName("SunJCE");
        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        config.setIvGeneratorClassName("org.jasypt.iv.RandomIvGenerator");
        config.setStringOutputType("base64");
        encryptor.setConfig(config);

        return encryptor;
    }

    public static void main(String[] args) {
        PooledPBEStringEncryptor textEncryptor = new PooledPBEStringEncryptor();
        // 加密密钥
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setPassword("metajsayptCodeEncryp666888999");
        config.setAlgorithm("PBEWITHHMACSHA512ANDAES_256");
        config.setKeyObtentionIterations("1000");
        config.setPoolSize("1");
        config.setProviderName("SunJCE");
        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        config.setIvGeneratorClassName("org.jasypt.iv.RandomIvGenerator");
        config.setStringOutputType("base64");
        textEncryptor.setConfig(config);

        // 要加密的数据（如数据库的用户名或密码）
        String username = textEncryptor.encrypt("elastic");
        String password = textEncryptor.encrypt("elastic@yzxa2023");
        System.out.println("username:" + username);
        System.out.println("password:" + password);
    }
}

