package cn.qianxun.meta.common.core.utils;


import cn.qianxun.meta.common.core.exception.ServiceException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

/**
 * @author: fuzhilin
 * @description: 单个字段断言
 */
public abstract class SingleFieldAssert {
    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new ServiceException(message,500);
        }
    }

    public static void isNull(Object obj, String message) {
        if (null == obj) {
            throw new ServiceException(message,500);
        }

    }

    public static void  isNullOrEmptyArray(Object[] array,String message) {
        if(array == null) {
            throw new ServiceException(message,500);
        }

        if(array.length ==0) {
            throw new ServiceException(message,500);
        }

        for(Object item:array) {
            if(item == null) {
                throw new ServiceException(message,500);
            }
        }

    }


    public static void is(Object obj, String message) {

    }

    public static void isTrue(Boolean obj, String message) {
        if (null == obj || !obj) {
            throw new ServiceException(message,500);
        }
    }

    public static void isNotNull(Object obj, String message) {
        if (null != obj) {
            throw new ServiceException(message,500);
        }
    }

    public static void isNumeric(String str, String message) {
        Pattern pattern = compile("[0-9]*");
        if (StringUtils.isBlank(str)) {
            throw new ServiceException(message,500);
        }
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            throw new ServiceException(message,500);
        }
    }

    /**
     * 日期格式验证
     *
     * @param format  格式
     * @param str     日期字符串
     * @param message 错误信息
     */
    public static void isValidDate(String str, String format, String message) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat(format);
            Date dd = fmt.parse(str);
            if (!str.equals(fmt.format(dd))) {
                throw new ServiceException(message,500);
            }
        } catch (Exception e) {
            throw new ServiceException(message, 500);
        }

    }

    /**
     * 获取对象中属性为空的字段
     * @param object 所传的对象
     * @param list 接收空字段的集合
     * @return
     */
    public static List<String> checkFieldIsNUll(Object object, List<String> list){
        Field[] field = object.getClass().getDeclaredFields();
        for(int j=0 ; j<field.length ; j++){
            String name = field[j].getName();
            name = name.substring(0,1).toUpperCase()+name.substring(1);
            String type = field[j].getGenericType().toString();
            Method m;
            Object value;
            try {
                m = object.getClass().getMethod("get"+name);
                value = m.invoke(object);
                if(value == null || "".equals(value)){
                    list.add(name);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
