package cn.qianxun.meta.common.core.dto.component.assets;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2024/1/30 11:12
 * @Description 网站根据标签查询发送邮箱参数
 */
@Data
public class WebSendMailParamDTO {

    /**
     * 标签Id列表
     */
    private List<String> labelIdList;

    /**
     * 发送类型(1：发送给个人 2：根据单位类型发送)
     */
    private String sendType;

    /**
     * 发送类型(1：发送给个人 ，2：根据单位类型发送)
     */
    private Long sendUserId;

    /**
     * 主题
     */
    private String subject;

    /**
     * 内容
     */
    private String content;

}