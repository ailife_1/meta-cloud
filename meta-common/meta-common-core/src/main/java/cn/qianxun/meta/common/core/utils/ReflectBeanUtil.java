package cn.qianxun.meta.common.core.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @Author fuzhilin
 * @Date 2023/9/5 11:21
 * @Description 反射相关方法
 */
public class ReflectBeanUtil {

    /**
     * 根据类名反射创建对象
     *
     * @param name 类名
     * @return 对象
     * @throws Exception
     */
    public static Object getInstance(String name) throws Exception {
        Class<?> cls = Class.forName(name);
        return cls.newInstance();
    }

    /**
     * 获取包含所有父类字段在内的所有字段
     *
     * @param clazz 要获取字段的类
     * @return 字段名集合
     */
    public static List<String> getFields(Class clazz) {
        List<Field> fields = new ArrayList<>();
        //为null时到达了父类的最高级Object
        while (clazz.getSuperclass() != null) {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            //得到父类并赋值给自己
            clazz = clazz.getSuperclass();
        }
        List<String> fieldNames = new ArrayList<>();
        for (Field field : fields) {
            fieldNames.add(field.getName());
        }
        return fieldNames;
    }

    /**
     * 根据属性名称和java类型，获取对应的getter方法名
     *
     * @param property
     * @param
     * @return
     */
    public static String getGetterMethodName(String property, Class clazz) {
        StringBuilder sb = new StringBuilder();
        sb.append(property);
        if (Character.isLowerCase(sb.charAt(0))) {
            if (sb.length() == 1 || !Character.isUpperCase(sb.charAt(1))) {
                sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            }
        }
        if ("boolean".equals(clazz.getName())) {
            sb.insert(0, "is");
        } else {
            sb.insert(0, "get");
        }
        return sb.toString();
    }

    /**
     * 根据属性名称获取对应的setter方法名称
     *
     * @param property
     * @return
     */
    public static String getSetterMethodName(String property) {
        StringBuilder sb = new StringBuilder();
        sb.append(property);
        if (Character.isLowerCase(sb.charAt(0))) {
            if (sb.length() == 1 || !Character.isUpperCase(sb.charAt(1))) {
                sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            }
        }
        sb.insert(0, "set");
        return sb.toString();
    }

    /**
     * 根据方法名调用无参方法
     *
     * @param object     要执行该方法的对象
     * @param methodName 方法名
     * @return
     */
    public static Object invokeMethodByName(Object object, String methodName) {
        Object o = null;
        try {
            Method method = object.getClass().getMethod(methodName);
            method.setAccessible(true);
            o = method.invoke(object);
        } catch (Exception e) {
        } finally {
            return o;
        }
    }
}