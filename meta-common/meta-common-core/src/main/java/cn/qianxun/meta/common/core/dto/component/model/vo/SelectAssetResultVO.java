package cn.qianxun.meta.common.core.dto.component.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/11/1 15:36
 * @Description 根据参数查询资产结果返回参数VO
 */
@Data
public class SelectAssetResultVO {


    /**
     * 资产类型
     */
    private String assetType;

    private List<AssetListDTO> assetListList;


    @Data
    public static class AssetListDTO {

        /**
         * 资产
         */
        private List<String> asset;

        /**
         * 单位类型(返回单位的时候需要) 1、建设 2、运营 3、使用
         */
        private String deptFlag;

    }
}