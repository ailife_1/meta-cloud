package cn.qianxun.meta.common.core.dto.component.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/11/1 15:01
 * @Description
 */
@Data
public class MarkLabelContextBeanDTO {

    /**
     * 模型id
     */
    private String modelId;

    /**
     * 上次查询结果字段
     */
    private String lastResultField;

    /**
     * 标记标签
     */
    private String markLabel;

    /**
     * 资产集合
     */
    private List<String> assets;

    /**
     * 标记对象
     */
    private String markObj;

    /**
     * 节点
     */
    private String node;

    /**
     * className
     */
    private String className;
}