package cn.qianxun.meta.common.core.enums;

/**
 * 业务类型
 */
public enum BusinessType {
    ASSET(1, "资产"),
    RISK(2, "风险"),
    ;


    BusinessType(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int type;
    public String msg;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}