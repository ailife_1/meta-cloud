package cn.qianxun.meta.common.core.dto.component.assets;

import lombok.Data;

import java.util.List;

/**
 * 规则修改标签dto
 */
@Data
public class AssetLabelDTO {

    /**
     * 标签ID
     */
    private List<Long> labelIds;

    /**
     * 资产类型 1信息系统 2web 3port 4ip
     */
    private String assetType;

}