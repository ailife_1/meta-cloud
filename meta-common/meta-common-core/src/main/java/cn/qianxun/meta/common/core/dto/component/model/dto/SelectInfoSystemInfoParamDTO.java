package cn.qianxun.meta.common.core.dto.component.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/11/1 11:39
 * @Description 根据参数查询信息系统信息DTO
 */
@Data
public class SelectInfoSystemInfoParamDTO {
    /**
     * 资产集合
     */
    private List<String> assets;

    /**
     * 备案等级 字典(filing_level)
     */
    private List<String> filingLevel;

    /**
     * 端口集合
     */
    private List<String> postList;

    /**
     * ip集合
     */
    private List<String> ipList;

    /**
     * 系统类型(字典 sys_type)
     */
    private List<String> systemType;

    /**
     * 系统状态 字典(sys_status)
     */
    private List<String> systemStatus;

    /**
     * 信息系统标签ids
     */
    private List<String> infoSystemLabs;

    /**
     * 查询字段
     */
    private String resultField;

    /**
     * 上次查询结果字段
     */
    private String lastResultField;

    /**
     * className
     */
    private String className;


    /**
     * 节点
     */
    private String node;

}