package cn.qianxun.meta.common.core.constant.ProviderConstant;


/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/9/11 14:01
 **/
public interface AssetsProviderConstant {
    /**
     * 远程调用公共前缀
     */
    String PROVIDER = "/provider";

    String ASSETS = "/assets";

    /**
     * 版本
     */
    String V1 = "/v1";

    /**
     * 远程调用用戶信息
     */
    String ASSET_IP = "/assetIp";
    String ASSET_INFO_SYSTEM = "/assetInfoSystem";
    String ASSET_PORT = "/assetPort";
    String ASSET_WEB = "/assetWeb";
    String ASSET_LABEL = "/assetLabel";
    String ASSET_MODEL_RESULT = "/assetModelResult";
    String ASSET_COMMON = "/assetCommon";


    /**
     * 修改单位(信息系统)
     */
    String PROVIDER_ASSETS_V1_ASSET_INFO_SYSTEM_updateDept = PROVIDER + ASSETS + V1 + ASSET_INFO_SYSTEM + "/updateDept";

    /**
     * 删除资产(信息系统)
     */
    String PROVIDER_ASSETS_V1_ASSET_INFO_SYSTEM_deleteAsset = PROVIDER + ASSETS + V1 + ASSET_INFO_SYSTEM + "/deleteAsset";

    /**
     * 根据上一步资产ID查询当前资产
     */
    String PROVIDER_ASSETS_V1_ASSET_INFO_SYSTEM_SELECT_ASSET_BY_LAST_ASSET_ID = PROVIDER + ASSETS + V1 + ASSET_INFO_SYSTEM + "/selectAssetByLastAssetId";

    /**
     * 添加信息系统(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_insertInfoSystem = PROVIDER + ASSETS + V1 + ASSET_INFO_SYSTEM + "/insertInfoSystem";

    /**
     * 查询信息系统列表(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_getInfoSystemList = PROVIDER + ASSETS + V1 + ASSET_INFO_SYSTEM + "/getInfoSystemList";

    /**
     * 根据参数查询信息系统
     */
    String PROVIDER_ASSETS_V1_ASSET_INFO_SYSTEM_selectSystemInfoByParam = PROVIDER + ASSETS + V1 + ASSET_INFO_SYSTEM + "/selectSystemInfoByParam";

    /**
     * 删除信息系统(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_deleteInfoSystem = PROVIDER + ASSETS + V1 + ASSET_INFO_SYSTEM + "/deleteInfoSystem";

    /**
     * 信息系统详情(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_infoSystemDetails = PROVIDER + ASSETS + V1 + ASSET_INFO_SYSTEM + "/infoSystemDetails";

    /**
     * 修改信息系统(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_editInfoSystem = PROVIDER + ASSETS + V1 + ASSET_INFO_SYSTEM + "/editInfoSystem";

    /**
     * 修改单位(ip)
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_updateDept = PROVIDER + ASSETS + V1 + ASSET_IP + "/updateDept";

    /**
     * 删除资产(ip)
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_deleteAsset = PROVIDER + ASSETS + V1 + ASSET_IP + "/deleteAsset";

    /**
     * 修改信息系统(ip)
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_updateInfoSystem = PROVIDER + ASSETS + V1 + ASSET_IP + "/updateInfoSystem";

    /**
     * 修改单位(web)
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_updateDept = PROVIDER + ASSETS + V1 + ASSET_WEB + "/updateDept";

    /**
     * 删除资产(web)
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_deleteAsset = PROVIDER + ASSETS + V1 + ASSET_WEB + "/deleteAsset";

    /**
     * 修改信息系统(web)
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_updateInfoSystem = PROVIDER + ASSETS + V1 + ASSET_WEB + "/updateInfoSystem";

    /**
     * 添加网站(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_insertWeb = PROVIDER + ASSETS + V1 + ASSET_WEB + "/insertWeb";

    /**
     * 查询网站列表(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_getWebList = PROVIDER + ASSETS + V1 + ASSET_WEB + "/getWebList";

    /**
     * 查询网站列表(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_WEB_URL_LIST = PROVIDER + ASSETS + V1 + ASSET_WEB + "/webUrlList";

    /**
     * 定时更新网站附属信息
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_handleUpdateWebInfoJob = PROVIDER + ASSETS + V1 + ASSET_WEB + "/handleUpdateWebInfoJob";

    /**
     * 根据条件查询网站列表
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_selectWebList = PROVIDER + ASSETS + V1 + ASSET_WEB + "/selectWebList";
    /**
     * 根据网站地址获取标签
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_selectLabelByUrl = PROVIDER + ASSETS + V1 + ASSET_WEB + "/selectLabelByUrl";

    /**
     * 根据参数查询网站
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_selectWebInfoByParam = PROVIDER + ASSETS + V1 + ASSET_WEB + "/selectWebInfoByParam";

    /**
     * 批量更新网站的安全值
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_batchUpdateWebSafeValue = PROVIDER + ASSETS + V1 + ASSET_WEB + "/batchUpdateWebSafeValue";

    /**
     * 根据标签查询网站
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_selectWebByLabel = PROVIDER + ASSETS + V1 + ASSET_WEB + "/selectWebByLabel";

    /**
     * 删除网站(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_deleteWeb = PROVIDER + ASSETS + V1 + ASSET_WEB + "/deleteWeb";

    /**
     * 网站详情(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_webDetails = PROVIDER + ASSETS + V1 + ASSET_WEB + "/webDetails";

    /**
     * 修改网站(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_editWeb = PROVIDER + ASSETS + V1 + ASSET_WEB + "/editWeb";

    /**
     * 修改单位(port)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_updateDept = PROVIDER + ASSETS + V1 + ASSET_PORT + "/updateDept";

    /**
     * 删除资产(port)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_deleteAsset = PROVIDER + ASSETS + V1 + ASSET_PORT + "/deleteAsset";

    /**
     * 修改信息系统(port)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_updateInfoSystem = PROVIDER + ASSETS + V1 + ASSET_PORT + "/updateInfoSystem";

    /**
     * 添加port(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_insertPort = PROVIDER + ASSETS + V1 + ASSET_PORT + "/insertPort";

    /**
     * 查询port列表(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_getPortList = PROVIDER + ASSETS + V1 + ASSET_PORT + "/getPortList";

    /**
     * 根据参数查询port信息
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_selectPortInfoByParam = PROVIDER + ASSETS + V1 + ASSET_PORT + "/selectPortInfoByParam";

    /**
     * 删除port(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_deletePort = PROVIDER + ASSETS + V1 + ASSET_PORT + "/deletePort";

    /**
     * port详情(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_portDetails = PROVIDER + ASSETS + V1 + ASSET_PORT + "/portDetails";

    /**
     * 修改port(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_PORT_editPort = PROVIDER + ASSETS + V1 + ASSET_PORT + "/editPort";


    /**
     * 根据条件查询IP列表
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_selectIpList = PROVIDER + ASSETS + V1 + ASSET_IP + "/selectIpList";


    /**
     * 校验ip是否存在
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_checkIpIsExist = PROVIDER + ASSETS + V1 + ASSET_IP + "/checkIpIsExist";

    /**
     * 添加ip(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_insertIp = PROVIDER + ASSETS + V1 + ASSET_IP + "/insertIp";

    /**
     * 查询ip列表(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_getIpList = PROVIDER + ASSETS + V1 + ASSET_IP + "/getIpList";

    /**
     * 创建ip扫描任务
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_handleCreateIpTaskJob = PROVIDER + ASSETS + V1 + ASSET_IP + "/handleCreateIpTaskJob";

    /**
     * 根据参数查询ip信息
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_selectIpInfoByParam = PROVIDER + ASSETS + V1 + ASSET_IP + "/selectIpInfoByParam";

    /**
     * 解析杭州数据
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_analysisHangZhouIpData = PROVIDER + ASSETS + V1 + ASSET_IP + "/analysisHangZhouIpData";

    /**
     * 获取ip所有数据
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_ipAllList = PROVIDER + ASSETS + V1 + ASSET_IP + "/ipAllList";

    /**
     * 批量更新ip的安全值
     */
    String PROVIDER_ASSETS_V1_ASSET_WEB_batchUpdateIpSafeValue = PROVIDER + ASSETS + V1 + ASSET_IP + "/batchUpdateIpSafeValue";

    /**
     * 删除ip(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_deleteIp = PROVIDER + ASSETS + V1 + ASSET_IP + "/deleteIp";

    /**
     * ip详情(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_ipDetails = PROVIDER + ASSETS + V1 + ASSET_IP + "/ipDetails";

    /**
     * 修改ip(API)
     */
    String PROVIDER_ASSETS_V1_ASSET_IP_editIp = PROVIDER + ASSETS + V1 + ASSET_IP + "/editIp";

    /**
     * 修改继承标签
     */
    String PROVIDER_ASSETS_V1_ASSET_LABEL_updateInheritLabel = PROVIDER + ASSETS + V1 + ASSET_LABEL + "/updateInheritLabel";

    /**
     * 修改自身标签
     */
    String PROVIDER_ASSETS_V1_ASSET_LABEL_getLabelByAssetIds = PROVIDER + ASSETS + V1 + ASSET_LABEL + "/getLabelByAssetIds";

    /**
     * 获取标签列表
     */
    String PROVIDER_ASSETS_V1_ASSET_LABEL_getLabelList = PROVIDER + ASSETS + V1 + ASSET_LABEL + "/getLabelList";

    /**
     * 标记客户标签
     */
    String PROVIDER_ASSETS_V1_ASSET_MODEL_RESULT_markCusLabel = PROVIDER + ASSETS + V1 + ASSET_MODEL_RESULT + "/markCusLabel";

    /**
     * 执行任务资产标签任务
     */
    String PROVIDER_ASSETS_V1_ASSET_LABEL_handleAssetLabelTaskJob = PROVIDER + ASSETS + V1 + ASSET_LABEL + "/handleAssetLabelTaskJob";

    /**
     * 批量刷新单位
     */
    String PROVIDER_ASSETS_V1_ASSET_COMMON_batchRefreshDept = PROVIDER + ASSETS + V1 + ASSET_COMMON + "/batchRefreshDept";

    /**
     * 批量刷新标签
     */
    String PROVIDER_ASSETS_V1_ASSET_COMMON_batchRefreshLabel = PROVIDER + ASSETS + V1 + ASSET_COMMON + "/batchRefreshLabel";

    /**
     * 批量刷新信息系统
     */
    String PROVIDER_ASSETS_V1_ASSET_COMMON_batchRefreshInfoSystem = PROVIDER + ASSETS + V1 + ASSET_COMMON + "/batchRefreshInfoSystem";

    /**
     * 资产仓库拉取资产
     */
    String PROVIDER_ASSETS_V1_ASSET_COMMON_pullAssetsByHouse = PROVIDER + ASSETS + V1 + ASSET_COMMON + "/pullAssetsByHouse";

    /**
     * 从唯一表拉取资产到资产仓库
     */
    String PROVIDER_ASSETS_V1_ASSET_COMMON_pullAssetsByOnly = PROVIDER + ASSETS + V1 + ASSET_COMMON + "/pullAssetsByOnly";
}
