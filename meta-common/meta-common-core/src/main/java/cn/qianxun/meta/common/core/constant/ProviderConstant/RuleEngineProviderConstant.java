package cn.qianxun.meta.common.core.constant.ProviderConstant;


/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/9/11 14:01
 **/
public interface RuleEngineProviderConstant {
    /**
     * 远程调用公共前缀
     */
    String PROVIDER = "/provider";

    String RULE = "/rule";

    /**
     * 版本
     */
    String V1 = "/v1";

    /**
     * 远程调用用戶信息
     */
    String RULE_ENGINE_CHAIN = "/chain";


    /**
     * 修改规则信息
     */
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_updateChain = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/updateChain";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_saveChain = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/saveChain";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_removeChain = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/removeChain";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_getChainByIdOrChainId = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/getChainByIdOrChainId";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_saveChainBusiness = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/saveChainBusiness";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_updateChainBusiness = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/updateChainBusiness";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_removeChainBusiness = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/removeChainBusiness";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_getChainBusinessById = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/getChainBusinessById";

    //启动规则流程
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_initiateRuleProcess = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/initiateRuleProcess";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_initiateRuleProcess2 = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/initiateRuleProcess2";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_getChainNameByType = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/getChainNameByType";

    /**
     * 生成串行编排EL表达式
     */
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_generateThenElData = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/generateThenElData";


    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_saveActionBusiness = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/saveActionBusiness";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_updateActionBusiness = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/updateActionBusiness";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_removeActionBusiness = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/removeActionBusiness";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_getActionBusinessById = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/getActionBusinessById";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_getActionBusinessListByBusinessId = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/getActionBusinessListByBusinessId";

    /**
     * 日志告警
     */
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_addLogWarnRisk = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/secLog/add";
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_startFlow = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/logWarnRisk/startFlow";

    /**
     * 根据资产获取待验证和已验证漏洞数
     */
    String PROVIDER_ASSETS_V1_RULE_ENGINE_CHAIN_getHouseNumByAssetName = PROVIDER + RULE + V1 + RULE_ENGINE_CHAIN + "/getHouseNumByAssetName";
}
