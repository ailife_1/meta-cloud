package cn.qianxun.meta.common.core.annotation;

import java.lang.annotation.*;

/**
 * @Author fuzhilin
 * @Date 2023/11/30 10:25
 * @Description 防止重复提交
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface RepeatSubmit {
    /**
     * 间隔时间(ms)，小于此时间视为重复提交
     */
    int interval() default 5000;

    /**
     * 提示消息
     */
    String message() default "不允许重复提交，请稍候再试";
}
