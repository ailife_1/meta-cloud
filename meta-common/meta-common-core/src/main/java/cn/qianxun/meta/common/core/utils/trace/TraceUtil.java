package cn.qianxun.meta.common.core.utils.trace;

import cn.qianxun.meta.common.core.constant.FeignConstant;
import cn.qianxun.meta.common.core.utils.StringUtils;
import org.slf4j.MDC;

import javax.servlet.http.HttpServletRequest;

/**
 * 链路追踪工具类
 *
 * @author fuzhilin
 */
public class TraceUtil {

    /**
     * 从header和参数中获取traceId
     * 从前端传入数据
     *
     * @param request 　HttpServletRequest
     * @return traceId
     */
    public static String getTraceId(HttpServletRequest request) {
        String traceId = request.getParameter(FeignConstant.META_TRACE_ID);
        if (StringUtils.isBlank(traceId)) {
            traceId = request.getHeader(FeignConstant.META_TRACE_ID);
        }
        return traceId;
    }

    /**
     * 传递traceId至MDC
     *
     * @param traceId 　跟踪ID
     */
    public static void mdcTraceId(String traceId) {
        if (StringUtils.isNotBlank(traceId)) {
            MDC.put(FeignConstant.LOG_TRACE_ID, traceId);
        }
    }
}
