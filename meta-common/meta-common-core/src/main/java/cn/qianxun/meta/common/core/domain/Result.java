package cn.qianxun.meta.common.core.domain;


import cn.hutool.core.util.ObjectUtil;
import cn.qianxun.meta.common.core.constant.Constants;
import cn.qianxun.meta.common.core.enums.RestCode;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema( description = "返回类")
@Data
public class Result<T> {
    private static final long serialVersionUID = 1L;


    /**
     * 成功
     */
    public static final Integer SUCCESS = Integer.valueOf(Constants.SUCCESS);

    /**
     * 失败
     */
    public static final Integer FAIL = Integer.valueOf(Constants.FAIL);

    /**
     * 状态码
     */
    @Schema(description = "结果码", type = "Stirng", requiredMode = Schema.RequiredMode.REQUIRED)
    private int code;

    /**
     * 返回内容
     */
    @Schema(description = "结果描述", type = "String", requiredMode = Schema.RequiredMode.REQUIRED)
    private String msg;

    /**
     * 数据对象
     */
    @Schema(description = "响应数据对象")
    private T data;

    /**
     * 初始化一个新创建的 Result 对象，使其表示一个空消息。
     */
    public Result() {
    }

    /**
     * 初始化一个新创建的 Result 对象
     *
     * @param code 状态码
     * @param msg  返回内容
     */
    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
    }

    /**
     * 初始化一个新创建的 Result 对象
     *
     * @param code 状态码
     * @param msg  返回内容
     * @param data 数据对象
     */
    public Result(int code, T data, String msg) {
        this.code = code;
        this.msg = msg;
        if (ObjectUtil.isNotNull(data)) {
            this.data = data;
        }
    }

    public Result(T data) {
        this.code = RestCode.SUCCESS.getCode();
        this.msg = RestCode.SUCCESS.getMsg();
        this.data = data;
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static Result<String> success() {
        return new Result<>(RestCode.SUCCESS.getCode(), "操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(RestCode.SUCCESS.getCode(), data, "操作成功");
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @return 成功消息
     */
    public static <T> Result<T> success(String msg) {
        return new Result<>(RestCode.SUCCESS.getCode(), msg);
    }

    /**
     * 返回成功消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static <T> Result<T> success(String msg, T data) {
        return new Result<>(RestCode.SUCCESS.getCode(), data, msg);
    }

    public static <T> Result<T> custom(int status, T t, String message) {
        return new Result<>(status, t, message);
    }

    public static <T> Result<T> custom(int status, String message) {
        return new Result<>(status, message);
    }

    public static <T> Result<T> fail(T t, String message) {
        return new Result<>(RestCode.FAIL.getCode(), t, message);
    }

    public static <T> Result<T> fail(String message) {
        return new Result<>(RestCode.FAIL.getCode(), message);
    }


    public static Result<String> fail() {
        return new Result<>(RestCode.FAIL.getCode(), "操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param msg  返回内容
     * @return 警告消息
     */
    public static Result<String> fail(int code, String msg) {
        return new Result<>(code, msg);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}
