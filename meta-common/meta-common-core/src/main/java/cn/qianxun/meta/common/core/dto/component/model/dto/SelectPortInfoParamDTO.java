package cn.qianxun.meta.common.core.dto.component.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/11/1 11:39
 * @Description 根据参数查询port列表DTO
 */
@Data
public class SelectPortInfoParamDTO {
    /**
     * 资产集合
     */
    private List<String> assets;

    /**
     * 端口标签ids
     */
    private List<Long> portLabs;

    /**
     * 设备类型（组件）
     */
    private String deviceType;

    /**
     * 协议类型
     */
    private String protocol;

    /**
     * 查询字段
     */
    private String resultField;

    /**
     * 上次查询结果字段
     */
    private String lastResultField;

    /**
     * 节点
     */
    private String node;

    /**
     * 端口集合
     */
    private List<String> postList;

    /**
     * ip集合
     */
    private List<String> ipList;

    /**
     * className
     */
    private String className;
}