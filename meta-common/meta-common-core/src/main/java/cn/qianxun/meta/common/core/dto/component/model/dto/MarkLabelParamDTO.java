package cn.qianxun.meta.common.core.dto.component.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/11/2 10:08
 * @Description
 */
@Data
public class MarkLabelParamDTO {

    /**
     * 模型id
     */
    private Long modelId;

    /**
     * 标记标签
     */
    private String markLabel;

    /**
     * 标记对象
     */
    private String markObj;

    /**
     * 资产集合
     */
    private List<String> assets;

    /**
     * className
     */
    private String className;
}