package cn.qianxun.meta.common.core.enums;

/**
 * 业务的唯一标识，用于流程中不是工作流配置的业务调用
 */
public enum AssetRuleModule {
    UPDATE_ASSET_CONSTRUCT_DEPT(1, "更新资产的建设单位", "update_asset_construct_dept"),
    UPDATE_ASSET_OPERATION_DEPT(2, "更新资产的运营单位", "update_asset_operation_dept"),
    UPDATE_ASSET_USE_DEPT(3, "更新资产的使用单位", "update_asset_use_dept"),
    UPDATE_ASSET_LABEL(4, "更新资产的标签", "update_asset_label"),
    UPDATE_ASSET_INFO_SYSTEM(5, "更新资产的信息系统", "update_asset_info_system"),
    DELETE_ASSET(6, "删除资产", "delete_asset"),
    ;


    public Integer type;
    public String msg;
    public String identify;


    AssetRuleModule(Integer type, String msg, String identify) {

        this.type = type;
        this.msg = msg;
        this.identify = identify;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public static AssetRuleModule getRuleModule(Integer type) {
        for (AssetRuleModule assetRuleModule : values()) {
            if (type.equals(assetRuleModule.getType())) {
                return assetRuleModule;
            }
        }
        throw new RuntimeException("'AssetRuleModule' not found By " + type);
    }


}