package cn.qianxun.meta.common.core.dto.component.assets;

import lombok.Data;

import java.util.List;

/**
 * 修改信息系统规则参数
 *
 * @author fuzhilin
 */
@Data
public class ChangeInfoSystemParamDTO {
    /**
     * 资产ID
     */
    private List<Long> assetIds;

    /**
     * 信息系统ID
     */
    private Long infoSystemId;

    /**
     * 资产类型 1信息系统 2web 3服务组件 4IP设备
     */
    private String assetType;

    /**
     * 覆盖原有值 0否 1是
     */
    private Integer overOriginalValue;

    /**
     * className
     */
    private String className;

    /**
     * false：移除 true 保存
     */
    private Boolean removeOrSave;

    /**
     * 信息系统列表
     */
    private List<Long> assetInfoList;

}