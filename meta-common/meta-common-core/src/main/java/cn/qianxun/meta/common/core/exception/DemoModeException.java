package cn.qianxun.meta.common.core.exception;

/**
 * 演示模式异常
 *
 * @author fuzhilin
 */
public class DemoModeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DemoModeException() {
    }
}
