package cn.qianxun.meta.common.core.constant.ProviderConstant;

public interface MetaSeclogConstant {

    /**
     * 远程调用公共前缀
     */
    String PROVIDER = "/provider";

    /**
     * 版本
     */
    String V1 = "/v1";
    String meta_SECLOG = "/meta-seclog";
    /**
     * 远程调用日志告警
     */
    String LOG_WARN = "/logWarn";
    /**
     * 根据日期范围、目标资产查询告警日志数量；
     */
    String PROVIDER_meta_SECLOG_V1_LOG_WARN_GETLOGWARNNUM = PROVIDER + meta_SECLOG + V1 + LOG_WARN + "/getLogWarnNum";
}
