package cn.qianxun.meta.common.core.dto.component.assets;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2024/1/30 11:16
 * @Description
 */
@Data
public class WebDTO {

    /**
     * 网站ID
     */
    private Long id;

    /**
     * 标签id
     */
    private Long labelId;

    /**
     * 标签id
     */
    private String labelName;

    /**
     * 网站地址
     */
    private String url;

    /**
     * 网站标新信息(标题)
     */
    private String title;

    /**
     * 邮箱
     */
    private String email;
}