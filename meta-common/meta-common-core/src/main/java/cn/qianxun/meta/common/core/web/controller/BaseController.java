package cn.qianxun.meta.common.core.web.controller;

import cn.qianxun.meta.common.core.domain.Result;
import lombok.extern.slf4j.Slf4j;

/**
 * web层通用数据处理
 *
 * @author Lion Li
 */
@Slf4j
public class BaseController {

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected Result toAjax(int rows) {
        return rows > 0 ? Result.success() : Result.fail();
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected Result toAjax(boolean result) {
        return result ? Result.success() : Result.fail();
    }

}
