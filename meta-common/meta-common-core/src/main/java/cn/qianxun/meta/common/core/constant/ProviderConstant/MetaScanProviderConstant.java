package cn.qianxun.meta.common.core.constant.ProviderConstant;

/**
 * @Author fuzhilin
 * @Date 2023/10/16 11:39
 * @Description
 */
public interface MetaScanProviderConstant {
    /**
     * 远程调用公共前缀
     */
    String PROVIDER = "/provider";

    String SCAN = "/scan";

    /**
     * 版本
     */
    String V1 = "/v1";

    /**
     * 版本
     */
    String WEB_TOOL = "/webTool";

    /**
     * 获取网站截图
     */

    String PROVIDER_SCAN_V1_WEB_TOOL_GET_WEB_SCREENSHOT = PROVIDER + SCAN + V1 + WEB_TOOL + "/getWebScreenshot";
}
