package cn.qianxun.meta.common.core.dto.component.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/11/1 11:39
 * @Description 根据参数查询web列表DTO
 */
@Data
public class SelectDeptInfoParamDTO {
    /**
     * 资产集合
     */
    private List<String> assets;

    /**
     * 单位类型(字典:dept_flag)
     */
    private String deptFlag;

    /**
     * 查询字段
     */
    private String resultField;

    /**
     * 上次查询结果字段
     */
    private String lastResultField;

    /**
     * 节点
     */
    private String node;

    /**
     * className
     */
    private String className;
}