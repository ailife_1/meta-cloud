package cn.qianxun.meta.common.core.constant;

/**
 * 通用常量信息
 *
 */
public class Constants {

    /**
     * EasyExcel批量保存，每隔5条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    public static final int BATCH_COUNT = 2000;

    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * 规则的 application_name
     */
    public static final String APPLICATION_NAME = "meta";

    /**
     * 超管角色
     */
    public static final String SYS_ROLE_PER = "admin";


    /**
     * 规则的 chain_name前缀
     */
    public static final String CHAIN_NAME = "chain";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";


    public static final Integer ZERO = 0;
    public static final Integer ONE_NUM = 1;
    public static final Integer TWO_NUM = 2;
    public static final Integer FIVE_HUNDRED = 500;
    public static final Long THREE_HUNDRED = 300L;
    public static final Long THOUSAND_FIVE_HUNDRED = 2000L;
    public static final CharSequence HTTP = "http://";
    public static final CharSequence HTTPS = "https://";
    public static final String PC = "PC";
    public static final String ONE = "1";
    public static final String TOW = "2";
    public static final String THREE = "3";
    public static final String FOUR = "4";
    public static final String FIVE = "5";
    public static final String YES = "1";
    public static final String NO = "2";
    public static final String APPID = "appId:";


    /**
     * 资产类类型
     */
    public static final String INFOSYSTEM = "infoSystem";
    public static final String DEPT = "dept";
    public static final String WEB = "web";
    public static final String IP = "ip";
    public static final String PORT = "port";
    public static final String ALL = "all";

    public static final String SYS_DICT_KEY = "sys_dict:";
    public static final String SYS_DICT_LABEL_KEY = "sys_dict_label:";

    /**
     * 定时任务类型立即执行
     */
    public static final String NONE = "NONE";

    /**
     * 定时任务类型间隔执行
     */
    public static final String CRON = "CRON";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 行政区域 redis缓存key
     */
    public static final String SYS_AREA_KEY = "sys_area_key";

    /**
     * 安全评估模型 redis缓存key
     */
    public static final String EVALUATION_MODEL = "evaluation_model:";

    /**
     * 拉取资产结束时间 redis缓存key
     */
    public static final String PULL_ASSETS = "pull_assets:";

    /**
     * 资产标签任务 redis缓存key
     */
    public static final String ASSET_LABEL_TASK = "asset_label_task:";

    /**
     * 行政区域 redis缓存Map的 key
     */
    public static final String SYS_AREA_MAP_KEY = "sys_area_map_key";
    /**
     * 行政区域 redis缓存Map的 key
     */
    public static final String SYS_AREA_MAP_OBJ_KEY = "sys_area_map_obj_key";

    /**
     * APPID登录token redis缓存Map的 key
     */
    public static final String APPID_ACCESS_TOKEN = "appIdAccessToken:";
    public static final String APPID_REFRESH_TOKEN = "appidRefreshToken:";

    /**
     * 区域状态1正常2禁用
     */
    public static final String AREA_STATUS_NORMAL = "1";
    public static final String AREA_STATUS_DISABLE = "2";

    /**
     * 状态 0正常1禁用2长时间未登录锁定
     */
    public static final String STATUS_NORMAL = "0";
    public static final String STATUS_DISABLE = "1";
    public static final Integer STATUS_DELETE = 2;
    public static final String STATUS_LOCK = "2";

    public static final String NO_DEL_NORMAL = "0";
    public static final String YES_DEL_NORMAL = "2";

    public static final int THREAD_POOL_NUM = 8;

    public static final String AREA_LEVEL_ONE = "1";
    public static final String AREA_LEVEL_TWO = "2";
    public static final String AREA_LEVEL_THREE = "3";


    /**
     * 登录成功状态
     */
    public static final String LOGIN_SUCCESS_STATUS = "0";

    /**
     * 登录失败状态
     */
    public static final String LOGIN_FAIL_STATUS = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";


    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    public static final String GET = "get";
    public static final String POST = "post";
    public static final String POST_FORM = "postform";
    public static final String BODY = "body";
    public static final String DELETE = "delete";
    public static final String PUT = "put";

    /**
     * 验证码有效期（分钟）
     */
    long CAPTCHA_EXPIRATION = 2;

    /**
     * 防重提交 redis key
     */
    String REPEAT_SUBMIT_KEY = "repeat_submit:";

    String ACCESS_TOKEN = "access_token";

    /**
     * 环境
     */
    public static final String DEV = "dev";
    public static final String PRO = "pro";


    /**
     * 动作的入参
     */
    public static final Integer ACTION_PARAM_INPUT = 0;

    /**
     * 动作的出参
     */
    public static final Integer ACTION_PARAM_OUTPUT = 1;

    /**
     * 资源的参数
     */
    public static final Integer RESOURCE_PARAM = 2;
    /**
     * 应用初始化资源的参数
     */
    public static final Integer APP_RESOURCE_PARAM = 3;

    public static final String LOCAL_HOST = "0.0.0.0";

    /**
     * open-api请求前缀
     */
    public static final String OPEN_API = "/open-api";

    /**
     * 审核的组件判断
     */
    public static final String RULE_APPROVE = "IF(approvalRuleCommon";

    /**
     * 比较符类路径
     */
    public static final String COMPARE_CLASS_PATH = "cn.qianxun.meta.rule.base.compare.util.CompareUtil";
    /**
     * 审核的组件名称
     */
    public static final String RULE_APPROVE_NAME = "approvalRuleCommon";

    /**
     * 规则判断组件
     */
    public static final String PUB_RULE_COMPARE_JUDGE = "pubRuleCompareJudge";
    /**
     * 获取类的属性时的key
     */
    public static final String CLASS_NAME_STR = "className";
    /**
     * 资源的公共属性key
     */
    public static final String LOGIC_RESOURCE_INFO_KEY = "logic_resource_info_key";
    public static final Integer USER = 0;
    public static final Integer ROLE = 1;

    /**
     * 动作类级联
     */
    public static final String ACTION_CLASS_CASCADE = "action_class_cascade";


}
