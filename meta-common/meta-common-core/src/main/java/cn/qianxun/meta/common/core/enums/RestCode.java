package cn.qianxun.meta.common.core.enums;

public enum RestCode {
    SUCCESS(200, "操作成功"),
    FAIL(500, "服务异常"),
    NOT_IMPLEMENTED(501, "接口未实现"),
    UNSUPPORTED_TYPE(415, "不支持的数据，媒体类型"),
    CONFLICT(409, "资源冲突，或者资源被锁"),
    BAD_METHOD(405, "不允许的http方法"),
    NOT_FOUND(404, "资源，服务未找到"),
    FORBIDDEN(403, "访问受限，授权过期"),
    UNAUTHORIZED(401, " 未授权"),
    BAD_REQUEST(400, "参数列表错误"),
    NOT_MODIFIED(304, "资源没有被修改"),
    SEE_OTHER(303, "重定向"),
    MOVED_PERM(301, "资源已被移除"),
    NO_CONTENT(204, "操作已经执行成功，但是没有返回数据"),
    ACCEPTED(202, "请求已经被接受"),
    CREATED(201, "对象创建成功"),
    WRONG_PAGE(10100, "页码不存在"),
    DATA_QUERY_FAIL(10101, "数据获取失败"),
    NAME_DUMP(10102, "名称重复");


    RestCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int code;
    public String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}