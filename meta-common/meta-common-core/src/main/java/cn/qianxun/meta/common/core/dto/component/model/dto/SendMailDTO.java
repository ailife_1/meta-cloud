package cn.qianxun.meta.common.core.dto.component.model.dto;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2024/1/30 10:10
 * @Description 发送邮箱请求参数DTO
 */
@Data
public class SendMailDTO {

    /**
     * 邮箱
     */
    private String mail;

    /**
     * 主题
     */
    private String subject;

    /**
     * 内容
     */
    private String content;

    /**
     * 附件
     */
    private String base64;
}
