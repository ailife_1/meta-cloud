package cn.qianxun.meta.common.core.dto.component.assets;

import lombok.Data;

import java.util.List;

/**
 * 标签变更规则参数
 *
 * @author fuzhilin
 */
@Data
public class ChangeLabelParamDTO {

    /**
     * 资产ID
     */
    private List<Long> assetIds;

    /**
     * 资产类型 1信息系统 2web 3服务组件 4IP设备
     */
    private String assetType;
    /**
     * 覆盖原有值 0否 1是
     */
    private Integer overOriginalValue;

    /**
     * className
     */
    private String className;

    /**
     * 标签列表
     */
    private List<AssetLabelDTO> labelList;

    /**
     * 类型(1.手动 2.自动)
     */
    private String type;

    /**
     * 任务id
     */
    private String jobId;

}