package cn.qianxun.meta.common.core.web.dto;

import lombok.Data;

/**
 * @author fuzhilin
 */
@Data
public class SelectVO {

    /**
     * id
     */
    private String value;

    /**
     * name
     */
    private String label;
}