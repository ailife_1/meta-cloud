package cn.qianxun.meta.common.core.constant.ProviderConstant;


/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description
 * @Date 2023/10/12 14:01
 **/
public interface RiskMonitorProviderConstant {
    /**
     * 远程调用公共前缀
     */
    String PROVIDER = "/provider";

    String RISK_MONITOR = "/riskMonitor";

    /**
     * 版本
     */
    String V1 = "/v1";

    /**
     * 远程调用web漏洞
     */
    String WEB_SCAN = "/webScan";

    /**
     * 远程调用主机漏洞
     */
    String HOST_SCAN = "/webScan";

    /**
     * 创建Web安全值
     */
    String PROVIDER_RISK_MONITOR_V1_WEB_SCAN_CREATE_WEB_SAFE_VALUE = PROVIDER + RISK_MONITOR + V1 + WEB_SCAN + "/createWebSafeValue";
    /**
     * 目标资产查询主机漏洞数量
     */
    String PROVIDER_RISK_MONITOR_V1_HOST_SCAN_GET_HOST_VULN_NUM = PROVIDER + RISK_MONITOR + V1 + HOST_SCAN + "/gethostVulnNum";
    /**
     * 根据日期范围、目标资产查询web漏洞数量
     */
    String PROVIDER_RISK_MONITOR_V1_WEB_SCAN_GET_WEB_VULN_NUM = PROVIDER + RISK_MONITOR + V1 + WEB_SCAN + "/getWebVulnNum";


}
