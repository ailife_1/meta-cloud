package cn.qianxun.meta.common.core.enums;

/**
 * 业务的唯一标识，用于流程中不是工作流配置的业务调用
 */
public enum BusinessIdentify {
    UPDATE_ASSET_CONSTRUCT_DEPT("update_asset_construct_dept", "更新资产的建设单位"),
    UPDATE_ASSET_OPERATION_DEPT("update_asset_operation_dept", "更新资产的运营单位"),
    UPDATE_ASSET_USE_DEPT("update_asset_use_dept", "更新资产的使用单位"),
    UPDATE_ASSET_LABEL("UPDATE_ASSET_LABEL", "更新资产的标签"),
    UPDATE_ASSET_INFO_SYSTEM("update_asset_info_system", "更新资产的信息系统"),
    DELETE_ASSET("delete_asset", "删除资产"),
    ;


    BusinessIdentify(String identify, String msg) {
        this.identify = identify;
        this.msg = msg;
    }

    public String identify;
    public String msg;

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }




}