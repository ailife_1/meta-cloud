package cn.qianxun.meta.common.core.constant;

/**
 * 安全值常量信息
 *
 * @author fuzhilin
 */
public class SafeValConstants {

    //安全值计算类型
    public static String SAFE_WEB_LOOPHOLE = "safe_web_loophole";//网站漏洞
    public static String SAFE_ATTACK_EVENT = "safe_attack_event";//攻击事件
    public static String SAFE_HOST_LOOPHOLE = "safe_host_loophole";//被黑事件
    public static String DOMAIN_SAFE = "domainSafe";//域名安全
    public static String RISK_CHAIN = "riskChain";//风险外链
    public static String SENSITIVE_PATH = "sensitivePath";//敏感路径
    public static String HIGH_RISK_PORT = "highRiskPort";//高危端口
    //下限值
    public static String MINI_LIMIT = "minLimit";//下限值

}
