package cn.qianxun.meta.common.core.dto.component.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/11/1 11:39
 * @Description 根据参数查询ip列表DTO
 */
@Data
public class SelectIpInfoParamDTO {
    /**
     * 资产集合
     */
    private List<String> assets;

    /**
     * ip标签ids
     */
    private List<Long> ipLabs;

    /**
     * 存活状态
     */
    private String ipStatus;

    /**
     * 资产类型
     */
    private String ipAssetType;

    /**
     * 数据来源(字典 data_source)
     */
    private String dataSource;

    /**
     * 查询字段
     */
    private String resultField;

    /**
     * 上次查询结果字段
     */
    private String lastResultField;

    /**
     * 节点
     */
    private String node;

    /**
     * className
     */
    private String className;
}