package cn.qianxun.meta.common.flowable.core;

import lombok.Data;

import java.util.Map;

/**
 * @Author fuzhilin
 * @Date 2023/10/11 11:22
 * @Description 新表单属性
 */
@Data
public class NewFormConf {

    /**
     * 标题
     */
    private String title;

    /**
     * 禁用表单
     */
    private Boolean disabled = false;
    /**
     * 表单按钮
     */
    private Boolean formBtns = true;

    /**
     * 表单模型
     */
    private Map<String, Object> formModel;

    /**
     * 表单数据
     */
    private Map<String, Object> formData;
}