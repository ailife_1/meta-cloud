### 服务端口号

|        服务名称        |  端口号  |
|:------------------:|:-----:|
| meta-xxl-job-admin  | 29900 |
|    meta-gateway     | 28880 |
|      meta-auth      | 28881 |
|     meta-system     | 28882 |
          

