package cn.qianxun.meta.dept.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author fuzhilin
 * @Date 2023/10/17 14:21
 * @Description 修改单位状态参数DTO
 */
@Data
public class SysDeptStatusDTO {

    /**
     * 单位ID
     */
    @NotNull(message = "单位id不能为空")
    private Long id;


    /**
     * 部门状态（0正常 1停用）
     */
    @NotBlank(message = "状态不能为空")
    private String status;
}