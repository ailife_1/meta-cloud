package cn.qianxun.meta.auth.api.service;

import cn.qianxun.meta.auth.api.entity.AuthAppApiRela;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 应用API接口关联关系 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 01:41:24
 */
public interface IAuthAppApiRelaService extends IService<AuthAppApiRela> {

}
