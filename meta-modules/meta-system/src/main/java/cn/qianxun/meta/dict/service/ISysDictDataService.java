package cn.qianxun.meta.dict.service;


import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.dict.dto.AddDictDataDTO;
import cn.qianxun.meta.dict.dto.QueryDictDataDTO;
import cn.qianxun.meta.dict.entity.SysDictData;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.dict.vo.SysDictDataVO;
import cn.qianxun.meta.dict.vo.SysDictTypeItemsVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典数据表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023/08/24
 */
public interface ISysDictDataService extends IService<SysDictData> {

    /**
     * 获取字典数据列表
     *
     * @param dto
     * @return
     */
    RowsData<SysDictDataVO> selectDictDataList(QueryDictDataDTO dto);

    /**
     * 查询字典数据详细
     *
     * @param dictCode
     * @return
     */
    SysDictDataVO details(Long dictCode);

    /**
     * 根据字典类型查询字典数据信息
     *
     * @param dictType
     * @return
     */
    List<SysDictDataVO> selectDictDataByType(String dictType);

    /**
     * 根据父字典类型查询子字典数据信息
     *
     * @param dictType
     * @return
     */
    List<SysDictTypeItemsVO> dictDateByParentType(String dictType);

    /**
     * 新增字典类型
     *
     * @param dto
     */
    void addDictData(AddDictDataDTO dto);

    /**
     * 修改字典类型
     *
     * @param dto
     */
    void editDictData(AddDictDataDTO dto);

    /**
     * 删除字典类型
     *
     * @param vo
     */
    void deleteDictDataByIds(RemoveLongDTO vo);

    /**
     * 获取字典数据所有内容
     *
     * @return
     */
    Map<String, List<SysDictDataVO>> dictList();

    /**
     * 查询字典数据详细
     *
     * @param dictType
     * @param dictValue
     * @return
     */
    SysDictData getDictData(String dictType, String dictValue);

}
