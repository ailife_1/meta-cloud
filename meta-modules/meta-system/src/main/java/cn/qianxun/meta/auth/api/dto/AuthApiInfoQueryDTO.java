package cn.qianxun.meta.auth.api.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 13:50
 * @Description 认证API接口查询参数DTO
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AuthApiInfoQueryDTO extends BaseQueryDTO {

    /**
     * 应用id
     */
    private Long appId;

    /**
     * 是否分配应用 0未分配 1已分配
     */
    private Integer isShare;

    /**
     * 接口名称
     */
    private String apiName;

    /**
     * 接口地址
     */
    private String apiUrl;

    /**
     * 所属模块（字典:sys_operate_module）
     */
    private String apiMod;

    /**
     * 接口状态（1.启用 2.停用）
     */
    private Integer apiStatus;
}
