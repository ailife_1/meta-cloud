package cn.qianxun.meta.role.service.impl;

import cn.qianxun.meta.role.entity.SysRoleDept;
import cn.qianxun.meta.role.mapper.SysRoleDeptMapper;
import cn.qianxun.meta.role.service.ISysRoleDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色和部门关联表 服务实现类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:11
 */
@Service
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptMapper, SysRoleDept> implements ISysRoleDeptService {

}
