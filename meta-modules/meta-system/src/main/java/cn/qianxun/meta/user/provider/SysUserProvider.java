package cn.qianxun.meta.user.provider;

import cn.qianxun.meta.user.service.ISysUserService;
import cn.qianxun.meta.auth.application.service.IAuthApplicationService;
import cn.qianxun.meta.common.core.domain.LoginUser;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.user.api.feign.ISysUserProvider;
import cn.qianxun.meta.user.api.vo.ApiAuthApplicationVO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/21 18:07
 **/
@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysUserProvider implements ISysUserProvider {

    private final ISysUserService userService;
    private final IAuthApplicationService authApplicationService;

    @Override
    public LoginUser getUserInfo(String username) {
        return userService.getUserInfo(username);
    }

    @Override
    public Result<ApiAuthApplicationVO> getAppInfoByAppId(String appId, String appSecret) {
        return Result.success("查询成功", authApplicationService.getAppInfoByAppId(appId, appSecret));
    }

    @Override
    public Result<Object> updateApplicationLoginNum(Long id) {
        authApplicationService.updateApplicationLoginNum(id);
        return Result.success("操作成功");
    }

    @Override
    public Result<LoginUser> getUserInfoByUserId(Long userId) {
        return Result.success("查询成功", userService.getUserInfoByUserId(userId));
    }

    @Override
    public Result<List<LoginUser>> getUserInfoById(List<String> ids) {
        return Result.success("查询成功", userService.getUserInfoById(ids));
    }
}
