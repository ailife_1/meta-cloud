package cn.qianxun.meta.menu.mapper;

import cn.qianxun.meta.menu.dto.RouterDTO;
import cn.qianxun.meta.menu.dto.SysMenuQueryDTO;
import cn.qianxun.meta.menu.vo.MenuCascadeVO;
import cn.qianxun.meta.menu.vo.SysMenuListVO;
import cn.qianxun.meta.menu.vo.SysMenuModuleVO;
import cn.qianxun.meta.menu.vo.SysMenuTreeVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import cn.qianxun.meta.menu.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单查询传递信息
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-24 09:16:24
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 获取所有菜单列表
     *
     * @param dto
     * @return
     */
    List<SysMenuListVO> getMenuList(SysMenuQueryDTO dto);

    /**
     * 是否存在菜单子节点
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    int hasChildByMenuId(Long menuId);

    /**
     * 查询菜单使用数量
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    int checkMenuExistRole(Long menuId);

    /**
     * 根据用户ID查询全部权限（后台需要）
     *
     * @param userId    用户
     * @param modularId 模块id
     * @return 权限列表
     */
    List<String> selectMenuPermsAllByUserId(@Param("userId") Long userId, @Param("modularId") Long modularId);

    /**
     * 根据用户ID查询前端所需权限（前端需要）
     *
     * @param userId    用户
     * @param modularId 模块id
     * @return 权限列表
     */
    List<String> selectMenuPermsByUserId(@Param("userId") Long userId, @Param("modularId") Long modularId);


    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    List<String> selectMenuPermsByUserIdSingle(@Param("userId") Long userId);

    /**
     * 根据用户ID查询菜单
     *
     * @param dto
     * @return
     */
    List<SysMenuTreeVO> selectMenuTreeAll(RouterDTO dto);


    /**
     * 菜单级联
     *
     * @return
     */
    List<MenuCascadeVO> menuCascade();

    /**
     * 根据用户ID查询菜单树
     *
     * @param dto
     * @return
     */
    List<SysMenuTreeVO> selectMenuTreeByUserId(RouterDTO dto);

    /**
     * 根据用户ID查询菜单
     *
     * @param dto
     * @return
     */
    List<SysMenuModuleVO> selectMenuByUserId(RouterDTO dto);

    List<Long> selectMenuListByRoleId(@Param("roleId") Long roleId, @Param("menuCheckStrictly") boolean menuCheckStrictly);

    List<SysMenu> selectMenuListByUserId(@Param(Constants.WRAPPER) Wrapper<SysMenu> queryWrapper);
}
