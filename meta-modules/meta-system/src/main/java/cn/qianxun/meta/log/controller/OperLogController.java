package cn.qianxun.meta.log.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.vo.RemoveStringDTO;
import cn.qianxun.meta.common.log.annotation.Log;
import cn.qianxun.meta.common.log.enums.BusinessType;
import cn.qianxun.meta.log.api.dto.SysOperLog;
import cn.qianxun.meta.log.dto.OperLogQueryDTO;
import cn.qianxun.meta.log.service.ISysOperLogService;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.log.api.dto.SysOperLog;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 操作日志管理
 *
 * @Author fuzhilin
 * @Date 2021/9/3 0003 15:38
 * @Description
 */
@RestController
@RequestMapping("/web/v1/operLog")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class OperLogController {

    private final ISysOperLogService SysOperLogService;

    /**
     * 操作日志列表
     *
     * @param vo 查询参数
     * @return
     */
    @SneakyThrows
    @SaCheckPermission("web:v1:operLog:list")
    @PostMapping("/list")
    public Result<RowsData<SysOperLog>> selectOperLogList(@RequestBody OperLogQueryDTO vo) {
        return Result.success("查询成功", SysOperLogService.selectOperLogList(vo));
    }


    /**
     * 操作日志删除
     *
     * @param vo 删除参数
     * @return
     */
    @SneakyThrows
    @SaCheckPermission("web:v1:operLog:remove")
    @Log(title = "操作日志", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    public Result<Object> deleteOperLogByIds(@RequestBody @Validated RemoveStringDTO vo) {
        return Result.success("删除成功！", SysOperLogService.deleteOperLogByIds(vo));
    }


}
