package cn.qianxun.meta.safevalue.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.qianxun.meta.safevalue.dto.AddSysSafeValueParamDTO;
import cn.qianxun.meta.safevalue.service.ISysSafeValueParamService;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.safevalue.vo.SafeValTotalNumVO;
import cn.qianxun.meta.safevalue.vo.SafeValueParamDetailVO;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 安全值参数配置 前端控制器
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-07 04:26:40
 */
@Tag(name = "安全值参数配置")
@RestController
@RequestMapping("/web/v1/safeValue")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysSafeValueParamController {
    private final ISysSafeValueParamService sysSafeValueParamService;

    /**
     * 查询安全值配置详情
     */
    @SaCheckPermission("web:v1:safeValue:detail")
    @GetMapping(value = "/detail")
    public Result<SafeValueParamDetailVO> detail() {
        return Result.success("查询成功", sysSafeValueParamService.detail());
    }

    /**
     * 保存安全值配置
     */
    @SaCheckPermission("web:v1:safeValue:save")
    @PostMapping("/save")
    public Result<Object> save(@RequestBody @Validated AddSysSafeValueParamDTO dto) {
        sysSafeValueParamService.saveSafeValueParam(dto);
        return Result.success("保存成功");
    }

    /**
     * 安全值总统计
     */
    @SaCheckPermission("web:v3:safeValCount:safeValCount")
    @GetMapping("/safeValCount")
    public Result<SafeValTotalNumVO> getSafeValCount() {
        return Result.success("查询成功", sysSafeValueParamService.getSafeValCount());
    }

}
