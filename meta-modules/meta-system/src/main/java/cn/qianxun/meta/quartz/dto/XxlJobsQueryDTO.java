package cn.qianxun.meta.quartz.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description XxlJobsQueryDTO对象
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class XxlJobsQueryDTO extends BaseQueryDTO {

    /**
     * 任务分组
     */
    private int jobGroup;

    /**
     * 任务描述
     */
    private String jobDesc;

    /**
     * 作者
     */
    private String author;

    /**
     * 调度类型 字典schedule_type
     */
    private String scheduleType;

    /**
     * 执行器任务handler
     */
    private String executorHandler;

    /**
     * 状态
     */
    private int triggerStatus;


    /**
     * 添加开始时间 yyyy-MM-dd HH:mm:ss
     */
    private String addBeginTime;

    /**
     * 添加结束时间 yyyy-MM-dd HH:mm:ss
     */
    private String addEndTime;


}
