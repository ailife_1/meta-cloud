package cn.qianxun.meta.area.controller;

import cn.qianxun.meta.area.dto.SysAreaCasCaderDTO;
import cn.qianxun.meta.area.service.ISysAreaService;
import cn.qianxun.meta.common.core.domain.Result;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 行政区域管理表 前端控制器
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-29 09:09:01
 */
@Tag(name = "区域管理")
@RestController
@Validated
@RequestMapping("/web/v1/sysArea")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysAreaController {

    private final ISysAreaService sysAreaService;

    /**
     * 从缓存中获取行政区域列表数据，用于地区三级级联选择
     *
     * @return
     */
    //@SaCheckPermission("web:v3:sysArea:getAreaListCasCaderData")
    @GetMapping("/getAreaListCasCaderData")
    public Result<List<SysAreaCasCaderDTO>> getAreaListCasCaderData() {
        return Result.success("查询成功", sysAreaService.getAreaListCasCaderData());
    }

}
