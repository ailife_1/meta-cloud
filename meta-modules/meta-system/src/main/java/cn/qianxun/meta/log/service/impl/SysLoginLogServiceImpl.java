package cn.qianxun.meta.log.service.impl;

import cn.easyes.core.biz.EsPageInfo;
import cn.easyes.core.conditions.LambdaEsQueryWrapper;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.utils.StringUtils;
import cn.qianxun.meta.common.core.web.vo.RemoveStringDTO;
import cn.qianxun.meta.log.service.ISysLoginLogService;
import cn.qianxun.meta.log.api.dto.SysLoginLog;
import cn.qianxun.meta.log.dto.LoginLogQueryDTO;
import cn.qianxun.meta.log.esmapper.SysLoginLogMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/9/4 16:12
 **/
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysLoginLogServiceImpl implements ISysLoginLogService {
    private final SysLoginLogMapper sysLoginLogMapper;

    @Override
    public RowsData<SysLoginLog> selectLoginLogList(LoginLogQueryDTO vo) {
        LambdaEsQueryWrapper<SysLoginLog> lambdaEsQueryWrapper = new LambdaEsQueryWrapper<>();
        lambdaEsQueryWrapper.like(StringUtils.isNotEmpty(vo.getUserName()),"user_name.keyword", vo.getUserName())
                .eq(StringUtils.isNotEmpty(vo.getStatus()), SysLoginLog::getStatus, vo.getStatus())
                .like(StringUtils.isNotEmpty(vo.getIpaddr()), "ip_addr.keyword", vo.getIpaddr())
                .ge(StringUtils.isNotEmpty(vo.getBeginTime()), SysLoginLog::getLoginTime, vo.getBeginTime())
                .le(StringUtils.isNotEmpty(vo.getEndTime()), SysLoginLog::getLoginTime, vo.getEndTime()).orderByDesc(SysLoginLog::getLoginTime);
        EsPageInfo<SysLoginLog> SysOperLogEsPageInfo = sysLoginLogMapper.pageQuery(lambdaEsQueryWrapper, vo.getPageNum(), vo.getPageSize());
        RowsData<SysLoginLog> SysOperLogRowsData = new RowsData<>(SysOperLogEsPageInfo.getList(), SysOperLogEsPageInfo.getTotal(), vo.getPageNum());
        return SysOperLogRowsData;
    }

    @Override
    public Boolean deleteLoginLogByIds(RemoveStringDTO vo) {
        return sysLoginLogMapper.deleteBatchIds(vo.getRemoveIdList()) > 0;
    }
}
