package cn.qianxun.meta.quartz.service;

import cn.qianxun.meta.common.core.dto.RowsData;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxl.job.core.biz.model.ReturnT;
import cn.qianxun.meta.quartz.domain.XxlJobInfo;
import cn.qianxun.meta.quartz.dto.XxlJobEditDTO;
import cn.qianxun.meta.quartz.dto.XxlJobInsertDTO;
import cn.qianxun.meta.quartz.dto.XxlJobsQueryDTO;
import cn.qianxun.meta.quartz.vo.XxlJobInfoVO;
import cn.qianxun.meta.quartz.vo.XxlJobSelectVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description
 */
public interface IXxlJobInfoService extends IService<XxlJobInfo> {

    /**
     * 查询任务列表
     *
     * @param queryDTO dto
     * @return List
     */
    RowsData<XxlJobInfoVO> getXxlJobs(XxlJobsQueryDTO queryDTO);

    /**
     * 获取任务调度时间
     *
     * @param ids ids
     * @return Map
     */
    Map<String, XxlJobInfo> getJobTriggerTime(List<String> ids);


    /**
     * 新增任务
     *
     * @param insertDTO dto
     * @return Result
     */
    ReturnT<String> add(XxlJobInsertDTO insertDTO);

    /**
     * 编辑任务
     *
     * @param editDTO editDTO
     * @return Result
     */
    ReturnT<String> update(XxlJobEditDTO editDTO);

    /**
     * 删除任务
     *
     * @param id id
     * @return Result
     */
    ReturnT<String> delJob(String id);

    /**
     * 停止任务
     *
     * @param id id
     * @return Result
     */
    ReturnT<String> stop(String id);

    /**
     * start job
     *
     * @param id id
     * @return Result
     */
    ReturnT<String> start(int id);


    int scheduleUpdate(XxlJobInfo xxlJobInfo);

    List<XxlJobInfo> scheduleJobQuery(@Param("maxNextTime") long maxNextTime, @Param("pagesize") int pagesize);


    /**
     * 任务下拉选
     */
    List<XxlJobSelectVO> getJobsByGroup();


}
