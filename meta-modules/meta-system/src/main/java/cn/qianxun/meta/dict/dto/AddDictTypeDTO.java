package cn.qianxun.meta.dict.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author fuzhilin
 * @Description
 * @since 2023/08/24
 */
@Data
@Schema(name = "添加字典类型DTO")
public class AddDictTypeDTO {
    @Schema(description = "字典id")
    private Long dictId;

    @NotBlank(message = "字典名称不能为空")
    @Schema(description = "字典名称")
    private String dictName;

    @NotBlank(message = "字典类型不能为空")
    @Schema(description = "字典类型")
    private String dictType;

    @NotBlank(message = "字典状态不能为空")
    @Schema(description = "状态（0正常 1停用）")
    private String status;

    @Schema(description = "备注")
    private String remark;
}
