package cn.qianxun.meta.auth.application.enums;

import lombok.Getter;

/**
 * @Author fuzhilin
 * @Date 2023/9/13 14:18
 * @Description 应用类型枚举
 */
@Getter
public enum ApplicationTypeEnum {
    API(1, "api接口调用"),
    AUTH(2, "统一认证");

    private final Integer code;
    private final String info;

    ApplicationTypeEnum(Integer code, String info) {
        this.code = code;
        this.info = info;
    }

}
