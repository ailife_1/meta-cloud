package cn.qianxun.meta.dict.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.dto.SelectLongDTO;
import cn.qianxun.meta.common.core.web.dto.SelectStringDTO;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.common.log.annotation.Log;
import cn.qianxun.meta.common.log.enums.BusinessType;
import cn.qianxun.meta.dict.dto.AddDictDataDTO;
import cn.qianxun.meta.dict.dto.QueryDictDataDTO;
import cn.qianxun.meta.dict.service.ISysDictDataService;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.dict.vo.SysDictDataVO;
import cn.qianxun.meta.dict.vo.SysDictTypeItemsVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典数据表 前端控制器
 * </p>
 *
 * @author fuzhilin
 * @since 2023/08/24
 */
@Tag(name = "字典数据管理")
@RestController
@Validated
@RequestMapping("/web/v1/sysDictData")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysDictDataController {
    private final ISysDictDataService sysDictDataService;

    @SaCheckPermission("web:v1:sysDictData:list")
    @Operation(summary = "获取字典数据列表")
    @PostMapping("/list")
    public Result<RowsData<SysDictDataVO>> list(@RequestBody QueryDictDataDTO dto) {
        return Result.success(sysDictDataService.selectDictDataList(dto));
    }

    @SaCheckPermission("web:v1:sysDictData:details")
    @Operation(summary = "查询字典数据详细")
    @PostMapping(value = "/details")
    public Result<SysDictDataVO> details(@RequestBody SelectLongDTO dictCode) {
        return Result.success(sysDictDataService.details(dictCode.getIdLong()));
    }

    @SaCheckPermission("web:v1:sysDictData:dictType")
    @Operation(summary = "根据字典类型查询字典数据信息")
    @PostMapping(value = "/dictType")
    public Result<List<SysDictDataVO>> dictType(@RequestBody SelectStringDTO dictType) {
        return Result.success(sysDictDataService.selectDictDataByType(dictType.getIdStr()));
    }

    @SaCheckPermission("web:v1:sysDictData:byParentType")
    @Operation(summary = "根据父字典类型查询子字典数据信息")
    @PostMapping(value = "/byParentType")
    public Result<List<SysDictTypeItemsVO>> dictDateByParentType(@RequestBody SelectStringDTO dictType) {
        return Result.success(sysDictDataService.dictDateByParentType(dictType.getIdStr()));
    }

    @SaCheckPermission("web:v1:sysDictData:add")
    @Log(title = "字典数据管理-新增字典数据", businessType = BusinessType.INSERT)
    @Operation(summary = "新增字典数据")
    @PostMapping("addDictData")
    public Result<String> addDictData(@Validated @RequestBody AddDictDataDTO dto) {
        sysDictDataService.addDictData(dto);
        return Result.success("添加成功");
    }

    @SaCheckPermission("web:v1:sysDictData:edit")
    @Log(title = "字典数据管理-修改字典数据", businessType = BusinessType.UPDATE)
    @Operation(summary = "修改字典数据")
    @PostMapping("/editDictData")
    public Result<String> editDictData(@Validated @RequestBody AddDictDataDTO dto) {
        sysDictDataService.editDictData(dto);
        return Result.success("修改成功");
    }

    @Log(title = "字典数据管理-删除字典数据", businessType = BusinessType.DELETE)
    @Operation(summary = "删除字典数据")
    @SaCheckPermission("web:v1:sysDictData:remove")
    @PostMapping("/remove")
    public Result<String> deleteDictDataByIds(@RequestBody RemoveLongDTO vo) {
        sysDictDataService.deleteDictDataByIds(vo);
        return Result.success("删除成功！");
    }

    @Operation(summary = "获取字典数据所有内容")
    @PostMapping("/dictList")
    public Result<Map<String, List<SysDictDataVO>>> dictList() {
        return Result.success(sysDictDataService.dictList());
    }

}

