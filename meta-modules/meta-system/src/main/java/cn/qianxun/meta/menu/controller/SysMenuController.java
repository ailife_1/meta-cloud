package cn.qianxun.meta.menu.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.lang.tree.Tree;
import cn.qianxun.meta.common.core.web.dto.SelectStringDTO;
import cn.qianxun.meta.menu.dto.AddSysMenuDTO;
import cn.qianxun.meta.menu.dto.EditSysMenuDTO;
import cn.qianxun.meta.menu.dto.SysMenuModuleQueryDTO;
import cn.qianxun.meta.menu.dto.SysMenuQueryDTO;
import cn.qianxun.meta.menu.entity.SysMenu;
import cn.qianxun.meta.menu.service.ISysMenuService;
import cn.qianxun.meta.menu.vo.MenuCascadeVO;
import cn.qianxun.meta.menu.vo.SysMenuDetailsVO;
import cn.qianxun.meta.menu.vo.SysMenuListVO;
import cn.qianxun.meta.menu.vo.SysMenuModuleVO;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.common.satoken.utils.LoginHelper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单信息表 前端控制器
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-21 02:16:24
 */
@Tag(name = "菜单管理")
@RestController
@Validated
@RequestMapping("/web/v1/sysMenu")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysMenuController {
    private final ISysMenuService sysMenuService;

    @SaCheckPermission("web:v1:sysMenu:list")
    @Operation(summary = "菜单列表")
    @PostMapping("/list")
    public Result<List<SysMenuListVO>> getMenuList(@RequestBody @Validated SysMenuQueryDTO dto) {
        return Result.success("查询成功！", sysMenuService.getMenuList(dto));
    }

    @SaCheckPermission("web:v1:sysMenu:add")
    @Operation(summary = "添加菜单")
    @PostMapping("/addMenu")
    public Result<String> addMenu(@RequestBody @Validated AddSysMenuDTO dto) {
        sysMenuService.addMenu(dto);
        return Result.success("添加成功！");
    }

    @SaCheckPermission("web:v1:sysMenu:edit")
    @Operation(summary = "修改菜单")
    @PostMapping("/editMenu")
    public Result<String> editMenu(@RequestBody @Validated EditSysMenuDTO dto) {
        sysMenuService.editMenu(dto);
        return Result.success("修改成功！");
    }

    @SaCheckPermission("web:v1:sysMenu:details")
    @Operation(summary = "根据id获取菜单详情")
    @PostMapping("/details")
    public Result<SysMenuDetailsVO> details(@RequestBody SelectStringDTO selecStringDTO) {
        return Result.success("查询成功！", sysMenuService.details(selecStringDTO.getIdStr()));
    }

    @SaCheckPermission("web:v1:sysMenu:delete")
    @Operation(summary = "根据id删除菜单")
    @PostMapping("/deleteMenuById")
    public Result<SysMenuDetailsVO> deleteMenuById(@RequestBody SelectStringDTO selecStringDTO) {
        sysMenuService.deleteMenuById(selecStringDTO.getIdStr());
        return Result.success("删除成功！");
    }


    @SaCheckPermission("web:v1:sysMenu:menuCascade")
    @Operation(summary = "菜单级联")
    @PostMapping("/menuCascade")
    public Result<List<MenuCascadeVO>> menuCascade() {
        return Result.success("查询成功！", sysMenuService.menuCascade());
    }

    @SaCheckPermission("web:v1:sysMenu:menuModuleList")
    @Operation(summary = "菜单模块列表")
    @PostMapping("/menuModuleList")
    public Result<List<SysMenuModuleVO>> getMenuModuleList(@RequestBody @Validated SysMenuModuleQueryDTO dto) {
        return Result.success("查询成功！", sysMenuService.getMenuModuleList(dto));
    }


    /**
     * 获取菜单下拉树列表
     */
    @PostMapping("/treeselect")
    public Result<List<Tree<Long>>> treeselect(@RequestBody SysMenu menu) {
        Long userId = LoginHelper.getUserId();
        List<SysMenu> menus = sysMenuService.selectMenuList(menu, userId);
        return Result.success(sysMenuService.buildMenuTreeSelect(menus));
    }

    /**
     * 加载对应角色菜单列表树
     */
    @PostMapping(value = "/roleMenuTreeselectByRoleId")
    public Result<Map<String, Object>> roleMenuTreeselect() {
        Long userId = LoginHelper.getUserId();
        List<SysMenu> menus = sysMenuService.selectMenuList(userId);
        Map<String, Object> ajax = new HashMap<>();
        ajax.put("menus", sysMenuService.buildMenuTreeSelect(menus));
        return Result.success(ajax);
    }

}

