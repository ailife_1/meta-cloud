package cn.qianxun.meta.config.feign;

import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.config.api.feign.ISysConfigProvider;
import cn.qianxun.meta.config.service.ISysConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author fuzhilin
 * @Date 2023/16/35
 * @Description
 */
@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysConfigProvider implements ISysConfigProvider {

    private final ISysConfigService configService;

    @Override
    public Result<String> getConfigKey(@RequestParam String configKey) {
        return Result.success("查询成功!", configService.selectConfigByKey(configKey));
    }
}
