package cn.qianxun.meta.area.dto;

import lombok.Data;

import java.util.List;

/**
 * 行政区域DTO响应类
 *
 * @Description
 */
@Data
public class SysAreaDTO {

    /**
     * 区域编码
     */
    private Long code;
    /**
     * 区域名称
     */
    private String name;
    /**
     * 区域简称
     */
    private String shortName;
    /**
     * 级别：1.省级2.市级3.县区级
     */
    private String level;
    /**
     * 父级区域代码
     */
    private Long parentId;
    /**
     * 区域排序
     */
    private Integer orderNum;
    /**
     * 描述信息
     */
    private String remark;
    /**
     * 是否自定义：1是，2否
     */
    private String isSelf;
    /**
     * 是否启用：1.启用，2.停用
     */
    private String status;
    /**
     * 父级菜单ids
     */
    private List<Long> areaNameArray;
    /**
     * 是否存在子级 true存在false不存在
     */
    private Boolean hasChild;

}
