package cn.qianxun.meta.quartz.mapper;


import cn.qianxun.meta.quartz.dto.XxlJobLogQueryDTO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.qianxun.meta.quartz.domain.XxlJobLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description
 */
@Mapper
public interface XxlJobLogMapper extends BaseMapper<XxlJobLog> {

    /**
     * 任务日志列表
     *
     * @param queryDTO dto
     * @return List
     */
    List<XxlJobLog> getXxlJobLogs(XxlJobLogQueryDTO queryDTO);

    int clearLog(@Param("logIds") List<String> logIds);

    int updateHandleInfo(XxlJobLog xxlJobLog);

    int updateTriggerInfo(XxlJobLog xxlJobLog);


    int delLogByJobId(@Param("jobId") String jobId);


    int delLogGlueByJobId(@Param("jobId") String jobId);

}
