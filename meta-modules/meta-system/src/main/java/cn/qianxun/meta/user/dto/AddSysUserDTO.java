package cn.qianxun.meta.user.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.Date;

/**
 *
 */
@Schema(description = "新增/修改用户VO")
@Data
public class AddSysUserDTO {
    @Schema(description = "用户ID")
    private Long userId;

    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    @Schema(description = "用户昵称")
    private String nickName;

    @NotNull(message = "用户部门不能为空")
    @Schema(description = "部门名称")
    private Long deptId;

    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符")
    @Schema(description = "用户账号")
    private String userName;

    @NotBlank(message = "用户性别不能为空")
    @Schema(description = "用户性别（0男 1女 2未知）")
    private String sex;

    @NotBlank(message = "用户手机号不能为空")
    @Schema(description = "手机号码")
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    private String phonenumber;

    @NotBlank(message = "用户邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    @Schema(description = "用户邮箱")
    private String email;

    @Schema(description = "密码")
    private String password;


    private Long roleId;

    /**
     * 角色ID数据组
     */
    @NotNull(message = "用户角色不能为空")
    private Long[] roleIds;

    @NotBlank(message = "用户状态不能为空")
    @Schema(description = "帐号状态（0正常 1停用）")
    private String status;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "限制登录次数")
    private String limitLoginNum;

    @Schema(description = "限制登录时间")
    private String limitLoginTime;

}
