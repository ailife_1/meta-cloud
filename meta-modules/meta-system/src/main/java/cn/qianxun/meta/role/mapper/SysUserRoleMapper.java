package cn.qianxun.meta.role.mapper;

import cn.qianxun.meta.role.entity.SysUserRole;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * <p>
 * 用户和角色关联表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:32
 */
public interface SysUserRoleMapper extends BaseMapperPlus<SysUserRoleMapper, SysUserRole, SysUserRole> {

}
