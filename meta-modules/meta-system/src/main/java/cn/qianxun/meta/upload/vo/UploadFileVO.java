package cn.qianxun.meta.upload.vo;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/6 11:47
 * @Description 文件上传相应参数VO
 */
@Data
public class UploadFileVO {
    /**
     * bucketName
     */
    private String bucketName;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件目录
     */
    private String fileDir;

    /**
     * 文件路径
     */
    private String url;

    /**
     * 生成文件id
     */
    private String fileId;

    /**
     * 文件类型
     */
    private String type;


}
