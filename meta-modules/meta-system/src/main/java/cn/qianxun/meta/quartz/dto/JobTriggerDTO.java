package cn.qianxun.meta.quartz.dto;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description JobTriggerDTO对象
 */
@Data
public class JobTriggerDTO {

    /**
     * 任务id
     */
    private int id;

    /**
     * 执行参数
     */

    private String executorParam;

    /**
     * 执行器地址
     */
    private String addressList;

}
