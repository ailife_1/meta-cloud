package cn.qianxun.meta.config.service;


import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.config.dto.AddSysConfigDTO;
import cn.qianxun.meta.config.dto.SysConfigQueryDTO;
import cn.qianxun.meta.config.entity.SysConfig;
import cn.qianxun.meta.config.vo.SysConfigVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Author fuzhilin
 * @Date 2023/16/35
 * @Description
 */
public interface ISysConfigService extends IService<SysConfig> {

    /**
     * 根据参数键名查询参数值
     *
     * @param configKey
     * @return
     */
    String selectConfigByKey(String configKey);

    /**
     * 获取参数配置列表
     *
     * @param dto
     * @return
     */
    RowsData<SysConfigVO> selectConfigList(SysConfigQueryDTO dto);

    /**
     * 根据id获取参数配置详情
     *
     * @param configId
     * @return
     */
    SysConfigVO details(String configId);

    /**
     * 新增参数配置
     *
     * @param dto
     */
    void addConfig(AddSysConfigDTO dto);

    /**
     * 修改参数配置
     *
     * @param dto
     */
    void editConfig(AddSysConfigDTO dto);

    /**
     * 删除参数配置
     *
     * @param dto
     */
    void deleteConfigByIds(RemoveLongDTO dto);

    /**
     * 清空缓存
     */
    void clearCache();

    /**
     * 参数配置列表
     *
     * @return
     */
    Map<String, List<SysConfigVO>> configList();
}
