package cn.qianxun.meta.safevalue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.qianxun.meta.safevalue.entity.SysSafeValueParam;

/**
 * <p>
 * 安全值参数配置 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-07 04:26:40
 */
public interface SysSafeValueParamMapper extends BaseMapper<SysSafeValueParam> {

}
