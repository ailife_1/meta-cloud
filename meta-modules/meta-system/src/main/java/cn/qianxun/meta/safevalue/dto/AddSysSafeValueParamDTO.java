package cn.qianxun.meta.safevalue.dto;

import cn.qianxun.meta.safevalue.vo.SafeValueParamItemVO;
import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/7 17:35
 * @Description 添加安全值参数请求参数DTO
 */
@Data
public class AddSysSafeValueParamDTO {

    /**
     * id
     */
    private String id;

    /**
     * 参数配置
     */
    private List<SafeValueParamItemVO> content;
}
