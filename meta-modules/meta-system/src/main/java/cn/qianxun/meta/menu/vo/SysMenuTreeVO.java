package cn.qianxun.meta.menu.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 菜单查询传递信息
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-24 09:16:24
 */
@Schema(name = "菜单树响应VO")
@Data
public class SysMenuTreeVO {
    @Schema(description = "菜单ID")
    private Long menuId;

    @Schema(description = "菜单名称")
    private String menuName;

    @Schema(description = "父菜单ID")
    private Long parentId;

    @Schema(description = "显示顺序")
    private Integer orderNum;

    @Schema(description = "路由地址")
    private String path;

    @Schema(description = "组件路径")
    private String component;

    @Schema(description = "是否为外链（0是 1否）")
    private String isFrame;

    @Schema(description = "菜单类型（M模块 D目录 N菜单 B按钮 C统计图）")
    private String menuType;

    @Schema(description = "是否是列表页面（1是，0否）")
    private String isList;

    @Schema(description = "菜单状态（0显示 1隐藏）")
    private String visible;

    @Schema(description = "菜单状态（0正常 1停用）")
    private String status;

    @Schema(description = "当前路由是否不缓存（默认值：false）0 true 1 false")
    private String isCache;

    @Schema(description = "权限标识")
    private String perms;

    @Schema(description = "菜单图标")
    private String icon;

    @Schema(description = "菜单使用平台（字典menu_platform）")
    private String menuPlatform;

    @Schema(description = "三级菜单是否展示 0否 1是")
    private Integer isShow;

    @Schema(description = "是否推荐 0否 1是")
    private Integer isRecommend;

    @Schema(description = "创建者")
    private String createBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "子菜单")
    private List<SysMenuTreeVO> children;

    @Schema(description = "父级菜单ids")
    private List<Long> menuNameArray;

}
