package cn.qianxun.meta.board.service;

import cn.qianxun.meta.board.dto.ScreenBoardDTO;
import cn.qianxun.meta.board.entity.ScreenBoard;
import cn.qianxun.meta.board.vo.ScreenBoardVO;
import cn.qianxun.meta.common.core.dto.RowsData;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * 大屏看板表(ScreenBoard)表服务接口
 *
 * @author fanfu1024
 * @since 2023-11-10 10:37:04
 */
public interface IScreenBoardService extends IService<ScreenBoard> {

    RowsData<ScreenBoardVO> pageList(ScreenBoardDTO chartBoardDTO);

    void updateAssignFlag(Integer assignFlag, List<Long> ids);

    void updateSorted(List<ScreenBoard> list);

    /**
     * 获取大屏数据权限
     *
     * @return
     */
    Set<String> getScreenBoardPermission();
}

