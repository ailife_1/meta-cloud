package cn.qianxun.meta.menu.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * <p>
 * 菜单查询传递信息
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-24 09:16:24
 */
@Schema(name = "菜单级联响应VO")
@Data
public class MenuCascadeVO {
    @Schema(name = "菜单ID")
    private Long menuId;

    @Schema(name = "菜单名称")
    private String menuName;

    @Schema(name = "父菜单ID")
    private Long parentId;

    @Schema(name = "菜单类型（M模块 D目录 N菜单 B按钮 C统计图）")
    private String menuType;
}
