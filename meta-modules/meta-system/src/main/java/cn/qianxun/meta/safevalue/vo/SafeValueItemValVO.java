package cn.qianxun.meta.safevalue.vo;


import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2021/8/23 0023 15:37
 * @Description 安全值配置参数条目 VO
 */
@Data
public class SafeValueItemValVO {

    /**
     * 条目key
     */
    private String key;

    /**
     * 条目val
     */
    private String value;

}
