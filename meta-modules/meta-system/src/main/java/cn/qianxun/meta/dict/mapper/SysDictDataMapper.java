package cn.qianxun.meta.dict.mapper;


import cn.qianxun.meta.dict.dto.QueryDictDataDTO;
import cn.qianxun.meta.dict.entity.SysDictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.qianxun.meta.dict.vo.SysDictDataVO;

import java.util.List;

/**
 * <p>
 * 字典数据表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023/08/24
 */
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

    /**
     * 获取字典数据列表
     *
     * @param vo
     * @return
     */
    List<SysDictDataVO> selectDictDataList(QueryDictDataDTO vo);

    /**
     * 根据id删除字典数据
     *
     * @param removeIdList
     * @return
     */
    int deleteDictDataByIds(List<Long> removeIdList);
}
