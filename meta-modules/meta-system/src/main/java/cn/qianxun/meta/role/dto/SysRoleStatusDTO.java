package cn.qianxun.meta.role.dto;

import lombok.Data;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/28 10:34
 **/
@Data
public class SysRoleStatusDTO {

    /**
     * 角色ID
     */
    private Long roleId;


    /**
     * 角色状态（0正常 1停用）
     */
    private String status;


}
