package cn.qianxun.meta.board.service;

import cn.qianxun.meta.board.dto.ChartBoardDTO;
import cn.qianxun.meta.board.entity.ChartBoard;
import cn.qianxun.meta.board.vo.ChartBoardVO;
import cn.qianxun.meta.common.core.dto.RowsData;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * 看板管理 (ChartBoard)表服务接口
 *
 * @author fanfu1024
 * @since 2023-11-08 14:57:16
 */
public interface IChartBoardService extends IService<ChartBoard> {


    RowsData<ChartBoardVO> pageList(ChartBoardDTO chartBoardDTO);

    void updateAssignFlag(Integer assignFlag, List<Long> ids);

    void updateSorted(List<ChartBoard> list);

    /**
     * 获取看板数据权限
     *
     * @return
     */
    Set<String> getChartBoardPermission();
}

