package cn.qianxun.meta.board.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.qianxun.meta.board.dto.ChartBoardDTO;
import cn.qianxun.meta.board.entity.ChartBoard;
import cn.qianxun.meta.board.mapper.ChartBoardMapper;
import cn.qianxun.meta.board.service.IChartBoardService;
import cn.qianxun.meta.board.vo.ChartBoardVO;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 看板管理 (ChartBoard)表服务实现类
 *
 * @author fanfu1024
 * @since 2023-11-08 14:57:16
 */
@Service("chartBoardService")
public class ChartBoardServiceImpl extends ServiceImpl<ChartBoardMapper, ChartBoard> implements IChartBoardService {
    @Autowired
    private ChartBoardMapper chartBoardMapper;

    @Override
    public RowsData<ChartBoardVO> pageList(ChartBoardDTO chartBoardDTO) {
        Page<Object> page = PageHelper.startPage(chartBoardDTO.getPageNum(), chartBoardDTO.getPageSize());
        QueryWrapper<ChartBoard> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StringUtils.isNotBlank(chartBoardDTO.getFullName()), "full_name", chartBoardDTO.getFullName())
                .like(StringUtils.isNotBlank(chartBoardDTO.getSimpleName()), "simle_name", chartBoardDTO.getSimpleName())
                .like(StringUtils.isNotBlank(chartBoardDTO.getBoardCode()), "borad_code", chartBoardDTO.getBoardCode())
                .eq(ObjectUtil.isNotEmpty(chartBoardDTO.getStatus()), "status", chartBoardDTO.getStatus())
                .eq(StringUtils.isNotBlank(chartBoardDTO.getBoardType()), "board_type", chartBoardDTO.getBoardType())
                .eq("del_flag", 0)
                .orderByAsc(Arrays.asList("sorted", "id"));
        List<ChartBoard> list = this.chartBoardMapper.selectList(queryWrapper);
        RowsData<ChartBoardVO> rowsData = new RowsData<>();
        if (CollectionUtil.isNotEmpty(list)) {
            List<ChartBoardVO> chartBoardVOS = BeanUtil.copyToList(list, ChartBoardVO.class);
            rowsData.setRows(chartBoardVOS);
            rowsData.setPage(chartBoardDTO.getPageNum());
            rowsData.setTotal(page.getTotal());
        }
        return rowsData;
    }

    @Override
    public void updateAssignFlag(Integer assignFlag, List<Long> ids) {
        this.chartBoardMapper.updateAssignFlag(assignFlag, ids);
    }

    @Override
    public void updateSorted(List<ChartBoard> list) {
        this.chartBoardMapper.updateBatchById(list);
    }

    @Override
    public Set<String> getChartBoardPermission() {
        Set<String> perms = baseMapper.getChartBoardPermission();
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms) {
            if (StringUtils.isNotEmpty(perm)) {
                permsSet.addAll(StringUtils.splitList(perm.trim()));
            }
        }
        return permsSet;
    }
}

