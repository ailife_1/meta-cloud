package cn.qianxun.meta.auth.api.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 14:34
 * @Description 分配API接口请求参数DTO
 */
@Data
public class DistributionApiDTO {

    /**
     * 应用id
     */
    @NotNull(message = "appId不能为空")
    private Long appId;

    /**
     * apiId集合
     */
    @NotEmpty(message = "apiId不能为空")
    private List<Long> apiIdList;

    /**
     * 新增还是移除 1新增 2移除
     */
    @NotNull(message = "请选择新增还是移除")
    private Integer addOrDel;

}
