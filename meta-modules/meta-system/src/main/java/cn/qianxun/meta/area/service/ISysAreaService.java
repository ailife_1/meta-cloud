package cn.qianxun.meta.area.service;

import cn.qianxun.meta.area.vo.SysAreaInsertVO;
import cn.qianxun.meta.area.vo.SysAreaQueryVO;
import cn.qianxun.meta.area.vo.SysAreaStatusVO;
import cn.qianxun.meta.area.vo.SysAreaUpdateVO;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.area.dto.SysAreaCasCaderDTO;
import cn.qianxun.meta.area.dto.SysAreaDTO;
import cn.qianxun.meta.area.entity.SysArea;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 行政区域管理表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-29 09:09:01
 */
public interface ISysAreaService extends IService<SysArea> {
    /**
     * 查询行政区域列表
     *
     * @param vo
     * @return
     */
    List<SysAreaDTO> getAreaList(SysAreaQueryVO vo);

    /**
     * 新增行政区域
     *
     * @param vo
     */
    void addArea(SysAreaInsertVO vo);

    /**
     * 修改行政区域
     *
     * @param vo
     */
    void editArea(SysAreaUpdateVO vo);

    /**
     * 获取行政区域详细信息
     *
     * @param code
     * @return
     */
    SysAreaDTO details(Long code);

    /**
     * 删除行政区域
     *
     * @param vo
     */
    void deleteAreaByIds(RemoveLongDTO vo);

    /**
     * 修改状态
     *
     * @param vo
     */
    void editStatus(SysAreaStatusVO vo);

    /**
     * 查询省、市两级行政区域列表信息
     *
     * @return
     */
    List<SysAreaDTO> queryProvinceCityTwoList();

    /**
     * 从缓存中获取行政区域列表数据，用于地区三级级联选择
     *
     * @return
     */
    List<SysAreaCasCaderDTO> getAreaListCasCaderData();


    /**
     * 查询省市区最下级code
     *
     * @return
     */
    Long getLastCode(String province, String city, String area);
}
