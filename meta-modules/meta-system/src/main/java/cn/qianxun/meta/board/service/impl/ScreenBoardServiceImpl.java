package cn.qianxun.meta.board.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.qianxun.meta.board.dto.ScreenBoardDTO;
import cn.qianxun.meta.board.entity.ScreenBoard;
import cn.qianxun.meta.board.mapper.ScreenBoardMapper;
import cn.qianxun.meta.board.vo.ScreenBoardVO;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.qianxun.meta.board.service.IScreenBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 大屏看板表(ScreenBoard)表服务实现类
 *
 * @author fanfu1024
 * @since 2023-11-10 10:37:04
 */
@Service("screenBoardService")
public class ScreenBoardServiceImpl extends ServiceImpl<ScreenBoardMapper, ScreenBoard> implements IScreenBoardService {
    @Autowired
    private ScreenBoardMapper screenBoardMapper;

    @Override
    public RowsData<ScreenBoardVO> pageList(ScreenBoardDTO screenBoardDTO) {
        Page<Object> page = PageHelper.startPage(screenBoardDTO.getPageNum(), screenBoardDTO.getPageSize());
        QueryWrapper<ScreenBoard> queryWrapper = new QueryWrapper<ScreenBoard>();
        queryWrapper.orderByAsc(Arrays.asList("sorted", "id"))
                .like(StringUtils.isNotBlank(screenBoardDTO.getFullName()), "full_name", screenBoardDTO.getFullName())
                .like(StringUtils.isNotBlank(screenBoardDTO.getSimpleName()), "simle_name", screenBoardDTO.getSimpleName())
                .like(StringUtils.isNotBlank(screenBoardDTO.getBoardCode()), "borad_code", screenBoardDTO.getBoardCode())
                .eq(ObjectUtil.isNotEmpty(screenBoardDTO.getStatus()), "status", screenBoardDTO.getStatus())
                .eq(StringUtils.isNotBlank(screenBoardDTO.getBoardType()), "board_type", screenBoardDTO.getBoardType())
                .eq("del_flag", 0);
        List<ScreenBoard> list = this.screenBoardMapper.selectList(queryWrapper);
        RowsData<ScreenBoardVO> rowsData = new RowsData<>();
        if (CollectionUtil.isNotEmpty(list)) {
            List<ScreenBoardVO> screenBoardVOS = BeanUtil.copyToList(list, ScreenBoardVO.class);
            rowsData.setRows(screenBoardVOS);
            rowsData.setPage(screenBoardDTO.getPageNum());
            rowsData.setTotal(page.getTotal());
        }
        return rowsData;
    }

    @Override
    public void updateAssignFlag(Integer assignFlag, List<Long> ids) {
        this.screenBoardMapper.updateAssignFlag(assignFlag, ids);
    }

    @Override
    public void updateSorted(List<ScreenBoard> list) {
        this.screenBoardMapper.updateBatchById(list);
    }

    @Override
    public Set<String> getScreenBoardPermission() {
        Set<String> perms = baseMapper.getScreenBoardPermission();
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms) {
            if (StringUtils.isNotEmpty(perm)) {
                permsSet.addAll(StringUtils.splitList(perm.trim()));
            }
        }
        return permsSet;
    }
}

