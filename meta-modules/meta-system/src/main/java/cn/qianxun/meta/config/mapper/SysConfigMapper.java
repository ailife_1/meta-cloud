package cn.qianxun.meta.config.mapper;

import cn.qianxun.meta.config.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.qianxun.meta.config.dto.SysConfigQueryDTO;
import cn.qianxun.meta.config.vo.SysConfigVO;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/16/35
 * @Description
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

    /**
     * 获取参数配置列表
     *
     * @param vo
     * @return
     */
    List<SysConfigVO> selectConfigList(SysConfigQueryDTO vo);

    /**
     * 删除参数配置
     *
     * @param removeIdList
     * @return
     */
    int deleteConfigByIds(List<Long> removeIdList);
}
