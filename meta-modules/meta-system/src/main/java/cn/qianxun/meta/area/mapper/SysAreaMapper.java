package cn.qianxun.meta.area.mapper;

import cn.qianxun.meta.area.dto.SysAreaDTO;
import cn.qianxun.meta.area.vo.SysAreaQueryVO;
import cn.qianxun.meta.area.entity.SysArea;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 行政区域管理表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-29 09:09:01
 */
public interface SysAreaMapper extends BaseMapperPlus<SysAreaMapper, SysArea, SysArea> {

    /**
     * 查询行政区域列表
     *
     * @param vo
     * @return
     */
    List<SysAreaDTO> getAreaList(SysAreaQueryVO vo);

    /**
     * 批量更改区域状态
     *
     * @param status
     * @param childIds
     * @return
     */
    void updateByIds(@Param("status") String status, @Param("childIds") List<Long> childIds);

}
