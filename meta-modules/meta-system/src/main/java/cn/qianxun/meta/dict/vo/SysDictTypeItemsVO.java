package cn.qianxun.meta.dict.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @author fuzhilin
 * @Description
 * @since 2023/08/24
 */
@Schema(name = "字典类型响应VO")
@Data
public class SysDictTypeItemsVO {
    @Schema(description = "字典id")
    private Long dictId;

    @Schema(description = "字典名称(下拉列表)")
    private String dictName;

    @Schema(description = "字典类型")
    private String dictType;

    @Schema(description = "字典(下拉列表项)")
    private List<SysDictDataVO> items;
}
