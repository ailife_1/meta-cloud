package cn.qianxun.meta.log.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.vo.RemoveStringDTO;
import cn.qianxun.meta.common.log.annotation.Log;
import cn.qianxun.meta.common.log.enums.BusinessType;
import cn.qianxun.meta.log.dto.LoginLogQueryDTO;
import cn.qianxun.meta.log.service.ISysLoginLogService;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.log.api.dto.SysLoginLog;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录日志管理
 *
 * @Author fuzhilin
 * @Date 2021/9/3 0003 15:37
 * @Description 登录日志管理
 */
@RestController
@RequestMapping("/web/v1/loginLog")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class LoginLogController {

    private final ISysLoginLogService sysLoginLogService;


    /**
     * 登录日志的查询参数
     *
     * @param vo 登录日志的查询参数
     * @return
     */
    @SneakyThrows
    @SaCheckPermission("web:v1:loginLog:list")
    @PostMapping("/list")
    public Result<RowsData<SysLoginLog>> selectLoginLogList(@RequestBody LoginLogQueryDTO vo) {
        return Result.success("查询成功", sysLoginLogService.selectLoginLogList(vo));
    }


    /**
     * 登录日志删除
     *
     * @param vo 登录日志删除的参数
     * @return
     */
    @SneakyThrows
    @SaCheckPermission("web:v1:loginLog:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    public Result<Object> deleteLoginLogByIds(@RequestBody @Validated RemoveStringDTO vo) {
        return Result.success("删除成功！", sysLoginLogService.deleteLoginLogByIds(vo));
    }


}
