package cn.qianxun.meta.board.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.qianxun.meta.board.dto.*;
import cn.qianxun.meta.board.entity.ChartBoard;
import cn.qianxun.meta.board.service.IChartBoardService;
import cn.qianxun.meta.board.vo.ChartBoardVO;
import cn.qianxun.meta.common.core.dto.RowsData;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import cn.qianxun.meta.board.dto.*;
import cn.qianxun.meta.common.core.constant.VersionConstant;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.common.core.exception.ServiceException;
import cn.qianxun.meta.common.satoken.utils.LoginHelper;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * 数据看板
 *
 * @author fanfu1024
 * @since 2023-11-08 14:57:15
 */
@RestController
@RequestMapping(VersionConstant.WEB_V1 +"/chartBoard")
public class ChartBoardController {
    /**
     * 服务对象
     */
    @Resource
    private IChartBoardService chartBoardService;

    /**
     * 添加看板
     *
     * @param addDTO
     * @return
     */
    @SaCheckPermission("web:v1:chartBoard:add")
    @PostMapping("add")
    public Result add(@RequestBody @Validated AddChartBoardDTO addDTO) {
        ChartBoard chartBoard = new ChartBoard();
        BeanUtil.copyProperties(addDTO, chartBoard);
        chartBoard.setCreatedTime(new Date());
        chartBoard.setCreatedBy(String.valueOf(LoginHelper.getUserId()));
        this.chartBoardService.save(chartBoard);
        return Result.success();
    }

    /**
     * 更新看板
     *
     * @param updateChartBoardDTO
     * @return
     */
    @SaCheckPermission("web:v1:chartBoard:update")
    @PostMapping("update")
    public Result update(@RequestBody @Validated UpdateChartBoardDTO updateChartBoardDTO) {
        ChartBoard chartBoard = new ChartBoard();
        BeanUtil.copyProperties(updateChartBoardDTO, chartBoard);
        chartBoard.setUpdatedBy(String.valueOf(LoginHelper.getUserId()));
        chartBoard.setCreatedTime(new Date());
        this.chartBoardService.updateById(chartBoard);
        return Result.success();
    }

    /**
     * 删除看板
     *
     * @param id
     * @return
     */
    @SaCheckPermission("web:v1:chartBoard:del")
    @PostMapping("del/{id}")
    public Result del(@PathVariable(value = "id") Long id) {
        ChartBoard tmp = this.chartBoardService.getById(id);
        if (ObjectUtil.isNotNull(tmp)) {
            Integer assignFlag = tmp.getAssignFlag();
            if (assignFlag==1) {
                throw new ServiceException("删除失败:看板已分配!");
            }
        }
        ChartBoard chartBoard = new ChartBoard();
        chartBoard.setId(id);
        chartBoard.setDelFlag(1);
        this.chartBoardService.updateById(chartBoard);
        return Result.success();
    }

    /**
     * 列表查询
     *
     * @param chartBoardDTO
     * @return
     */
    @SaCheckPermission("web:v1:chartBoard:pageList")
    @PostMapping("pageList")
    public Result<RowsData<ChartBoardVO>> pageList(@RequestBody ChartBoardDTO chartBoardDTO) {
        RowsData<ChartBoardVO> rowsData = this.chartBoardService.pageList(chartBoardDTO);
        return Result.success(rowsData);
    }

    /**
     * 详情查询
     *
     * @param id
     * @return
     */
    @SaCheckPermission("web:v1:chartBoard:detail")
    @GetMapping("detail/{id}")
    public Result<ChartBoardVO> detail(@PathVariable(value = "id") Long id) {
        ChartBoard chartBoard = this.chartBoardService.getById(id);
        ChartBoardVO chartBoardVO = new ChartBoardVO();
        BeanUtil.copyProperties(chartBoard, chartBoardVO);
        return Result.success(chartBoardVO);
    }

    /**
     * 根据看板类型查询看板列表
     *
     * @param boardType
     * @return
     */
    @SaCheckPermission("web:v1:chartBoard:listByType")
    @GetMapping("listByType/{boardType}")
    public Result<List<ChartBoardVO>> listByType(@PathVariable(value = "boardType") Integer boardType) {
        List<ChartBoard> list = this.chartBoardService.list(new LambdaQueryWrapper<ChartBoard>().eq(ChartBoard::getBoardType, boardType).eq(ChartBoard::getDelFlag,0).orderByAsc(Arrays.asList(ChartBoard::getSorted,ChartBoard::getId)));
        List<ChartBoardVO> vos = BeanUtil.copyToList(list, ChartBoardVO.class);
        return Result.success(vos);
    }

    /**
     * 根据看板是否已经分配查询看板列表
     *
     * @param assignFlag 枚举值:1:是;0:否;
     * @return
     */
    @SaCheckPermission("web:v1:chartBoard:listByAssignType")
    @GetMapping("listByAssignType/{assignFlag}")
    public Result<List<ChartBoardVO>> listByAssignType(@PathVariable(value = "assignFlag") Integer assignFlag) {
        List<ChartBoard> list = this.chartBoardService.list(new LambdaQueryWrapper<ChartBoard>().eq(ChartBoard::getAssignFlag, assignFlag).eq(ChartBoard::getDelFlag,0).orderByAsc(Arrays.asList(ChartBoard::getSorted,ChartBoard::getId)));
        List<ChartBoardVO> chartBoardVOS = BeanUtil.copyToList(list, ChartBoardVO.class);
        return Result.success(chartBoardVOS);
    }

    /**
     * 操作(添加、移除)看板
     *
     * @param assignDTO
     * @return
     */
    @SaCheckPermission("web:v1:chartBoard:operator")
    @PostMapping("/operator")
    public Result operator(@RequestBody ChartBoardAssignDTO assignDTO) {
        List<Long> ids = assignDTO.getIds();
        if (assignDTO.getOperType() == 1) {
            this.chartBoardService.updateAssignFlag(1, ids);
        } else if (assignDTO.getOperType() == 2) {
            this.chartBoardService.updateAssignFlag(0, ids);
        }
        return Result.success();
    }

    /**
     * 更新看板的排列顺序
     */
    @SaCheckPermission("web:v1:chartBoard:updateSorted")
    @PostMapping("/updateSorted")
    public Result updateSorted(@RequestBody UpdateSortedDTO updateSortedDTO) {
        List<Long> ids = updateSortedDTO.getIds();
        int i = 0;
        List<ChartBoard> list = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(ids)) {
            ChartBoard chartBoard = null;
            for (Long id : ids) {
                chartBoard = new ChartBoard();
                chartBoard.setId(id);
                chartBoard.setSorted(++i);
                list.add(chartBoard);
            }
            this.chartBoardService.updateSorted(list);
        }
        return Result.success();
    }
}


