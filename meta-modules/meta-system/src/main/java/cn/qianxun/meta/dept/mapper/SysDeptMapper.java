package cn.qianxun.meta.dept.mapper;

import cn.qianxun.meta.common.core.dto.component.model.dto.SelectDeptInfoParamDTO;
import cn.qianxun.meta.dept.vo.SelectSysDeptResultVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import cn.qianxun.meta.common.mybatis.annotation.DataColumn;
import cn.qianxun.meta.common.mybatis.annotation.DataPermission;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;
import cn.qianxun.meta.dept.entity.SysDept;
import cn.qianxun.meta.dept.vo.SysDeptVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-21 02:30:54
 */
public interface SysDeptMapper extends BaseMapperPlus<SysDeptMapper, SysDept, SysDept> {

    @DataPermission({
            @DataColumn(key = "deptName", value = "dept_id")
    })
    List<SysDeptVO> selectDeptList(@Param(Constants.WRAPPER) Wrapper<SysDept> queryWrapper);

    List<Long> selectDeptListByRoleId(@Param("roleId") Long roleId, @Param("deptCheckStrictly") boolean deptCheckStrictly);

    /**
     * 根据参数查询单位信息
     *
     * @param dto
     * @return
     */
    List<SelectSysDeptResultVO> selectDeptInfoByParam(SelectDeptInfoParamDTO dto);
}
