package cn.qianxun.meta.dept.provider;

import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.dto.component.model.dto.SelectDeptInfoParamDTO;
import cn.qianxun.meta.common.core.dto.component.model.vo.SelectAssetResultVO;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.dept.entity.SysDept;
import cn.qianxun.meta.dept.service.ISysDeptService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import cn.qianxun.meta.common.core.constant.Constants;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.common.core.utils.string.StringUtils;
import cn.qianxun.meta.dept.api.dto.ApiAddDeptDTO;
import cn.qianxun.meta.dept.api.dto.ApiEditDeptDTO;
import cn.qianxun.meta.dept.api.dto.ApiSysDeptQueryDTO;
import cn.qianxun.meta.dept.api.dto.SelectDeptInfoByNameDTO;
import cn.qianxun.meta.dept.api.feign.ISysDeptProvider;
import cn.qianxun.meta.dept.api.vo.ApiDeptDetailsVO;
import cn.qianxun.meta.dept.api.vo.ApiSysDeptListVO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description
 * @Date 2023/9/13 9:06
 **/
@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysDeptProvider implements ISysDeptProvider {

    private final ISysDeptService sysDeptService;

    /**
     * 查询单位列表(资产模块)
     *
     * @param dto
     * @return
     */
    @Override
    public Result<RowsData<ApiSysDeptListVO>> selectDeptList(ApiSysDeptQueryDTO dto) {
        return Result.success("查询成功", sysDeptService.deptList(dto));
    }

    @Override
    public Result<String> insertDept(ApiAddDeptDTO dto) {
        sysDeptService.addDept(dto);
        return Result.success("添加成功");
    }

    @Override
    public Result<String> deleteDept(RemoveLongDTO dto) {
        sysDeptService.deleteDept(dto);
        return Result.success("删除成功");
    }

    @Override
    public Result<ApiDeptDetailsVO> deptDetails(Long id) {
        return Result.success("查询成功", sysDeptService.deptDetails(id));
    }

    @Override
    public Result<String> updateDept(ApiEditDeptDTO dto) {
        sysDeptService.editDept(dto);
        return Result.success("修改成功");
    }

    @Override
    public Result<SelectAssetResultVO> selectDeptInfoByParam(SelectDeptInfoParamDTO dto) {
        return Result.success("查询成功", sysDeptService.selectDeptInfoByParam(dto));
    }

    @Override
    public Result<Long> selectDeptCount(String staTime, String endTime) {
        LambdaQueryWrapper<SysDept> wrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotEmpty(staTime) && StringUtils.isNotEmpty(endTime)) {
            wrapper.between(SysDept::getCreateTime, staTime, endTime);
        }
        wrapper.eq(SysDept::getDelFlag, Constants.ZERO);
        return Result.success("查询成功", sysDeptService.count(wrapper));
    }

    @Override
    public Result<Map<String, Long>> selectDeptInfoByName(SelectDeptInfoByNameDTO dto) {
        return Result.success("查询成功", sysDeptService.selectDeptInfoByName(dto));
    }
}