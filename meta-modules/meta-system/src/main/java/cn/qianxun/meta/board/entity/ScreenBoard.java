package cn.qianxun.meta.board.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * 大屏看板表(ScreenBoard)表实体类
 *
 * @author fanfu1024
 * @since 2023-11-10 10:37:03
 */
@Data
public class ScreenBoard extends Model<ScreenBoard> {
    /**
     * id，主键
     */
    private Long id;
    /**
     * 可视化名称
     */
    private String fullName;
    /**
     * 可视化简称
     */
    private String simpleName;
    /**
     * 可视化标识
     */
    private String boardCode;
    /**
     * 可视化权限
     */
    private String boardPower;
    /**
     * 缩略图地址
     */
    private String imgUrl;
    /**
     * 组件状态，枚举值：1：启用；0：禁用；字典管理：数据看板-看板状态 chart_board_status
     */
    private Integer status;
    /**
     * 备注
     */
    private String bak;
    /**
     * 看板类型,字典管理：数据看板-看板类型chart_board_type，1、数据统计；2、图表统计
     */
    private String boardType;
    /**
     * 顺序
     */
    private Integer sorted;
    /**
     * 位置
     */
    private String location;
    /**
     * 创建人
     */
    private String createdBy;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String updatedBy;
    /**
     * 更新时间
     */
    private Date updatedTime;
    /**
     * 删除标志位，1：是；0：否；
     */
    private Integer delFlag;
    /**
     * 是否已分配的标志位，枚举值：1：是；0：否；默认值为0
     */
    private Integer assignFlag;

    /**
     * 是否是默认大屏，枚举值：1：是；0：否；
     */
    private Integer defaultFlag;

}

