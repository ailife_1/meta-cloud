package cn.qianxun.meta.dept.service;

import cn.hutool.core.lang.tree.Tree;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.dto.component.model.dto.SelectDeptInfoParamDTO;
import cn.qianxun.meta.common.core.dto.component.model.vo.SelectAssetResultVO;
import cn.qianxun.meta.common.core.web.dto.SelectLongDTO;
import cn.qianxun.meta.common.core.web.dto.SelectStringDTO;
import cn.qianxun.meta.common.core.web.dto.SelectVO;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.dept.dto.SysDeptAddDTO;
import cn.qianxun.meta.dept.dto.SysDeptQueryDTO;
import cn.qianxun.meta.dept.dto.SysDeptStatusDTO;
import cn.qianxun.meta.dept.entity.SysDept;
import cn.qianxun.meta.dept.vo.SysDeptVO;
import cn.qianxun.meta.user.dto.UserDeptRoleSelectDTO;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.common.security.resdata.SelectPageDTO;
import cn.qianxun.meta.dept.api.dto.ApiAddDeptDTO;
import cn.qianxun.meta.dept.api.dto.ApiEditDeptDTO;
import cn.qianxun.meta.dept.api.dto.ApiSysDeptQueryDTO;
import cn.qianxun.meta.dept.api.dto.SelectDeptInfoByNameDTO;
import cn.qianxun.meta.dept.api.vo.ApiDeptDetailsVO;
import cn.qianxun.meta.dept.api.vo.ApiSysDeptListVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-21 02:30:54
 */
public interface ISysDeptService extends IService<SysDept> {

    List<SysDeptVO> selectDeptList(SysDeptQueryDTO dto);

    List<SysDeptVO> excludeChild(SelectLongDTO selectLongDTO);

    void checkDeptDataScope(Long idLong);

    SysDeptVO selectDeptById(Long idLong);

    boolean checkDeptNameUnique(SysDeptAddDTO sysDeptAddDTO);

    Boolean insertDept(SysDeptAddDTO sysDeptAddDTO);

    long selectNormalChildrenDeptById(Long deptId);

    Boolean updateDept(SysDeptAddDTO dept);

    boolean hasChildByDeptId(Long idLong);

    boolean checkDeptExistUser(Long idLong);

    Boolean deleteDeptById(Long idLong);

    List<Long> selectDeptListByRoleId(Long idLong);

    List<Tree<Long>> selectDeptTreeList(SysDept sysDept);

    /**
     * 责任单位下拉选
     *
     * @param dto
     * @return
     */
    RowsData<SelectVO> getDeptList(SelectPageDTO dto);

    /**
     * 运维单位下拉选
     *
     * @param dto
     * @return
     */
    RowsData<SelectVO> operateSubList(SelectPageDTO dto);

    /**
     * 根据单位id获取单位负责人的电话和姓名
     *
     * @param dto
     * @return
     */
    SelectVO getDeptLeaderInfo(SelectStringDTO dto);

    /**
     * 使用单位下拉选
     *
     * @param dto
     * @return
     */
    RowsData<SelectVO> useSubList(SelectPageDTO dto);

    /**
     * 查询单位列表(资产模块)
     *
     * @param dto
     * @return
     */
    RowsData<ApiSysDeptListVO> deptList(ApiSysDeptQueryDTO dto);

    /**
     * 添加单位(API)
     *
     * @param dto
     */
    void addDept(ApiAddDeptDTO dto);

    /**
     * 删除单位(API)
     *
     * @param dto
     */
    void deleteDept(RemoveLongDTO dto);

    /**
     * 单位详情(APi)
     *
     * @param id
     * @return
     */
    ApiDeptDetailsVO deptDetails(Long id);

    /**
     * 修改单位(API)
     *
     * @param dto
     */
    void editDept(ApiEditDeptDTO dto);

    /**
     * 流程 单位下拉选
     *
     * @param dto
     * @return
     */
    List<SelectVO> getProcessDeptList(UserDeptRoleSelectDTO dto);

    /**
     * @param dto
     * @return
     */
    RowsData<SelectVO> getProcessDeptPageList(UserDeptRoleSelectDTO dto);

    /**
     * 修改单位状态
     *
     * @param dto
     */
    void status(SysDeptStatusDTO dto);

    /**
     * 根据参数查询单位信息
     *
     * @param dto
     * @return
     */
    SelectAssetResultVO selectDeptInfoByParam(SelectDeptInfoParamDTO dto);

    /**
     * 根据单位名称查询单位信息
     *
     * @return
     */
    Map<String, Long> selectDeptInfoByName(SelectDeptInfoByNameDTO dto);
}
