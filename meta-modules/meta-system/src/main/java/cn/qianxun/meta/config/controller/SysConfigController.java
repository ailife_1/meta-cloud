package cn.qianxun.meta.config.controller;


import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.dto.SelectStringDTO;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.common.log.annotation.Log;
import cn.qianxun.meta.common.log.enums.BusinessType;
import cn.qianxun.meta.config.dto.AddSysConfigDTO;
import cn.qianxun.meta.config.dto.SysConfigQueryDTO;
import cn.qianxun.meta.config.service.ISysConfigService;
import cn.qianxun.meta.config.vo.SysConfigVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author fuzhilin
 * @Date 2023/16/35
 * @Description
 */
@Tag(name = "参数配置管理")
@Validated
@RestController
@RequestMapping("/web/v1/sysConfig")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysConfigController {
    private final ISysConfigService configService;

    // @SaCheckPermission("web:v1:sysConfig:list")
    @PostMapping("/list")
    @Operation(summary = "获取参数配置列表")
    public Result<RowsData<SysConfigVO>> list(@RequestBody SysConfigQueryDTO dto) {
        return Result.success(configService.selectConfigList(dto));
    }

    @Operation(summary = "根据id获取参数配置详情")
    //  @SaCheckPermission("web:v1:sysConfig:query")
    @PostMapping(value = "/details")
    public Result<SysConfigVO> details(@RequestBody SelectStringDTO selectStringDTO) {
        return Result.success(configService.details(selectStringDTO.getIdStr()));
    }


    @Operation(summary = "新增参数配置")
    // @SaCheckPermission("web:v1:sysConfig:add")
    @Log(title = "参数管理", businessType = BusinessType.INSERT)
    @PostMapping("/addConfig")
    public Result<Object> addConfig(@Validated @RequestBody AddSysConfigDTO dto) {
        configService.addConfig(dto);
        return Result.success("添加成功");
    }

    @Operation(summary = "修改参数配置")
    //  @SaCheckPermission("web:v1:sysConfig:edit")
    @Log(title = "参数管理", businessType = BusinessType.UPDATE)
    @PostMapping("/editConfig")
    public Result<Object> editConfig(@Validated @RequestBody AddSysConfigDTO dto) {
        configService.editConfig(dto);
        return Result.success("修改成功");
    }

    @Operation(summary = "删除参数配置")
    // @SaCheckPermission("web:v1:sysConfig:remove")
    @Log(title = "参数管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    public Result<Object> deleteConfigByIds(@RequestBody RemoveLongDTO dto) {
        configService.deleteConfigByIds(dto);
        return Result.success("删除成功！");
    }

    @Operation(summary = "清空缓存")
    // @SaCheckPermission("web:v1:sysConfig:remove")
    @Log(title = "参数管理", businessType = BusinessType.CLEAN)
    @PostMapping("/clearCache")
    public Result<Object> clearCache() {
        configService.clearCache();
        return Result.success("操作成功");
    }


    // @SaCheckPermission("web:v1:sysConfig:configList")
    @PostMapping("/configList")
    @Operation(summary = "参数配置列表")
    public Result<Map<String, List<SysConfigVO>>> configList() {
        return Result.success("查询成功！", configService.configList());
    }

}

