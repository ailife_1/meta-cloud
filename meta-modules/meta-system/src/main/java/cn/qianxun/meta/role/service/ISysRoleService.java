package cn.qianxun.meta.role.service;

import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.dto.SelectVO;
import cn.qianxun.meta.role.dto.SysRoleAddDTO;
import cn.qianxun.meta.role.dto.SysRoleQueryDTO;
import cn.qianxun.meta.role.dto.SysRoleStatusDTO;
import cn.qianxun.meta.role.dto.SysUserRoleDTO;
import cn.qianxun.meta.role.entity.SysRole;
import cn.qianxun.meta.role.vo.SysRoleVO;
import cn.qianxun.meta.user.dto.UserDeptRoleSelectDTO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:28:37
 */
public interface ISysRoleService extends IService<SysRole> {

    RowsData<SysRoleVO> selectPageRoleList(SysRoleQueryDTO role);

    List<SysRole> selectRoleList(SysRoleQueryDTO role);

    void checkRoleDataScope(Long roleId);

    SysRoleVO selectRoleById(Long roleId);

    void checkRoleAllowed(SysRole role);

    boolean checkRoleNameUnique(SysRole role);

    boolean checkRoleKeyUnique(SysRole role);

    Integer insertRole(SysRoleAddDTO role);

    int updateRole(SysRoleAddDTO role);

    void cleanOnlineUserByRole(Long roleId);

    int authDataScope(SysRoleAddDTO role);

    int updateRoleStatus(SysRoleStatusDTO role);

    int deleteRoleByIds(List<Long> removeIdList);


    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    long countUserRoleByRoleId(Long roleId);

    List<SysRoleVO> selectRoleAll();

    int deleteAuthUser(SysUserRoleDTO sysUserRoleDTO);

    int insertAuthUsers(SysUserRoleDTO sysUserRoleDTO);

    List<SysRole> selectRolesByUserId(Long idLong);

    Set<String> selectRolePermissionByUserId(Long userId);

    /**
     * 流程 角色下拉选(不分页)
     *
     * @param dto
     * @return
     */
    List<SelectVO> getProcessRoleList(UserDeptRoleSelectDTO dto);

    /**
     * 流程 角色下拉选(不分页)
     *
     * @param dto
     * @return
     */
    RowsData<SelectVO> getProcessRolePageList(UserDeptRoleSelectDTO dto);
}
