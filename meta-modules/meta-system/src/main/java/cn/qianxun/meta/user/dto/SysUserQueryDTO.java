package cn.qianxun.meta.user.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @Author fuzhilin
 * @Description
 */
@Schema(description = "用户列表查询VO")
@Data
public class SysUserQueryDTO extends BaseQueryDTO {
    @Schema(description = "用户账号")
    private String userName;

    @Schema(description = "用户ID")
    private Long userId;

    @Schema(description = "部门ID")
    private Long deptId;

    @Schema(description = "用户昵称")
    private String nickName;

    @Schema(description = "手机号码")
    private String phonenumber;

    @Schema(description = "单位名称")
    private String deptName;

    @Schema(description = "帐号状态（0正常 1停用）")
    private String status;

    @Schema(description = "角色名称")
    private String roleName;

    @Schema(description = "角色Id")
    private String roleId;

    @Schema(description = "开始时间")
    private String beginTime;

    @Schema(description = "结束时间")
    private String endTime;

}
