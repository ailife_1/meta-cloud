package cn.qianxun.meta.menu.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * <p>
 * 菜单查询传递信息
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-24 09:16:24
 */
@Schema(name = "菜单模块查询参数DTO")
@Data
public class SysMenuModuleQueryDTO {
    @NotBlank(message = "请选择菜单使用平台")
    @Schema(description = "菜单使用平台（字典menu_platform）")
    private String menuPlatform;
}
