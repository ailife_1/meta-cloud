package cn.qianxun.meta.quartz.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description XxlJobLogVO对象
 */
@Data
public class XxlJobLogVO {

    /**
     * ID
     */
    private Long id;

    /**
     * jobGroup
     */
    private Integer jobGroup;

    /**
     * 任务，主键ID
     */
    private Integer jobId;

    /**
     * 任务描述
     */
    private String jobDesc;

    /**
     * 调度-时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date triggerTime;

    /**
     * 调度-结果 200:成功,500:失败
     */
    private Integer triggerCode;

    /**
     * 调度-日志
     */
    private String triggerMsg;

    /**
     * 执行-时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date handleTime;

    /**
     * 执行-状态 0:空,200:成功,500:失败
     */
    private Integer handleCode;

    /**
     * 执行-日志
     */
    private String handleMsg;

    /**
     * 执行器地址，本次执行的地址
     */
    private String executorAddress;


}
