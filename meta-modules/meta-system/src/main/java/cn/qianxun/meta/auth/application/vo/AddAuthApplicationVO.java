package cn.qianxun.meta.auth.application.vo;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/14 9:27
 * @Description 添加认证应用响应参数VO
 */
@Data
public class AddAuthApplicationVO {
    /**
     * 应用密钥
     */
    private String appSecret;

    /**
     * 应用私钥
     */
    private String privateKey;
}
