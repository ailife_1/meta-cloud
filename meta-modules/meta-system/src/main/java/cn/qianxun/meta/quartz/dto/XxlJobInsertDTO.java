package cn.qianxun.meta.quartz.dto;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description XxlJobInsertDTO对象
 */

@Data
public class XxlJobInsertDTO {

    /**
     * jobGroup
     */
    private int jobGroup;

    /**
     * 任务描述
     */
    private String jobDesc;

    /**
     * 作者
     */
    private String author;


    /**
     * 调度类型 无NONE/CRON/固定速度FIX_RATE（second）
     */
    private String scheduleType;

    /**
     * 调度配置，值含义取决于调度类型
     */
    private String scheduleConf;

    /**
     * 调度过期策略
     * 忽略 DO_NOTHING
     * 立即执行一次 FIRE_ONCE_NOW
     */
    private String misfireStrategy;

    /**
     * 执行器路由策略
     * FIRST=第一个
     * LAST=最后一个
     * ROUND=轮询
     * RANDOM=随机
     * CONSISTENTHASH=一致性HASH
     * LFU=最不经常使用
     * LRU=最近最久未使用
     * FAILOVER=故障转移
     * BUSYOVER=忙碌转移
     * SHARD=分片广播
     */
    private String executorRouteStrategy;

    /**
     * 执行器任务handler
     */
    private String executorHandler;


    /**
     * 执行器任务参数
     */
    private String executorParam;

    /**
     * 阻塞处理策略
     * 单机串行 SERIAL_EXECUTION
     * 丢弃后续调度 DISCARD_LATER
     * 覆盖之前调度 COVER_EARLY
     */
    private String executorBlockStrategy;

    /**
     * 任务执行超时时间，单位秒
     */
    private int executorTimeout;

    /**
     * 失败重试次数
     */
    private int executorFailRetryCount;

    /**
     * GLUE类型(运行模式 默认BEAN)
     * GLUE_GROOVY (Java)
     * GLUE_SHELL (Shell)
     * GLUE_PYTHON (Python)
     * GLUE_PHP (PHP)
     * GLUE_NODEJS (Nodejs)
     * GLUE_POWERSHELL (PowerShell)
     */
    private String glueType;

    /**
     * GLUE源代码
     */
    private String glueSource;

    /**
     * GLUE备注
     */
    private String glueRemark;

    /**
     * 子任务ID，多个逗号分隔
     */
    private String childJobid;


}
