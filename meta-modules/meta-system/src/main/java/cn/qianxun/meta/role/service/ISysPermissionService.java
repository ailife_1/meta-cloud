package cn.qianxun.meta.role.service;


import cn.qianxun.meta.user.entity.SysUser;

import java.util.Set;

/**
 * 权限信息 服务层
 *
 * @author fuzhilin
 */
public interface ISysPermissionService {
    /**
     * 获取角色数据权限
     *
     * @param user 用户信息
     * @return 角色权限信息
     */
    Set<String> getRolePermission(SysUser user);

    /**
     * 获取菜单数据权限
     *
     * @param user 用户信息
     * @return 菜单权限信息
     */
    Set<String> getMenuPermission(SysUser user);

    /**
     * 获取看板数据权限
     *
     * @param user 用户信息
     * @return 角色权限信息
     */
    Set<String> getChartBoardPermission(SysUser user);

    /**
     * 获取大屏数据权限
     *
     * @param user 用户信息
     * @return 菜单权限信息
     */
    Set<String> getScreenBoardPermission(SysUser user);
}
