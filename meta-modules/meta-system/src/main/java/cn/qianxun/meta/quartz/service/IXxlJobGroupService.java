package cn.qianxun.meta.quartz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.quartz.domain.XxlJobGroup;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2021-09-29
 */
public interface IXxlJobGroupService extends IService<XxlJobGroup> {

}
