package cn.qianxun.meta.quartz.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description XxlJobLog对象
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class XxlJobLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 执行器主键ID
     */
    private int jobGroup;

    /**
     * 任务，主键ID
     */
    private int jobId;

    /**
     * 任务描述
     */
    @TableField(exist = false)
    private String jobDesc;

    /**
     * 执行器地址，本次执行的地址
     */
    private String executorAddress;

    /**
     * 执行器任务handler
     */
    private String executorHandler;

    /**
     * 执行器任务参数
     */
    private String executorParam;

    /**
     * 执行器任务分片参数，格式如 1/2
     */
    private String executorShardingParam;

    /**
     * 失败重试次数
     */
    private int executorFailRetryCount;

    /**
     * 调度-时间
     */
    private Date triggerTime;

    /**
     * 调度-结果 200:成功,500:失败
     */
    private int triggerCode;

    /**
     * 调度-日志
     */
    private String triggerMsg;

    /**
     * 执行-时间
     */
    private Date handleTime;

    /**
     * 执行-状态 0:空,200:成功,500:失败
     */
    private int handleCode;

    /**
     * 执行-日志
     */
    private String handleMsg;

    /**
     * 告警状态：0-默认、1-无需告警、2-告警成功、3-告警失败
     */
    private int alarmStatus;


}
