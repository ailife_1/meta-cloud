package cn.qianxun.meta.log.service;

import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.vo.RemoveStringDTO;
import cn.qianxun.meta.log.api.dto.SysOperLog;
import cn.qianxun.meta.log.dto.OperLogQueryDTO;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description 操作日志存储情况
 * @Date 2023/8/31 15:07
 **/
public interface ISysOperLogService {


    RowsData<SysOperLog> selectOperLogList(OperLogQueryDTO vo);

    Boolean deleteOperLogByIds(RemoveStringDTO vo);
}
