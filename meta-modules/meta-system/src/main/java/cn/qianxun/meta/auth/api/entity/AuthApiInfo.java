package cn.qianxun.meta.auth.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 认证API接口基本信息
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 01:41:24
 */
@Getter
@Setter
@TableName("auth_api_info")
public class AuthApiInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 接口名称
     */
    private String apiName;

    /**
     * 接口地址
     */
    private String apiUrl;

    /**
     * 接口权限字符
     */
    private String apiPerms;

    /**
     * 所属模块（字典:sys_operate_module）
     */
    private String apiMod;

    /**
     * 接口状态（1.启用 2.停用）
     */
    private Integer apiStatus;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 备注
     */
    private String remark;
}
