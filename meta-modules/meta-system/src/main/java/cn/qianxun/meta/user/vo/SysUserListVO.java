package cn.qianxun.meta.user.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.qianxun.meta.dept.entity.SysDept;
import cn.qianxun.meta.role.entity.SysRole;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author fuzhilin
 * @Description
 */
@Data
@Schema(description = "用户信息列表DTO")
public class SysUserListVO {
    @Schema(description = "用户id")
    private Long userId;

    @Schema(description = "用户账号")
    private String userName;

    @Schema(description = "用户昵称")
    private String nickName;

    @Schema(description = "用户邮箱")
    private String email;

    @Schema(description = "手机号码")
    private String phonenumber;

    @Schema(description = "用户性别")
    private String sex;

    @Schema(description = "用户头像")
    private String avatar;

    @Schema(description = "帐号状态（0正常 1停用）")
    private String status;

    @Schema(description = "角色名称")
    private String roleName;

    @Schema(description = "最后登陆IP")
    private String loginIp;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "最后登陆时间")
    private Date loginDate;

    @Schema(description = "单位名称")
    private String deptName;

    @Schema(description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 部门对象
     */
    @TableField(exist = false)
    private SysDept dept;

    /**
     * 角色对象
     */
    @TableField(exist = false)
    private List<SysRole> roles;
}
