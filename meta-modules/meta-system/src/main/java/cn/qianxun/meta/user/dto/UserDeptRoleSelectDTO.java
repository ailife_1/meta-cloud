package cn.qianxun.meta.user.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/26 17:39
 * @Description 用户、单位、角色 流程下拉选返回参数DTO
 */
@Data
public class UserDeptRoleSelectDTO extends BaseQueryDTO {
    /**
     * 搜索字段
     */
    private String field;

    /**
     * 单位id
     */
    private String deptId;
}