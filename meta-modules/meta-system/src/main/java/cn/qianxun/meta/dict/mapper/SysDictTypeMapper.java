package cn.qianxun.meta.dict.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.qianxun.meta.dict.entity.SysDictType;
import cn.qianxun.meta.dict.dto.QueryDictTypeDTO;
import cn.qianxun.meta.dict.vo.SysDictDataVO;
import cn.qianxun.meta.dict.vo.SysDictTypeVO;

import java.util.List;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023/08/24
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

    /**
     * 查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据
     */
    int countDictDataByType(String dictType);

    /**
     * 根据id删除字典类型
     *
     * @param removeIdList
     * @return
     */
    int deleteDictTypeByIds(List<Long> removeIdList);

    /**
     * 获取字典类型列表
     *
     * @param dto
     * @return
     */
    List<SysDictTypeVO> selectDictTypeList(QueryDictTypeDTO dto);

    /**
     * 获取字典的所有数据
     *
     * @return
     */
    List<SysDictDataVO> dictList();
}
