package cn.qianxun.meta.quartz.vo;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description XxlJobSelectVO对象
 */
@Data
public class XxlJobSelectVO {


    /**
     * label
     */
    private String label;

    /**
     * value
     */
    private String value;


}
