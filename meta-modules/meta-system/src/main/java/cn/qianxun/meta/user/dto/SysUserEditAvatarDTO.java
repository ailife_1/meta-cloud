package cn.qianxun.meta.user.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.*;

/**
 *  用户头像
 */
@Data
public class SysUserEditAvatarDTO {


    @Schema(description = "图片")
    private String avatar;

    @NotBlank(message = "用户昵称不能为空")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    @Schema(description = "用户昵称")
    private String nickName;

    @NotBlank(message = "用户性别不能为空")
    @Schema(description = "用户性别（0男 1女 2未知）")
    private String sex;

    @NotBlank(message = "用户手机号不能为空")
    @Schema(description = "手机号码")
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    private String phonenumber;

    @NotBlank(message = "用户邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    @Schema(description = "用户邮箱")
    private String email;



}
