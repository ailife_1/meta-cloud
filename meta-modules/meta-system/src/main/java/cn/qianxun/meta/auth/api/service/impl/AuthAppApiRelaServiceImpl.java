package cn.qianxun.meta.auth.api.service.impl;

import cn.qianxun.meta.auth.api.mapper.AuthAppApiRelaMapper;
import cn.qianxun.meta.auth.api.service.IAuthAppApiRelaService;
import cn.qianxun.meta.auth.api.entity.AuthAppApiRela;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 应用API接口关联关系 服务实现类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 01:41:24
 */
@Service
public class AuthAppApiRelaServiceImpl extends ServiceImpl<AuthAppApiRelaMapper, AuthAppApiRela> implements IAuthAppApiRelaService {

}
