package cn.qianxun.meta.auth.api.mapper;

import cn.qianxun.meta.auth.api.entity.AuthApiInfo;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * <p>
 * 认证API接口基本信息 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 01:41:24
 */
public interface AuthApiInfoMapper extends BaseMapperPlus<AuthApiInfoMapper, AuthApiInfo, AuthApiInfo> {

}
