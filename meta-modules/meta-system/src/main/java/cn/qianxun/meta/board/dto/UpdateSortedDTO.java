package cn.qianxun.meta.board.dto;

import lombok.Data;

import java.util.List;

@Data
public class UpdateSortedDTO {
    private List<Long> ids;
}
