package cn.qianxun.meta.role.service.impl;

import cn.qianxun.meta.board.service.IChartBoardService;
import cn.qianxun.meta.board.service.IScreenBoardService;
import cn.qianxun.meta.role.service.ISysPermissionService;
import cn.qianxun.meta.role.service.ISysRoleService;
import cn.qianxun.meta.user.entity.SysUser;
import cn.qianxun.meta.common.satoken.utils.LoginHelper;
import cn.qianxun.meta.menu.service.ISysMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * 用户权限处理
 *
 * @author fuzhilin
 */
@RequiredArgsConstructor
@Service
public class SysPermissionServiceImpl implements ISysPermissionService {

    private final ISysRoleService roleService;

    private final ISysMenuService menuService;
    private final IChartBoardService chartBoardService;
    private final IScreenBoardService screenBoardService;


    /**
     * 获取角色数据权限
     *
     * @param user 用户
     * @return 角色权限信息
     */
    @Override
    public Set<String> getRolePermission(SysUser user) {
        Set<String> roles = new HashSet<String>();
        // 管理员拥有所有权限
        if (LoginHelper.isAdmin(user.getUserId())) {
            roles.add("admin");
        } else {
            roles.addAll(roleService.selectRolePermissionByUserId(user.getUserId()));
        }
        return roles;
    }

    /**
     * 获取菜单数据权限
     *
     * @param user 用户
     * @return 菜单权限信息
     */
    @Override
    public Set<String> getMenuPermission(SysUser user) {
        Set<String> perms = new HashSet<String>();
        // 管理员拥有所有权限
        if (LoginHelper.isAdmin(user.getUserId())) {
            perms.add("*:*:*");
        } else {
            perms.addAll(menuService.selectMenuPermsByUserId(user.getUserId()));
        }
        return perms;
    }
    /**
     * 获取看板数据权限
     *
     * @param user 用户
     * @return 角色权限信息
     */
    @Override
    public Set<String> getChartBoardPermission(SysUser user) {
        Set<String> roles = new HashSet<String>();
        // 管理员拥有所有权限
        if (LoginHelper.isAdmin(user.getUserId())) {
            roles.add("admin");
        } else {
            roles.addAll(chartBoardService.getChartBoardPermission());
        }
        return roles;
    }

    /**
     * 获取大屏数据权限
     *
     * @param user 用户
     * @return 菜单权限信息
     */
    @Override
    public Set<String> getScreenBoardPermission(SysUser user) {
        Set<String> perms = new HashSet<String>();
        // 管理员拥有所有权限
        if (LoginHelper.isAdmin(user.getUserId())) {
            perms.add("*:*:*");
        } else {
            perms.addAll(screenBoardService.getScreenBoardPermission());
        }
        return perms;
    }
}
