package cn.qianxun.meta.area.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description
 */
@Schema(description = "行政区域可用状态变更VO")
@Data
public class SysAreaStatusVO {

    @NotNull(message = "区域编码不能为空")
    @Schema(description = "区域编码")
    private Long code;

    @NotNull(message = "可用状态不能为空")
    @Schema(description = "是否启用：1.启用，2.停用")
    private String status;
}
