package cn.qianxun.meta.dict.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author fuzhilin
 * @Description
 * @since 2023/08/24
 */
@Schema(name = "字典数据响应VO")
@Data
public class SysDictDataVO {
    @Schema(description = "字典编码id")
    private Long dictCode;

    @Schema(description = "字典排序")
    private Integer dictSort;

    @Schema(description = "字典标签")
    private String dictLabel;

    @Schema(description = "字典键值")
    private String dictValue;

    @Schema(description = "字典类型")
    private String dictType;

    @Schema(description = "状态（0正常 1停用）")
    private String status;

    @Schema(description = "备注")
    private String remark;
}
