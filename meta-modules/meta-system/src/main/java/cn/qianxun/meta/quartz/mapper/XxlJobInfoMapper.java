package cn.qianxun.meta.quartz.mapper;

import cn.qianxun.meta.quartz.dto.XxlJobsQueryDTO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.qianxun.meta.quartz.domain.XxlJobInfo;
import cn.qianxun.meta.quartz.vo.XxlJobSelectVO;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description
 */
@Mapper
public interface XxlJobInfoMapper extends BaseMapper<XxlJobInfo> {


    int save(XxlJobInfo info);


    /**
     * 查询任务列表
     *
     * @param dto dto
     * @return List
     */
    List<XxlJobInfo> getXxlJobs(XxlJobsQueryDTO dto);

    /**
     * 任务下拉选
     *
     * @return List
     */
    List<XxlJobSelectVO> getJobsByGroup();

    List<XxlJobInfo> scheduleJobQuery(@Param("maxNextTime") long maxNextTime, @Param("pagesize") int pagesize);


    int scheduleUpdate(XxlJobInfo xxlJobInfo);

    /**
     * 获取任务调度时间Map集合
     *
     * @param ids ids
     * @return Map
     */
    @MapKey("id")
    Map<String, XxlJobInfo> getJobTriggerTime(List<String> ids);
}
