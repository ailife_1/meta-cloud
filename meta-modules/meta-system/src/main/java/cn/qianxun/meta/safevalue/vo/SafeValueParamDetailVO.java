package cn.qianxun.meta.safevalue.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/7 16:35
 * @Description 安全值配置参数响应参数VO
 */
@Data
public class SafeValueParamDetailVO {
    /**
     * id
     */
    private String id;

    /**
     * 参数配置
     */
    private List<SafeValueParamItemVO> content;
}
