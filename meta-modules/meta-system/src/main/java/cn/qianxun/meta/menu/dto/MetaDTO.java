package cn.qianxun.meta.menu.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * <p>
 * 菜单路由传递信息
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-24 09:16:24
 */
@Schema(name = "路由显示信息DTO")
@Data
public class MetaDTO {
    @Schema(description = "设置该路由在侧边栏和面包屑中展示的名字")
    private String title;

    @Schema(description = "设置该路由的图标，对应路径src/icons/svg")
    private String icon;

    @Schema(description = "是否隐藏路由，当设置 true 的时候该路由不会再侧边栏出现")
    private Boolean hidden;

    @Schema(description = "当前路由是否不缓存（默认值：false）0 true 1 false")
    private Boolean noKeepAlive;

    public MetaDTO(String title, String icon, Boolean hidden, Boolean noKeepAlive) {
        this.title = title;
        this.icon = icon;
        this.hidden = hidden;
        this.noKeepAlive = noKeepAlive;
    }
}
