package cn.qianxun.meta.quartz.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description XxlJobLogQueryDTO对象
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class XxlJobLogQueryDTO extends BaseQueryDTO {

    /**
     * 任务分组
     */
    private int jobGroup;


    /**
     * 任务id
     */
    private int jobId;

    /**
     * 任务描述
     */
    private String jobDesc;

    /**
     * 调度-结果 200:成功,500:失败
     */
    private int triggerCode;

    /**
     * 执行-状态 0:空,200:成功,500:失败
     */
    private int handleCode;

    /**
     * 调度开始时间
     */
    private String triggerBeginTime;

    /**
     * 调度结束时间
     */
    private String triggerEndTime;

    /**
     * 更新开始时间
     */
    private String handleBeginTime;

    /**
     * 更新结束时间
     */
    private String handleEndTime;


}
