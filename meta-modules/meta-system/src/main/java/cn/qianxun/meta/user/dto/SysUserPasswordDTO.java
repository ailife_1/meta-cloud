package cn.qianxun.meta.user.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Pattern;

/**
 *
 */
@Schema(description = "新增/修改用户VO")
@Data
public class SysUserPasswordDTO {
    @Schema(description = "用户ID")
    private Long userId;


    @Schema(description = "密码")
    private String password;


}
