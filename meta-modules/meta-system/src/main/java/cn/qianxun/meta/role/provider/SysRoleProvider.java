package cn.qianxun.meta.role.provider;

import cn.qianxun.meta.role.vo.SysRoleVO;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.role.api.feign.ISysRoleProvider;
import cn.qianxun.meta.role.api.vo.ApiRoleDetailsVO;
import cn.qianxun.meta.role.service.ISysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author fuzhilin
 * @Date 2023/10/10 18:31
 * @Description
 */
@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysRoleProvider implements ISysRoleProvider {
    private final ISysRoleService sysRoleService;

    @Override
    public Result<ApiRoleDetailsVO> deptDetails(Long id) {
        ApiRoleDetailsVO detailsVO = new ApiRoleDetailsVO();
        SysRoleVO sysRoleVO = sysRoleService.selectRoleById(id);
        BeanUtils.copyProperties(sysRoleVO, detailsVO);
        return Result.success("查询成功", detailsVO);
    }
}