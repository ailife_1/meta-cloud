package cn.qianxun.meta.menu.service;


import cn.hutool.core.lang.tree.Tree;
import cn.qianxun.meta.menu.dto.*;
import cn.qianxun.meta.menu.vo.*;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.menu.entity.SysMenu;
import cn.qianxun.meta.menu.dto.*;
import cn.qianxun.meta.menu.vo.*;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单查询传递信息
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-24 09:16:24
 */
public interface ISysMenuService extends IService<SysMenu> {

    /**
     * 菜单列表
     *
     * @param dto
     * @return
     */
    List<SysMenuListVO> getMenuList(SysMenuQueryDTO dto);

    /**
     * 添加菜单
     *
     * @param dto
     */
    void addMenu(AddSysMenuDTO dto);

    /**
     * 修改菜单
     *
     * @param dto
     */
    void editMenu(EditSysMenuDTO dto);

    /**
     * 根据id获取菜单详情
     *
     * @param menuId
     * @return
     */
    SysMenuDetailsVO details(String menuId);

    /**
     * 根据id删除菜单
     *
     * @param menuId
     */
    void deleteMenuById(String menuId);

    /**
     * 是否存在菜单子节点
     *
     * @param menuId 菜单ID
     * @return 结果 true 存在 false 不存在
     */
    boolean hasChildByMenuId(Long menuId);

    /**
     * 查询菜单是否存在角色
     *
     * @param menuId 菜单ID
     * @return 结果 true 存在 false 不存在
     */
    boolean checkMenuExistRole(Long menuId);

    /**
     * 校验菜单名称是否唯一
     *
     * @param menu 菜单信息
     * @return 结果
     */
    String checkMenuNameUnique(SysMenu menu);

    /**
     * 根据用户ID查询权限
     *
     * @param userId           用户ID
     * @param businessPlatform 业务平台 1后台2前端
     * @param modularId        模块id
     * @return 权限列表
     */
    Set<String> selectMenuPermsByUserId(Long userId, String businessPlatform, Long modularId);

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    Set<String> selectMenuPermsByUserId(Long userId);

    /**
     * 查询菜单树信息
     *
     * @param dto
     * @return
     */
    List<SysMenuTreeVO> selectMenuTree(RouterDTO dto);

    /**
     * 构建前端路由所需要的菜单
     *
     * @param menus  菜单列表
     * @param isMain 是否主应用 true是 需要过滤 false 不是 不需要过滤
     * @return 路由列表
     */
    List<RouterVO> buildMenus(List<SysMenuTreeVO> menus, Boolean isMain);


    /**
     * 菜单级联
     *
     * @return
     */
    List<MenuCascadeVO> menuCascade();

    /**
     * 菜单模块列表
     *
     * @param dto
     * @return
     */
    List<SysMenuModuleVO> getMenuModuleList(SysMenuModuleQueryDTO dto);

    List<SysMenu> selectMenuList(SysMenu menu, Long userId);

    List<Tree<Long>> buildMenuTreeSelect(List<SysMenu> menus);

    List<SysMenu> selectMenuList(Long userId);

    List<Long> selectMenuListByRoleId(Long idLong);
}
