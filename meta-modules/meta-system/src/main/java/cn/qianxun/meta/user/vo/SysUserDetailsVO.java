package cn.qianxun.meta.user.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.qianxun.meta.dept.entity.SysDept;
import cn.qianxun.meta.role.entity.SysRole;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author fuzhilin
 * @Description
 */
@Schema(description = "用户详情响应DTO")
@Data
public class SysUserDetailsVO {
    @Schema(description = "用户ID")
    private Long userId;

    @Schema(description = "用户昵称")
    private String nickName;

    @Schema(description = "部门名称")
    private String deptName;

    @Schema(description = "用户账号")
    private String userName;

    @Schema(description = "用户性别（0男 1女 2未知）")
    private String sex;

    @Schema(description = "手机号码")
    private String phonenumber;

    @Schema(description = "用户邮箱")
    private String email;

    @Schema(description = "角色")
    private Long roleId;

    @Schema(description = "角色名称")
    private String roleName;

    @Schema(description = "帐号状态（0正常 1停用）")
    private String status;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "限制登录次数")
    private String limitLoginNum;

    @Schema(description = "限制登录时间")
    private Date limitLoginTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 部门对象
     */
    @TableField(exist = false)
    private SysDept dept;

    /**
     * 角色对象
     */
    @TableField(exist = false)
    private List<SysRole> roles;

    /**
     * 角色组
     */
    @TableField(exist = false)
    private Long[] roleIds;

    /**
     * 岗位组
     */
    @TableField(exist = false)
    private Long[] postIds;


}
