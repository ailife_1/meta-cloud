package cn.qianxun.meta.auth.application.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 10:19
 * @Description 添加认证应用请求参数DTO
 */
@Data
public class AddAuthApplicationDTO {
    /**
     * id
     */
    private Long id;

    /**
     * 应用名称
     */
    @NotBlank(message = "应用名称不能为空")
    private String name;

    /**
     * 主体（单位）
     */
    @NotBlank(message = "应用主体不能为空")
    private String subject;

    /**
     * 应用状态：1.启用 2.停用
     */
    @NotNull(message = "请选择应用状态")
    private Integer appStatus;

    /**
     * 限制登录次数
     */
    private Integer limitLoginNum;

    /**
     * 访问时间限制（天）
     */
    private String accessTimeLimit;

    /**
     * ip白名单
     */
    private String ipWhites;

    /**
     * 回调地址
     */
    private String callbackUrl;

    /**
     * 登出url
     */
    private String outUrl;

    /**
     * 应用类型 1API 2统一认证
     */
    @NotNull(message = "请选择应用类型")
    private Integer appType;

    /**
     * 联系人
     */
    private String linkPerson;

    /**
     * 联系人电话
     */
    private String linkPhone;

    /**
     * 应用描述
     */
    private String appDesc;
}
