package cn.qianxun.meta.file.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统文件记录表 前端控制器
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-06 03:21:51
 */
@RestController
@RequestMapping("/sysFileRecord")
public class SysFileRecordController {

}
