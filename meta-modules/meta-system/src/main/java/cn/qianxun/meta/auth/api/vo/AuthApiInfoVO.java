package cn.qianxun.meta.auth.api.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 14:01
 * @Description 认证API接口响应参数VO
 */
@Data
public class AuthApiInfoVO {
    /**
     * id
     */
    private Long id;

    /**
     * 接口名称
     */
    private String apiName;

    /**
     * 接口地址
     */
    private String apiUrl;

    /**
     * 接口权限字符
     */
    private String apiPerms;

    /**
     * 所属模块（字典:sys_operate_module）
     */
    private String apiMod;

    /**
     * 接口状态（1.启用 2.停用）
     */
    private Integer apiStatus;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 备注
     */
    private String remark;
}
