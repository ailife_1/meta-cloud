package cn.qianxun.meta.area.dto;

import lombok.Data;

/**
 * 行政区域DTO响应类
 */
@Data
public class SysAreaCasCaderDTO {
    /**
     * 区域编码
     */
    private Long value;
    /**
     * 区域名称
     */
    private String label;
    /**
     * 父级区域代码
     */
    private Long parentId;
}
