package cn.qianxun.meta.board.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.qianxun.meta.board.dto.*;
import cn.qianxun.meta.board.entity.ScreenBoard;
import cn.qianxun.meta.board.service.IScreenBoardService;
import cn.qianxun.meta.board.vo.ScreenBoardVO;
import cn.qianxun.meta.common.core.dto.RowsData;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import cn.qianxun.meta.board.dto.*;
import cn.qianxun.meta.common.core.constant.VersionConstant;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.common.core.exception.ServiceException;
import cn.qianxun.meta.common.satoken.utils.LoginHelper;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * 大屏设置
 *
 * @author fanfu1024
 * @since 2023-11-10 10:37:02
 */
@RestController
@RequestMapping(VersionConstant.WEB_V1 +"/screenBoard")
public class ScreenBoardController {
    /**
     * 服务对象
     */
    @Resource
    private IScreenBoardService screenBoardService;

    /**
     * 添加看板
     *
     * @param addDTO
     * @return
     */
    @SaCheckPermission("web:v1:screenBoard:add")
    @PostMapping("add")
    public Result add(@RequestBody @Validated AddScreenBoardDTO addDTO) {
        ScreenBoard screenBoard = new ScreenBoard();
        BeanUtil.copyProperties(addDTO, screenBoard);
        screenBoard.setCreatedTime(new Date());
        screenBoard.setCreatedBy(String.valueOf(LoginHelper.getUserId()));
        this.screenBoardService.save(screenBoard);
        return Result.success();
    }

    /**
     * 更新看板
     *
     * @param updateScreenBoardDTO
     * @return
     */
    @SaCheckPermission("web:v1:screenBoard:update")
    @PostMapping("update")
    public Result update(@RequestBody @Validated UpdateScreenBoardDTO updateScreenBoardDTO) {
        ScreenBoard screenBoard = new ScreenBoard();
        BeanUtil.copyProperties(updateScreenBoardDTO, screenBoard);
        screenBoard.setUpdatedBy(String.valueOf(LoginHelper.getUserId()));
        screenBoard.setCreatedTime(new Date());
        this.screenBoardService.updateById(screenBoard);
        return Result.success();
    }

    /**
     * 删除看板
     *
     * @param id
     * @return
     */
    @SaCheckPermission("web:v1:screenBoard:del")
    @PostMapping("del/{id}")
    public Result del(@PathVariable(value = "id") Long id) {
        ScreenBoard tmp = this.screenBoardService.getById(id);
        if (ObjectUtil.isNotNull(tmp)) {
            if (tmp.getAssignFlag()==1) {
                throw new ServiceException("删除失败:大屏已分配!");
            }
        }
        ScreenBoard screenBoard = new ScreenBoard();
        screenBoard.setId(id);
        screenBoard.setDelFlag(1);
        this.screenBoardService.updateById(screenBoard);
        return Result.success();
    }

    /**
     * 列表查询
     *
     * @param screenBoardDTO
     * @return
     */
    @SaCheckPermission("web:v1:screenBoard:pageList")
    @PostMapping("pageList")
    public Result<RowsData<ScreenBoardVO>> pageList(@RequestBody ScreenBoardDTO screenBoardDTO) {
        RowsData<ScreenBoardVO> rowsData = this.screenBoardService.pageList(screenBoardDTO);
        return Result.success(rowsData);
    }

    /**
     * 详情查询
     *
     * @param id
     * @return
     */
    @SaCheckPermission("web:v1:screenBoard:detail")
    @GetMapping("detail/{id}")
    public Result<ScreenBoardVO> detail(@PathVariable(value = "id") Long id) {
        ScreenBoard screenBoard = this.screenBoardService.getById(id);
        ScreenBoardVO screenBoardVO = new ScreenBoardVO();
        BeanUtil.copyProperties(screenBoard, screenBoardVO);
        return Result.success(screenBoardVO);
    }

    /**
     * 根据看板类型查询看板列表
     *
     * @param boardType
     * @return
     */
    @SaCheckPermission("web:v1:screenBoard:listByType")
    @GetMapping("listByType/{boardType}")
    public Result<List<ScreenBoardVO>> listByType(@PathVariable(value = "boardType") Integer boardType) {
        List<ScreenBoard> list = this.screenBoardService.list(new LambdaQueryWrapper<ScreenBoard>().eq(ScreenBoard::getBoardType, boardType).eq(ScreenBoard::getDefaultFlag,0).orderByAsc(Arrays.asList(ScreenBoard::getSorted,ScreenBoard::getId)));
        List<ScreenBoardVO> vos = BeanUtil.copyToList(list, ScreenBoardVO.class);
        return Result.success(vos);
    }

    /**
     * 根据看板是否已经分配查询看板列表
     *
     * @param assignFlag 枚举值:1:是;0:否;
     * @return
     */
    @SaCheckPermission("web:v1:screenBoard:listByAssignType")
    @GetMapping("listByAssignType/{assignFlag}")
    public Result<List<ScreenBoardVO>> listByAssignType(@PathVariable(value = "assignFlag") Integer assignFlag) {
        List<ScreenBoard> list = this.screenBoardService.list(new LambdaQueryWrapper<ScreenBoard>().eq(ScreenBoard::getAssignFlag, assignFlag).eq(ScreenBoard::getDelFlag,0).orderByAsc(Arrays.asList(ScreenBoard::getSorted,ScreenBoard::getId)));
        List<ScreenBoardVO> screenBoardVOS = BeanUtil.copyToList(list, ScreenBoardVO.class);
        return Result.success(screenBoardVOS);
    }

    /**
     * 操作(添加、移除)看板
     *
     * @param assignDTO
     * @return
     */
    @SaCheckPermission("web:v1:screenBoard:operator")
    @PostMapping("/operator")
    public Result operator(@RequestBody ScreenAssignDTO assignDTO) {
        List<Long> ids = assignDTO.getIds();
        if (assignDTO.getOperType() == 1) {
            this.screenBoardService.updateAssignFlag(1, ids);
        } else if (assignDTO.getOperType() == 2) {
            this.screenBoardService.updateAssignFlag(0, ids);
        }
        return Result.success();
    }

    /**
     * 更新看板的排列顺序
     */
    @SaCheckPermission("web:v1:screenBoard:updateSorted")
    @PostMapping("/updateSorted")
    public Result updateSorted(@RequestBody ScreenUpdateSortedDTO updateSortedDTO) {
        List<Long> ids = updateSortedDTO.getIds();
        int i = 0;
        List<ScreenBoard> list = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(ids)) {
            ScreenBoard screenBoard = null;
            for (Long id : ids) {
                screenBoard = new ScreenBoard();
                screenBoard.setId(id);
                screenBoard.setSorted(++i);
                list.add(screenBoard);
            }
            this.screenBoardService.updateSorted(list);
        }
        return Result.success();
    }
}

