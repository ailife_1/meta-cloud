package cn.qianxun.meta.file.service;

import cn.qianxun.meta.file.entity.SysFileRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统文件记录表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-06 03:21:51
 */
public interface ISysFileRecordService extends IService<SysFileRecord> {

}
