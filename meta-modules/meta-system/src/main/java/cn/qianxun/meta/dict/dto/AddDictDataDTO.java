package cn.qianxun.meta.dict.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author fuzhilin
 * @Description
 * @since 2023/08/24
 */
@Data
@Schema(name = "添加字典数据DTO")
public class AddDictDataDTO {
    @Schema(description = "字典编码id")
    private Long dictCode;

    @Schema(description = "字典排序")
    private Integer dictSort;

    @NotBlank(message = "字典标签不能为空")
    @Schema(description = "字典标签")
    private String dictLabel;

    @NotBlank(message = "字典键值不能为空")
    @Schema(description = "字典键值")
    private String dictValue;

    @NotBlank(message = "字典类型不能为空")
    @Schema(description = "字典类型")
    private String dictType;

    @NotBlank(message = "字典状态不能为空")
    @Schema(description = "状态（0正常 1停用）")
    private String status;

    @Schema(description = "备注")
    private String remark;
}
