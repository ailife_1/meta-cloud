package cn.qianxun.meta.user.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import cn.qianxun.meta.dept.entity.SysDept;
import cn.qianxun.meta.role.entity.SysRole;
import lombok.Data;

import java.util.List;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description 用户登录的信息类
 * @Date 2023/8/31 14:40
 **/
@Data
public class SysUserInfoVO {


    /**
     * 用户ID
     */

    private Long userId;

    /**
     * 部门ID
     */
    private Long deptId;

    /**
     * 用户账号
     */
    private String userName;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 手机号
     */
    private String phonenumber;

    /**
     * 邮箱
     */
    private String email;


    /**
     * 用户性别（0男 1女 2未知）
     */
    private String sex;

    /**
     * 头像地址
     */
    private String avatar;

    /**
     * 部门对象
     */
    @TableField(exist = false)
    private SysDept dept;

    /**
     * 角色对象
     */
    @TableField(exist = false)
    private List<SysRole> roles;


    /**
     * 数据权限 当前角色ID
     */
    @TableField(exist = false)
    private Long roleId;

    /**
     * 是否是超管
     */
    @TableField(exist = false)
    private boolean isAdmin;
}
