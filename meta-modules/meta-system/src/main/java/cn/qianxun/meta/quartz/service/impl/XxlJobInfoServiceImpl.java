package cn.qianxun.meta.quartz.service.impl;

import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.utils.StringUtils;
import cn.qianxun.meta.quartz.service.IXxlJobInfoService;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.enums.ExecutorBlockStrategyEnum;
import com.xxl.job.core.glue.GlueTypeEnum;
import cn.qianxun.meta.quartz.core.scheduler.MisfireStrategyEnum;
import cn.qianxun.meta.quartz.core.scheduler.ScheduleTypeEnum;
import cn.qianxun.meta.quartz.core.util.I18nUtil;
import cn.qianxun.meta.quartz.domain.XxlJobGroup;
import cn.qianxun.meta.quartz.domain.XxlJobInfo;
import cn.qianxun.meta.quartz.dto.XxlJobEditDTO;
import cn.qianxun.meta.quartz.dto.XxlJobInsertDTO;
import cn.qianxun.meta.quartz.dto.XxlJobsQueryDTO;
import cn.qianxun.meta.quartz.mapper.XxlJobInfoMapper;
import cn.qianxun.meta.quartz.route.ExecutorRouteStrategyEnum;
import cn.qianxun.meta.quartz.service.IXxlJobGroupService;
import cn.qianxun.meta.quartz.service.IXxlJobLogService;
import cn.qianxun.meta.quartz.thread.JobScheduleHelper;
import cn.qianxun.meta.quartz.vo.XxlJobInfoVO;
import cn.qianxun.meta.quartz.vo.XxlJobSelectVO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description
 */
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Transactional(readOnly = false, rollbackFor = {Exception.class, RuntimeException.class}, propagation = Propagation.REQUIRES_NEW)
public class XxlJobInfoServiceImpl extends ServiceImpl<XxlJobInfoMapper, XxlJobInfo> implements IXxlJobInfoService {

    private final XxlJobInfoMapper xxlJobInfoMapper;
    private final IXxlJobGroupService iXxlJobGroupService;
    private final IXxlJobLogService iXxlJobLogService;

    @Override
    public RowsData<XxlJobInfoVO> getXxlJobs(XxlJobsQueryDTO queryDTO) {
        try (Page<Object> page = PageHelper.startPage(queryDTO.getPageNum(), queryDTO.getPageSize())) {
            List<XxlJobInfo> xxlJobInfoList = xxlJobInfoMapper.getXxlJobs(queryDTO);
            List<XxlJobInfoVO> list = xxlJobInfoList.stream().map(item -> {
                XxlJobInfoVO dto = new XxlJobInfoVO();
                BeanUtils.copyProperties(item, dto);
                dto.setScheduleType(dto.getScheduleType() + ":" + dto.getScheduleConf());
                if (0 != item.getTriggerLastTime()) {
                    dto.setTriggerLastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(item.getTriggerLastTime())));
                }
                if (0 != item.getTriggerNextTime()) {
                    dto.setTriggerNextTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(item.getTriggerNextTime())));
                }
                return dto;
            }).collect(Collectors.toList());
            return new RowsData<XxlJobInfoVO>(list, page.getTotal(), queryDTO.getPageNum());
        }
    }

    @Override
    public Map<String, XxlJobInfo> getJobTriggerTime(List<String> ids) {
        return xxlJobInfoMapper.getJobTriggerTime(ids);
    }


    @Override
    public ReturnT<String> add(XxlJobInsertDTO insertDTO) {
        XxlJobInfo jobInfo = new XxlJobInfo();
        BeanUtils.copyProperties(insertDTO, jobInfo);
        //jobInfo.setAuthor("admin");
        XxlJobGroup group = iXxlJobGroupService.getById(jobInfo.getJobGroup());
        if (group == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("system_please_choose") + I18nUtil.getString("jobinfo_field_jobgroup")));
        }
        if (jobInfo.getJobDesc() == null || jobInfo.getJobDesc().trim().length() == 0) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("system_please_input") + I18nUtil.getString("jobinfo_field_jobdesc")));
        }
        if (jobInfo.getAuthor() == null || jobInfo.getAuthor().trim().length() == 0) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("system_please_input") + I18nUtil.getString("jobinfo_field_author")));
        }

        // valid trigger
        ScheduleTypeEnum scheduleTypeEnum = ScheduleTypeEnum.match(jobInfo.getScheduleType(), null);
        if (scheduleTypeEnum == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
        }
        if (scheduleTypeEnum == ScheduleTypeEnum.CRON) {
            if (jobInfo.getScheduleConf() == null || !CronExpression.isValidExpression(jobInfo.getScheduleConf())) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, "Cron" + I18nUtil.getString("system_unvalid"));
            }
        } else if (scheduleTypeEnum == ScheduleTypeEnum.FIX_RATE || scheduleTypeEnum == ScheduleTypeEnum.FIX_DELAY) {
            if (jobInfo.getScheduleConf() == null) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type")));
            }
            try {
                int fixSecond = Integer.valueOf(jobInfo.getScheduleConf());
                if (fixSecond < 1) {
                    return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
                }
            } catch (Exception e) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
            }
        }

        // valid job
        if (GlueTypeEnum.match(jobInfo.getGlueType()) == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("jobinfo_field_gluetype") + I18nUtil.getString("system_unvalid")));
        }
        if (GlueTypeEnum.BEAN == GlueTypeEnum.match(jobInfo.getGlueType()) && (jobInfo.getExecutorHandler() == null || jobInfo.getExecutorHandler().trim().length() == 0)) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("system_please_input") + "JobHandler"));
        }
        // 》fix "\r" in shell
        if (GlueTypeEnum.GLUE_SHELL == GlueTypeEnum.match(jobInfo.getGlueType()) && jobInfo.getGlueSource() != null) {
            jobInfo.setGlueSource(jobInfo.getGlueSource().replaceAll("\r", ""));
        }

        // valid advanced
        if (ExecutorRouteStrategyEnum.match(jobInfo.getExecutorRouteStrategy(), null) == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("jobinfo_field_executorRouteStrategy") + I18nUtil.getString("system_unvalid")));
        }
        if (MisfireStrategyEnum.match(jobInfo.getMisfireStrategy(), null) == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("misfire_strategy") + I18nUtil.getString("system_unvalid")));
        }
        if (ExecutorBlockStrategyEnum.match(jobInfo.getExecutorBlockStrategy(), null) == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("jobinfo_field_executorBlockStrategy") + I18nUtil.getString("system_unvalid")));
        }
        // 》ChildJobId valid
        String temp = "";
        if (jobInfo.getChildJobid() != null && jobInfo.getChildJobid().trim().length() > 0) {
            String[] childJobIds = jobInfo.getChildJobid().split(",");
            for (String childJobIdItem : childJobIds) {
                if (childJobIdItem != null && childJobIdItem.trim().length() > 0 && isNumeric(childJobIdItem)) {
                    XxlJobInfo childJobInfo = this.getById(Integer.parseInt(childJobIdItem));
                    if (childJobInfo == null) {
                        return new ReturnT<String>(ReturnT.FAIL_CODE, MessageFormat.format((I18nUtil.getString("jobinfo_field_childJobId") + "({0})" + I18nUtil.getString("system_not_found")), childJobIdItem));
                    }
                } else {
                    return new ReturnT<String>(ReturnT.FAIL_CODE, MessageFormat.format((I18nUtil.getString("jobinfo_field_childJobId") + "({0})" + I18nUtil.getString("system_unvalid")), childJobIdItem));
                }
            }

            // join , avoid "xxx,,"
            for (String item : childJobIds) {
                temp += item + ",";
            }
            temp = temp.substring(0, temp.length() - 1);
            jobInfo.setChildJobid(temp);

        }
        // add in db
        jobInfo.setAddTime(new Date());
        jobInfo.setUpdateTime(new Date());
        jobInfo.setGlueUpdatetime(new Date());
        xxlJobInfoMapper.save(jobInfo);
        if ("xxl-job-executor-rule".equals(group.getAppName())) {
            JSONObject parse = JSONObject.parseObject(jobInfo.getExecutorParam());
            JSONObject params = (JSONObject) parse.get("params");
            params.put("jobId", jobInfo.getId());
            if (StringUtils.isNotEmpty(temp)) {
                params.put("childId", temp);
            }
            parse.put("params", params);
            jobInfo.setExecutorParam(JSONObject.toJSONString(parse));
        }
        this.saveOrUpdate(jobInfo);
        return new ReturnT<String>(String.valueOf(jobInfo.getId()));
    }

    @Override
    public ReturnT<String> update(XxlJobEditDTO editDTO) {
        XxlJobInfo jobInfo = new XxlJobInfo();
        BeanUtils.copyProperties(editDTO, jobInfo);
        XxlJobGroup jobGroup = iXxlJobGroupService.getById(jobInfo.getJobGroup());
        if (jobGroup == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("jobinfo_field_jobgroup") + I18nUtil.getString("system_unvalid")));
        }
        if (jobInfo.getJobDesc() == null || jobInfo.getJobDesc().trim().length() == 0) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("system_please_input") + I18nUtil.getString("jobinfo_field_jobdesc")));
        }
        if (jobInfo.getAuthor() == null || jobInfo.getAuthor().trim().length() == 0) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("system_please_input") + I18nUtil.getString("jobinfo_field_author")));
        }
        // valid trigger
        ScheduleTypeEnum scheduleTypeEnum = ScheduleTypeEnum.match(jobInfo.getScheduleType(), null);
        if (scheduleTypeEnum == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
        }
        if (scheduleTypeEnum == ScheduleTypeEnum.CRON) {
            if (jobInfo.getScheduleConf() == null || !CronExpression.isValidExpression(jobInfo.getScheduleConf())) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, "Cron" + I18nUtil.getString("system_unvalid"));
            }
        } else if (scheduleTypeEnum == ScheduleTypeEnum.FIX_RATE || scheduleTypeEnum == ScheduleTypeEnum.FIX_DELAY) {
            if (jobInfo.getScheduleConf() == null) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
            }
            try {
                int fixSecond = Integer.valueOf(jobInfo.getScheduleConf());
                if (fixSecond < 1) {
                    return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
                }
            } catch (Exception e) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
            }
        }
        // valid advanced
        if (ExecutorRouteStrategyEnum.match(jobInfo.getExecutorRouteStrategy(), null) == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("jobinfo_field_executorRouteStrategy") + I18nUtil.getString("system_unvalid")));
        }
        if (MisfireStrategyEnum.match(jobInfo.getMisfireStrategy(), null) == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("misfire_strategy") + I18nUtil.getString("system_unvalid")));
        }
        if (ExecutorBlockStrategyEnum.match(jobInfo.getExecutorBlockStrategy(), null) == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("jobinfo_field_executorBlockStrategy") + I18nUtil.getString("system_unvalid")));
        }
        // 》ChildJobId valid

        if (jobInfo.getChildJobid() != null && jobInfo.getChildJobid().trim().length() > 0) {
            String[] childJobIds = jobInfo.getChildJobid().split(",");
            for (String childJobIdItem : childJobIds) {
                if (childJobIdItem != null && childJobIdItem.trim().length() > 0 && isNumeric(childJobIdItem)) {
                    XxlJobInfo childJobInfo = this.getById(Integer.parseInt(childJobIdItem));
                    if (childJobInfo == null) {
                        return new ReturnT<String>(ReturnT.FAIL_CODE, MessageFormat.format((I18nUtil.getString("jobinfo_field_childJobId") + "({0})" + I18nUtil.getString("system_not_found")), childJobIdItem));
                    }
                } else {
                    return new ReturnT<String>(ReturnT.FAIL_CODE, MessageFormat.format((I18nUtil.getString("jobinfo_field_childJobId") + "({0})" + I18nUtil.getString("system_unvalid")), childJobIdItem));
                }
            }

            String temp = "";
            for (String item : childJobIds) {
                temp += item + ",";
            }
            temp = temp.substring(0, temp.length() - 1);
            jobInfo.setChildJobid(temp);
            if ("xxl-job-executor-rule".equals(jobGroup.getAppName())) {
                JSONObject parse = JSONObject.parseObject(jobInfo.getExecutorParam());
                JSONObject params = (JSONObject) parse.get("params");
                params.put("jobId", jobInfo.getId());
                params.put("childId", temp);
                parse.put("params", params);
                jobInfo.setExecutorParam(JSONObject.toJSONString(parse));
            }
        } else {
            if ("xxl-job-executor-rule".equals(jobGroup.getAppName())) {
                JSONObject parse = JSONObject.parseObject(jobInfo.getExecutorParam());
                JSONObject params = (JSONObject) parse.get("params");
                params.put("jobId", jobInfo.getId());
                params.remove("childId");
                parse.put("params", params);
                jobInfo.setExecutorParam(JSONObject.toJSONString(parse));
            }
        }

        // stage job info
        XxlJobInfo exists_jobInfo = this.getById(jobInfo.getId());
        if (exists_jobInfo == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("jobinfo_field_id") + I18nUtil.getString("system_not_found")));
        }
        // next trigger time (5s后生效，避开预读周期)
        long nextTriggerTime = exists_jobInfo.getTriggerNextTime();
        boolean scheduleDataNotChanged = jobInfo.getScheduleType().equals(exists_jobInfo.getScheduleType()) && jobInfo.getScheduleConf().equals(exists_jobInfo.getScheduleConf());
        if (exists_jobInfo.getTriggerStatus() == 1 && !scheduleDataNotChanged) {
            try {
                Date nextValidTime = JobScheduleHelper.generateNextValidTime(jobInfo, new Date(System.currentTimeMillis() + JobScheduleHelper.PRE_READ_MS));
                if (nextValidTime == null) {
                    return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
                }
                nextTriggerTime = nextValidTime.getTime();
            } catch (Exception e) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
            }
        }
        BeanUtils.copyProperties(jobInfo, exists_jobInfo);
        exists_jobInfo.setTriggerNextTime(nextTriggerTime);
        exists_jobInfo.setUpdateTime(new Date());
        this.saveOrUpdate(exists_jobInfo);
        return ReturnT.SUCCESS;
    }

    @Override
    public ReturnT<String> delJob(String id) {
        XxlJobInfo jobInfo = this.getById(id);
        if (jobInfo == null) {
            return ReturnT.SUCCESS;
        }
        this.removeById(id);
        iXxlJobLogService.delLogByJobId(id);
        iXxlJobLogService.delLogGlueByJobId(id);
        return ReturnT.SUCCESS;

    }

    @Override
    public ReturnT<String> stop(String id) {
        XxlJobInfo xxlJobInfo = this.getById(id);

        xxlJobInfo.setTriggerStatus(0);
        xxlJobInfo.setTriggerLastTime(0L);
        xxlJobInfo.setTriggerNextTime(0L);

        xxlJobInfo.setUpdateTime(new Date());
        this.saveOrUpdate(xxlJobInfo);
        return ReturnT.SUCCESS;
    }

    private boolean isNumeric(String str) {
        try {
            int result = Integer.valueOf(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public ReturnT<String> start(int id) {
        XxlJobInfo xxlJobInfo = this.getById(id);
        // valid
        ScheduleTypeEnum scheduleTypeEnum = ScheduleTypeEnum.match(xxlJobInfo.getScheduleType(), ScheduleTypeEnum.NONE);
        if (ScheduleTypeEnum.NONE == scheduleTypeEnum) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type_none_limit_start")));
        }

        // next trigger time (5s后生效，避开预读周期)
        long nextTriggerTime = 0;
        try {
            Date nextValidTime = JobScheduleHelper.generateNextValidTime(xxlJobInfo, new Date(System.currentTimeMillis() + JobScheduleHelper.PRE_READ_MS));
            if (nextValidTime == null) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
            }
            nextTriggerTime = nextValidTime.getTime();
        } catch (Exception e) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, (I18nUtil.getString("schedule_type") + I18nUtil.getString("system_unvalid")));
        }

        xxlJobInfo.setTriggerStatus(1);
        xxlJobInfo.setTriggerLastTime(0L);
        xxlJobInfo.setTriggerNextTime(nextTriggerTime);

        xxlJobInfo.setUpdateTime(new Date());
        this.saveOrUpdate(xxlJobInfo);
        return ReturnT.SUCCESS;
    }

    @Override
    public int scheduleUpdate(XxlJobInfo xxlJobInfo) {
        return xxlJobInfoMapper.scheduleUpdate(xxlJobInfo);
    }

    @Override
    public List<XxlJobInfo> scheduleJobQuery(long maxNextTime, int pagesize) {
        return xxlJobInfoMapper.scheduleJobQuery(maxNextTime, pagesize);
    }


    @Override
    public List<XxlJobSelectVO> getJobsByGroup() {
        return xxlJobInfoMapper.getJobsByGroup();
    }

}
