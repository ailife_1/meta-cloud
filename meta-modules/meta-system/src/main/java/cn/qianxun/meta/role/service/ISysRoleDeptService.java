package cn.qianxun.meta.role.service;

import cn.qianxun.meta.role.entity.SysRoleDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色和部门关联表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:11
 */
public interface ISysRoleDeptService extends IService<SysRoleDept> {

}
