package cn.qianxun.meta.safevalue.service;

import cn.qianxun.meta.safevalue.dto.AddSysSafeValueParamDTO;
import cn.qianxun.meta.safevalue.entity.SysSafeValueParam;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.safevalue.vo.SafeValTotalNumVO;
import cn.qianxun.meta.safevalue.vo.SafeValueParamDetailVO;

/**
 * <p>
 * 安全值参数配置 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-07 04:26:40
 */
public interface ISysSafeValueParamService extends IService<SysSafeValueParam> {

    /**
     * 查询安全值配置详情
     *
     * @return SafeValueParamDetailVO
     */
    SafeValueParamDetailVO detail();

    /**
     * 保存安全值配置
     *
     * @param dto dto
     */
    void saveSafeValueParam(AddSysSafeValueParamDTO dto);

    /**
     * 安全值总统计
     *
     * @return SafeValTotalNumVO
     */
    SafeValTotalNumVO getSafeValCount();

    /**
     * 创建安全值
     */
    void createSafeValue();
}
