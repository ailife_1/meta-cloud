package cn.qianxun.meta.auth.api.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 13:56
 * @Description 添加认证API接口请求参数DTO
 */
@Data
public class AddAuthApiInfoDTO {

    /**
     * id
     */
    private Long id;

    /**
     * 接口名称
     */
    @NotBlank(message = "接口名称不能为空")
    private String apiName;

    /**
     * 接口地址
     */
    @NotBlank(message = "接口地址不能为空")
    private String apiUrl;

    /**
     * 接口权限字符
     */
    private String apiPerms;

    /**
     * 所属模块（字典:sys_operate_module）
     */
    @NotBlank(message = "所属模块不能为空")
    private String apiMod;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 备注
     */
    private String remark;
}
