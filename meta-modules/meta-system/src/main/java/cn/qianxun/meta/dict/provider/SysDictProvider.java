package cn.qianxun.meta.dict.provider;


import cn.qianxun.meta.common.core.utils.BeanCopyUtils;
import cn.qianxun.meta.dict.service.ISysDictDataService;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.dict.feign.ISysDictProvider;
import cn.qianxun.meta.dict.vo.ApiSysDictDataVO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2021/9/13 0013 14:30
 * @Description
 */
@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysDictProvider implements ISysDictProvider {
    private final ISysDictDataService sysDictDataService;


    @Override
    public Result<List<ApiSysDictDataVO>> getDictDataByDictType(String dictType) {
        return Result.success("查询成功", BeanCopyUtils.copyList(sysDictDataService.selectDictDataByType(dictType), ApiSysDictDataVO.class));
    }

    @Override
    public Result<ApiSysDictDataVO> getDictTypeAndValue(String dictType, String dictValue) {
        return Result.success(BeanCopyUtils.copy(sysDictDataService.getDictData(dictType, dictValue), ApiSysDictDataVO.class));
    }

}
