package cn.qianxun.meta.dept.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/25 11:19
 **/
@Schema(name = "单位的查询查询DTO")
@Data
public class SysDeptQueryDTO extends BaseQueryDTO {


    /**
     * deptId
     */
    private Long deptId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 部门简称
     */
    private String shortName;


    /**
     * 负责人
     */
    private String leader;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 机构性质
     */
    private String nature;

    /**
     * 区
     */
    private String areaName;

    /**
     * 市
     */
    private String cityName;

    /**
     * 省
     */
    private String provinceName;

    /**
     * 标准行业
     */
    private String standardIndustry;


    /**
     * 单位类型
     */
    private Integer deptType;

    /**
     * 行政级别
     */
    private Integer regionLevel;


    /**
     * 部门状态（0正常 1停用）
     */
    private String status;

    /**
     * 单位类型(字典:dept_flag)
     */
    private String deptFlag;



}
