package cn.qianxun.meta.role.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户和角色关联表
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:32
 */
@Getter
@Setter
public class SysUserRoleDTO {


    /**
     * 用户ID
     */
    private Long[] userIds;

    /**
     * 角色ID
     */
    private Long roleId;
}
