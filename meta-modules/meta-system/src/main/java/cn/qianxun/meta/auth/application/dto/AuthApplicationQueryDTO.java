package cn.qianxun.meta.auth.application.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 10:23
 * @Description 认证应用查询参数请求DTO
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AuthApplicationQueryDTO extends BaseQueryDTO {

    /**
     * 应用id
     */
    private Long appId;

    /**
     * 应用名称
     */
    private String name;

    /**
     * 主体（单位）
     */
    private String subject;

    /**
     * 应用状态：1.启用 2.停用
     */
    private Integer appStatus;

    /**
     * 应用类型 1API 2统一认证
     */
    private Integer appType;

}
