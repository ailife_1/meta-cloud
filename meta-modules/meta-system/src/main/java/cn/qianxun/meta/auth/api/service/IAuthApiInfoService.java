package cn.qianxun.meta.auth.api.service;

import cn.qianxun.meta.common.core.dto.ChangeStatusDTO;
import cn.qianxun.meta.common.core.dto.RowsData;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.auth.api.dto.AddAuthApiInfoDTO;
import cn.qianxun.meta.auth.api.dto.AuthApiInfoQueryDTO;
import cn.qianxun.meta.auth.api.dto.DistributionApiDTO;
import cn.qianxun.meta.auth.api.entity.AuthApiInfo;
import cn.qianxun.meta.auth.api.vo.AuthApiInfoVO;

/**
 * <p>
 * 认证API接口基本信息 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 01:41:24
 */
public interface IAuthApiInfoService extends IService<AuthApiInfo> {

    /**
     * 获取API接口列表
     *
     * @param dto dto
     * @return RowsData
     */
    RowsData<AuthApiInfoVO> selectApiInfoList(AuthApiInfoQueryDTO dto);

    /**
     * 添加API接口
     *
     * @param dto dto
     */
    void addApiInfo(AddAuthApiInfoDTO dto);

    /**
     * 修改API接口
     *
     * @param dto dto
     */
    void editApiInfo(AddAuthApiInfoDTO dto);

    /**
     * API接口详情
     *
     * @param id id
     * @return AuthApiInfoVO
     */
    AuthApiInfoVO details(Long id);

    /**
     * 变更API接口状态
     *
     * @param dto dto
     */
    void changeStatus(ChangeStatusDTO dto);

    /**
     * 分配API接口
     *
     * @param dto dto
     */
    void distribution(DistributionApiDTO dto);
}
