package cn.qianxun.meta.safevalue.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 安全值参数配置
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-07 04:26:40
 */
@Getter
@Setter
@TableName("sys_safe_value_param")
public class SysSafeValueParam implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 参数配置
     */
    private String content;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
