package cn.qianxun.meta.upload.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author fuzhilin
 * @Date 2023/10/13 14:11
 * @Description flowable表单上传响应参数VO
 */
@Data
@Accessors(chain = true)
public class FlowableUploadVO {

    /**
     * 文件key
     */
    private String fileKey;

    /**
     * 文件名称
     */
    private String name;

    /**
     * 文件路径
     */
    private String url;
}