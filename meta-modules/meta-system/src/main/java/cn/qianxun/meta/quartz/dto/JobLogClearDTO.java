package cn.qianxun.meta.quartz.dto;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description JobLogClearDTO对象
 */
@Data
public class JobLogClearDTO {

    /**
     * 日志ids
     */
    private String[] logIds;

}
