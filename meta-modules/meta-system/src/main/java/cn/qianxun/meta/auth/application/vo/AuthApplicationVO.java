package cn.qianxun.meta.auth.application.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 10:25
 * @Description 认证应用响应参数VO
 */
@Data
public class AuthApplicationVO {
    /**
     * id
     */
    private Long id;

    /**
     * 应用id
     */
    private Long appId;

    /**
     * 应用名称
     */
    private String name;

    /**
     * 主体（单位）
     */
    private String subject;

    /**
     * 应用状态：1.启用 2.停用
     */
    private Integer appStatus;

    /**
     * 限制登录次数
     */
    private Integer limitLoginNum;

    /**
     * 已登录次数
     */
    private Integer alreadyLoginNum;

    /**
     * 访问时间限制（天）
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date accessTimeLimit;

    /**
     * 应用公钥
     */
    private String pubKey;

    /**
     * ip白名单
     */
    private String ipWhites;

    /**
     * 回调地址
     */
    private String callbackUrl;

    /**
     * 登出url
     */
    private String outUrl;

    /**
     * 应用类型 1API 2统一认证
     */
    private Integer appType;

    /**
     * 联系人
     */
    private String linkPerson;

    /**
     * 联系人电话
     */
    private String linkPhone;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 应用描述
     */
    private String appDesc;

    /**
     * 是否在线 true是 false否
     */
    private boolean onlineStatus;
}
