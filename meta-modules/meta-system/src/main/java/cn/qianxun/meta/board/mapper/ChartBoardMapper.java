package cn.qianxun.meta.board.mapper;

import cn.qianxun.meta.board.entity.ChartBoard;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 看板管理 (ChartBoard)表数据库访问层
 *
 * @author fanfu1024
 * @since 2023-11-08 14:57:15
 */
public interface ChartBoardMapper extends BaseMapperPlus<ChartBoard, ChartBoard, ChartBoard> {

    Integer updateAssignFlag(@Param("assignFlag") Integer assignFlag, @Param("ids") List<Long> ids);

    /**
     * 获取看板数据权限
     *
     * @return
     */
    Set<String> getChartBoardPermission();
}

