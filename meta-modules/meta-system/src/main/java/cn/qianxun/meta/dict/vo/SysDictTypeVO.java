package cn.qianxun.meta.dict.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author fuzhilin
 * @Description
 * @since 2023/08/24
 */
@Schema(name = "字典类型响应VO")
@Data
public class SysDictTypeVO {
    @Schema(description = "字典id")
    private Long dictId;

    @Schema(description = "字典名称")
    private String dictName;

    @Schema(description = "字典类型")
    private String dictType;

    @Schema(description = "状态（0正常 1停用）")
    private String status;

    @Schema(description = "备注")
    private String remark;
}
