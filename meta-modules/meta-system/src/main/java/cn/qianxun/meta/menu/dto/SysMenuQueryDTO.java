package cn.qianxun.meta.menu.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 菜单查询传递信息
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-24 09:16:24
 */
@Schema(name = "菜单查询参数DTO")
@Data
public class SysMenuQueryDTO {

    @Schema(description = "菜单名称")
    private String menuName;

    @Schema(description = "菜单状态（0正常 1停用）")
    private String status;

    @NotNull(message = "父菜单ID不能为空")
    @Schema(description = "父菜单ID/模块id")
    private Long parentId;
}
