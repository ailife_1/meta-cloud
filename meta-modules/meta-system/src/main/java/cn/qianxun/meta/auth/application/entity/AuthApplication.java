package cn.qianxun.meta.auth.application.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 认证中心应用表
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 10:12:30
 */
@Getter
@Setter
@TableName("auth_application")
public class AuthApplication implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 应用id
     */
    private Long appId;

    /**
     * 应用名称
     */
    private String name;

    /**
     * 应用密钥
     */
    private String appSecret;

    /**
     * 主体（单位）
     */
    private String subject;

    /**
     * 应用状态：1.启用 2.停用
     */
    private Integer appStatus;

    /**
     * 限制登录次数
     */
    private Integer limitLoginNum;

    /**
     * 已登录次数
     */
    private Integer alreadyLoginNum;

    /**
     * 访问时间限制（天）
     */
    private Date accessTimeLimit;

    /**
     * 应用公钥
     */
    private String pubKey;

    /**
     * 应用私钥
     */
    private String privateKey;

    /**
     * ip白名单
     */
    private String ipWhites;

    /**
     * 回调地址
     */
    private String callbackUrl;

    /**
     * 登出url
     */
    private String outUrl;

    /**
     * 应用类型 1API 2统一认证
     */
    private Integer appType;

    /**
     * 联系人
     */
    private String linkPerson;

    /**
     * 联系人电话
     */
    private String linkPhone;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 应用描述
     */
    private String appDesc;
}
