package cn.qianxun.meta.log.esmapper;

import cn.easyes.core.conditions.interfaces.BaseEsMapper;
import cn.qianxun.meta.log.api.dto.SysOperLog;
import cn.qianxun.meta.log.api.dto.SysOperLog;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/31 16:41
 **/
public interface SysOperLogMapper extends BaseEsMapper<SysOperLog> {
}
