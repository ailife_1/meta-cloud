package cn.qianxun.meta.config.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/16/35
 * @Description
 */
@Schema(name = "系统参数查询DTO")
@Data
public class SysConfigQueryDTO extends BaseQueryDTO {

    @Schema(description = "参数名称")
    private String configName;

    @Schema(description = "参数键名")
    private String configKey;

    @Schema(description = "系统内置（Y是 N否）")
    private String configType;

    @Schema(description = "开始时间")
    private String beginTime;

    @Schema(description = "结束时间")
    private String endTime;
}
