package cn.qianxun.meta.role.mapper;

import cn.qianxun.meta.role.entity.SysRoleDept;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * <p>
 * 角色和部门关联表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:11
 */
public interface SysRoleDeptMapper extends BaseMapperPlus<SysRoleDeptMapper, SysRoleDept, SysRoleDept> {

}
