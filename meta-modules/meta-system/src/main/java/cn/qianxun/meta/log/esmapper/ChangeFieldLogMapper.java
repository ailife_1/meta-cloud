package cn.qianxun.meta.log.esmapper;

import cn.easyes.core.conditions.interfaces.BaseEsMapper;
import cn.qianxun.meta.log.api.dto.ChangeFieldValueLog;

/**
 * @Author fuzhilin
 * @Date 2023/9/5 20:05
 * @Description
 */
public interface ChangeFieldLogMapper extends BaseEsMapper<ChangeFieldValueLog> {
}
