package cn.qianxun.meta.role.service;

import cn.qianxun.meta.role.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户和角色关联表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:32
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
