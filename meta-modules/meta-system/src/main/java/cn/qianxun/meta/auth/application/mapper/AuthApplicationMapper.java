package cn.qianxun.meta.auth.application.mapper;

import cn.qianxun.meta.auth.application.entity.AuthApplication;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;

import java.util.List;

/**
 * <p>
 * 认证中心应用表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 10:12:30
 */
public interface AuthApplicationMapper extends BaseMapperPlus<AuthApplicationMapper, AuthApplication, AuthApplication> {

    /**
     * 根据appId获取应用权限
     *
     * @param appId appId
     * @return List
     */
    List<String> selectAppApiPermissionByAppId(String appId);
}
