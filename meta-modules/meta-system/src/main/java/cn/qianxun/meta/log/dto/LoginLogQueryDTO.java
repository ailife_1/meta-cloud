package cn.qianxun.meta.log.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2021/9/3 0003 15:44
 * @Description 登录日志查询参数DTO
 */
@Data
public class LoginLogQueryDTO extends BaseQueryDTO {
    /**
     * 用户账号
     */
    private String userName;
    /**
     * 登录状态 0成功 1失败
     */
    private String status;
    /**
     * 登录IP地址
     */
    private String ipaddr;
    /**
     * 开始时间
     */
    private String beginTime;
    /**
     * 结束时间
     */
    private String endTime;
}
