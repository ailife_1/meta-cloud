package cn.qianxun.meta.dict.service;


import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.dict.dto.AddDictTypeDTO;
import cn.qianxun.meta.dict.dto.QueryDictTypeDTO;
import cn.qianxun.meta.dict.entity.SysDictType;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.dict.vo.SysDictTypeVO;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023/08/24
 */
public interface ISysDictTypeService extends IService<SysDictType> {

    /**
     * 获取字典类型列表
     *
     * @param dto
     * @return
     */
    RowsData<SysDictTypeVO> selectDictTypeList(QueryDictTypeDTO dto);

    /**
     * 查询字典类型详情
     *
     * @param dictId
     * @return
     */
    SysDictTypeVO details(Long dictId);

    /**
     * 根据字典类型查询字典类型详情
     *
     * @param dictType
     * @return
     */
    SysDictTypeVO details(String dictType);

    /**
     * 新增字典类型
     *
     * @param dto
     */
    void addDictType(AddDictTypeDTO dto);

    /**
     * 修改字典类型
     *
     * @param dto
     */
    void editDictType(AddDictTypeDTO dto);

    /**
     * 删除字典类型
     *
     * @param vo
     */
    void deleteDictTypeByIds(RemoveLongDTO vo);

    /**
     * 清空缓存
     */
    void clearCache();
}
