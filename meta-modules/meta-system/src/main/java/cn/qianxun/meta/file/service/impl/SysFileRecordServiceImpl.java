package cn.qianxun.meta.file.service.impl;

import cn.qianxun.meta.file.service.ISysFileRecordService;
import cn.qianxun.meta.file.entity.SysFileRecord;
import cn.qianxun.meta.file.mapper.SysFileRecordMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统文件记录表 服务实现类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-06 03:21:51
 */
@Service
public class SysFileRecordServiceImpl extends ServiceImpl<SysFileRecordMapper, SysFileRecord> implements ISysFileRecordService {

}
