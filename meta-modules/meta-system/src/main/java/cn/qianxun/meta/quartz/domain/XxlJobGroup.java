package cn.qianxun.meta.quartz.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author fuzhilin
 * @since 2021-09-29
 * @Description  XxlJobGroup对象
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class XxlJobGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;

    private String appName;

    private String title;

    private int addressType;

    private String addressList;

    private Date updateTime;


    // registry list
    @TableField(exist = false)
    private List<String> registryList;  // 执行器地址列表(系统注册)

    public List<String> getRegistryList() {
        if (addressList != null && addressList.trim().length() > 0) {
            registryList = new ArrayList<String>(Arrays.asList(addressList.split(",")));
        }
        return registryList;
    }


}
