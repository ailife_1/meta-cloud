package cn.qianxun.meta.role.service;

import cn.qianxun.meta.role.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:21
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
