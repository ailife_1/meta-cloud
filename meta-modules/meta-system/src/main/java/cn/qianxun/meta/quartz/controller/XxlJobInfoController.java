package cn.qianxun.meta.quartz.controller;


import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.quartz.service.IXxlJobInfoService;
import cn.qianxun.meta.quartz.thread.JobScheduleHelper;
import com.xxl.job.core.biz.model.ReturnT;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.common.satoken.utils.LoginHelper;
import cn.qianxun.meta.quartz.domain.XxlJobInfo;
import cn.qianxun.meta.quartz.dto.JobTriggerDTO;
import cn.qianxun.meta.quartz.dto.XxlJobEditDTO;
import cn.qianxun.meta.quartz.dto.XxlJobInsertDTO;
import cn.qianxun.meta.quartz.dto.XxlJobsQueryDTO;
import cn.qianxun.meta.quartz.vo.XxlJobInfoVO;
import cn.qianxun.meta.quartz.vo.XxlJobSelectVO;
import cn.qianxun.meta.quartz.vo.XxlJobVO;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description
 */
@Tag(name = "任务管理")
@RestController
@RequestMapping("/web/v1/job")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class XxlJobInfoController {

    private final IXxlJobInfoService iXxlJobInfoService;

    /**
     * 任务列表
     */
    //@SaCheckPermission("web:v1:job:list")
    @PostMapping("/list")
    public Result<RowsData<XxlJobInfoVO>> getXxlJobs(@RequestBody XxlJobsQueryDTO queryDTO) {
        return Result.success("查询成功", iXxlJobInfoService.getXxlJobs(queryDTO));
    }

    /**
     * 任务详情
     */
    //@SaCheckPermission("web:v1:job:getInfo")
    @GetMapping("/getInfo")
    public Result<XxlJobVO> getXxlJobs(@RequestParam String jobId) {
        XxlJobInfo info = iXxlJobInfoService.getById(jobId);
        XxlJobVO dto = new XxlJobVO();
        BeanUtils.copyProperties(info, dto);
        return Result.success(dto);
    }

    /**
     * 任务新增
     */
    //@SaCheckPermission("web:v1:job:add")
    @PostMapping("/add")
    public ReturnT<String> add(@RequestBody XxlJobInsertDTO insertDTO) {
        insertDTO.setAuthor(LoginHelper.getUserName());
        return iXxlJobInfoService.add(insertDTO);
    }

    /**
     * 任务编辑
     */
    //@SaCheckPermission("web:v1:job:update")
    @PostMapping("/update")
    public ReturnT<String> update(@RequestBody XxlJobEditDTO editDTO) {
        editDTO.setAuthor(LoginHelper.getUserName());
        return iXxlJobInfoService.update(editDTO);
    }


    /**
     * 任务删除
     */
    //@SaCheckPermission("web:v1:job:delJob")
    @GetMapping("/delJob")
    public ReturnT<String> remove(@RequestParam String jobId) {
        return iXxlJobInfoService.delJob(jobId);
    }


    /**
     * 任务状态停止
     */
    //@SaCheckPermission("web:v1:job:stop")
    @GetMapping("/stop")
    public ReturnT<String> stop(@RequestParam String jobId) {
        return iXxlJobInfoService.stop(jobId);
    }


    /**
     * 任务状态启动
     */
    //@SaCheckPermission("web:v1:job:start")
    @GetMapping("/start")
    public ReturnT<String> start(@RequestParam String jobId) {
        return iXxlJobInfoService.start(Integer.parseInt(jobId));
    }

    /**
     * 任务启动一次
     */
    //@SaCheckPermission("web:v3:job:trigger")
    @PostMapping("/trigger")
    public Result<String> triggerJob(@RequestBody JobTriggerDTO jobTriggerDTO) {
        JobScheduleHelper.triggerJob(jobTriggerDTO.getId(), jobTriggerDTO.getExecutorParam(), jobTriggerDTO.getAddressList());
        return Result.success();
    }

    /**
     * 任务分组下拉列表
     */
    @GetMapping("/getJobsByGroup")
    public Result<List<XxlJobSelectVO>> getJobsByGroup() {
        return Result.success("查询成功", iXxlJobInfoService.getJobsByGroup());
    }


}

