package cn.qianxun.meta.role.service.impl;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.utils.BeanCopyUtils;
import cn.qianxun.meta.common.core.utils.StringUtils;
import cn.qianxun.meta.common.core.web.dto.SelectVO;
import cn.qianxun.meta.role.dto.SysRoleAddDTO;
import cn.qianxun.meta.role.dto.SysRoleQueryDTO;
import cn.qianxun.meta.role.dto.SysRoleStatusDTO;
import cn.qianxun.meta.role.dto.SysUserRoleDTO;
import cn.qianxun.meta.role.entity.SysRole;
import cn.qianxun.meta.role.entity.SysRoleDept;
import cn.qianxun.meta.role.entity.SysRoleMenu;
import cn.qianxun.meta.role.entity.SysUserRole;
import cn.qianxun.meta.role.mapper.SysRoleDeptMapper;
import cn.qianxun.meta.role.mapper.SysRoleMapper;
import cn.qianxun.meta.role.mapper.SysRoleMenuMapper;
import cn.qianxun.meta.role.mapper.SysUserRoleMapper;
import cn.qianxun.meta.role.vo.SysRoleVO;
import cn.qianxun.meta.user.dto.UserDeptRoleSelectDTO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import cn.qianxun.meta.common.core.constant.Constants;
import cn.qianxun.meta.common.core.constant.UserConstants;
import cn.qianxun.meta.common.core.domain.LoginUser;
import cn.qianxun.meta.common.core.exception.ServiceException;
import cn.qianxun.meta.common.mybatis.core.page.PageQuery;
import cn.qianxun.meta.common.satoken.utils.LoginHelper;
import cn.qianxun.meta.role.service.ISysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色信息表 服务实现类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:28:37
 */
@Service
@RequiredArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {


    private final SysRoleMenuMapper roleMenuMapper;
    private final SysUserRoleMapper userRoleMapper;
    private final SysRoleDeptMapper roleDeptMapper;


    @Override
    public RowsData<SysRoleVO> selectPageRoleList(SysRoleQueryDTO dto) {
        PageQuery pageQuery = new PageQuery();
        pageQuery.setPageNum(dto.getPageNum());
        pageQuery.setPageSize(dto.getPageSize());
        Page<SysRole> page = baseMapper.selectPageRoleList(pageQuery.build(), this.buildQueryWrapper(dto));
        List<SysRole> records = page.getRecords();
        List<SysRoleVO> sysRoleVOList = BeanUtil.copyToList(records, SysRoleVO.class);
        return new RowsData<SysRoleVO>(sysRoleVOList, page.getTotal(), dto.getPageNum());
    }

    @Override
    public List<SysRole> selectRoleList(SysRoleQueryDTO role) {
        return baseMapper.selectRoleList(this.buildQueryWrapper(role));
    }

    private Wrapper<SysRole> buildQueryWrapper(SysRoleQueryDTO role) {
        Map<String, Object> params = new HashMap<>();
        QueryWrapper<SysRole> wrapper = Wrappers.query();
        wrapper.eq("r.del_flag", UserConstants.ROLE_NORMAL).eq(ObjectUtil.isNotNull(role.getRoleId()), "r.role_id", role.getRoleId()).like(StringUtils.isNotBlank(role.getRoleName()), "r.role_name", role.getRoleName()).eq(StringUtils.isNotBlank(role.getStatus()), "r.status", role.getStatus()).like(StringUtils.isNotBlank(role.getRoleKey()), "r.role_key", role.getRoleKey());
        wrapper.orderByAsc("r.role_sort").orderByAsc("r.create_time");
        ;
        return wrapper;
    }

    @Override
    public void checkRoleDataScope(Long roleId) {
        if (!LoginHelper.isAdmin()) {
            SysRoleQueryDTO role = new SysRoleQueryDTO();
            role.setRoleId(roleId);
            List<SysRole> roles = this.selectRoleList(role);
            if (CollUtil.isEmpty(roles)) {
                throw new ServiceException("没有权限访问角色数据！");
            }
        }
    }

    @Override
    public SysRoleVO selectRoleById(Long roleId) {
        SysRole sysRole = baseMapper.selectById(roleId);
        SysRoleVO sysRoleVO = new SysRoleVO();
        BeanCopyUtils.copy(sysRole, sysRoleVO);
        List<SysRoleMenu> sysRoleMenus = roleMenuMapper.selectList(new LambdaQueryWrapper<SysRoleMenu>().eq(SysRoleMenu::getRoleId, roleId));
        if (ObjectUtil.isNotEmpty(sysRoleMenus)) {
            Long[] menuIds = sysRoleMenus.stream().map(SysRoleMenu::getMenuId).toArray(Long[]::new);
            ;
            sysRoleVO.setMenuIds(menuIds);
        }
        List<SysRoleDept> sysRoleDepts = roleDeptMapper.selectList(new LambdaQueryWrapper<SysRoleDept>().eq(SysRoleDept::getRoleId, roleId));
        if (ObjectUtil.isNotEmpty(sysRoleDepts)) {
            Long[] deptIds = sysRoleDepts.stream().map(SysRoleDept::getDeptId).toArray(Long[]::new);
            ;
            sysRoleVO.setDeptIds(deptIds);
        }
        return sysRoleVO;
    }


    @Override
    public void checkRoleAllowed(SysRole role) {
        if (ObjectUtil.isNotNull(role.getRoleId()) && role.isAdmin()) {
            throw new ServiceException("不允许操作超级管理员角色");
        }
        // 新增不允许使用 管理员标识符
        if (ObjectUtil.isNull(role.getRoleId()) && StringUtils.equals(role.getRoleKey(), UserConstants.ADMIN_ROLE_KEY)) {
            throw new ServiceException("不允许使用系统内置管理员角色标识符!");
        }
        // 修改不允许修改 管理员标识符
        if (ObjectUtil.isNotNull(role.getRoleId())) {
            SysRole sysRole = baseMapper.selectById(role.getRoleId());
            // 如果标识符不相等 判断为修改了管理员标识符
            if (!StringUtils.equals(sysRole.getRoleKey(), role.getRoleKey()) && StringUtils.equals(sysRole.getRoleKey(), UserConstants.ADMIN_ROLE_KEY)) {
                throw new ServiceException("不允许修改系统内置管理员角色标识符!");
            }
        }
    }

    @Override
    public boolean checkRoleNameUnique(SysRole role) {
        boolean exist = baseMapper.exists(new LambdaQueryWrapper<SysRole>().eq(SysRole::getRoleName, role.getRoleName()).ne(ObjectUtil.isNotNull(role.getRoleId()), SysRole::getRoleId, role.getRoleId()));
        return !exist;
    }

    @Override
    public boolean checkRoleKeyUnique(SysRole role) {
        boolean exist = baseMapper.exists(new LambdaQueryWrapper<SysRole>().eq(SysRole::getRoleKey, role.getRoleKey()).ne(ObjectUtil.isNotNull(role.getRoleId()), SysRole::getRoleId, role.getRoleId()));
        return !exist;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer insertRole(SysRoleAddDTO role) {
        SysRole sysRole = new SysRole();
        BeanCopyUtils.copy(role, sysRole);
        // 新增角色信息
        baseMapper.insert(sysRole);
        return insertRoleMenu(sysRole);
    }

    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public int insertRoleMenu(SysRole role) {
        int rows = 1;
        // 新增用户与角色管理
        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        for (Long menuId : role.getMenuIds()) {
            SysRoleMenu rm = new SysRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (list.size() > 0) {
            rows = roleMenuMapper.insertBatch(list) ? list.size() : 0;
        }
        return rows;
    }

    @Override
    public int updateRole(SysRoleAddDTO role) {
        SysRole sysRole = new SysRole();
        BeanCopyUtils.copy(role, sysRole);
        // 修改角色信息
        baseMapper.updateById(sysRole);
        // 删除角色与菜单关联
        roleMenuMapper.delete(new LambdaQueryWrapper<SysRoleMenu>().eq(SysRoleMenu::getRoleId, role.getRoleId()));
        return insertRoleMenu(sysRole);
    }

    @Override
    public void cleanOnlineUserByRole(Long roleId) {
        // 如果角色未绑定用户 直接返回
        Long num = userRoleMapper.selectCount(new LambdaQueryWrapper<SysUserRole>().eq(SysUserRole::getRoleId, roleId));
        if (num == 0) {
            return;
        }
        List<String> keys = StpUtil.searchTokenValue("", 0, -1, false);
        if (CollUtil.isEmpty(keys)) {
            return;
        }
        // 角色关联的在线用户量过大会导致redis阻塞卡顿 谨慎操作
        keys.parallelStream().forEach(key -> {
            String token = StringUtils.substringAfterLast(key, ":");
            // 如果已经过期则跳过
            if (StpUtil.stpLogic.getTokenActiveTimeoutByToken(token) < -1) {
                return;
            }
            LoginUser loginUser = LoginHelper.getLoginUser(token);
            if (loginUser.getRoles().stream().anyMatch(r -> r.getRoleId().equals(roleId))) {
                try {
                    StpUtil.logoutByTokenValue(token);
                } catch (NotLoginException ignored) {
                }
            }
        });
    }

    @Override
    public int authDataScope(SysRoleAddDTO role) {
        SysRole sysRole = new SysRole();
        BeanCopyUtils.copy(role, sysRole);
        // 修改角色信息
        baseMapper.updateById(sysRole);
        // 删除角色与部门关联
        roleDeptMapper.delete(new LambdaQueryWrapper<SysRoleDept>().eq(SysRoleDept::getRoleId, role.getRoleId()));
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(sysRole);
    }

    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    public int insertRoleDept(SysRole role) {
        int rows = 1;
        // 新增角色与部门（数据权限）管理
        List<SysRoleDept> list = new ArrayList<SysRoleDept>();
        for (Long deptId : role.getDeptIds()) {
            SysRoleDept rd = new SysRoleDept();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0) {
            rows = roleDeptMapper.insertBatch(list) ? list.size() : 0;
        }
        return rows;
    }


    @Override
    public int updateRoleStatus(SysRoleStatusDTO role) {
        SysRole sysRole = new SysRole();
        BeanCopyUtils.copy(role, sysRole);
        return baseMapper.updateById(sysRole);
    }

    @Override
    public int deleteRoleByIds(List<Long> roleIds) {
        for (Long roleId : roleIds) {
            SysRole role = baseMapper.selectById(roleId);
            checkRoleAllowed(role);
            checkRoleDataScope(roleId);
            if (countUserRoleByRoleId(roleId) > 0) {
                throw new ServiceException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
        }
        // 删除角色与菜单关联
        roleMenuMapper.delete(new LambdaQueryWrapper<SysRoleMenu>().in(SysRoleMenu::getRoleId, roleIds));
        // 删除角色与部门关联
        roleDeptMapper.delete(new LambdaQueryWrapper<SysRoleDept>().in(SysRoleDept::getRoleId, roleIds));
        return baseMapper.deleteBatchIds(roleIds);
    }

    @Override
    public long countUserRoleByRoleId(Long roleId) {
        return userRoleMapper.selectCount(new LambdaQueryWrapper<SysUserRole>().eq(SysUserRole::getRoleId, roleId));
    }

    @Override
    public List<SysRoleVO> selectRoleAll() {
        List<SysRole> sysRoles = this.selectRoleList(new SysRoleQueryDTO());
        List<SysRoleVO> sysRoleVOList = BeanUtil.copyToList(sysRoles, SysRoleVO.class);
        return sysRoleVOList;
    }

    @Override
    public int deleteAuthUser(SysUserRoleDTO userRole) {
        Long[] userIds = userRole.getUserIds();
        int rows = userRoleMapper.delete(new LambdaQueryWrapper<SysUserRole>().eq(SysUserRole::getRoleId, userRole.getRoleId()).in(SysUserRole::getUserId, userIds));
        if (rows > 0) {
            cleanOnlineUserByRole(userRole.getRoleId());
        }
        return rows;
    }

    @Override
    public int insertAuthUsers(SysUserRoleDTO sysUserRoleDTO) {
        // 新增用户与角色管理
        int rows = 1;
        List<SysUserRole> list = new ArrayList<SysUserRole>();
        for (Long userId : sysUserRoleDTO.getUserIds()) {
            SysUserRole ur = new SysUserRole();
            ur.setUserId(userId);
            ur.setRoleId(sysUserRoleDTO.getRoleId());
            list.add(ur);
        }
        if (list.size() > 0) {
            rows = userRoleMapper.insertBatch(list) ? list.size() : 0;
        }
        if (rows > 0) {
            cleanOnlineUserByRole(sysUserRoleDTO.getRoleId());
        }
        return rows;
    }

    @Override
    public List<SysRole> selectRolesByUserId(Long userId) {
        List<SysRole> userRoles = baseMapper.selectRolePermissionByUserId(userId);
        List<SysRoleVO> roles = selectRoleAll();
        List<SysRole> rolesTmp = BeanCopyUtils.copyList(roles, SysRole.class);
        for (SysRole role : rolesTmp) {
            for (SysRole userRole : userRoles) {
                if (role.getRoleId().longValue() == userRole.getRoleId().longValue()) {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return rolesTmp;
    }

    @Override
    public Set<String> selectRolePermissionByUserId(Long userId) {
        List<SysRole> perms = baseMapper.selectRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRole perm : perms) {
            if (ObjectUtil.isNotNull(perm)) {
                permsSet.addAll(StringUtils.splitList(perm.getRoleKey().trim()));
            }
        }
        return permsSet;
    }

    @Override
    public List<SelectVO> getProcessRoleList(UserDeptRoleSelectDTO dto) {
        LambdaQueryWrapper<SysRole> wrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotEmpty(dto.getField())) {
            wrapper.like(SysRole::getRoleName, dto.getField());
        }
        wrapper.ne(SysRole::getRoleId, 1L);
        wrapper.eq(SysRole::getDelFlag, Constants.ZERO);
        wrapper.orderByDesc(SysRole::getCreateTime);
        List<SysRole> sysRoleList = baseMapper.selectList(wrapper);
        return sysRoleList.stream().map(role -> {
            SelectVO vo = new SelectVO();
            vo.setValue(String.valueOf(role.getRoleId()));
            vo.setLabel(role.getRoleName());
            return vo;
        }).collect(Collectors.toList());
    }

    @Override
    public RowsData<SelectVO> getProcessRolePageList(UserDeptRoleSelectDTO dto) {
        LambdaQueryWrapper<SysRole> wrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotEmpty(dto.getField())) {
            wrapper.like(SysRole::getRoleName, dto.getField());
        }
        wrapper.ne(SysRole::getRoleId, 1L);
        wrapper.eq(SysRole::getDelFlag, Constants.ZERO);
        wrapper.orderByDesc(SysRole::getCreateTime);
        com.github.pagehelper.Page<Object> page = PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<SysRole> sysRoleList = baseMapper.selectList(wrapper);
        List<SelectVO> list = sysRoleList.stream().map(role -> {
            SelectVO vo = new SelectVO();
            vo.setValue(String.valueOf(role.getRoleId()));
            vo.setLabel(role.getRoleName());
            return vo;
        }).collect(Collectors.toList());
        return new RowsData<>(list, page.getTotal(), dto.getPageNum());
    }
}
