package cn.qianxun.meta.auth.application.service;

import cn.qianxun.meta.auth.application.dto.AddAuthApplicationDTO;
import cn.qianxun.meta.auth.application.dto.AuthApplicationQueryDTO;
import cn.qianxun.meta.auth.application.vo.AddAuthApplicationVO;
import cn.qianxun.meta.auth.application.vo.ApplicationNameListVO;
import cn.qianxun.meta.auth.application.vo.AuthApplicationVO;
import cn.qianxun.meta.common.core.dto.ChangeStatusDTO;
import cn.qianxun.meta.common.core.dto.RowsData;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.auth.application.entity.AuthApplication;
import cn.qianxun.meta.user.api.vo.ApiAuthApplicationVO;

import java.util.List;

/**
 * <p>
 * 认证中心应用表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 10:12:30
 */
public interface IAuthApplicationService extends IService<AuthApplication> {

    /**
     * 获取应用列表
     *
     * @param dto dto
     * @return RowsData
     */
    RowsData<AuthApplicationVO> selectApplicationList(AuthApplicationQueryDTO dto);

    /**
     * 添加应用
     *
     * @param dto dto
     * @return AddAuthApplicationVO
     */
    AddAuthApplicationVO addApplication(AddAuthApplicationDTO dto);

    /**
     * 修改应用
     *
     * @param dto dto
     */
    void editApplication(AddAuthApplicationDTO dto);

    /**
     * 应用详情
     *
     * @param id id
     * @return AuthApplicationVO
     */
    AuthApplicationVO details(Long id);

    /**
     * 应用名称列表
     * 应用类型 1API 2统一认证
     *
     * @param appType appType
     * @return List
     */
    List<ApplicationNameListVO> applicationNameList(Integer appType);

    /**
     * 变更应用状态
     *
     * @param dto dto
     */
    void changeStatus(ChangeStatusDTO dto);

    /**
     * 根据appId获取登录用户信息
     *
     * @param appId     appId
     * @param appSecret appSecret
     * @return ApiAuthApplicationVO
     */
    ApiAuthApplicationVO getAppInfoByAppId(String appId, String appSecret);

    /**
     * 重置应用密钥
     *
     * @param id id
     * @return Secret
     */
    String resetSecret(Long id);

    /**
     * 强制下线
     *
     * @param id id
     */
    void forceOffline(Long id);

    /**
     * 更新应用次数
     *
     * @param id id
     */
    void updateApplicationLoginNum(Long id);
}
