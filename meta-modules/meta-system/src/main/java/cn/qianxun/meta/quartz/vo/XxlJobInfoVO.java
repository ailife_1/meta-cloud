package cn.qianxun.meta.quartz.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description XxlJobInfoVO对象
 */
@Data
public class XxlJobInfoVO {


    /**
     * 执行器主键ID
     */
    private Integer id;

    /**
     * jobGroup
     */
    private Integer jobGroup;

    /**
     * 任务描述
     */
    private String jobDesc;

    /**
     * 作者
     */
    private String author;

    /**
     * 调度类型
     */
    private String scheduleType;

    /**
     * 调度配置，值含义取决于调度类型
     */
    private String scheduleConf;

    /**
     * 执行器任务handler
     */
    private String executorHandler;

    /**
     * 子任务ID，多个逗号分隔
     */
    private String childJobid;

    /**
     * 执行器任务参数
     */
    private String executorParam;

    /**
     * 调度状态：0-停止，1-运行
     */
    private Integer triggerStatus;

    /**
     * 添加时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 上次调度时间
     */
    private String triggerLastTime;

    /**
     * 下次调度时间
     */
    private String triggerNextTime;


}
