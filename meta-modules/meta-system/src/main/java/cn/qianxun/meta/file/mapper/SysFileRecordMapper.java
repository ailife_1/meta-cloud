package cn.qianxun.meta.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.qianxun.meta.file.entity.SysFileRecord;

/**
 * <p>
 * 系统文件记录表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-06 03:21:51
 */
public interface SysFileRecordMapper extends BaseMapper<SysFileRecord> {

}
