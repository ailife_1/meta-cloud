package cn.qianxun.meta.role.mapper;

import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;
import cn.qianxun.meta.role.entity.SysRoleMenu;

/**
 * <p>
 * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:21
 */
public interface SysRoleMenuMapper extends BaseMapperPlus<SysRoleMenuMapper, SysRoleMenu, SysRoleMenu> {

}
