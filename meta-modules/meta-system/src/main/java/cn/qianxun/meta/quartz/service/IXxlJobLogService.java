package cn.qianxun.meta.quartz.service;

import cn.qianxun.meta.common.core.dto.RowsData;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.quartz.domain.XxlJobLog;
import cn.qianxun.meta.quartz.dto.XxlJobLogQueryDTO;
import cn.qianxun.meta.quartz.vo.XxlJobLogVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description
 */
public interface IXxlJobLogService extends IService<XxlJobLog> {

    /**
     * 任务日志列表
     * @param dto dto
     * @return List
     */
    RowsData<XxlJobLogVO> getXxlJobLogs(XxlJobLogQueryDTO dto);

    int clearLog( List<String> logIds);

    int updateHandleInfo(XxlJobLog xxlJobLog);

    int updateTriggerInfo(XxlJobLog xxlJobLog);

    int delLogByJobId(@Param("jobId") String jobId);

    int delLogGlueByJobId(@Param("jobId") String jobId);

}
