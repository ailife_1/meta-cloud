package cn.qianxun.meta.log.provider;

import cn.easyes.core.biz.EsPageInfo;
import cn.easyes.core.conditions.LambdaEsQueryWrapper;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.utils.StringUtils;
import cn.qianxun.meta.log.api.dto.*;
import cn.qianxun.meta.log.esmapper.ChangeFieldLogMapper;
import cn.qianxun.meta.log.esmapper.SysOperLogMapper;
import cn.qianxun.meta.safevalue.esmapper.SafeValueLogMapper;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.log.api.dto.*;
import cn.qianxun.meta.log.api.feign.ISysLogProvider;
import cn.qianxun.meta.log.esmapper.SysLoginLogMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/31 15:06
 **/
@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysLogProvider implements ISysLogProvider {
    private final SysOperLogMapper SysOperLogMapper;
    private final SysLoginLogMapper sysLoginLogMapper;
    private final ChangeFieldLogMapper changeFieldLogMapper;
    private final SafeValueLogMapper safeValueLogMapper;

    @Override
    public void saveLog(SysOperLog sysOperLog) {
        sysOperLog.setOperTime(new Date());
        SysOperLogMapper.insert(sysOperLog);
    }

    @Override
    public void saveLoginLog(SysLoginLog sysLogininfor) {
        sysLogininfor.setLoginTime(new Date());
        sysLoginLogMapper.insert(sysLogininfor);
    }

    @Override
    public void saveChangeFieldLog(List<ChangeFieldValueLog> fieldValueList) {
        changeFieldLogMapper.insertBatch(fieldValueList);
    }

    @Override
    public Result<RowsData<ChangeFieldValueLog>> selectChangeFieldLogByAssetId(ApiSelectPageDTO dto) {
        LambdaEsQueryWrapper<ChangeFieldValueLog> wrapper = new LambdaEsQueryWrapper<>();
        if (StringUtils.isNotNull(dto.getAssetId())) {
            wrapper.eq(ChangeFieldValueLog::getObjId, dto.getAssetId());
        }
        wrapper.orderByDesc(ChangeFieldValueLog::getOperationTime);
        EsPageInfo<ChangeFieldValueLog> esPageInfo = changeFieldLogMapper.pageQuery(wrapper, dto.getPageNum(), dto.getPageSize());
        return Result.success(new RowsData<ChangeFieldValueLog>(esPageInfo.getList(), esPageInfo.getTotal(), esPageInfo.getPages()));
    }

    @Override
    public void saveSafeValueLog(List<SaveSafeValueLog> safeValueLogList) {
        safeValueLogMapper.insertBatch(safeValueLogList);
    }
}
