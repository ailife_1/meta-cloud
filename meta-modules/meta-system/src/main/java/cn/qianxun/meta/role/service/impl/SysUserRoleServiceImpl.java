package cn.qianxun.meta.role.service.impl;

import cn.qianxun.meta.role.entity.SysUserRole;
import cn.qianxun.meta.role.mapper.SysUserRoleMapper;
import cn.qianxun.meta.role.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户和角色关联表 服务实现类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:32
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
