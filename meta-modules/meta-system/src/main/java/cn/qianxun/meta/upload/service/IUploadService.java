package cn.qianxun.meta.upload.service;

import cn.qianxun.meta.upload.vo.FlowableUploadVO;
import cn.qianxun.meta.upload.vo.UploadFileVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author fuzhilin
 * @Date 2023/9/6 11:41
 * @Description
 */
public interface IUploadService {
    /**
     * 向传递的bucketName中上传文件
     *
     * @param bucketName 桶名称
     * @param file       file
     * @return UploadFileVO
     */
    UploadFileVO uploadByBucketName(String bucketName, MultipartFile file);

    /**
     * 根据文件id下载资源
     *
     * @param fileId   文件id
     * @param response response
     */
    void downloadByFileId(String fileId, HttpServletResponse response);

    /**
     * 根据文件id获取预览地址
     *
     * @param fileId fileId
     * @return 预览地址
     */
    String getPreviewByFileId(String fileId);

    /**
     * 向传递的bucketName中上传文件(token在param)
     * @param bucketName
     * @param file
     * @return
     */
    FlowableUploadVO authUploadByBucketName(String bucketName, MultipartFile file);
}
