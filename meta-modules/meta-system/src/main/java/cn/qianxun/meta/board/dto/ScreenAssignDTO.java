package cn.qianxun.meta.board.dto;

import lombok.Data;

import java.util.List;

@Data
public class ScreenAssignDTO {
    /**
     * 看板组件id
     */
    private List<Long> ids;
    /**
     * 操作动作,枚举值:1、添加;2、移除
     */
    private Integer operType;
}
