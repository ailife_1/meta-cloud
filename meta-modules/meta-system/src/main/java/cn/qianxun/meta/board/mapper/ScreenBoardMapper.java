package cn.qianxun.meta.board.mapper;

import cn.qianxun.meta.board.entity.ScreenBoard;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 大屏看板表(ScreenBoard)表数据库访问层
 *
 * @author fanfu1024
 * @since 2023-11-10 10:37:03
 */
public interface ScreenBoardMapper extends BaseMapperPlus<ScreenBoard,ScreenBoard,ScreenBoard> {

    Integer updateAssignFlag(@Param("assignFlag") Integer assignFlag, @Param("ids") List<Long> ids);

    /**
     * 获取大屏数据权限
     * @return
     */
    Set<String> getScreenBoardPermission();

    /**
     * getUserInfo获取默认大屏地址
     * @return
     */
    String getDefaultLargeScreen();
}

