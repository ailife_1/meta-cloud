package cn.qianxun.meta.area.vo;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @Description
 */
@Schema(description = "行政区域Query响应类")
@Data
public class SysAreaQueryVO extends BaseQueryDTO {

    @Schema(description = "区域编码")
    private Long code;

    @Schema(description = "区域名称")
    private String name;

    @Schema(description = "区域简称")
    private String shortName;

    @Schema(description = "父级区域代码")
    private Long parentId;

    @Schema(description = "市/县对应区域")
    private String sameCode;

    @Schema(description = "行政级别：1.省级2.市级3.县区级")
    private String level;

    @Schema(description = "是否自定义：1是，2否")
    private String isSelf;

    @Schema(description = "是否启用：1.启用，2.停用")
    private String status;

}
