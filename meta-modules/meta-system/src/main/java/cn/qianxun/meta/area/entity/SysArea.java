package cn.qianxun.meta.area.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 行政区域管理表
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-29 09:09:01
 */
@Getter
@Setter
@TableName("sys_area")
@Schema(name = "SysArea", description = "行政区域管理表")
public class SysArea implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "区域编码")
    @TableId(value = "code", type = IdType.ASSIGN_ID)
    private Long code;

    @Schema(description = "父级区域编码")
    @TableField("parent_id")
    private Long parentId;

    @Schema(description = "区域名称")
    @TableField("name")
    private String name;

    @Schema(description = "区域简称")
    @TableField("short_name")
    private String shortName;

    @Schema(description = "行政区域归属（适用县区对应）")
    @TableField("same_code")
    private String sameCode;

    @Schema(description = "级别：1.省级2.市级3.县区级")
    @TableField("level")
    private String level;

    @Schema(description = "排序")
    @TableField("order_num")
    private Integer orderNum;

    @Schema(description = "描述信息")
    @TableField("remark")
    private String remark;

    @Schema(description = "是否自定义：1是，2否")
    @TableField("is_self")
    private String isSelf;

    @Schema(description = "是否启用：1.启用，2.停用")
    @TableField("status")
    private String status;

    @Schema(description = "创建人Id")
    @TableField("user_id")
    private Long userId;

    @Schema(description = "创建者")
    @TableField("create_by")
    private String createBy;

    @Schema(description = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @Schema(description = "更新者")
    @TableField("update_by")
    private String updateBy;

    @Schema(description = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
