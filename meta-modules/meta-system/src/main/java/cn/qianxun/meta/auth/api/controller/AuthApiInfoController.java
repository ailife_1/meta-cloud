package cn.qianxun.meta.auth.api.controller;

import cn.qianxun.meta.auth.api.service.IAuthApiInfoService;
import cn.qianxun.meta.common.core.dto.ChangeStatusDTO;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.auth.api.dto.AddAuthApiInfoDTO;
import cn.qianxun.meta.auth.api.dto.AuthApiInfoQueryDTO;
import cn.qianxun.meta.auth.api.dto.DistributionApiDTO;
import cn.qianxun.meta.auth.api.vo.AuthApiInfoVO;
import cn.qianxun.meta.common.core.domain.Result;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 认证API接口基本信息 前端控制器
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 01:41:24
 */
@Tag(name = "认证API接口管理")
@RestController
@RequestMapping("/web/v1/authApiInfo")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AuthApiInfoController {
    private final IAuthApiInfoService authApiInfoService;

    /**
     * 获取API接口列表
     */
    //@SaCheckPermission(value = "web:v1:authApiInfo:list")
    @PostMapping("/list")
    public Result<RowsData<AuthApiInfoVO>> selectApiInfoList(@RequestBody AuthApiInfoQueryDTO dto) {
        return Result.success("查询成功", authApiInfoService.selectApiInfoList(dto));
    }

    /**
     * 添加API接口
     */
    //@SaCheckPermission(value = "web:v1:authApiInfo:add")
    @PostMapping("/add")
    public Result<Object> addApiInfo(@RequestBody @Validated AddAuthApiInfoDTO dto) {
        authApiInfoService.addApiInfo(dto);
        return Result.success("添加成功");
    }

    /**
     * 修改API接口
     */
    //@SaCheckPermission(value = "web:v1:authApiInfo:edit")
    @PostMapping("/edit")
    public Result<Object> editApiInfo(@RequestBody @Validated AddAuthApiInfoDTO dto) {
        authApiInfoService.editApiInfo(dto);
        return Result.success("修改成功");
    }


    /**
     * API接口详情
     */
    //@SaCheckPermission(value = "web:v1:authApiInfo:details")
    @GetMapping("/details")
    public Result<AuthApiInfoVO> details(@RequestParam Long id) {
        return Result.success(authApiInfoService.details(id));
    }

    /**
     * 变更API接口状态
     */
    //@SaCheckPermission(value = "web:v1:authApiInfo:changeStatus")
    @PostMapping("/changeStatus")
    public Result<Object> changeStatus(@RequestBody ChangeStatusDTO dto) {
        authApiInfoService.changeStatus(dto);
        return Result.success("变更成功");
    }

    /**
     * 分配API接口
     */
    @PostMapping("/distribution")
    public Result<Object> distribution(@RequestBody @Validated DistributionApiDTO dto) {
        authApiInfoService.distribution(dto);
        return Result.success("操作成功");
    }
}
