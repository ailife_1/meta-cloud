package cn.qianxun.meta.menu.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author fuzhilin
 * @Description
 * @since 2023/08/24
 */
@Schema(name = "路由查询参数DTO")
@Data
public class RouterDTO {

    @NotBlank(message = "请选择菜单使用平台")
    @Schema(description = "菜单使用平台（字典menu_platform）")
    private String menuPlatform;

    @Schema(description = "模块id")
    private Long modularId;

    @Schema(description = "登录用户id", hidden = true)
    private Long userId;
}
