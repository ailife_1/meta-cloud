package cn.qianxun.meta.file.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 系统文件记录表
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-06 03:26:09
 */
@Data
@TableName("sys_file_record")
@Schema(name = "SysFileRecord", description = "系统文件记录表")
public class SysFileRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文件id
     */
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 桶名称
     */
    private String bucketName;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件目录
     */
    private String fileDir;

    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 文件类型
     */
    private String fileType;

    /**
     * 上传时间
     */
    private Date createTime;
}
