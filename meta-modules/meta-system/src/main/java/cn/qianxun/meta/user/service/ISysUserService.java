package cn.qianxun.meta.user.service;

import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.dto.SelectVO;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.user.dto.*;
import cn.qianxun.meta.user.vo.SysUserDetailsVO;
import cn.qianxun.meta.user.vo.SysUserListVO;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.qianxun.meta.common.core.domain.LoginUser;
import cn.qianxun.meta.menu.vo.RouterVO;
import cn.qianxun.meta.user.dto.*;
import cn.qianxun.meta.user.entity.SysUser;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-21 02:16:24
 */
public interface ISysUserService extends IService<SysUser> {

    /***
     * 获取用户的列表信息
     * @param dto 查询条件
     * @return
     */
    RowsData<SysUserListVO> selectUserList(SysUserQueryDTO dto);

    /**
     * 用户的新增方法
     *
     * @param dto 添加用户传递的信息
     * @return
     */
    Integer addUser(AddSysUserDTO dto);

    /**
     * 用户的编辑方法
     *
     * @param dto 编辑用户传递的信息
     * @return
     */
    Integer editUser(AddSysUserDTO dto);

    /**
     * 用户的删除方法
     *
     * @param dto 删除用户传递ID信息
     * @return
     */
    Integer deleteUserByIds(RemoveLongDTO dto);

    /**
     * 用户的查看详情方法
     *
     * @param userId 获取用户的查看详情方法
     * @return
     */
    SysUserDetailsVO details(Long userId);

    LoginUser getUserInfo(String username);

    void checkUserAllowed(SysUser user);

    void checkUserDataScope(Long userId);

    Integer resetPwd(SysUserPasswordDTO user);

    Integer updateUserStatus(SysUser user);

    void insertUserAuth(Long userId, Long[] roleIds);

    SysUser selectUserById(Long userId);

    Map<String, Object> getInfo();

    List<RouterVO> getRouter();

    /**
     * 流程 用户下拉选
     *
     * @param dto
     * @return
     */
    List<SelectVO> getProcessUserList(UserDeptRoleSelectDTO dto);

    /**
     * 流程 用户下拉选(分页)
     *
     * @param dto
     * @return
     */
    RowsData<SelectVO> getProcessUserPageList(UserDeptRoleSelectDTO dto);

    /**
     * 根据id获取用户信息
     *
     * @param userId
     * @return
     */
    LoginUser getUserInfoByUserId(Long userId);

    /**
     * 根据单位id、角色id、用户id获取用户信息
     *
     * @param ids id列表
     * @return
     */
    List<LoginUser> getUserInfoById(List<String> ids);

    Boolean updateUserInfoSelf(SysUserEditAvatarDTO sysUserEditAvatarDTO);

    Boolean updatePwd(SysUserEditPasswordDTO sysUserEditPasswordDTO);

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean checkUserNameUnique(SysUser user);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean checkPhoneUnique(SysUser user);

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean checkEmailUnique(SysUser user);

    /**
     * 获取用户邮箱
     *
     * @return List<SelectVO>
     */
    List<SelectVO> getUserEmil();
}
