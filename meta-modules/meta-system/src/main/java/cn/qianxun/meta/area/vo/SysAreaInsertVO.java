package cn.qianxun.meta.area.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @Description
 */
@Schema(description = "行政区域InsertVO响应类")
@Data
public class SysAreaInsertVO {

    @Schema(description = "区域编码")
    private Long code;

    @NotBlank(message = "区域名称不能为空")
    @Size(min = 0, max = 30, message = "区域名称长度不能超过30个字符")
    @Schema(description = "区域名称")
    private String name;

    @Schema(description = "区域简称")
    private String shortName;

    @NotNull(message = "父级区域代码不能为空")
    @Schema(description = "父级区域代码")
    private Long parentId;

    @Schema(description = "行政级别：1.省级2.市级3.县区级", hidden = true)
    private String level;

    @Schema(description = "区域排序")
    private Integer orderNum;

    @Schema(description = "描述信息")
    private String remark;

    @Schema(description = "是否自定义：1是，2否")
    private String isSelf;

    @Schema(description = "是否启用：1.启用，2.停用")
    private String status;

}
