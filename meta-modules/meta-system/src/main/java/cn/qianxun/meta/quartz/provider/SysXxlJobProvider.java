package cn.qianxun.meta.quartz.provider;

import cn.qianxun.meta.quartz.service.IXxlJobInfoService;
import cn.qianxun.meta.quartz.thread.JobScheduleHelper;
import com.alibaba.fastjson.JSON;
import com.xxl.job.core.biz.model.ReturnT;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.job.api.dto.ApiAddXxlJobDTO;
import cn.qianxun.meta.job.api.dto.ApiEditXxlJobDTO;
import cn.qianxun.meta.job.api.dto.ApiJobTriggerDTO;
import cn.qianxun.meta.job.api.feign.ISysXxlJobProvider;
import cn.qianxun.meta.job.api.vo.ApiAddXxlJobVO;
import cn.qianxun.meta.job.api.vo.ApiXxlJobInfoVO;
import cn.qianxun.meta.quartz.domain.XxlJobInfo;
import cn.qianxun.meta.quartz.dto.XxlJobEditDTO;
import cn.qianxun.meta.quartz.dto.XxlJobInsertDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author fuzhilin
 * @Date 2023/9/11 11:36
 * @Description
 */
@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysXxlJobProvider implements ISysXxlJobProvider {
    private final IXxlJobInfoService iXxlJobInfoService;

    @Override
    public ApiAddXxlJobVO addXxlJob(ApiAddXxlJobDTO dto) {
        XxlJobInsertDTO insertDTO = new XxlJobInsertDTO();
        BeanUtils.copyProperties(dto, insertDTO);
        ReturnT<String> jobId = iXxlJobInfoService.add(insertDTO);
        log.info("addXxlJob返回信息：{}", JSON.toJSONString(jobId));
        return new ApiAddXxlJobVO().setJobId(jobId.getContent()).setCode(jobId.getCode()).setMsg(jobId.getMsg());
    }

    @Override
    public ApiAddXxlJobVO editXxlJob(ApiEditXxlJobDTO dto) {
        XxlJobEditDTO updateDTO = new XxlJobEditDTO();
        BeanUtils.copyProperties(dto, updateDTO);
        ReturnT<String> jobId = iXxlJobInfoService.update(updateDTO);
        log.info("editXxlJob返回信息：{}", JSON.toJSONString(jobId));
        return new ApiAddXxlJobVO().setJobId(String.valueOf(dto.getId())).setCode(jobId.getCode()).setMsg(jobId.getMsg());
    }

    @Override
    public void triggerJob(ApiJobTriggerDTO dto) {
        JobScheduleHelper.triggerJob(dto.getId(), dto.getExecutorParam(), dto.getAddressList());
    }

    @Override
    public void stop(String jobId) {
        ReturnT<String> stop = iXxlJobInfoService.stop(jobId);
        log.info("stopXxlJob返回信息：{}", JSON.toJSONString(stop));
    }

    @Override
    public void start(String jobId) {
        ReturnT<String> start = iXxlJobInfoService.start(Integer.parseInt(jobId));
        log.info("startXxlJob返回信息：{}", JSON.toJSONString(start));
    }

    @Override
    public void remove(String jobId) {
        ReturnT<String> remove = iXxlJobInfoService.delJob(jobId);
        log.info("removeXxlJob返回信息：{}", JSON.toJSONString(remove));
    }

    @Override
    public Result<Map<String, ApiXxlJobInfoVO>> getJobTriggerTime(List<String> ids) {
        Map<String, XxlJobInfo> jobTriggerTime = iXxlJobInfoService.getJobTriggerTime(ids);
        Map<String, ApiXxlJobInfoVO> map = new HashMap<>();
        for (Map.Entry<String, XxlJobInfo> entry : jobTriggerTime.entrySet()) {
            XxlJobInfo value = entry.getValue();
            ApiXxlJobInfoVO infoVO = new ApiXxlJobInfoVO();
            BeanUtils.copyProperties(value, infoVO);
            map.put(entry.getKey(), infoVO);
        }
        return Result.success("查询成功", map);
    }
}
