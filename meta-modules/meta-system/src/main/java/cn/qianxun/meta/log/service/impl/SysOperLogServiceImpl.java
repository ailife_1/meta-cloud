package cn.qianxun.meta.log.service.impl;

import cn.easyes.core.biz.EsPageInfo;
import cn.easyes.core.conditions.LambdaEsQueryWrapper;
import cn.hutool.core.util.ObjectUtil;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.utils.StringUtils;
import cn.qianxun.meta.common.core.web.vo.RemoveStringDTO;
import cn.qianxun.meta.log.api.dto.SysOperLog;
import cn.qianxun.meta.log.dto.OperLogQueryDTO;
import cn.qianxun.meta.log.esmapper.SysOperLogMapper;
import cn.qianxun.meta.log.service.ISysOperLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/9/4 16:12
 **/
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysOperLogServiceImpl implements ISysOperLogService {
    private final SysOperLogMapper sysOperLogMapper;

    @Override
    public RowsData<SysOperLog> selectOperLogList(OperLogQueryDTO vo) {
        LambdaEsQueryWrapper<SysOperLog> lambdaEsQueryWrapper = new LambdaEsQueryWrapper<>();
        lambdaEsQueryWrapper.like(StringUtils.isNotEmpty(vo.getTitle()), "title.keyword", vo.getTitle()).eq(ObjectUtil.isNotEmpty(vo.getStatus()), SysOperLog::getStatus, vo.getStatus()).eq(ObjectUtil.isNotEmpty(vo.getBusinessType()), SysOperLog::getBusinessType, vo.getBusinessType()).like(ObjectUtil.isNotEmpty(vo.getMethod()), "method.keyword", vo.getMethod()).like(ObjectUtil.isNotEmpty(vo.getMethod()), "method.request_method", vo.getMethod()).like(ObjectUtil.isNotEmpty(vo.getOperIp()), "oper_ip.request_method", vo.getOperIp()).like(ObjectUtil.isNotEmpty(vo.getOperParam()), "oper_param.request_method", vo.getOperParam()).like(StringUtils.isNotEmpty(vo.getOperName()), "oper_name.request_method", vo.getOperName()).ge(StringUtils.isNotEmpty(vo.getBeginTime()), SysOperLog::getOperTime, vo.getBeginTime()).le(StringUtils.isNotEmpty(vo.getEndTime()), SysOperLog::getOperTime, vo.getEndTime()).orderByDesc(SysOperLog::getOperTime);
        EsPageInfo<SysOperLog> SysOperLogEsPageInfo = sysOperLogMapper.pageQuery(lambdaEsQueryWrapper, vo.getPageNum(), vo.getPageSize());
        RowsData<SysOperLog> SysOperLogRowsData = new RowsData<>(SysOperLogEsPageInfo.getList(), SysOperLogEsPageInfo.getTotal(), vo.getPageNum());
        return SysOperLogRowsData;
    }

    @Override
    public Boolean deleteOperLogByIds(RemoveStringDTO vo) {
        return sysOperLogMapper.deleteBatchIds(vo.getRemoveIdList()) > 0;
    }
}
