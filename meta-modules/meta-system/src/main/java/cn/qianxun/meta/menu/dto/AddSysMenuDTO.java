package cn.qianxun.meta.menu.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * <p>
 * 菜单添加的传输对象
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-24 09:16:24
 */
@Schema(name = "添加菜单参数DTO")
@Data
public class AddSysMenuDTO {


    @Schema(description = "菜单名称")
    private String menuName;

    @Schema(description = "父菜单ID")
    private Long parentId;

    @Schema(description = "显示顺序")
    private Integer orderNum;

    @Schema(description = "路由地址")
    private String path;

    @Schema(description = "组件路径")
    private String component;

    @Schema(description = "路由参数")
    private String queryParam;

    @Schema(description = "是否为外链（0是 1否）")
    private Integer isFrame;

    @Schema(description = "是否为列表页面")
    private Integer isList;

    @Schema(description = "菜单类型（M模块 D目录 N菜单 B按钮 C统计图）")
    private String menuType;

    @Schema(description = "显示状态（0显示 1隐藏）")
    private String visible;

    @Schema(description = "菜单状态（0正常 1停用）")
    private String status;

    @Schema(description = "是否缓存（0缓存 1不缓存")
    private String isCache;

    @Schema(description = "模块id")
    private Long modularId;

    @Schema(description = "权限标识")
    private String perms;

    @Schema(description = "菜单图标")
    private String icon;

    @Schema(description = "菜单使用平台")
    private String menuPlatform;

    @Schema(description = "三级菜单是否展示 0否 1是")
    private Integer isThreeShow;

    @Schema(description = "是否推荐 0否 1是")
    private Integer isRecommend;

    @Schema(description = "备注")
    private String remark;


}
