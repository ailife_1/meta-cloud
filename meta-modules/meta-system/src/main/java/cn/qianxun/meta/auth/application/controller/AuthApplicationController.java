package cn.qianxun.meta.auth.application.controller;

import cn.qianxun.meta.auth.application.dto.AddAuthApplicationDTO;
import cn.qianxun.meta.auth.application.dto.AuthApplicationQueryDTO;
import cn.qianxun.meta.auth.application.service.IAuthApplicationService;
import cn.qianxun.meta.auth.application.vo.AddAuthApplicationVO;
import cn.qianxun.meta.auth.application.vo.ApplicationNameListVO;
import cn.qianxun.meta.auth.application.vo.AuthApplicationVO;
import cn.qianxun.meta.common.core.dto.ChangeStatusDTO;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.domain.Result;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 认证中心应用表 前端控制器
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 10:12:30
 */
@Tag(name = "应用管理")
@RestController
@RequestMapping("/web/v1/authApplication")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AuthApplicationController {
    private final IAuthApplicationService authApplicationService;


    /**
     * 获取应用列表
     */
    //@SaCheckPermission(type = "appId", value = "web:v1:authApplication:list")
    @PostMapping("/list")
    public Result<RowsData<AuthApplicationVO>> selectApplicationList(@RequestBody AuthApplicationQueryDTO dto) {
        return Result.success("查询成功", authApplicationService.selectApplicationList(dto));
    }

    /**
     * 添加应用
     */
    //@SaCheckPermission(type = "appId", value = "web:v1:authApplication:add")
    @PostMapping("/add")
    public Result<AddAuthApplicationVO> addApplication(@RequestBody @Validated AddAuthApplicationDTO dto) {
        return Result.success("添加成功", authApplicationService.addApplication(dto));
    }

    /**
     * 修改应用
     */
    //@SaCheckPermission(type = "appId", value = "web:v1:authApplication:edit")
    @PostMapping("/edit")
    public Result<Object> editApplication(@RequestBody @Validated AddAuthApplicationDTO dto) {
        authApplicationService.editApplication(dto);
        return Result.success("修改成功");
    }


    /**
     * 应用详情
     */
    //@SaCheckPermission(type = "appId", value = "web:v1:authApplication:details")
    @GetMapping("/details")
    public Result<AuthApplicationVO> details(@RequestParam Long id) {
        return Result.success(authApplicationService.details(id));
    }

    /**
     * 应用名称列表
     * 应用类型 1API 2统一认证
     */
    //@SaCheckPermission(type = "appId", value = "web:v1:authApplication:applicationNameList")
    @GetMapping("/applicationNameList")
    public Result<List<ApplicationNameListVO>> applicationNameList(@RequestParam Integer appType) {
        return Result.success(authApplicationService.applicationNameList(appType));
    }

    /**
     * 变更应用状态
     */
    @PostMapping("/changeStatus")
    public Result<Object> changeStatus(@RequestBody ChangeStatusDTO dto) {
        authApplicationService.changeStatus(dto);
        return Result.success("变更成功");
    }

    /**
     * 重置应用密钥
     */
    //@SaCheckPermission(type = "appId", value = "web:v1:authApplication:resetSecret")
    @GetMapping("/resetSecret")
    public Result<String> resetSecret(@RequestParam Long id) {
        return Result.success("重置成功", authApplicationService.resetSecret(id));
    }

    /**
     * 强制下线
     */
    //@SaCheckPermission(type = "appId", value = "web:v1:authApplication:forceOffline")
    @GetMapping("/forceOffline")
    public Result<Object> forceOffline(@RequestParam Long id) {
        authApplicationService.forceOffline(id);
        return Result.success("操作成功");
    }


}
