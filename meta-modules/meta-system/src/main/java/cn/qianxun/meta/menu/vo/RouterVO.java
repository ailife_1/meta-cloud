package cn.qianxun.meta.menu.vo;


import cn.qianxun.meta.menu.dto.MetaDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @author fuzhilin
 * @Description
 * @since 2023/08/24
 */
@Schema(name = "路由信息响应VO")
@Data
public class RouterVO {
    @Schema(description = "路由名字")
    private String name;

    @Schema(description = "路由地址")
    private String path;

    @Schema(description = "重定向地址，当设置 noRedirect 的时候该路由在面包屑导航中不可被点击")
    private String redirect;

    @Schema(description = "组件地址")
    private String component;

    @Schema(description = "当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面")
    private Boolean alwaysShow;

    @Schema(description = "meta")
    private MetaDTO meta;

    @Schema(description = "meunType")
    private String meunType;

    @Schema(description = "是否推荐 0否 1是")
    private Integer isRecommend;

    @Schema(description = "子路由")
    private List<RouterVO> children;
}
