package cn.qianxun.meta.board.dto;

import lombok.Data;

import java.util.List;

@Data
public class ScreenUpdateSortedDTO {
    private List<Long> ids;
}
