package cn.qianxun.meta.quartz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.qianxun.meta.quartz.domain.XxlJobGroup;
import cn.qianxun.meta.quartz.mapper.XxlJobGroupMapper;
import cn.qianxun.meta.quartz.service.IXxlJobGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuzhilin
 * @since 2021-09-29
 */
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Transactional(readOnly = false, rollbackFor = {Exception.class, RuntimeException.class}, propagation = Propagation.REQUIRES_NEW)//开启新事务
public class XxlJobGroupServiceImpl extends ServiceImpl<XxlJobGroupMapper, XxlJobGroup> implements IXxlJobGroupService {

}
