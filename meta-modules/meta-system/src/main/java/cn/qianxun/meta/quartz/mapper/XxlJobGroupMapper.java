package cn.qianxun.meta.quartz.mapper;

import cn.qianxun.meta.quartz.domain.XxlJobGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2021-09-29
 */
public interface XxlJobGroupMapper extends BaseMapper<XxlJobGroup> {

}
