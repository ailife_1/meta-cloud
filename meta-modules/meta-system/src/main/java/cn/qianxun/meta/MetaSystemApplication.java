package cn.qianxun.meta;

import cn.qianxun.meta.common.mybatis.annotation.EnableCustomConfig;
import cn.qianxun.meta.feign.feign.annotation.EnableMetaFeign;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/21 16:07
 **/
@EnableMetaFeign
@SpringBootApplication
@EnableCustomConfig
public class MetaSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(MetaSystemApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  系统管理启动成功   ლ(´ڡ`ლ)ﾞ  \n");
    }
}
