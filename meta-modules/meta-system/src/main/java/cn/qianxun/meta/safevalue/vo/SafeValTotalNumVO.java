package cn.qianxun.meta.safevalue.vo;


import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 0023 15:37
 * @Description 总安全值统计
 */
@Data
public class SafeValTotalNumVO {

    /**
     * 当前安全值
     */
    private String score;

    /**
     * 变化量
     */
    private String change;

    /**
     * 安全等级:high/middle/low
     */
    private String level;

}
