package cn.qianxun.meta.safevalue.esmapper;

import cn.easyes.core.conditions.interfaces.BaseEsMapper;
import cn.qianxun.meta.log.api.dto.SaveSafeValueLog;

/**
 * @Author fuzhilin
 * @Date 2023/9/5 20:05
 * @Description
 */
public interface SafeValueLogMapper extends BaseEsMapper<SaveSafeValueLog> {
}
