package cn.qianxun.meta.log.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import lombok.Data;

/**
 * @Author fuzhilin
 * @Description 操作日志查询参数DTO
 */
@Data
public class OperLogQueryDTO extends BaseQueryDTO {

    /**
     * 操作模块
     */
    private String title;
    /**
     * 业务类型（0其它 1新增 2修改 3删除）
     */
    private Integer businessType;
    /**
     * 请求方法
     */
    private String method;
    /**
     * 操作人员
     */
    private String operName;
    /**
     * 操作地址
     */
    private String operIp;
    /**
     * 请求参数
     */
    private String operParam;
    /**
     * 操作状态（0正常 1异常）
     */
    private Integer status;
    /**
     * 开始时间
     */
    private String beginTime;
    /**
     * 结束时间
     */
    private String endTime;
}
