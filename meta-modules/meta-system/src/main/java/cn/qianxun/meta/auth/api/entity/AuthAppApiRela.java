package cn.qianxun.meta.auth.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 应用API接口关联关系
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 01:41:24
 */
@Getter
@Setter
@TableName("auth_app_api_rela")
public class AuthAppApiRela implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 应用id
     */
    private Long appId;

    /**
     * API接口id
     */
    private Long apiId;
}
