package cn.qianxun.meta.user.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Pattern;

/**
 *  密码修改类
 */
@Data
public class SysUserEditPasswordDTO {


    @Schema(description = "旧密码")
    private String oldPassword;

    @Schema(description = "新密码")
    private String newPassword;


}
