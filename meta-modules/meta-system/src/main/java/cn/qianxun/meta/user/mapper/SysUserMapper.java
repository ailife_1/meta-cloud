package cn.qianxun.meta.user.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.qianxun.meta.common.core.domain.LoginUser;
import cn.qianxun.meta.common.mybatis.annotation.DataColumn;
import cn.qianxun.meta.common.mybatis.annotation.DataPermission;
import cn.qianxun.meta.user.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-21 02:16:24
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
    @DataPermission({
            @DataColumn(key = "deptName", value = "d.dept_id"),
            @DataColumn(key = "userName", value = "u.user_id")
    })
    Page<SysUser> selectPageUserList(@Param("page") Page<SysUser> page, @Param(Constants.WRAPPER) Wrapper<SysUser> queryWrapper);

    SysUser selectUserByUserName(String username);

    SysUser selectUserById(Long userId);

    /**
     * 根据条件分页查询用户列表
     *
     * @param queryWrapper 查询条件
     * @return 用户信息集合信息
     */
    @DataPermission({
            @DataColumn(key = "deptName", value = "d.dept_id"),
            @DataColumn(key = "userName", value = "u.user_id")
    })
    List<SysUser> selectUserList(@Param(Constants.WRAPPER) Wrapper<SysUser> queryWrapper);

    /**
     * 根据单位id、角色id、用户id获取用户信息
     *
     * @param ids id列表
     * @return List<LoginUser>
     */
    List<LoginUser> getUserInfoById(@Param("ids") List<String> ids);
}
