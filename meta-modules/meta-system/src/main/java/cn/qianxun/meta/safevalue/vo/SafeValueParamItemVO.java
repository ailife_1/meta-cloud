package cn.qianxun.meta.safevalue.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/7 16:38
 * @Description 安全值参数配置类型参数VO
 */
@Data
public class SafeValueParamItemVO {
    /**
     * 参数类型 字典safe_value_param
     */
    private String type;

    /**
     * 下限值
     */
    private String minLimit;


    /**
     * 条目集合
     */
    private List<SafeValueItemValVO> items;
}
