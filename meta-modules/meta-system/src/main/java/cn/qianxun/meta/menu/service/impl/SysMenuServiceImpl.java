package cn.qianxun.meta.menu.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.util.ObjectUtil;
import cn.qianxun.meta.common.core.utils.StringUtils;
import cn.qianxun.meta.common.core.utils.TreeBuildUtils;
import cn.qianxun.meta.menu.dto.*;
import cn.qianxun.meta.menu.entity.SysMenu;
import cn.qianxun.meta.menu.mapper.SysMenuMapper;
import cn.qianxun.meta.menu.service.ISysMenuService;
import cn.qianxun.meta.menu.vo.*;
import cn.qianxun.meta.role.entity.SysRole;
import cn.qianxun.meta.role.mapper.SysRoleMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.qianxun.meta.common.core.constant.Constants;
import cn.qianxun.meta.common.core.constant.UserConstants;
import cn.qianxun.meta.common.core.exception.ServiceException;
import cn.qianxun.meta.common.satoken.utils.LoginHelper;
import cn.qianxun.meta.menu.dto.*;
import cn.qianxun.meta.menu.vo.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单查询传递信息
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-24 09:16:24
 */
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {
    private final SysMenuMapper sysMenuMapper;
    private final SysRoleMapper sysRoleMapper;

    @Override
    public List<SysMenuListVO> getMenuList(SysMenuQueryDTO dto) {
        List<SysMenuListVO> menuList = sysMenuMapper.getMenuList(dto);
        return menuList;
    }

    @Override
    public void addMenu(AddSysMenuDTO dto) {
        SysMenu menu = new SysMenu();
        BeanUtils.copyProperties(dto, menu);
        if (UserConstants.NOT_UNIQUE.equals(checkMenuNameUnique(menu))) {
            throw new ServiceException("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        } else if (UserConstants.YES_FRAME.equals(String.valueOf(menu.getIsFrame())) && !StringUtils.startsWithAny(menu.getPath(), Constants.HTTP, Constants.HTTPS)) {
            throw new ServiceException("新增菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        }
        menu.setCreateBy(LoginHelper.getLoginUser().getUserName());
        menu.setCreateTime(new Date());
        if (UserConstants.TYPE_DIR.equals(menu.getMenuType())) {
            menu.setModularId(menu.getParentId());
        } else {
            SysMenu sysMenu = sysMenuMapper.selectById(menu.getParentId());
            if (ObjectUtil.isNotNull(sysMenu) && StringUtils.isNotEmpty(String.valueOf(sysMenu.getModularId()))) {
                menu.setModularId(sysMenu.getModularId());
            }
        }
        menu.setMenuPlatform(Constants.PC);
        sysMenuMapper.insert(menu);
    }

    @Override
    public void editMenu(EditSysMenuDTO dto) {
        SysMenu menu = new SysMenu();
        BeanUtils.copyProperties(dto, menu);

        if (UserConstants.NOT_UNIQUE.equals(checkMenuNameUnique(menu))) {
            throw new ServiceException("修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        } else if (UserConstants.YES_FRAME.equals(String.valueOf(menu.getIsFrame())) && !StringUtils.startsWithAny(menu.getPath(), Constants.HTTP, Constants.HTTPS)) {
            throw new ServiceException("修改菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        } else if (menu.getMenuId().equals(menu.getParentId())) {
            throw new ServiceException("修改菜单'" + menu.getMenuName() + "'失败，上级菜单不能选择自己");
        }
        if (UserConstants.TYPE_DIR.equals(menu.getMenuType()) || UserConstants.TYPE_MENU.equals(menu.getMenuType())) {
            SysMenu sysMenu = sysMenuMapper.selectById(menu.getMenuId());
            if (!menu.getMenuType().equals(sysMenu.getMenuType())) {
                if (hasChildByMenuId(menu.getMenuId())) {
                    throw new ServiceException("存在子菜单,不允许变更菜单类型！");
                }
            }
        }
        menu.setUpdateBy(LoginHelper.getLoginUser().getUserName());
        menu.setUpdateTime(new Date());
        sysMenuMapper.updateById(menu);

    }

    @Override
    public SysMenuDetailsVO details(String menuId) {
        SysMenuDetailsVO detailsDTO = new SysMenuDetailsVO();
        SysMenu sysMenu = sysMenuMapper.selectById(menuId);
        if (ObjectUtil.isNull(sysMenu)) {
            throw new ServiceException("所查菜单不存在，请检查参数是否有误！");
        }
        BeanUtils.copyProperties(sysMenu, detailsDTO);
        return detailsDTO;
    }

    @Override
    public void deleteMenuById(String menuId) {
        if (hasChildByMenuId(Long.valueOf(menuId))) {
            throw new ServiceException("存在子菜单,不允许删除");
        }
        if (checkMenuExistRole(Long.valueOf(menuId))) {
            throw new ServiceException("菜单已分配,不允许删除");
        }
        sysMenuMapper.deleteById(menuId);
    }

    /**
     * 是否存在菜单子节点
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    @Override
    public boolean hasChildByMenuId(Long menuId) {
        int result = sysMenuMapper.hasChildByMenuId(menuId);
        return result > 0 ? true : false;
    }

    /**
     * 查询菜单使用数量
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    @Override
    public boolean checkMenuExistRole(Long menuId) {
        int result = sysMenuMapper.checkMenuExistRole(menuId);
        return result > 0 ? true : false;
    }

    /**
     * 校验菜单名称是否唯一
     *
     * @param menu 菜单信息
     * @return 结果
     */
    @Override
    public String checkMenuNameUnique(SysMenu menu) {
        Long menuId = ObjectUtil.isNull(menu.getMenuId()) ? -1L : menu.getMenuId();
        SysMenu info = sysMenuMapper.selectOne(new LambdaQueryWrapper<SysMenu>().eq(SysMenu::getMenuName, menu.getMenuName()).eq(SysMenu::getParentId, menu.getParentId()).last("limit 1"));
        if (ObjectUtil.isNotNull(info) && info.getMenuId().longValue() != menuId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 根据用户ID查询权限
     *
     * @param userId           用户ID
     * @param businessPlatform 业务平台 1后台2前端
     * @param modularId        模块id
     * @return 权限列表
     */
    @Override
    public Set<String> selectMenuPermsByUserId(Long userId, String businessPlatform, Long modularId) {
        Set<String> permsSet = new HashSet<>();
        if (Constants.ONE.equals(businessPlatform)) {
            List<String> perms = sysMenuMapper.selectMenuPermsAllByUserId(userId, modularId);
            for (String perm : perms) {
                if (StringUtils.isNotEmpty(perm)) {
                    permsSet.addAll(Arrays.asList(perm.trim().split(",")));
                }
            }
        } else if (Constants.TOW.equals(businessPlatform)) {
            List<String> perms = sysMenuMapper.selectMenuPermsByUserId(userId, modularId);
            for (String perm : perms) {
                if (StringUtils.isNotEmpty(perm)) {
                    permsSet.addAll(Arrays.asList(perm.trim().split(",")[0]));
                }
            }
        }
        Set<String> permsSettmp = new HashSet<>();
        permsSet.stream().forEach(perm -> {
            permsSettmp.add(perm.trim());
        });
        return permsSettmp;
    }

    @Override
    public Set<String> selectMenuPermsByUserId(Long userId) {
        List<String> perms = baseMapper.selectMenuPermsByUserIdSingle(userId);
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms) {
            if (StringUtils.isNotEmpty(perm)) {
                permsSet.addAll(StringUtils.splitList(perm.trim()));
            }
        }
        return permsSet;
    }

    @Override
    public List<SysMenuTreeVO> selectMenuTree(RouterDTO dto) {
        List<SysMenuTreeVO> menus = null;
        dto.setUserId(LoginHelper.getUserId());
        if (LoginHelper.isAdmin(dto.getUserId())) {
            menus = sysMenuMapper.selectMenuTreeAll(dto);
        } else {
            menus = sysMenuMapper.selectMenuTreeByUserId(dto);
        }
        for (SysMenuTreeVO menu : menus) {
            if (UserConstants.TYPE_BUTTON.equals(menu.getMenuType()) && StringUtils.isNotEmpty(menu.getPath())) {
                for (SysMenuTreeVO zMenu : menus) {
                    if (menu.getParentId().equals(zMenu.getMenuId()) && UserConstants.TYPE_MENU.equals(zMenu.getMenuType())) {
                        menu.setParentId(zMenu.getParentId());
                    }
                }
            }
        }
        Long parentId = 0L;
        if (ObjectUtil.isNotNull(dto.getModularId())) {
            parentId = dto.getModularId();
        }
        return getChildPerms(menus, parentId);
    }

    @Override
    public List<RouterVO> buildMenus(List<SysMenuTreeVO> menus, Boolean isMain) {
        List<RouterVO> routers = new LinkedList<RouterVO>();
        for (SysMenuTreeVO menu : menus) {
            if (isMain && UserConstants.TYPE_MENU.equals(menu.getMenuType()) && Constants.ZERO.equals(String.valueOf(menu.getIsShow()))) {
                continue;
            }
            RouterVO router = new RouterVO();
            router.setName(getRouteName(menu));
            router.setPath(getRouterPath(menu));
            router.setComponent(getComponent(menu));
            router.setMeta(new MetaDTO(menu.getMenuName(), menu.getIcon(), Constants.YES.equals(menu.getVisible()), String.valueOf(Constants.ZERO).equals(menu.getIsCache())));
            router.setMeunType(menu.getMenuType());
            router.setIsRecommend(menu.getIsRecommend());
            List<SysMenuTreeVO> cMenus = menu.getChildren();
            if (!CollectionUtils.isEmpty(cMenus)) {
                if (UserConstants.TYPE_MOD.equals(menu.getMenuType()) || UserConstants.TYPE_DIR.equals(menu.getMenuType())) {
                    router.setAlwaysShow(true);
                    router.setChildren(buildMenus(cMenus, isMain));
                }
            } else if (isMeunFrame(menu)) {
                List<RouterVO> childrenList = new ArrayList<RouterVO>();
                RouterVO children = new RouterVO();
                children.setPath(menu.getPath());
                children.setComponent(menu.getComponent());
                children.setName(StringUtils.capitalize(menu.getPath()));
                children.setMeta(new MetaDTO(menu.getMenuName(), menu.getIcon(), Constants.YES.equals(menu.getVisible()), String.valueOf(Constants.ZERO).equals(menu.getIsCache())));
                childrenList.add(children);
                router.setChildren(childrenList);
            }
            routers.add(router);
        }
        return routers;
    }

    @Override
    public List<MenuCascadeVO> menuCascade() {
        return sysMenuMapper.menuCascade();
    }

    @Override
    public List<SysMenuModuleVO> getMenuModuleList(SysMenuModuleQueryDTO dto) {
        LambdaQueryWrapper<SysMenu> wrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotEmpty(dto.getMenuPlatform())) {
            wrapper.eq(SysMenu::getMenuPlatform, dto.getMenuPlatform());
        }
        wrapper.eq(SysMenu::getMenuType, "M");
        wrapper.eq(SysMenu::getStatus, Constants.ZERO);
        wrapper.orderByAsc(SysMenu::getOrderNum);
        List<SysMenu> menus = sysMenuMapper.selectList(wrapper);
        List<SysMenuModuleVO> list = menus.stream().map(menu -> {
            SysMenuModuleVO vo = new SysMenuModuleVO();
            BeanUtils.copyProperties(menu, vo);
            return vo;
        }).collect(Collectors.toList());
        return list;
    }

    @Override
    public List<SysMenu> selectMenuList(SysMenu menu, Long userId) {
        List<SysMenu> menuList = null;
        // 管理员显示所有菜单信息
        if (LoginHelper.isAdmin(userId)) {
            menuList = baseMapper.selectList(new LambdaQueryWrapper<SysMenu>()
                    .like(StringUtils.isNotBlank(menu.getMenuName()), SysMenu::getMenuName, menu.getMenuName())
                    .eq(StringUtils.isNotBlank(menu.getVisible()), SysMenu::getVisible, menu.getVisible())
                    .eq(StringUtils.isNotBlank(menu.getStatus()), SysMenu::getStatus, menu.getStatus())
                    .orderByAsc(SysMenu::getParentId)
                    .orderByAsc(SysMenu::getOrderNum));
        } else {
            QueryWrapper<SysMenu> wrapper = Wrappers.query();
            wrapper.eq("sur.user_id", userId)
                    .like(StringUtils.isNotBlank(menu.getMenuName()), "m.menu_name", menu.getMenuName())
                    .eq(StringUtils.isNotBlank(menu.getVisible()), "m.visible", menu.getVisible())
                    .eq(StringUtils.isNotBlank(menu.getStatus()), "m.status", menu.getStatus())
                    .orderByAsc("m.parent_id")
                    .orderByAsc("m.order_num");
            menuList = baseMapper.selectMenuListByUserId(wrapper);
        }
        return menuList;
    }

    @Override
    public List<Tree<Long>> buildMenuTreeSelect(List<SysMenu> menus) {
        if (CollUtil.isEmpty(menus)) {
            return CollUtil.newArrayList();
        }
        return TreeBuildUtils.build(menus, (menu, tree) ->
                tree.setId(menu.getMenuId())
                        .setParentId(menu.getParentId())
                        .setName(menu.getMenuName())
                        .setWeight(menu.getOrderNum()));
    }

    @Override
    public List<SysMenu> selectMenuList(Long userId) {
        return selectMenuList(new SysMenu(), userId);
    }

    @Override
    public List<Long> selectMenuListByRoleId(Long roleId) {
        SysRole role = sysRoleMapper.selectById(roleId);
        return baseMapper.selectMenuListByRoleId(roleId, role.getMenuCheckStrictly());
    }

    /**
     * 根据父节点的ID获取所有子节点
     *
     * @param list     分类表
     * @param parentId 传入的父节点ID
     * @return String
     */
    public List<SysMenuTreeVO> getChildPerms(List<SysMenuTreeVO> list, Long parentId) {
        List<SysMenuTreeVO> returnList = new ArrayList<SysMenuTreeVO>();
        for (Iterator<SysMenuTreeVO> iterator = list.iterator(); iterator.hasNext(); ) {
            SysMenuTreeVO t = (SysMenuTreeVO) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId().equals(parentId)) {
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        return returnList;
    }

    /**
     * 递归列表
     *
     * @param list
     * @param t
     */
    private void recursionFn(List<SysMenuTreeVO> list, SysMenuTreeVO t) {
        // 得到子节点列表
        List<SysMenuTreeVO> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysMenuTreeVO tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysMenuTreeVO> getChildList(List<SysMenuTreeVO> list, SysMenuTreeVO t) {
        List<SysMenuTreeVO> tlist = new ArrayList<SysMenuTreeVO>();
        Iterator<SysMenuTreeVO> it = list.iterator();
        while (it.hasNext()) {
            SysMenuTreeVO n = (SysMenuTreeVO) it.next();
            if (n.getParentId().longValue() == t.getMenuId().longValue()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysMenuTreeVO> list, SysMenuTreeVO t) {
        return getChildList(list, t).size() > 0 ? true : false;
    }

    /**
     * 获取路由名称
     *
     * @param menu 菜单信息
     * @return 路由名称
     */
    public String getRouteName(SysMenuTreeVO menu) {
        String routerName = StringUtils.capitalize(menu.getPath());
        // 非外链并且是一级目录（类型为目录）
        if (isMeunFrame(menu)) {
            routerName = StringUtils.EMPTY;
        }
        return routerName;
    }

    /**
     * 获取路由地址
     *
     * @param menu 菜单信息
     * @return 路由地址
     */
    public String getRouterPath(SysMenuTreeVO menu) {
        String routerPath = menu.getPath();
        //如果组件路径为Layout则path加"/"
        if (UserConstants.LAYOUT.equals(menu.getComponent())) {
            routerPath = "/" + menu.getPath();
        }
        // 非外链并且是一级目录（类型为目录）
        if (0 == menu.getParentId().intValue() && UserConstants.TYPE_DIR.equals(menu.getMenuType()) && UserConstants.NO_FRAME.equals(menu.getIsFrame())) {
            routerPath = "/" + menu.getPath();
        }
        // 非外链并且是一级目录（类型为菜单）
        else if (isMeunFrame(menu)) {
            routerPath = "/";
        }
        return routerPath;
    }

    /**
     * 获取组件信息
     *
     * @param menu 菜单信息
     * @return 组件信息
     */
    public String getComponent(SysMenuTreeVO menu) {
        String component = UserConstants.LAYOUT;
        if (StringUtils.isNotEmpty(menu.getComponent()) && !isMeunFrame(menu)) {
            component = menu.getComponent();
        }
        return component;
    }

    /**
     * 是否为菜单内部跳转
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isMeunFrame(SysMenuTreeVO menu) {
        return menu.getParentId().intValue() == 0 && UserConstants.TYPE_MENU.equals(menu.getMenuType()) && menu.getIsFrame().equals(UserConstants.NO_FRAME);
    }
}
