package cn.qianxun.meta.board.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * 看板管理 (ChartBoard)表实体类
 *
 * @author fanfu1024
 * @since 2023-11-08 14:57:15
 */
@Data
@TableName("chart_board")
public class ChartBoard extends Model<ChartBoard> {
    /**
     * id，主键
     */
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;
    /**
     * 可视化名称
     */
    private String fullName;
    /**
     * 可视化简称
     */
    private String simpleName;
    /**
     * 可视化标识
     */
    private String boardCode;
    /**
     * 可视化权限
     */
    private String boardPower;
    /**
     * 缩略图地址
     */
    private String imgUrl;
    /**
     * 组件状态，枚举值：1：启用；0：禁用；
     */
    private String status;
    /**
     * 备注
     */
    private String bak;
    /**
     * 类型
     */
    private String boardType;
    /**
     * 顺序
     */
    private Integer sorted;
    /**
     * 创建人
     */
    private String createdBy;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String updatedBy;
    /**
     * 更新时间
     */
    private Date updatedTime;
    /**
     * 删除标志位，1：是；0：否；
     */
    private Integer delFlag;
    /**
     * 是否已分配的标志位，枚举值：1：是；0：否；默认值为0
     */
    private Integer assignFlag;
}

