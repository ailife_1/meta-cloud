package cn.qianxun.meta.dept.vo;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/11/4 9:07
 * @Description 根据参数查询单位结果返回参数VO
 */
@Data
public class SelectSysDeptResultVO {
    /**
     * 部门id
     */
    private Long deptId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 单位类型(dept_flag)
     */
    private String deptFlag;
}