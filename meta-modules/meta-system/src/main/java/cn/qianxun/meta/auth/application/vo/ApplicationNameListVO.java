package cn.qianxun.meta.auth.application.vo;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 0005 11:08
 * @Description 认证中心应用名称VO
 */
@Data
public class ApplicationNameListVO {

    /**
     * 应用id
     */
    private Long appId;

    /**
     * 应用名称
     */
    private String name;
}
