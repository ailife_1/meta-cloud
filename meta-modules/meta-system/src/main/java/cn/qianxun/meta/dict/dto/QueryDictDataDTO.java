package cn.qianxun.meta.dict.dto;

import cn.qianxun.meta.common.security.resdata.BaseQueryDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * @author fuzhilin
 * @Description
 * @since 2023/08/24
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@Schema(name = "字典数据查询DTO")
public class QueryDictDataDTO extends BaseQueryDTO {

    @Schema(description = "字典标签")
    private String dictLabel;

    @Schema(description = "字典类型")
    private String dictType;

    @Schema(description = "状态（0正常 1停用）")
    private String status;
}
