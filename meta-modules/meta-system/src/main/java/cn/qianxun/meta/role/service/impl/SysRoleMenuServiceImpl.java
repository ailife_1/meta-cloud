package cn.qianxun.meta.role.service.impl;

import cn.qianxun.meta.role.entity.SysRoleMenu;
import cn.qianxun.meta.role.mapper.SysRoleMenuMapper;
import cn.qianxun.meta.role.service.ISysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色和菜单关联表 服务实现类
 * </p>
 *
 * @author fuzhilin
 * @since 2023-08-28 09:29:21
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

}
