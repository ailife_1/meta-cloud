package cn.qianxun.meta.dict.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.web.dto.SelectLongDTO;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.common.log.annotation.Log;
import cn.qianxun.meta.common.log.enums.BusinessType;
import cn.qianxun.meta.dict.dto.AddDictTypeDTO;
import cn.qianxun.meta.dict.dto.QueryDictTypeDTO;
import cn.qianxun.meta.dict.service.ISysDictTypeService;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.dict.vo.SysDictTypeVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 字典类型表 前端控制器
 * </p>
 *
 * @author fuzhilin
 * @since 2023/08/24
 */
@Tag(name = "字典类型管理")
@RestController
@RequestMapping("/web/v1/sysDictType")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysDictTypeController {
    private final ISysDictTypeService sysDictTypeService;

    @SaCheckPermission("web:v1:sysDictType:list")
    @Operation(summary = "获取字典类型列表")
    @PostMapping("/list")
    public Result<RowsData<SysDictTypeVO>> list(@RequestBody QueryDictTypeDTO dto) {
        return Result.success(sysDictTypeService.selectDictTypeList(dto));
    }

    @SaCheckPermission("web:v1:sysDictType:details")
    @Operation(summary = "查询字典类型详情")
    @PostMapping(value = "/details")
    public Result<SysDictTypeVO> details(@RequestBody SelectLongDTO dictCode) {
        return Result.success(sysDictTypeService.details(dictCode.getIdLong()));
    }

    @SaCheckPermission("web:v1:sysDictType:add")
    @Log(title = "字典类型管理-新增字典类型", businessType = BusinessType.INSERT)
    @Operation(summary = "新增字典类型")
    @PostMapping("/addDictType")
    public Result<String> addDictType(@Validated @RequestBody AddDictTypeDTO dto) {
        sysDictTypeService.addDictType(dto);
        return Result.success("添加成功");
    }

    @SaCheckPermission("web:v1:sysDictType:edit")
    @Log(title = "字典类型管理-修改字典类型", businessType = BusinessType.UPDATE)
    @Operation(summary = "修改字典类型")
    @PostMapping("/editDictType")
    public Result<String> editDictType(@Validated @RequestBody AddDictTypeDTO dto) {
        sysDictTypeService.editDictType(dto);
        return Result.success("修改成功");
    }

    @SaCheckPermission("web:v1:sysDictType:remove")
    @Log(title = "字典类型管理-删除字典数据", businessType = BusinessType.DELETE)
    @Operation(summary = "删除字典类型")
    @PostMapping("/remove")
    public Result<String> deleteDictTypeByIds(@RequestBody RemoveLongDTO dto) {
        sysDictTypeService.deleteDictTypeByIds(dto);
        return Result.success("删除成功！");
    }

    @SaCheckPermission("web:v1:sysDictType:clearCache")
    @Log(title = "字典类型管理-清空缓存", businessType = BusinessType.CLEAN)
    @Operation(summary = "清空缓存")
    @PostMapping("/clearCache")
    public Result<String> clearCache() {
        sysDictTypeService.clearCache();
        return Result.success();
    }
}

