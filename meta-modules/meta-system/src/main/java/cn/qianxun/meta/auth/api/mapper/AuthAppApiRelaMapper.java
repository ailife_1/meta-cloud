package cn.qianxun.meta.auth.api.mapper;

import cn.qianxun.meta.auth.api.entity.AuthAppApiRela;
import cn.qianxun.meta.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * <p>
 * 应用API接口关联关系 Mapper 接口
 * </p>
 *
 * @author fuzhilin
 * @since 2023-09-12 01:41:24
 */
public interface AuthAppApiRelaMapper extends BaseMapperPlus<AuthAppApiRelaMapper,AuthAppApiRela,AuthAppApiRela> {

}
