package cn.qianxun.meta.config.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author fuzhilin
 * @Date 2023/16/35
 * @Description
 */
@Schema(name = "新增系统参数DTO")
@Data
public class AddSysConfigDTO {

    @Schema(description = "参数主键")
    private Long configId;

    @NotBlank(message = "参数名称不能为空")
    @Schema(description = "参数名称")
    private String configName;

    @NotBlank(message = "参数键名不能为空")
    @Schema(description = "参数键名")
    private String configKey;

    @NotBlank(message = "参数键值不能为空")
    @Schema(description = "参数键值")
    private String configValue;

    @NotBlank(message = "请选择是否系统内置")
    @Schema(description = "系统内置（Y是 N否）")
    private String configType;

    @Schema(description = "备注")
    private String remark;
}
