package cn.qianxun.meta.datascope.api.feign;

import cn.qianxun.meta.common.core.constant.FeignConstant;
import cn.qianxun.meta.common.core.constant.ProviderConstant.SystemProviderConstant;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author fuzhilin
 * @Description
 */
@Hidden
@FeignClient(name = FeignConstant.META_CLOUD_SYSTEM)
public interface ISysUserDataScopeProvider {


    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_USER_getRoleCustom)
     String getRoleCustom(@RequestParam("roleId")Long roleId);
    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_USER_getDeptAndChild)
     String getDeptAndChild(@RequestParam("deptId")Long deptId);
}
