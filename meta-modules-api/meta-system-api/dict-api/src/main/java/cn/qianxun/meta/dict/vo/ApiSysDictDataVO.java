package cn.qianxun.meta.dict.vo;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/13 0013 14:26
 * @Description API-字典数据响应VO
 */
@Data
public class ApiSysDictDataVO {
    /**
     * 字典编码id
     */
    private Long dictCode;

    /**
     * 字典排序
     */
    private Integer dictSort;

    /**
     * 字典标签
     */
    private String dictLabel;

    /**
     * 字典键值
     */
    private String dictValue;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 状态（0正常 1停用）
     */
    private String status;

    /**
     * 备注
     */
    private String remark;
}
