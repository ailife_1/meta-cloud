package cn.qianxun.meta.dict.feign;


import cn.qianxun.meta.common.core.constant.FeignConstant;
import cn.qianxun.meta.common.core.constant.ProviderConstant.SystemProviderConstant;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.dict.vo.ApiSysDictDataVO;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/9/13 0013 14:24
 * @Description
 */
@Hidden
@FeignClient(FeignConstant.META_CLOUD_SYSTEM)
public interface ISysDictProvider {
    /**
     * 根据字典类型获取字典数据
     *
     * @param dictType
     * @return Result
     */
    @GetMapping(SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_DICT_GET_DICT_DATA_BY_DICT_TYPE)
    Result<List<ApiSysDictDataVO>> getDictDataByDictType(@RequestParam("dictType") String dictType);

    /**
     * 根据字典类型和字典值查询字典数据信息
     *
     * @param dictType  字典类型
     * @param dictValue 字典键值
     * @return Result
     */
    @GetMapping(value = SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_DICT_GET_DICT_TYPE_AND_VALUE)
    Result<ApiSysDictDataVO> getDictTypeAndValue(@RequestParam("dictType") String dictType, @RequestParam("dictValue") String dictValue);

}

