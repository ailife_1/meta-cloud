package cn.qianxun.meta.user.api.vo;

import lombok.Data;

import java.util.Date;
import java.util.Set;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 15:58
 * @Description API认证应用信息响应参数VO
 */
@Data
public class ApiAuthApplicationVO {
    /**
     * id
     */
    private Long id;

    /**
     * 应用id
     */
    private Long appId;

    /**
     * 应用名称
     */
    private String name;

    /**
     * 应用状态：1.启用 2.停用
     */
    private Integer appStatus;

    /**
     * 限制登录次数
     */
    private Integer limitLoginNum;

    /**
     * 已登录次数
     */
    private Integer alreadyLoginNum;

    /**
     * 访问时间限制（天）
     */
    private Date accessTimeLimit;

    /**
     * ip白名单
     */
    private String ipWhites;

    /**
     * 菜单权限
     */
    private Set<String> menuPermission;


}
