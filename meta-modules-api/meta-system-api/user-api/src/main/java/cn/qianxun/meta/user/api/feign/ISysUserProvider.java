package cn.qianxun.meta.user.api.feign;

import cn.qianxun.meta.common.core.constant.FeignConstant;
import cn.qianxun.meta.common.core.constant.ProviderConstant.SystemProviderConstant;
import cn.qianxun.meta.common.core.domain.LoginUser;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.user.api.vo.ApiAuthApplicationVO;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author fuzhilin
 * @Description
 */
@Hidden
@FeignClient(name = FeignConstant.META_CLOUD_SYSTEM)
public interface ISysUserProvider {

    /**
     * 根据用户名获取登录用户信息
     *
     * @param username 用户名
     * @return LoginUser
     */
    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_USER_getUserInfo)
    LoginUser getUserInfo(@RequestParam("username") String username);

    /**
     * 根据appId获取登录用户信息
     *
     * @param appId appId
     * @return ApiAuthApplicationVO
     */
    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_USER_GET_APP_INFO_BY_APPID)
    Result<ApiAuthApplicationVO> getAppInfoByAppId(@RequestParam("appId") String appId, @RequestParam("appSecret") String appSecret);

    /**
     * 更新应用次数
     *
     * @param id id
     */
    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_USER_UPDATE_APPLICATION_LOGIN_NUM)
    Result<Object> updateApplicationLoginNum(@RequestParam("id") Long id);

    /**
     * 根据用户id获取用户信息
     *
     * @param userId 用户id
     * @return  String
     */
    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_USER_getUserInfoByUserId)
    Result<LoginUser> getUserInfoByUserId(@RequestParam("userId") Long userId);

    /**
     * 根据单位id、角色id、用户id获取用户信息
     *
     * @param ids id列表
     * @return  String
     */
    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_USER_getUserInfoByIds)
    Result<List<LoginUser>> getUserInfoById(@RequestParam("ids") List<String> ids);

}
