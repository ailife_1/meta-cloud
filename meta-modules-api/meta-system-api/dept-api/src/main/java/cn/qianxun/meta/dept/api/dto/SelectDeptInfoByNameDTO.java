package cn.qianxun.meta.dept.api.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author fuzhilin
 * @Date 2023/11/14 9:06
 * @Description 根据单位名称查询单位id查询参数DTO
 */
@Data
public class SelectDeptInfoByNameDTO {
    private List<DeptList> deptList;

    @Data
    public static class DeptList {
        /**
         * 单位名称
         */
        private List<String> deptNames;

        /**
         * 单位类型(1、建设单位 2、运维单位 3、使用单位)
         */
        private String deptFlag;
    }
}