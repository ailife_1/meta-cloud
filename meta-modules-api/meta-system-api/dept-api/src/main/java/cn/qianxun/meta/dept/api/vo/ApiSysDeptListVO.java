package cn.qianxun.meta.dept.api.vo;

import lombok.Data;

import java.util.Date;

/**
 * API-单位
 */
@Data
public class ApiSysDeptListVO {

    /**
     * 部门id
     */
    private Long deptId;

    /**
     * 父部门id
     */
    private Long parentId;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 社会统一信用代码
     */
    private String orgCode;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 部门简称
     */
    private String shortName;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 机构性质
     */
    private String nature;

    /**
     * 区
     */
    private String areaName;

    /**
     * 市
     */
    private String cityName;

    /**
     * 省
     */
    private String provinceName;

    /**
     * 标准行业
     */
    private String standardIndustry;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 单位类型
     */
    private Integer deptType;

    /**
     * 行政级别
     */
    private Integer regionLevel;

    /**
     * 详细地址
     */
    private String detailAddress;

    /**
     * 部门状态（0正常 1停用）
     */
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 单位类型(dept_flag)
     */
    private String deptFlag;
}