package cn.qianxun.meta.dept.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * API-查询单位列表参数DTO
 */
@Data
public class ApiSysDeptQueryDTO {
    /**
     * deptId
     */
    private Long deptId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 部门简称
     */
    private String shortName;


    /**
     * 负责人
     */
    private String leader;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 机构性质
     */
    private String nature;

    /**
     * 区
     */
    private String areaName;

    /**
     * 市
     */
    private String cityName;

    /**
     * 省
     */
    private String provinceName;

    /**
     * 标准行业
     */
    private String standardIndustry;


    /**
     * 单位类型
     */
    private Integer deptType;

    /**
     * 行政级别
     */
    private Integer regionLevel;


    /**
     * 部门状态（0正常 1停用）
     */
    private String status;

    /**
     * 单位类型(字典:dept_flag)
     */
    private String deptFlag;

    /**
     * 页码
     */
    @Schema(description = "页码", requiredMode = Schema.RequiredMode.REQUIRED, example = "1")
    private Integer pageNum = 1;

    /**
     * 页面显示长度
     */
    @Schema(description = "页面显示长度", requiredMode = Schema.RequiredMode.REQUIRED, example = "10")
    private Integer pageSize = 10;

}