package cn.qianxun.meta.dept.api.feign;

import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.dto.component.model.dto.SelectDeptInfoParamDTO;
import cn.qianxun.meta.common.core.dto.component.model.vo.SelectAssetResultVO;
import cn.qianxun.meta.common.core.web.vo.RemoveLongDTO;
import cn.qianxun.meta.dept.api.dto.ApiAddDeptDTO;
import cn.qianxun.meta.dept.api.dto.ApiEditDeptDTO;
import cn.qianxun.meta.dept.api.dto.ApiSysDeptQueryDTO;
import cn.qianxun.meta.dept.api.dto.SelectDeptInfoByNameDTO;
import cn.qianxun.meta.dept.api.vo.ApiDeptDetailsVO;
import cn.qianxun.meta.dept.api.vo.ApiSysDeptListVO;
import cn.qianxun.meta.common.core.constant.FeignConstant;
import cn.qianxun.meta.common.core.constant.ProviderConstant.SystemProviderConstant;
import cn.qianxun.meta.common.core.domain.Result;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @Author fuzhilin
 * @Description
 */
@Hidden
@FeignClient(name = FeignConstant.META_CLOUD_SYSTEM)
public interface ISysDeptProvider {

    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_DEPT_selectDeptList)
    Result<RowsData<ApiSysDeptListVO>> selectDeptList(@RequestBody ApiSysDeptQueryDTO dto);

    /**
     * 添加单位
     *
     * @param dto
     * @return
     */
    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_DEPT_insertDept)
    Result<String> insertDept(@RequestBody @Validated ApiAddDeptDTO dto);

    /**
     * 删除单位
     *
     * @param dto
     * @return
     */
    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_DEPT_deleteDept)
    Result<String> deleteDept(@RequestBody RemoveLongDTO dto);

    /**
     * 单位详情
     *
     * @param id
     * @return
     */
    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_DEPT_deptDetails)
    Result<ApiDeptDetailsVO> deptDetails(@RequestParam("id") Long id);

    /**
     * 修改单位
     *
     * @param dto
     * @return
     */
    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_DEPT_updateDept)
    Result<String> updateDept(@RequestBody @Validated ApiEditDeptDTO dto);

    /**
     * 根据参数查询端口信息
     *
     * @param dto
     * @return
     */
    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_DEPT_selectDeptInfoByParam)
    Result<SelectAssetResultVO> selectDeptInfoByParam(@RequestBody SelectDeptInfoParamDTO dto);


    /**
     * 组织机构数量
     *
     * @return num
     */
    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_DEPT_selectDeptCount)
    Result<Long> selectDeptCount(@RequestParam(name = "staTime", defaultValue = "") String staTime, @RequestParam(name = "endTime", defaultValue = "") String endTime);

    /**
     * 根据单位名称查询单位id
     *
     * @return num
     */
    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_DEPT_selectDeptInfoByName)
    Result<Map<String, Long>> selectDeptInfoByName(@RequestBody SelectDeptInfoByNameDTO dto);
}
