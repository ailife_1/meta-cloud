package cn.qianxun.meta.dept.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * API-添加单位参数DTO
 */
@Data
public class ApiAddDeptDTO {

    /**
     * 父部门id
     */
    private Long parentId;

    /**
     * 社会统一信用代码
     */
    private String orgCode;

    /**
     * 单位名称
     */
    @NotBlank(message = "单位名称不能为空")
    private String deptName;

    /**
     * 单位简称
     */
    @NotBlank(message = "单位简称不能为空")
    private String shortName;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 机构性质
     */
    private String nature;

    /**
     * 区
     */
    private String areaName;

    /**
     * 市
     */
    private String cityName;

    /**
     * 省
     */
    private String provinceName;

    /**
     * 标准行业
     */
    private String standardIndustry;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 行政级别
     */
    private Integer regionLevel;

    /**
     * 详细地址
     */
    private String detailAddress;

    /**
     * 单位状态（0正常 1停用）
     */
    private String status;

    /**
     * 单位类型(dept_flag)
     */
    @NotBlank(message = "单位类型不能为空")
    private String deptFlag;

    /**
     * 登陆人id
     */
    @Schema(hidden = true)
    private Long loginUserId;
}