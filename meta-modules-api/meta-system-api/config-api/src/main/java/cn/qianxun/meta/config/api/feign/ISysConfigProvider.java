package cn.qianxun.meta.config.api.feign;

import cn.qianxun.meta.common.core.constant.FeignConstant;
import cn.qianxun.meta.common.core.constant.ProviderConstant.SystemProviderConstant;
import cn.qianxun.meta.common.core.domain.Result;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author fuzhilin
 * @Description
 */
@Hidden
@FeignClient( name = FeignConstant.META_CLOUD_SYSTEM)
public interface ISysConfigProvider {

    /**
     * 根据参数键名查询参数值
     *
     * @param configKey
     * @return
     */
    @GetMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_CONFIG_configKey)
    Result<String> getConfigKey(@RequestParam("configKey") String configKey);

}
