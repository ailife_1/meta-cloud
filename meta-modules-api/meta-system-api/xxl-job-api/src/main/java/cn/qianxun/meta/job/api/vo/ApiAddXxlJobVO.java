package cn.qianxun.meta.job.api.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author fuzhilin
 * @Date 2023/9/11 11:23
 * @Description Api-添加xxl-job响应参数VO
 */
@Accessors(chain = true)
@Data
public class ApiAddXxlJobVO {

    /**
     * 任务id
     */
    private String jobId;

    /**
     * 状态码
     */
    private Integer code;

    /**
     * msg
     */
    private String msg;
}
