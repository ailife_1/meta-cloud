package cn.qianxun.meta.job.api.feign;

import cn.qianxun.meta.common.core.constant.FeignConstant;
import cn.qianxun.meta.common.core.constant.ProviderConstant.SystemProviderConstant;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.job.api.dto.ApiAddXxlJobDTO;
import cn.qianxun.meta.job.api.dto.ApiEditXxlJobDTO;
import cn.qianxun.meta.job.api.dto.ApiJobTriggerDTO;
import cn.qianxun.meta.job.api.vo.ApiAddXxlJobVO;
import cn.qianxun.meta.job.api.vo.ApiXxlJobInfoVO;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @Author fuzhilin
 * @Date 2023/9/11 11:20
 * @Description
 */
@Hidden
@FeignClient(name = FeignConstant.META_CLOUD_SYSTEM)
public interface ISysXxlJobProvider {

    /**
     * 添加xxl_job任务
     *
     * @param dto dto
     * @return ApiAddXxlJobVO
     */
    @PostMapping(SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_XXL_JOB_ADD_XXL_JOB)
    ApiAddXxlJobVO addXxlJob(@RequestBody ApiAddXxlJobDTO dto);

    /**
     * 修改xxl_job任务
     *
     * @param dto dto
     * @return ApiEditXxlJobDTO
     */
    @PostMapping(SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_XXL_JOB_EDIT_XXL_JOB)
    ApiAddXxlJobVO editXxlJob(@RequestBody ApiEditXxlJobDTO dto);

    /**
     * 任务执行一次
     *
     * @param dto dto
     */
    @PostMapping(SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_XXL_JOB_TRIGGER_JOB)
    void triggerJob(@RequestBody ApiJobTriggerDTO dto);

    /**
     * 任务停止
     *
     * @param jobId 任务id
     */
    @GetMapping(SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_XXL_JOB_STOP)
    void stop(@RequestParam("jobId") String jobId);


    /**
     * 任务启动
     *
     * @param jobId 任务id
     */
    @GetMapping(SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_XXL_JOB_START)
    void start(@RequestParam("jobId") String jobId);

    /**
     * 任务删除
     *
     * @param jobId 任务id
     */
    @GetMapping(SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_XXL_JOB_DEL_JOB)
    void remove(@RequestParam("jobId") String jobId);

    /**
     * 根据任务id集合h获取执行时间
     * @param ids
     * @return
     */
    @GetMapping(SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_XXL_JOB_getJobTriggerTime)
    Result<Map<String, ApiXxlJobInfoVO>> getJobTriggerTime(@RequestParam("list") List<String> ids);
}
