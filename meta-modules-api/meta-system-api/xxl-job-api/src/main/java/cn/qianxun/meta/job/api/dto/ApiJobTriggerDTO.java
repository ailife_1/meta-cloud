package cn.qianxun.meta.job.api.dto;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 16:57
 * @Description Api-任务执行一次请求参数DTO对象
 */
@Data
public class ApiJobTriggerDTO {

    /**
     * 任务id
     */
    private int id;

    /**
     * 执行参数
     */

    private String executorParam;

    /**
     * 执行器地址
     */
    private String addressList;

}
