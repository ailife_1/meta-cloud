package cn.qianxun.meta.log.api.dto;

import cn.easyes.annotation.IndexField;
import cn.easyes.annotation.IndexId;
import cn.easyes.annotation.IndexName;
import cn.easyes.annotation.rely.FieldType;
import cn.easyes.annotation.rely.IdType;
import cn.qianxun.meta.common.excel.annotation.ExcelDictFormat;
import cn.qianxun.meta.common.excel.convert.ExcelDictConvert;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统访问记录表 sys_logininfor
 *
 * @author Lion Li
 */

@Data
@NoArgsConstructor
@ExcelIgnoreUnannotated
@IndexName(value = "sys_login_log",maxResultWindow = Integer.MAX_VALUE,replicasNum = 0)
public class SysLoginLog implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "序号")
    private Long infoId;

    /**
     * ID
     */
    @IndexId(type=IdType.NONE)
    private String id;

    /**
     * 用户账号
     */
    @ExcelProperty(value = "用户账号")
    @IndexField(value = "user_name",fieldType = FieldType.KEYWORD_TEXT)
    private String userName;

    /**
     * 状态 0成功 1失败
     */
    @ExcelProperty(value = "状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_common_status")
    private String status;

    /**
     * 地址
     */
    @ExcelProperty(value = "地址")
    @IndexField(value = "ip_addr",fieldType = FieldType.KEYWORD_TEXT)
    private String ipaddr;

    /**
     * 登录地点
     */
    @ExcelProperty(value = "登录地点")
    @IndexField(value = "login_location",fieldType = FieldType.KEYWORD_TEXT)
    private String loginLocation;

    /**
     * 浏览器类型
     */
    @ExcelProperty(value = "浏览器类型")
    @IndexField(value = "browser",fieldType = FieldType.KEYWORD_TEXT)
    private String browser;

    /**
     * 操作系统
     */
    @ExcelProperty(value = "操作系统")
    @IndexField(value = "os",fieldType = FieldType.KEYWORD_TEXT)
    private String os;

    /**
     * 描述
     */
    @ExcelProperty(value = "描述")
    @IndexField(value = "msg",fieldType = FieldType.KEYWORD_TEXT)
    private String msg;

    /**
     * 访问时间
     */
    @ExcelProperty(value = "访问时间")
    private Date loginTime;

}
