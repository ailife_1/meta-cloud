package cn.qianxun.meta.log.api.feign;

import cn.qianxun.meta.common.core.dto.RowsData;
import cn.qianxun.meta.common.core.constant.FeignConstant;
import cn.qianxun.meta.common.core.constant.ProviderConstant.SystemProviderConstant;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.log.api.dto.*;
import cn.qianxun.meta.log.api.dto.*;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Author fuzhilin
 * @Description
 */
@Hidden
@FeignClient(name = FeignConstant.META_CLOUD_SYSTEM)
public interface ISysLogProvider {

    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_LOG_saveLog)
    void saveLog(@RequestBody SysOperLog sysOperLog);

    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_LOG_saveLogininfor)
    void saveLoginLog(@RequestBody SysLoginLog sysLogininfor);

    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_LOG_SAVE_CHANGE_FIELD_LOG)
    void saveChangeFieldLog(@RequestBody List<ChangeFieldValueLog> fieldValueList);

    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_LOG_SELECT_CHANGE_FIELD_LOG_BY_ASSET_ID)
    Result<RowsData<ChangeFieldValueLog>> selectChangeFieldLogByAssetId(@RequestBody ApiSelectPageDTO dto);

    @PostMapping(SystemProviderConstant.PROVIDER_AUTH_V1_SYS_LOG_SAVE_SAFE_VALUE_LOG)
    void saveSafeValueLog(@RequestBody List<SaveSafeValueLog> safeValueLogList);
}
