package cn.qianxun.meta.log.api.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 根据资产id查询变更情况参数
 */
@Data
public class ApiSelectPageDTO {

    @Schema(description = "搜索字段")
    private String assetId;

    @Schema(description = "页码")
    private Integer pageNum;

    @Schema(description = "页面显示长度")
    private Integer pageSize;
}