package cn.qianxun.meta.log.api.dto;

import cn.easyes.annotation.IndexId;
import cn.easyes.annotation.IndexName;
import cn.easyes.annotation.rely.IdType;
import lombok.Data;

import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2023/9/5 10:11
 * @Description 字段值改变值响应参数VO
 */
@Data
@IndexName(value = "change_field_value_log",maxResultWindow = Integer.MAX_VALUE,replicasNum = 0)
public class ChangeFieldValueLog {

    /**
     * id
     */
    @IndexId(type = IdType.NONE)
    private String id;

    /**
     * 对象id
     */
    private String objId;

    /**
     * 所属模块
     */
    private String module;

    /**
     * 字段名称
     */
    private String filedName;

    /**
     * 旧值
     */
    private Object oldValue;

    /**
     * 新值
     */
    private Object newValue;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 操作时间
     */
    private Date operationTime;
}
