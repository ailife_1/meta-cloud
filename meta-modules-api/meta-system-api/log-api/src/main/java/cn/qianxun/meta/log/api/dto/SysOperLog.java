package cn.qianxun.meta.log.api.dto;

import cn.easyes.annotation.IndexField;
import cn.easyes.annotation.IndexId;
import cn.easyes.annotation.IndexName;
import cn.easyes.annotation.rely.FieldType;
import cn.easyes.annotation.rely.IdType;
import cn.qianxun.meta.common.excel.annotation.ExcelDictFormat;
import cn.qianxun.meta.common.excel.convert.ExcelDictConvert;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 操作日志记录表 oper_log
 *
 * @author Lion Li
 */

@Data
@NoArgsConstructor
@ExcelIgnoreUnannotated
@IndexName(value = "sys_oper_log",maxResultWindow = Integer.MAX_VALUE,replicasNum = 0)
public class SysOperLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @IndexId(type = IdType.NONE)
    private String id;

    /**
     * 日志主键
     */
    @ExcelProperty(value = "日志主键")
    @IndexField( "oper_id")
    private Long operId;

    /**
     * 操作模块
     */
    @ExcelProperty(value = "操作模块")
    @IndexField(value = "title",fieldType = FieldType.KEYWORD_TEXT)
    private String title;

    /**
     * 业务类型（0其它 1新增 2修改 3删除）
     */
    @ExcelProperty(value = "业务类型", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_oper_type")
    private Integer businessType;

    /**
     * 业务类型数组
     */
    @TableField(exist = false)
    private Integer[] businessTypes;

    /**
     * 请求方法
     */
    @ExcelProperty(value = "请求方法")
    @IndexField(value = "method",fieldType = FieldType.KEYWORD_TEXT)
    private String method;

    /**
     * 请求方式
     */
    @ExcelProperty(value = "请求方式")
    private String requestMethod;

    /**
     * 操作类别（0其它 1后台用户 2手机端用户）
     */
    @ExcelProperty(value = "操作类别", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "0=其它,1=后台用户,2=手机端用户")
    private Integer operatorType;

    /**
     * 操作人员
     */
    @ExcelProperty(value = "操作人员")
    @IndexField(value = "oper_name",fieldType = FieldType.KEYWORD_TEXT)
    private String operName;

    /**
     * 部门名称
     */
    @ExcelProperty(value = "部门名称")
    @IndexField(value = "dept_name",fieldType = FieldType.KEYWORD_TEXT)
    private String deptName;

    /**
     * 请求url
     */
    @ExcelProperty(value = "请求地址")
    @IndexField(value = "oper_url",fieldType = FieldType.KEYWORD_TEXT)
    private String operUrl;

    /**
     * 操作地址
     */
    @ExcelProperty(value = "操作地址")
    @IndexField(value = "oper_ip",fieldType = FieldType.KEYWORD_TEXT)
    private String operIp;

    /**
     * 操作地点
     */
    @ExcelProperty(value = "操作地点")
    @IndexField(value = "oper_location",fieldType = FieldType.KEYWORD_TEXT)
    private String operLocation;

    /**
     * 请求参数
     */
    @ExcelProperty(value = "请求参数")
    @IndexField(value = "oper_param",fieldType = FieldType.KEYWORD_TEXT)
    private String operParam;

    /**
     * 返回参数
     */
    @ExcelProperty(value = "返回参数")
    @IndexField(value = "json_result",fieldType = FieldType.KEYWORD_TEXT)
    private String jsonResult;

    /**
     * 操作状态（0正常 1异常）
     */
    @ExcelProperty(value = "状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_common_status")
    private Integer status;

    /**
     * 错误消息
     */
    @ExcelProperty(value = "错误消息")
    @IndexField(value = "error_msg",fieldType = FieldType.KEYWORD_TEXT)
    private String errorMsg;

    /**
     * 操作时间
     */
    @ExcelProperty(value = "操作时间")
    private Date operTime;


}
