package cn.qianxun.meta.log.api.dto;

import cn.easyes.annotation.IndexId;
import cn.easyes.annotation.IndexName;
import cn.easyes.annotation.rely.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author fuzhilin
 * @Date 2023/9/8 9:12
 * @Description
 */
@Data
@IndexName(value = "safe_value_log",maxResultWindow = Integer.MAX_VALUE,replicasNum = 0)
public class SaveSafeValueLog implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    @IndexId(type = IdType.NONE)
    private String id;


    /**
     * 安全值分数
     */
    private String score;

    /**
     * 资产名称
     */
    private String assetName;

    /**
     * 业务类型 0总安全值 1网站 2IP 3端口 4信息系统
     */
    private String businessType;

    /**
     * 业务id
     */
    private String businessId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 老安全值
     */
    private String oldScore;
}
