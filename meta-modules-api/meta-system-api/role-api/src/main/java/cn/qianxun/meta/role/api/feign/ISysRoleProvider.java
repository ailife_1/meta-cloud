package cn.qianxun.meta.role.api.feign;

import cn.qianxun.meta.common.core.constant.FeignConstant;
import cn.qianxun.meta.common.core.constant.ProviderConstant.SystemProviderConstant;
import cn.qianxun.meta.common.core.domain.Result;
import cn.qianxun.meta.role.api.vo.ApiRoleDetailsVO;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author fuzhilin
 * @Date 2023/10/10 18:25
 * @Description
 */
@Hidden
@FeignClient(name = FeignConstant.META_CLOUD_SYSTEM)
public interface ISysRoleProvider {

    /**
     * 单位详情
     *
     * @param id
     * @return
     */
    @GetMapping(SystemProviderConstant.PROVIDER_SYSTEM_V1_SYS_ROLE_roleDetails)
    Result<ApiRoleDetailsVO> deptDetails(@RequestParam("id") Long id);
}
