package cn.qianxun.meta.auth;

import cn.qianxun.meta.feign.feign.annotation.EnableMetaFeign;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //认证中心启动类
 * @Date 2023/8/9 10:10
 **/
@EnableMetaFeign
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class, org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration.class})
public class SmartMetaAuthApplication {

    public static void main(String[] args) {
        SpringApplication metaSystemApplication = new SpringApplication(SmartMetaAuthApplication.class);
        metaSystemApplication.setAllowBeanDefinitionOverriding(true);
        metaSystemApplication.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  认证中心启动成功   ლ(´ڡ`ლ)ﾞ  \n");
    }
}
