package cn.qianxun.meta.auth.thirdlogin.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author fuzhilin
 * @Date 2024/1/23 0011 9:23
 */
@Data
public class ThirdLoginVO {

    /**
     * 账号
     */
    @NotBlank(message = "账号不能为空")
    private String account;

    /**
     * 时间戳
     */
    @NotBlank(message = "时间戳不能为空")
    private String timestamp;

    /**
     * token
     */
    @NotBlank(message = "token不能为空")
    private String token;
}
