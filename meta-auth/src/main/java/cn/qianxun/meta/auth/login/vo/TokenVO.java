package cn.qianxun.meta.auth.login.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/23 18:14
 **/
@Schema(description = "token信息")
@Data
public class TokenVO {

    @Schema(description = "token")
    private String token;

    @Schema(description = "是否强制修改密码")
    private Boolean editPassWord = false;
}
