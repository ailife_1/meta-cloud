package cn.qianxun.meta.auth.thirdlogin.util;

import lombok.Getter;

/**
 * @Author fuzhilin
 * @Date 2022/7/11 0011 10:11
 * @Description 三方登录code码
 */
@Getter
public enum ThirdLoginEnum {
    /**
     * CODE
     */
    CODE("e94845f6b1e9481c9f993ca1fb4861ea", "code"),

    /**
     * ACCESS_KEY
     */
    ACCESS_KEY("a882b3642a36493c90819f514aed1c2d", "accessKey"),
    ;


    private String code;
    private String name;

    ThirdLoginEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }
}
