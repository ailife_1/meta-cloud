package cn.qianxun.meta.auth.login.controller;


import cn.qianxun.meta.auth.login.dto.ApplicationLoginDTO;
import cn.qianxun.meta.auth.login.service.ISysLoginService;
import cn.qianxun.meta.auth.login.vo.ApiTokenVO;
import cn.qianxun.meta.common.core.domain.Result;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 0013 14:10
 * @Description
 */
@Tag(name = "应用登录管理")
@RestController
@RequestMapping("/web/v1/appAuth")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SysApplicationLoginController {
    private final ISysLoginService sysLoginService;

    /**
     * 应用登录接口
     */
    @PostMapping("/login")
    public Result<ApiTokenVO> operateLogin(@RequestBody @Validated ApplicationLoginDTO dto) {
        return Result.success("登录成功", sysLoginService.applicationLogin(dto));
    }

    /**
     * 通过刷新token，重新生成应用token
     */
    @GetMapping("/createToken")
    public Result<ApiTokenVO> createApplicationTokenByRefreshToken(@RequestParam String refreshToken) {
        return Result.success(sysLoginService.getApplicationToken(refreshToken));
    }

}
