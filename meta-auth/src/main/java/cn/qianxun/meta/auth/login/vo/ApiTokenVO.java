package cn.qianxun.meta.auth.login.vo;

import lombok.Data;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 0013 14:12
 * @Description 返回结果token
 */
@Data
public class ApiTokenVO {
    /**
     * 访问token
     */
    private String accessToken;

    /**
     * 刷新token
     */
    private String refreshToken;

    /**
     * 访问token过期时间
     */
    private Long expireTime;
}
