package cn.qianxun.meta.auth.login.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author fuzhilin
 * @Date 2023/9/12 0017 11:33
 * @Description 第三方应用登录VO
 */
@Data
public class ApplicationLoginDTO {

    /**
     * AppId
     */
    @NotBlank(message = "appId不能为空")
    private String appId;

    /**
     * AppSecret
     */
    @NotBlank(message = "appSecret不能为空")
    private String appSecret;

}
