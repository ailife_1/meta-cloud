package cn.qianxun.meta.auth.login.controller;

import cn.qianxun.meta.auth.thirdlogin.dto.ThirdLoginVO;
import cn.qianxun.meta.auth.login.dto.LoginInfoDTO;
import cn.qianxun.meta.auth.login.service.ISysLoginService;
import cn.qianxun.meta.auth.login.vo.TokenVO;
import cn.qianxun.meta.common.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/18 16:46
 **/
@RestController
@Tag(description = "登录管理", name = "登录管理")
@RequestMapping("/web/v1/auth")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Validated
public class SysLoginController {

    private final ISysLoginService sysLoginService;

    @Operation(description = "登录")
    @PostMapping("/login")
    public Result<TokenVO> login(@RequestBody LoginInfoDTO dto) {
        TokenVO token = sysLoginService.login(dto);
        return Result.success("登录成功", token);
    }

    @Operation(description = "第三方登录")
    @PostMapping("/thirdLogin")
    public Result<TokenVO> thirdLogin(@RequestBody @Validated ThirdLoginVO vo) {
        return Result.success("登录成功", sysLoginService.thirdLogin(vo));
    }

}
