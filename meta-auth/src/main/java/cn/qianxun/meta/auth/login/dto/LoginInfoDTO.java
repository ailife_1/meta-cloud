package cn.qianxun.meta.auth.login.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/21 16:57
 **/

@Data
@Schema(description = "登录参数dto")
public class LoginInfoDTO {

    @NotBlank(message = "账号不能为空")
    @Schema(description = "账号")
    private String username;

    @NotBlank(message = "密码不能为空")
    @Schema(description = "密码")
    private String password;

//    @NotBlank(message = "唯一标识不能为空")
//    @Schema(description = "唯一标识")
//    private String uuid;

    private String pointJson;

    private String token;

    private String captchaVerification;

    private String captchaType;

}
