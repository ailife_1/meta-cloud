package cn.qianxun.meta.auth.login.service;

import cn.qianxun.meta.auth.login.dto.ApplicationLoginDTO;
import cn.qianxun.meta.auth.login.dto.LoginInfoDTO;
import cn.qianxun.meta.auth.thirdlogin.dto.ThirdLoginVO;
import cn.qianxun.meta.auth.login.vo.ApiTokenVO;
import cn.qianxun.meta.auth.login.vo.TokenVO;

/**
 * Copyright:版权所有(c)qianxun
 *
 * @Author fuzhilin
 * @Description //TODO
 * @Date 2023/8/23 18:12
 **/
public interface ISysLoginService {

    /**
     * 账号密码登录
     *
     * @param dto dto
     * @return TokenVO
     */
    TokenVO login(LoginInfoDTO dto);

    /**
     * 应用登录接口
     *
     * @param dto dto
     * @return ApiTokenVO
     */
    ApiTokenVO applicationLogin(ApplicationLoginDTO dto);

    /**
     * 通过刷新token，重新生成应用token
     * @param refreshToken 刷新token
     * @return ApiTokenVO
     */
    ApiTokenVO getApplicationToken(String refreshToken);

    /**
     * 濮阳第三方登录
     * @param vo vo
     * @return TokenVO
     */
    TokenVO thirdLogin(ThirdLoginVO vo);
}
