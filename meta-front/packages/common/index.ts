import Chart from './package/chart/index.vue'
import Vue3Cron from './package/vue3Cron/index.vue'
import * as echarts from 'echarts'
import { Vue3SeamlessScroll } from "vue3-seamless-scroll"


Chart.install = function(Vue) {
  Vue.component(Chart.name, Chart);
};
Vue3Cron.install = function (Vue) { 
 Vue.component(Vue3Cron.name, Vue3Cron);
}

export {
  echarts,
  Chart,
  Vue3SeamlessScroll,
  Vue3Cron
}