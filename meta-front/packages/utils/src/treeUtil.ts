// 生成树形结构
export function handleTree(data: any, id: string | number, parentId: string | number) {
	id = id || 'id'
	parentId = parentId || 'parentId'
	// children = children || 'children'
	//对源数据深度克隆
	const cloneData = JSON.parse(JSON.stringify(data))
	//循环所有项
	const treeData = cloneData.filter((father: any) => {
		const branchArr = cloneData.filter((child: any) => {
			//返回每一项的子级数组
			return father[id] === child[parentId]
		})
		const branchArr1 = cloneData.filter((child: any) => {
			//返回每一项的子级数组
			return father[parentId] === child[id]
		})

		branchArr.length > 0 ? (father.children = branchArr) : ''
		//返回第一层
		return branchArr1.length == 0
	})
	return treeData != '' ? treeData : data
}
