import request from '../utils/request'

export function useBpmnApi() {
	return {
		getUserList: (params?: object) => {
			return request({
				url: '/meta-system/web/v1/sysUser/list',
				method: 'post',
				data: params,
			})
		},
		getDeptList: (params?: object) => {
			return request({
				url: '/meta-system/web/v1/sysDept/list',
				method: 'post',
				data: params,
			})
		},
		getRolesList: (params?: object) => {
			return request({
				url: '/meta-system/web/v1/sysRole/getProcessRoleList',
				method: 'post',
				data:params,
			})
		},
		getFormList: (params?: object) => {
			return request({
				url: '/sop-joint-disposal/web/v1/form/formList',
				method: 'post',
				params,
			})
		},
	}
}
