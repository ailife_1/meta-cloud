
import ProcessDesigner from './package/ProcessDesigner.vue';
import ProcessViewer from './package/ProcessViewer.vue'

ProcessDesigner.install = function(Vue) {
  Vue.component(ProcessDesigner.name, ProcessDesigner);
};
ProcessViewer.install = function (Vue) { 
 Vue.component(ProcessViewer.name, ProcessViewer);
}
export { 
  ProcessDesigner,
  ProcessViewer
};