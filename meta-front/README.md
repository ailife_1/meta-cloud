# 通用-基础平台

## 项目依赖安装与启动

-   [安装 node 版本要求 16.14.0+](https://nodejs.org/download/release/v16.14.2/)

-   [安装 nvm](https://nodejs.org/download/release/v16.14.2/)

```
// 安装Pnpm
npm install -g pnpm

//安装依赖
pnpm install

//项目启动
pnpm run meta:dev

// 项目打包
pnpm run meta:build

默认账号：密码 admin/Admin@123

```

## 组件

-   1、大数据表格 vxe-table 介绍

*   [介绍](http://172.16.16.116/)
*   [官网](https://vxetable.cn)

![](./assets/vxe-table.jpg)

## 基于字典数据的下拉选择（支持单选&多选）

```
// 组件已全局安装可直接使用

// dictKey:字典名称
<dict-select v-model="value" :dictKey="'sys_oper_type'" placeholder="请选择通知状态" />


```

## hooks 介绍

-   useTableList 表格 hooks 支持分页

-   useList 列表 hooks

-   useModal 弹窗 hooks

## .env 文件

Vite 使用 dotenv 从你的 环境目录 中的下列文件加载额外的环境变量：

```
.env # 所有情况下都会加载
.env.local # 所有情况下都会加载，但会被 git 忽略
.env.[mode] # 只在指定模式下加载
.env.[mode].local # 只在指定模式下加载，但会被 git 忽略

```

环境加载优先级

-   一份用于指定模式的文件（例如 .env.production）会比通用形式的优先级更高（例如 .env）。

-   .env 类文件会在 Vite 启动一开始时被加载，而改动会在重启服务器后生效。

-   为了防止意外地将一些环境变量泄漏到客户端，只有以 VITE\_ 为前缀的变量才会暴露给经过 vite 处理的代码

-   .env.\*.local 文件应是本地的，可以包含敏感变量。你应该将 .local 添加到你的 .gitignore 中，以避免它们被 git 检入

-   由于任何暴露给 Vite 源码的变量最终都将出现在客户端包中，VITE\_\* 变量应该不包含任何敏感信息

## 模式

-   默认情况下，开发服务器 (dev 命令) 运行在 development (开发) 模式，而 build 命令则运行在 production (生产) 模式。

-   这意味着当执行 vite build 时，它会自动加载 .env.production 中可能存在的环境变量：

*   可以通过传递`--mode`选项标志来覆盖命令使用的默认模式，例如需要创建一个 test 模式构建应用

```
vite build --mode test

```

此时还需要创建.env.test 文件

## 