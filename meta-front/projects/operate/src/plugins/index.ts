import type { App } from 'vue'
import { initVxeTable } from '@/plugins/vxeTable/vxe-table'
import JsonViewer from 'vue3-json-viewer'
import 'vue3-json-viewer/dist/index.css'

import VForm3 from 'vform3-builds'
import request from '@/api/request'
import 'vform3-builds/dist/designer.style.css'

import Tools from '@/utils/tools'
import { directive } from '@/directive/index'
import StaticTable from '@/components/staticTable/index.vue'
import DictSelect from '@/components/DictSelect.vue'
import SelectMultiple from '@/components/SelectMultiple.vue'
import SelectSimple from '@/components/SelectSimple.vue'

/**
 *  初始化框架
 * @param app
 */
export function initApp(app: App) {
	initVxeTable(app)
	Tools.createSvg(app)
	directive(app)
	app.use(JsonViewer)
	app.use(VForm3)
	app.component('static-table', StaticTable)
	app.component('dict-select', DictSelect)
	app.component('select-multiple', SelectMultiple)
	app.component('select-simple', SelectSimple)
	window.request = request
}
