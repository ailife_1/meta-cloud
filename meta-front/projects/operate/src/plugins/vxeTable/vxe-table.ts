import type { App } from 'vue'
import { Table, Validator, Column, Tooltip, Icon, Edit, Input, Select } from 'vxe-table'
import '@/plugins/vxeTable/vxe-table.scss'
export function initVxeTable(app: App) {
	app.use(Table).use(Column).use(Tooltip).use(Icon).use(Edit).use(Input).use(Select).use(Validator)
}
