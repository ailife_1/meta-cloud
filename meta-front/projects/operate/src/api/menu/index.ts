import request from '@/api/request'

/**
 * 框架初始化接口
 * 后端控制路由，isRequestRoutes 为 true，则开启后端控制路由
 * 1：获取菜单接口
 * 2：获取个人信息接口
 * 3：全局字典接口
 */
export function useMenuApi() {
	return {
		getMenuList: (params?: object) => {
			return request({
				url: '/meta-system/web/v1/sysUser/getRouters',
				method: 'get',
				params,
			})
		},
		getUserInfo: (params?: object) => {
			return request({
				url: '/meta-system/web/v1/sysUser/getInfo',
				method: 'get',
				params,
			})
		},
		getDictList: (params?: object) => {
			return request({
				url: '/meta-system/web/v1/sysDictData/dictList',
				method: 'post',
				params,
			})
		},
	}
}
