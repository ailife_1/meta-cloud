import { nextTick } from 'vue'
import '@/theme/common/loading.scss'
import { useThemeConfig } from '@/stores/themeConfig'

/**
 * 页面全局 Loading
 * @method start 创建 loading
 * @method done 移除 loading
 */
export const NextLoading = {
	// 创建 loading
	start: () => {
		const storesThemeConfig = useThemeConfig()
		const { themeConfig } = storeToRefs(storesThemeConfig)
		const bodys: Element = document.body
		const div = <HTMLElement>document.createElement('div')
		div.setAttribute('class', 'loading-next')
		const htmls = `
			<div class="loading-next-box">
				<div class="ball-scale-multiple">
					<div></div>
					<div></div>
					<div></div>
				</div>
				<h1>${themeConfig.value.globalTitle}</h1>
			</div>


		`
		div.innerHTML = htmls
		bodys.insertBefore(div, bodys.childNodes[0])
		window.nextLoading = true
	},
	// 移除 loading
	done: (time = 0) => {
		nextTick(() => {
			setTimeout(() => {
				window.nextLoading = false
				const el = <HTMLElement>document.querySelector('.loading-next')
				el?.parentNode?.removeChild(el)
			}, time)
		})
	},
}
