/* eslint-disable no-unused-vars */
// 通用函数
import useClipboard from 'vue-clipboard3'
import { ElMessage } from 'element-plus'

const { toClipboard } = useClipboard()

// 百分比格式化
export function percentFormat(row: EmptyArrayType, column: number, cellValue: string) {
	return cellValue ? `${cellValue}%` : '-'
}
// 小数格式化
export function scaleFormat(value = '0', scale = 4) {
	return Number.parseFloat(value).toFixed(scale)
}
// 小数格式化
export function scale2Format(value = '0') {
	return Number.parseFloat(value).toFixed(2)
}
// 点击复制文本
export function copyText(text: string) {
	return new Promise((resolve, reject) => {
		try {
			// 复制
			toClipboard(text)
			// 下面可以设置复制成功的提示框等操作
			ElMessage.success('复制成功!')
			resolve(text)
		} catch (e) {
			// 复制失败
			ElMessage.error('复制失败!')
			reject(e)
		}
	})
}
