import { ElMessage, ElMessageBox, ElLoading, ElNotification } from 'element-plus'
import { VNode } from 'vue'
import request from '@/api/request'

declare interface globalType {
	$fetchPostData: any
	$fetchGetData: any
	$fetchUploadFile: any
	$fetchDownFile: any
	$fetchGetDownFile: any
	$baseLoading(index?: string | undefined, text?: string): any
	$baseMessage(message: string | VNode, type?: 'success' | 'warning' | 'info' | 'error', customClass?: string, dangerouslyUseHTMLString?: boolean): any
	$baseAlert(content: string | VNode, title: string, callback: (() => unknown) | undefined): any
	$baseConfirm(content: string | VNode, title: string, callback1: any, callback2: any, confirmButtonText: string, cancelButtonText: string): any
	$baseNotify(message: string, title: string, type?: 'success' | 'warning' | 'info' | 'error', position?: 'top-right' | 'top-left' | 'bottom-right' | 'bottom-left', duration?: number): any
}

const global: globalType = {
	/**
	 * @description Post请求
	 * @param url
	 * @param params
	 * @returns
	 */
	$fetchPostData: (url: any, params: any) => {
		return new Promise(resolve => {
			request.post(url, params).then(res => {
				return resolve(res)
			})
		})
	},
	/**
	 * @description Get请求
	 * @param url
	 * @param params
	 * @returns
	 */
	$fetchGetData: (url: string, params: any) => {
		return new Promise(resolve => {
			request.get(url, { params: params }).then(res => {
				return resolve(res)
			})
		})
	},
	/**
	 * @description 上传请求
	 * @param url
	 * @param formData
	 * @returns
	 */
	$fetchUploadFile: (url: string, formData: any) => {
		return new Promise(resolve => {
			request
				.post(url, formData, {
					headers: {
						'Content-Type': 'multipart/form-data',
					},
				})
				.then(res => {
					return resolve(res)
				})
		})
	},
	$fetchDownFile: (url: string, formData: any) => {
		return new Promise(resolve => {
			request
				.post(url, formData, {
					responseType: 'blob',
					headers: {
						'Content-Type': 'application/octet-stream',
					},
				})
				.then(res => {
					return resolve(res)
				})
		})
	},
	$fetchGetDownFile: (url: string) => {
		return new Promise(resolve => {
			request
				.get(url, {
					responseType: 'blob',
					headers: {
						'Content-Type': 'application/octet-stream',
					},
				})
				.then(res => {
					return resolve(res)
				})
		})
	},
	/**
	 * @description 全局加载层
	 * @param {number} index 自定义加载图标类名ID
	 * @param {string} text 显示在加载图标下方的加载文案
	 */
	$baseLoading: (text = '') => {
		return ElLoading.service({
			lock: true,
			text,
			background: 'hsla(0,0%,100%,.8)',
		})
	},
	/**
	 * @description 全局Message
	 * @param {string} message 消息文字
	 * @param {'success'|'warning'|'info'|'error'} type 主题
	 * @param {string} customClass 自定义类名
	 * @param {boolean} dangerouslyUseHTMLString 是否将message属性作为HTML片段处理
	 */
	$baseMessage: (message, type = 'info', customClass, dangerouslyUseHTMLString) => {
		ElMessage({
			message,
			type,
			customClass,
			duration: 3000,
			offset: 90,
			dangerouslyUseHTMLString,
			showClose: true,
		})
	},
	/**
	 * @description 全局Alert
	 * @param {string|VNode} content 消息正文内容
	 * @param {string} title 标题
	 * @param {function} callback 若不使用Promise,可以使用此参数指定MessageBox关闭后的回调
	 */
	$baseAlert: (content, title = '温馨提示', callback = undefined) => {
		if (title && typeof title == 'function') {
			callback = title
			title = '温馨提示'
		}
		ElMessageBox.alert(content, title, {
			confirmButtonText: '确定',
			dangerouslyUseHTMLString: true,
			callback: () => {
				if (callback) callback()
			},
		}).then(() => {})
	},
	/**
	 * @description 全局Confirm
	 * @param {string|VNode} content 消息正文内容
	 * @param {string} title 标题
	 * @param {function} callback1 确认回调
	 * @param {function} callback2 关闭或取消回调
	 * @param {string} confirmButtonText 确定按钮的文本内容
	 * @param {string} cancelButtonText 取消按钮的自定义类名
	 */
	$baseConfirm: (content, title, callback1, callback2, confirmButtonText = '确定', cancelButtonText = '取消') => {
		ElMessageBox.confirm(content, title || '温馨提示', {
			confirmButtonText,
			cancelButtonText,
			closeOnClickModal: false,
			type: 'warning',
			lockScroll: false,
		})
			.then(() => {
				if (callback1) {
					callback1()
				}
			})
			.catch(() => {
				if (callback2) {
					callback2()
				}
			})
	},
	/**
	 * @description 全局Notification
	 * @param {string} message 说明文字
	 * @param {string} title 标题
	 * @param {'success'|'warning'|'info'|'error'} type 主题样式,如果不在可选值内将被忽略
	 * @param {'top-right'|'top-left'|'bottom-right'|'bottom-left'} position 自定义弹出位置
	 * @param duration 显示时间,毫秒
	 */
	$baseNotify: (message, title, type = 'success', position = 'top-right', duration = 3000) => {
		ElNotification({
			title,
			message,
			type,
			duration,
			position,
		})
	},
}
export default global
