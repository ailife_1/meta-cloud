const tokenTableName = 'meta-page-token'
/**
 * window.localStorage 浏览器永久缓存
 * @method set 设置永久缓存
 * @method get 获取永久缓存
 * @method remove 移除永久缓存
 * @method clear 移除全部永久缓存
 */
export const Local = {
	setKey(key: string) {
		return `${key}`
	},
	// 设置永久缓存
	set(key: string, val: string) {
		window.localStorage.setItem(key, val)
	},
	// 获取永久缓存
	get(key: string) {
		return window.localStorage.getItem(key)
	},
	// 移除永久缓存
	remove(key: string) {
		window.localStorage.removeItem(key)
	},
	// 移除全部永久缓存
	clear() {
		window.localStorage.clear()
	},
}

/**
 * window.sessionStorage 浏览器临时缓存
 * @method set 设置临时缓存
 * @method get 获取临时缓存
 * @method remove 移除临时缓存
 * @method clear 移除全部临时缓存
 */
export const Session = {
	// 设置临时缓存
	set<T>(key: string, val: T) {
		window.sessionStorage.setItem(Local.setKey(key), JSON.stringify(val))
	},
	// 获取临时缓存
	get(key: string) {
		const json = <string>window.sessionStorage.getItem(Local.setKey(key))
		return JSON.parse(json)
	},
	// 移除临时缓存
	remove(key: string) {
		window.sessionStorage.removeItem(Local.setKey(key))
	},
	// 移除全部临时缓存
	clear() {
		window.sessionStorage.clear()
	},
}
/**
 * 获取Token
 * @returns
 */
export function getToken() {
	return Local.get(tokenTableName)
}
/**
 * 设置Token
 * @param token
 * @returns
 */
export function setToken(token: string) {
	return Local.set(tokenTableName, token)
}
/**
 * 清除token
 * @returns
 */
export function removeToken() {
	return Local.remove(tokenTableName)
}
