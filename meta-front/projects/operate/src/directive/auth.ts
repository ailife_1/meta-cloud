import type { App } from 'vue'
import { useUserInfo } from '@/stores/userInfo'
import { array } from '@meta/utils'

/**
 * 用户权限指令
 * @directive 单个权限验证（v-has="xxx"）
 * @directive 多个权限验证，满足一个则显示（v-has-only="[xxx,xxx]"）
 * @directive 多个权限验证，全部满足则显示（v-has-all="[xxx,xxx]"）
 */
export function authDirective(app: App) {
	// 单个权限验证（v-has="xxx"）
	app.directive('has', {
		mounted(el, binding) {
			const stores = useUserInfo()
			if (!stores.userInfos.authBtnList.some((v: string) => v === binding.value || v == '*:*:*')) el.parentNode.removeChild(el)
		},
	})
	// 多个权限验证，满足一个则显示（v-has-only="[xxx,xxx]"）
	app.directive('has-only', {
		mounted(el, binding) {
			let flag = false
			const stores = useUserInfo()
			stores.userInfos.authBtnList.map((val: string) => {
				binding.value.map((v: string) => {
					if (val === v || val == '*:*:*') flag = true
				})
			})
			if (!flag) el.parentNode.removeChild(el)
		},
	})
	// 多个权限验证，全部满足则显示（v-has-all="[xxx,xxx]"）
	app.directive('has-all', {
		mounted(el, binding) {
			const stores = useUserInfo()
			const flag = array.judementSameArr(binding.value, stores.userInfos.authBtnList)
			if (!flag) el.parentNode.removeChild(el)
		},
	})
}
