import type { App } from 'vue'
export function throttleDirective(app: App) {
	app.directive('throttle', {
		mounted(el, binding) {
			el.addEventListener('click', () => {
				if (!el.disabled) {
					el.disabled = true
					setTimeout(() => {
						el.disabled = false
					}, binding.value || 2500)
				}
			})
		},
	})
}
