import type { App } from 'vue'
import { authDirective } from '@/directive/auth'
// import { dragDirective } from '@/directive/drag'
import { throttleDirective } from '@/directive/throttle'

/**
 * 导出指令方法：v-xxx
 * @methods authDirective 用户权限指令，用法：v-auth

 * @methods dragDirective 自定义拖动指令，用法：v-drag
 */
export function directive(app: App) {
	// 用户权限指令
	authDirective(app)
	// 自定义拖动指令
	// dragDirective(app)
	// 防抖指令
	throttleDirective(app)
}
