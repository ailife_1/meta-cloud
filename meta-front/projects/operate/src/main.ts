import { createApp } from 'vue'
import pinia from '@/stores/index'
import App from '@/App.vue'
import router from '@/router'
import ElementPlus from 'element-plus'
import '@/theme/index.scss'
import { initApp } from './plugins'
// 初始化App
const app = createApp(App)
initApp(app)

app.use(pinia).use(router).use(ElementPlus).mount('#app')
