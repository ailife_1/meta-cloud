import global from '@/utils/global'
import { treeUtil } from '@meta/utils'

/**
 * 获取区域数据
 * @param {*} isInit 是否需要初始化请求
 * @param {*} lifeStatus true为onMounted  false 为onActivated
 * @returns
 */
export function useArea(isInit: boolean, lifeStatus: boolean) {
	const state = reactive({
		areaData: [],
	})
	if (isInit) {
		if (lifeStatus) {
			onMounted(() => {
				getAreaData()
			})
		} else {
			onActivated(() => {
				getAreaData()
			})
		}
	}

	const handleAreaChange = (e: any) => {
		if (e && state.areaData.length == 0) {
			getAreaData()
		}
	}
	const getAreaData = () => {
		global.$fetchGetData(`/meta-system/web/v1/sysArea/getAreaListCasCaderData`, {}).then((data: any) => {
			if (data.code == 200) {
				state.areaData = treeUtil.handleTree(data.data, 'value', 'parentId')
			} else {
				global.$baseMessage(data.msg, 'error')
			}
		})
	}
	return {
		...toRefs(state),
		handleAreaChange,
	}
}
/**
 * 获取运营商数据
 * @param isInit
 * @param lifeStatus
 * @returns
 */
export function useIspList(isInit: boolean, lifeStatus: boolean) {
	const state = reactive({
		list: [],
	})
	if (isInit) {
		if (lifeStatus) {
			onMounted(() => {
				getListData()
			})
		} else {
			onActivated(() => {
				getListData()
			})
		}
	}

	const handleIspChange = (e: any) => {
		if (e && state.list.length == 0) {
			getListData()
		}
	}
	const getListData = () => {
		global.$fetchGetData(`/sop-assets/web/v1/assetIp/getIspList`, {}).then((data: any) => {
			if (data.code == 200) {
				state.list = data.data || []
			} else {
				global.$baseMessage(data.msg, 'error')
			}
		})
	}
	return {
		...toRefs(state),
		handleIspChange,
	}
}
/**
 * 获取协议列表
 * @param isInit
 * @param lifeStatus
 * @returns
 */
export function useProtocolList(isInit: boolean, lifeStatus: boolean) {
	const state = reactive({
		protocolList: [],
	})
	if (isInit) {
		if (lifeStatus) {
			onMounted(() => {
				getListData()
			})
		} else {
			onActivated(() => {
				getListData()
			})
		}
	}

	const handleProtocolChange = (e: any) => {
		if (e && state.protocolList.length == 0) {
			getListData()
		}
	}
	const getListData = () => {
		global.$fetchGetData(`/sop-assets/web/v1/assetPort/getProtocolList`, {}).then((data: any) => {
			if (data.code == 200) {
				state.protocolList = data.data || []
			} else {
				global.$baseMessage(data.msg, 'error')
			}
		})
	}
	return {
		...toRefs(state),
		handleProtocolChange,
	}
}
export function useInfoSysList(isInit = true, lifeStatus = true) {
	const state = reactive({
		infoSysList: [],
	})
	if (isInit) {
		if (lifeStatus) {
			onMounted(() => {
				getListData()
			})
		} else {
			onActivated(() => {
				getListData()
			})
		}
	}

	const handleInfoSysChange = (e: any) => {
		if (e && state.infoSysList.length == 0) {
			getListData()
		}
	}
	const getListData = () => {
		global.$fetchPostData(`/sop-assets/web/v1/assetInfoSystem/queryList`, {}).then((data: any) => {
			if (data.code == 200) {
				state.infoSysList = data.data || []
			} else {
				global.$baseMessage(data.msg, 'error')
			}
		})
	}
	return {
		...toRefs(state),
		handleInfoSysChange,
	}
}
