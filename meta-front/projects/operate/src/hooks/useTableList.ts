import { toRefs, reactive, onMounted, onActivated } from 'vue'
import { useUserInfo } from '@/stores/userInfo'
import global from '@/utils/global'
/**
 *
 * @param url
 * @param searchForm
 * @param lifeStatus 是否在onMounted 生命周期里显示 true：onMounted  false： onActivated
 * @param initReq 是否初始化請求
 * @returns
 */
export default function useTableList(url: string, searchForm: any, lifeStatus = true, initReq = true, pageSize = 10) {
	const state = reactive({
		loading: false,
		layout: 'total, sizes, prev, pager, next, jumper',
		dataCount: 0,
		tableData: [],
		pageNum: 1, //当前页面
		pageSize: pageSize,
		selectionAry: [],
		refPeakVuxTable: null,
		emptyStatus: false,
		searchMore: true,
		divideParams: {},
	})
	const getTableList = () => {
		state.loading = true
		global
			.$fetchPostData(`${url}`, {
				pageNum: state.pageNum,
				pageSize: state.pageSize,
				...searchForm,
				...state.divideParams,
			})
			.then((data: any) => {
				const res = data
				if (res.code == 200) {
					state.loading = false
					if (res.data.total > 0) {
						state.dataCount = Number(res.data.total)
						state.tableData = res.data.rows
						state.emptyStatus = false
					} else {
						state.dataCount = 0
						state.tableData = []
						state.emptyStatus = true
					}
				} else {
					global.$baseMessage(res.mes, 'error')
				}
			})
	}
	if (initReq) {
		if (lifeStatus) {
			onMounted(async () => {
				console.log('onMounted')
				handleInitQuery()
			})
		} else {
			onActivated(async () => {
				console.log('onActivated')
				handleInitQuery()
			})
		}
	}
	/**
	 * 单条件搜索
	 * @param params
	 */
	const handleInitQuery = () => {
		state.pageNum = 1
		getTableList()
	}
	/**
	 * 多条件搜索
	 * @param params
	 */
	const handleInitQueryDivide = (params: any) => {
		state.pageNum = 1
		state.divideParams = params
		getTableList()
	}
	/**
	 * 单条件刷新
	 * @param status
	 */
	const handleModalOk = (status: number) => {
		if (status == 1) {
			handleInitQuery()
		} else {
			getTableList()
		}
	}
	/**
	 * 多条件刷新
	 * @param status
	 */
	const handleModalOkDivide = (status: number, params: any) => {
		if (status == 1) {
			handleInitQueryDivide(params)
		} else {
			getTableList()
		}
	}
	const handleSelectionChange = (records: any) => {
		state.selectionAry = records
	}
	const handleSizeChange = (val: number) => {
		state.pageSize = val
		getTableList()
	}
	const handleCurrentChange = (val: number) => {
		state.pageNum = val
		getTableList()
	}
	/**
	 * 获取时间方法
	 * @param timeAry
	 * @returns
	 */
	const getTimeParseAry = (timeAry: any, beginStr = 'beginTime', endStr = 'endTime') => {
		const resObj: any = {}
		if (timeAry.length > 0) {
			resObj[beginStr] = timeAry[0]
			resObj[endStr] = timeAry[1]
			return resObj
		} else {
			resObj[beginStr] = null
			resObj[endStr] = null
			return resObj
		}
	}

	const getDictKeyText = (key: string, value: string) => {
		const { dictList } = useUserInfo()
		const list = dictList[key] || []
		const resAry = list.filter((item: any) => item.dictValue == value)
		return resAry.length > 0 ? resAry[0].dictLabel : '--'
	}

	return {
		...toRefs(state),
		handleInitQuery,
		handleSelectionChange,
		handleSizeChange,
		handleCurrentChange,
		getTableList,
		handleModalOk,
		getTimeParseAry,
		getDictKeyText,
		handleInitQueryDivide,
		handleModalOkDivide,
	}
}
