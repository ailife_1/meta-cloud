import { ref } from 'vue'

export default function useModal() {
	const editModalRef = ref()

	const handleAdd = () => {
		editModalRef.value?.handleShowModal()
	}

	const handleUpdate = (row: any) => {
		editModalRef.value?.handleShowModal(row)
	}
	return {
		editModalRef,
		handleAdd,
		handleUpdate,
	}
}
