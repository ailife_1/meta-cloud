import { toRefs, reactive, onMounted, onActivated } from 'vue'

import global from '@/utils/global'
/**
 *
 * @param url
 * @param searchForm
 * @param lifeStatus 是否在onMounted 生命周期里显示 true：onMounted  false： onActivated
 * @param initReq 是否初始化請求
 * @returns
 */
export default function useList(url: string, searchForm: any, lifeStatus = true, initReq = true) {
	const state = reactive({
		loading: false,
		tableData: [],
		serachStatus: true,
		selectionAry: [],
		refPeakVuxTable: null,
		emptyStatus: false,
		searchMore: true,
	})
	const getDataList = () => {
		state.loading = true
		global
			.$fetchPostData(`${url}`, {
				...searchForm,
			})
			.then((data: any) => {
				if (data.code == 200) {
					state.loading = false
					if (data.data.length > 0) {
						state.tableData = data.data
						state.emptyStatus = false
					} else {
						state.tableData = []
						state.emptyStatus = true
					}
				} else {
					global.$baseMessage(data.error, 'error')
				}
			})
	}
	if (initReq) {
		if (lifeStatus) {
			onMounted(async () => {
				console.log('onMounted')
				getDataList()
			})
		} else {
			onActivated(async () => {
				console.log('onActivated')
				getDataList()
			})
		}
	}
	const handleInitQuery = () => {
		getDataList()
	}
	const handleModalOk = () => {
		getDataList()
	}

	return {
		...toRefs(state),
		handleInitQuery,
		handleModalOk,
	}
}
