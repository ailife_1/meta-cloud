package cn.qianxun.meta.gateway.config;

import org.springframework.context.annotation.Configuration;

/**
 * 路由配置信息
 *
 * @author fuzhilin
 */
@Configuration
public class RouterFunctionConfiguration {

//暂时注释掉，用不上，后期根据需求进行添加
//    @SuppressWarnings("rawtypes")
//    @Bean
//    public RouterFunction routerFunction() {
//        return RouterFunctions.route().build();
//    }
}
