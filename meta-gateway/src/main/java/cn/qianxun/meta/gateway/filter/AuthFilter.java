package cn.qianxun.meta.gateway.filter;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.reactor.filter.SaReactorFilter;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import cn.qianxun.meta.gateway.config.properties.IgnoreWhiteProperties;
import cn.qianxun.meta.common.satoken.utils.StpAppIdUtil;
import cn.qianxun.meta.common.core.constant.HttpStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * [Sa-Token 权限认证] 拦截器
 *
 * @author Lion Li
 */
@Configuration
public class AuthFilter {

    /**
     * 注册 Sa-Token 全局过滤器
     */
    @Bean
    public SaReactorFilter getSaReactorFilter(IgnoreWhiteProperties ignoreWhite) {
        return new SaReactorFilter()
                // 拦截地址
                .addInclude("/**")
                //.addExclude("/favicon.ico", "/actuator/**")
                .addExclude(ignoreWhite.getWhites().toArray(String[]::new))
                // 鉴权方法：每次访问进入
                .setAuth(obj -> {
                    // 登录校验 -- 拦截所有路由
                    SaRouter.match("/**")
                            .notMatch(ignoreWhite.getWhites())
                            .check(r -> {
                                System.out.println("进入系统判断。");
                                // 检查是否登录 是否有token
                                String requestPath = SaHolder.getRequest().getRequestPath();
                                if (requestPath.startsWith("/meta-api/api/")) {
                                    StpAppIdUtil.checkLogin();
                                } else if (requestPath.contains("/web/v1/upload/authUploadByBucketName")) {
                                    //自定义上传
                                   /* SaRequest request = SaHolder.getRequest();
                                    Map<String, String> paramMap = request.getParamMap();
                                    String authorization = request.getParam("Authorization");
                                    if (!StpUtil.isLogin(StpUtil.getLoginId(authorization))) {
                                        throw NotLoginException.newInstance(StpUtil.getLoginType(), INVALID_TOKEN, INVALID_TOKEN_MESSAGE, authorization).setCode(SaErrorCode.CODE_11012);
                                    }*/
                                } else {
                                    StpUtil.checkLogin();
                                }

                                // 有效率影响 用于临时测试
                                // if (log.isDebugEnabled()) {
                                //     log.debug("剩余有效时间: {}", StpUtil.getTokenTimeout());
                                //     log.debug("临时有效时间: {}", StpUtil.getTokenActivityTimeout());
                                // }
                            });
                }).setError(e -> SaResult.error("认证失败，无法访问系统资源").setCode(HttpStatus.UNAUTHORIZED));
    }
}
