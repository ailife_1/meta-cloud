package cn.qianxun.meta.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author by fuzhilin
 * @Classname SmartmetaGatewayApplication
 * @Description 通用平台网关启动成功
 * @Date 2023/8/7 14:01
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SmartMetaGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(SmartMetaGatewayApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  通用平台网关启动成功   ლ(´ڡ`ლ)ﾞ  \n");
    }
}
